QT += widgets sql network

TEMPLATE = lib

HEADERS += \
    $$PWD/common.h \
    ../../src/singleton.h \
    ../../src/sql_class.h \
    ../../src/wtimport.h \
    ../../src/importlmmworkbook.h \
    ../../src/epub.h \
    ../../src/internet.h \
    ../../src/jwpub.h \
    ../../src/xml_reader.h \
    ../../src/zipper.h \
    ../../src/zip/qzipreader_p.h \
    ../../src/zip/qzipwriter_p.h \
    ../../src/jwpub.h \
    ../../src/internet.h \
    ../../src/lmm_meeting.h \
    ../../src/lmm_schedule.h \
    ../../src/lmm_assignment.h \
    ../../src/lmm_assignment_ex.h \
    ../../src/lmm_cbs.h \
    ../../src/cpersons.h \
    ../../src/person.h \
    ../../src/ccongregation.h \
    ../../src/sortfilterproxymodel.h \
    ../../src/sqlcombo.h \
    ../../src/school.h \
    ../../src/school_item.h \
    ../../src/school_setting.h \
    ../../src/schoolstudy.h

SOURCES += \
    ../../src/sql_class.cpp \
    ../../src/wtimport.cpp \
    ../../src/importlmmworkbook.cpp \
    ../../src/xml_reader.cpp \
    ../../src/epub.cpp \
    ../../src/zipper.cpp \
    ../../src/zip/qzip.cpp \
    ../../src/jwpub.cpp \
    ../../src/internet.cpp \
    ../../src/lmm_meeting.cpp \
    ../../src/lmm_schedule.cpp \
    ../../src/lmm_assignment.cpp \
    ../../src/lmm_assignment_ex.cpp \
    ../../src/lmm_cbs.cpp \
    ../../src/cpersons.cpp \
    ../../src/person.cpp \
    ../../src/ccongregation.cpp \
    ../../src/sqlcombo.cpp \
    ../../src/school.cpp \
    ../../src/school_item.cpp \
    ../../src/school_setting.cpp \
    ../../src/schoolstudy.cpp

macx: LIBS += -lz
unix: LIBS += -lz

RESOURCES += ../../src/database.qrc

DEFINES += TB_LIBRARY
