/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "checkupdates.h"
#include <QMessageBox>
#include <QDebug>

checkupdates::checkupdates(QString version, int waitseconds)
{
    this->theocbaseversion = version;
    this->wait = waitseconds;

#if defined Q_OS_WIN
    updaterpath = qApp->applicationDirPath() + "/autoupdate/autoupdate-windows.exe";
#elif defined Q_OS_MAC
    updaterpath = qApp->applicationDirPath() + "/autoupdate-mac.app/Contents/MacOS/installbuilder.sh";
#else
    updaterpath = qApp->applicationDirPath() + "/autoupdate/autoupdate-linux.run";
    if (!QFile::exists(updaterpath)){
        // 64 -bit
        updaterpath = qApp->applicationDirPath() + "/autoupdate/autoupdate-linux-x64.run";
    }
#endif
}

void checkupdates::run()
{
    if (wait > 0) this->sleep(wait);

    if (this->checkUpdatesFromServer()){
        emit updatesFound();
    }

//    QNetworkAccessManager m_manager;
//    connect(&m_manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(updatesChecked(QNetworkReply*)));
//    m_manager.get(QNetworkRequest(QUrl("http://theocbase.net/get_version.php")));

    this->exec();
}

void checkupdates::startUpdateInstall()
{
    QString temppath = QDir::tempPath() + "/theocbase-autoupdate";
    QDir dir = QDir(temppath);
    if (dir.exists()){
        QFileInfoList files = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files);
        for(int file = 0; file < files.count(); file++)
        {
            qDebug() << "delete file:" << files.at(file).fileName() <<  dir.remove(files.at(file).fileName());
        }
    }

    QStringList args;
    args << "--mode" << "unattended" << "--unattendedmodebehavior" << "download" << "--unattendedmodeui" << "minimalWithDialogs";
    args << "--update_download_location" << temppath;
    int ret = QProcess::startDetached(updaterpath,args);
    qDebug() << "autoupdater" << ret;
    qApp->quit();
}

void checkupdates::updatesChecked(QNetworkReply *reply)
{
    QString str = reply->readAll();
    QStringList list = str.split("<br />");

    QString os = "1";
#ifdef Q_OS_WIN
    os = "1";
#elif defined Q_OS_LINUX
    os = "2";
#else
    os = "3";
#endif
qDebug() << "OS=" + os;
    for (int i = 0;i < list.size(); ++i)
    {
        QString text = list.at(i).mid(2);
        QStringList textlist = text.split(" ");
        if(textlist.count() > 2) text = textlist.at(0) + " " + textlist.at(1);

        //if(list.at(i).left(1) == os && list.at(i).mid(2) != theocbaseversion)
        if(list.at(i).left(1) == os && text != theocbaseversion)
        {
            QString msg = "<html>" + tr("New version available") + "<br>";
            msg.append("<a href=\"http://theocbase.net/\">" +
                       tr("Download") + " " + list.at(i).mid(2) + "</a></html>");
            if(wait > 0){
                QToolButton *button = new QToolButton();
                button->setText(tr("New version..."));
                button->setIcon(QIcon(":/icons/update.svg"));
                button->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
                connect(button,SIGNAL(clicked()),this,SLOT(buttonClicked()));
                sb->addWidget(button);
            }else{
                QMessageBox::information(0,"TheocBase",msg);
            }
            return;
        }
    }
    if(wait == 0) QMessageBox::information(0,"TheocBase",tr("No new update available"));

}

void checkupdates::buttonClicked()
{
    QUrl url("http://theocbase.net/");
    url.setScheme("http");
    QDesktopServices::openUrl(url);
}


bool checkupdates::checkUpdatesFromServer()
{
    QStringList args;
    qDebug() << updaterpath;
    args << "--mode" << "unattended";

    int ret = QProcess::execute(updaterpath, args);
    qDebug() << "updatercheck" << ret;
    if (ret == 0){
        return true;
    }else{
        return false;
    }
}



