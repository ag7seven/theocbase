/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOL_SETTING_H
#define SCHOOL_SETTING_H

#include <QString>
#include "sql_class.h"

/**
 * @brief The school_setting class to represent a setting for school assignment
 *        It's used when assignment is for sisters
 */
class school_setting
{
public:
    /**
     * @brief school_setting Initialize school_setting class
     */
    school_setting();

    /**
     * @brief getNumber Get number of setting
     * @return number
     */
    int getNumber();

    /**
     * @brief setNumber Set number of setting
     * @param number
     */
    void setNumber(int number);

    /**
     * @brief getName Get name of setting
     * @return name
     */
    QString getName();

    /**
     * @brief setName Set name of setting
     * @param name Setting name
     */
    void setName(QString name);

    /**
     * @brief getOnlyBrothers
     * @return
     */
    bool getOnlyBrothers();

    /**
     * @brief setOnlyBrothers
     * @param brothers
     */
    void setOnlyBrothers(bool brothers);

private:
    int m_number;
    QString m_name;
    bool m_onlybrothers;
    sql_class *sql;
};

#endif // SCHOOL_SETTING_H
