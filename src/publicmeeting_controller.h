#ifndef PUBLICMEETING_CONTROLLER_H
#define PUBLICMEETING_CONTROLLER_H

#include <QObject>
#include <QDate>
#include <QDateTime>
#include <QPushButton>
#include "cpublictalks.h"
#include "availability/weekendmeetingavailabilitychecker.h"
#include "sortfilterproxymodel.h"
#include "todo.h"

class publicmeeting_controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(cptmeeting *meeting READ getMeeting NOTIFY meetingChanged)

public:
    class ThemeFilterProxyModel : public SortFilterProxyModel {
    public: enum ThemeListRoles {
            id = Qt::UserRole+1,
            name,
            date,
            icon,
            // hidden fields
            date_val,
            number
        };
        explicit ThemeFilterProxyModel(QObject *parent = nullptr, QStandardItemModel *itemmodel = nullptr, int defaultSortRole = 0, QString settingName = "") : SortFilterProxyModel(parent, itemmodel, defaultSortRole, settingName){}
        virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;
    };

    explicit publicmeeting_controller(QObject *parent = nullptr);
    QDateTime date() const;
    bool setDate(QDateTime date);

    cptmeeting *getMeeting();

    Q_INVOKABLE QAbstractItemModel *brotherList(person::UseFor usefor);

    Q_INVOKABLE QAbstractItemModel *speakerListLocal();

    Q_INVOKABLE QAbstractItemModel *speakerList(const int talkId, const int congregation_id = 0);

    Q_INVOKABLE QAbstractItemModel *themeList(int speakerId = 0);

    Q_INVOKABLE QAbstractItemModel *congregationList();

    Q_INVOKABLE QAbstractItemModel *hospitalityList();

    Q_INVOKABLE bool moveToTodo();

    Q_INVOKABLE bool moveTo(const QDateTime selectedDate);

    Q_INVOKABLE void reload();

signals:
    void dateChanged(QDateTime date);
    void meetingChanged();

public slots:

private:
    void loadMeeting();    
    QHash<int,QByteArray> defaultRoles();
    QDate m_date;
    QSharedPointer<cptmeeting> m_meeting;
    bool meetingLoaded;
};

#endif // PUBLICMEETING_CONTROLLER_H
