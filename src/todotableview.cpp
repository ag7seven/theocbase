#include "todotableview.h"

todoTableViewDelegate::todoTableViewDelegate(QObject *parent) :
    QItemDelegate(parent)
{}

QWidget *todoTableViewDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QWidget *editor;
    int colIdx(index.column());
    if (colIdx < 2) {
        return 0;
    } else if (colIdx < 3) {
        editor = QItemDelegate::createEditor(parent, option, index);
    } else if (colIdx < 6) {
        cEditableComboBox *actualEditor = new cEditableComboBox(parent);
        actualEditor->setEditable(true);
        //editor->addItem("");
        connect(actualEditor,SIGNAL(beforeShowPopup(QString,cEditableComboBox*,bool)),this,SLOT(beforeShowComboPopup(QString,cEditableComboBox*,bool)));
        connect(actualEditor,SIGNAL(activated(int)),this,SLOT(on_combo_activated(int)));
        connect(actualEditor,SIGNAL(focusOut(cEditableComboBox*,cEditableComboBox::states)),this,SLOT(on_combo_focusOut(cEditableComboBox*,cEditableComboBox::states)));
        editor = actualEditor;
    } else {
        editor = QItemDelegate::createEditor(parent, option, index);
    }
    editor->setProperty("todo", index.sibling(index.row(), 0).data());
    editor->setProperty("rowid", QVariant(index.row()));
    editor->setProperty("colid", QVariant(colIdx));
    return editor;
}

void todoTableViewDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    int idx(index.column());
    QString data = index.data(Qt::EditRole).toString();
    if (idx < 2) {
        // do nothing
    } else if (idx < 3) {
        QLineEdit *actualEditor = static_cast<QLineEdit*>(editor);
        actualEditor->setText(data);
    } else if (idx < 6) {
        cEditableComboBox *actualEditor = static_cast<cEditableComboBox*>(editor);
        actualEditor->setCurrentText(data);
    } else {
        QLineEdit *actualEditor = static_cast<QLineEdit*>(editor);
        actualEditor->setText(data);
    }
}

void todoTableViewDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    todo *t = (todo*)editor->property("todo").value<void *>();
    int idx(index.column());
    if (idx < 2) {
        // do nothing
    } else if (idx < 3) {
        QLineEdit *actualEditor = static_cast<QLineEdit*>(editor);
        QString txt(actualEditor->text());
        t->setDate(general::TextToDate(txt));
        t->save();
        model->setData(index, txt, Qt::EditRole);
    } else if (idx < 6) {
        cEditableComboBox *actualEditor = static_cast<cEditableComboBox*>(editor);
        QString txt(actualEditor->currentText());
        switch (idx) {
        case 2:
            t->setDate(general::TextToDate(txt));
            break;
        case 3:
            t->setSpeaker(txt);
            break;
        case 4:
            t->setCongregation(txt);
            break;
        case 5:
            t->setTheme(txt);
            break;
        }
        t->save();
        model->setData(index, txt, Qt::EditRole);
    } else {
        QLineEdit *actualEditor = static_cast<QLineEdit*>(editor);
        QString txt(actualEditor->text());
        t->setNotes(txt);
        t->save();
        model->setData(index, txt, Qt::EditRole);
    }
}

void todoTableViewDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
}

void todoTableViewDelegate::beforeShowComboPopup(QString cname, cEditableComboBox *combo, bool filterRequested)
{
    emit beforeShowPopup(cname, combo, filterRequested);
}

void todoTableViewDelegate::on_combo_activated(int index)
{
    emit activated(index);
}

void todoTableViewDelegate::on_combo_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state)
{
    emit focusOut(combo, state);
}


