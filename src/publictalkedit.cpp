/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "publictalkedit.h"
#include "ui_publictalkedit.h"

publictalkedit::publictalkedit(QWidget *parent)
    : QWidget(parent), ui(new Ui::publictalkedit), ptCongIdFilter(0)
{
    ui->setupUi(this);
    sql = &Singleton<sql_class>::Instance();

    createCalendarPopup();
    okFont = ui->labelPTTheme->font();
    filterFont = okFont;
    filterFont.setStrikeOut(true);

    ui->comboChairman->setPersonType(person::Chairman, ccongregation::pm);
    ui->comboReader->setPersonType(person::WtReader, ccongregation::wt);
    ui->comboConductor->setPersonType(person::WtCondoctor, ccongregation::wt);

    // todo: ideally modify this class to use the following:
    //ui->comboPTSpeaker->setPerson(person::PublicTalk, ccongregation::pm);
    //ui->comboOutSpeaker->setPerson(person::PublicTalk, outside, ccongregation::pm);
    // and use a different class (other than cPersonComboBox) to handle ui->comboOutTheme

    connect(ui->comboTheme, SIGNAL(beforeShowPopup(QString, cEditableComboBox *, bool)), this, SLOT(beforeShowThemeCombo(QString, cEditableComboBox *, bool)));
    connect(ui->comboPTCong, SIGNAL(beforeShowPopup(QString, cEditableComboBox *, bool)), this, SLOT(beforeShowPTCongCombo(QString, cEditableComboBox *, bool)));
    connect(ui->comboPTSpeaker, SIGNAL(beforeShowPopup(QString, cEditableComboBox *, bool)), this, SLOT(beforeShowSpeakerCombo(QString, cEditableComboBox *, bool)));

    ui->comboTheme->lineEdit()->setPlaceholderText(tr("Theme"));
    ui->comboPTSpeaker->lineEdit()->setPlaceholderText(tr("Speaker"));
    ui->comboPTCong->lineEdit()->setPlaceholderText(tr("Congregation"));
    ui->comboChairman->lineEdit()->setPlaceholderText(tr("Chairman"));
    ui->comboConductor->lineEdit()->setPlaceholderText(tr("Conductor"));
    ui->comboReader->lineEdit()->setPlaceholderText(tr("Reader"));

    connect(ui->comboOutSpeaker, SIGNAL(beforeShowPopup(QString, cPersonComboBox *)), this, SLOT(beforeShowOutSpeakerCombo(QString, cPersonComboBox *)));
    connect(ui->comboOutTheme, SIGNAL(beforeShowPopup(QString, cPersonComboBox *)), this, SLOT(beforeShowOutThemeCombo(QString, cPersonComboBox *)));
    connect(ui->comboOutCong, SIGNAL(beforeShowPopup(QString, cPersonComboBox *)), this, SLOT(beforeShowOutCongCombo(QString, cPersonComboBox *)));

    ui->tableOutgoing->setColumnHidden(0, true);
    ui->tableOutgoing->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tableOutgoing->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->tableOutgoing->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->tableOutgoing->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Stretch);
    ui->tableOutgoing->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);

    todoModel = new QStandardItemModel(1, 7);
    todoModel->setHorizontalHeaderItem(0, new QStandardItem("id"));
    todoModel->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("I/O", "Incoming/Outgoing")));
    todoModel->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("Date")));
    todoModel->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("Speaker")));
    todoModel->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("Congregation")));
    todoModel->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("Theme")));
    todoModel->setHorizontalHeaderItem(6, new QStandardItem(QObject::tr("Notes")));
    ui->todoTable->setModel(todoModel);
    todoViewDelegate = new todoTableViewDelegate(this);
    ui->todoTable->setItemDelegate(todoViewDelegate);
    ui->todoTable->setColumnHidden(0, true);
    ui->todoTable->verticalHeader()->hide();
    ui->todoTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Interactive);
    ui->todoTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Interactive);
    ui->todoTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Interactive);
    ui->todoTable->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Interactive);
    ui->todoTable->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Interactive);
    ui->todoTable->horizontalHeader()->setSectionResizeMode(6, QHeaderView::Interactive);
    connect(todoViewDelegate, SIGNAL(beforeShowPopup(QString, cEditableComboBox *, bool)), this, SLOT(beforeShowTodoPopup(QString, cEditableComboBox *, bool)));
    connect(todoViewDelegate, SIGNAL(focusOut(cEditableComboBox *, cEditableComboBox::states)), this, SLOT(on_todoCombo_focusOut(cEditableComboBox *, cEditableComboBox::states)));

    // wt source
    ui->comboWtSource->blockSignals(true);
    ui->comboWtSource->lineEdit()->setPlaceholderText(tr("Watchtower Study Edition"));
    ui->comboWtSource->addItem("");
    QLocale locale(QLocale::system().name());
    for (int i = 1; i <= 12; i++) {
        ui->comboWtSource->addItem(locale.standaloneMonthName(i, QLocale::LongFormat));
    }
    ui->comboWtSource->blockSignals(false);
}

publictalkedit::~publictalkedit()
{
    delete ui;
}

void publictalkedit::setDate(QDate date)
{
    ccongregation cc;
    allCongregations = cc.getAllCongregations();

    int meetingday = cc.getMeetingDay(date, ccongregation::pm);
    currentDate = date;
    ptDate = date.addDays(myCongregation.getPublicmeeting(date).getMeetingday() - 1);
    ui->pushButton->setFocus();
    currentMeeting.reset(cp.getMeeting(date));

    ui->comboTheme->clear();
    ui->comboTheme->setModelColumn(0);
    ui->comboTheme->setCurrentText(currentMeeting->theme.theme != "" ? currentMeeting->theme.theme + " (" + QVariant(currentMeeting->theme.number).toString() + ")" : "");
    ui->comboTheme->setState(cEditableComboBox::idle);

    ui->comboPTSpeaker->clear();
    ui->comboPTSpeaker->setModelColumn(0);
    if (currentMeeting->speaker()) {
        ptCongIdFilter = currentMeeting->speaker()->congregationid();
        ui->comboPTCong->setCurrentText(currentMeeting->speaker()->congregationName());
        ui->comboPTSpeaker->setCurrentText(currentMeeting->speaker()->fullname());
    } else {
        ptCongIdFilter = 0;
        ui->comboPTCong->setCurrentText("");
        ui->comboPTSpeaker->setCurrentText("");
    }
    ui->comboPTCong->setState(cEditableComboBox::idle);
    ui->comboPTSpeaker->setState(cEditableComboBox::idle);

    // chairman
    ui->comboChairman->clear();
    ui->comboChairman->setDate(date);
    if (currentMeeting->chairman()) {
        ui->comboChairman->setCurrentText(currentMeeting->chairman()->fullname());
    } else {
        ui->comboChairman->setCurrentText("");
    }
    ui->framePt->setEnabled(meetingday > 0);

    // Wt study reader
    ui->comboReader->clear();
    ccongregation::exceptions ex = cc.isException(date);
    if (ex == ccongregation::circuit_overseer_visit) {
        // There is no reader if circuit overseer's visit
        ui->comboReader->setCurrentText("");
        ui->comboReader->setEnabled(false);
    } else {
        ui->comboReader->setDate(date);
        if (currentMeeting->wtReader()) {
            ui->comboReader->setCurrentText(currentMeeting->wtReader()->fullname());
        } else {
            ui->comboReader->setCurrentText("");
        }
        ui->comboReader->setEnabled(true);
    }

    // Wt study conductor
    ui->comboConductor->clear();
    ui->comboConductor->setDate(date);
    if (currentMeeting->wtConductor()) {
        ui->comboConductor->setCurrentText(currentMeeting->wtConductor()->fullname());
    } else {
        person *conductor = cp.defaultWtConductor();
        if (conductor) {
            ui->comboConductor->setCurrentText(conductor->fullname());
        } else {
            ui->comboConductor->setCurrentText("");
        }
    }
    // wt source
    int wtnumber = QVariant(currentMeeting->wtsource).toInt();
    ui->comboWtSource->blockSignals(true);
    ui->comboWtSource->setCurrentIndex(wtnumber);
    ui->comboWtSource->blockSignals(false);
    ui->editWTTheme->setText(currentMeeting->wttheme);

    // final talk
    ui->editFinalTalk->setText(currentMeeting->getFinalTalk());
    ui->labelFinalTalk->setVisible(ex == ccongregation::circuit_overseer_visit);
    ui->editFinalTalk->setVisible(ex == ccongregation::circuit_overseer_visit);

    // songs
    ui->spin_song_talk->blockSignals(true);
    ui->spin_song_talk->setValue(currentMeeting->song_talk);
    ui->spin_song_talk->blockSignals(false);

    ui->spin_song_wt_start->blockSignals(true);
    ui->spin_song_wt_start->setValue(currentMeeting->song_wt_start);
    ui->spin_song_wt_start->blockSignals(false);

    ui->spin_song_wt_end->blockSignals(true);
    ui->spin_song_wt_end->setValue(currentMeeting->song_wt_end);
    ui->spin_song_wt_end->blockSignals(false);
    ui->spin_song_wt_start->setEnabled(meetingday > 0);
    ui->spin_song_wt_end->setEnabled(meetingday > 0);
    ui->comboConductor->setEnabled(meetingday > 0);
    ui->comboReader->setEnabled(meetingday > 0);

    // outgoing speakers
    QList<cpoutgoing *> list = cp.getOutgoingSpeakers(currentDate);
    ui->tableOutgoing->setRowCount(list.size());
    for (int i = 0; i < list.size(); ++i) {
        cpoutgoing *o = list[i];
        if (!o->getSpeaker())
            continue;
        ccongregation::meeting_dayandtime const &mtg = o->getCongregation().getPublicmeeting(date);
        // the following logic will put the day in parenthesis if it's a different day than the current congregation
        QString displaytime = mtg.getMeetingtime() + (o->date() == ptDate ? "" : " (" + QDate::shortDayName(o->date().dayOfWeek()) + ")");

        ui->tableOutgoing->setItem(i, 0, new QTableWidgetItem(QVariant(o->getId()).toString()));
        ui->tableOutgoing->setItem(i, 1, new QTableWidgetItem(o->getSpeaker()->fullname()));
        ui->tableOutgoing->setItem(i, 2, new QTableWidgetItem(o->getCongregation().name));
        ui->tableOutgoing->setItem(i, 3, new QTableWidgetItem(displaytime));
        ui->tableOutgoing->setItem(i, 4, new QTableWidgetItem(o->getTheme().theme));
        ui->tableOutgoing->setItem(i, 5, new QTableWidgetItem(QVariant(o->getTheme().number).toString()));
    }

    fillTodo();
}

void publictalkedit::updateScheduledTalks(int speakerId, int oldCongregationId, int newCongregationId, bool removed)
{
    // move scheduled talks to To Do List
    // (when speaker moves to another congregation or is no longer speaker)
    qDebug() << "storeScheduledTalksInToDoList" << speakerId << oldCongregationId << newCongregationId;

    ccongregation c;
    cpublictalks cpt;
    cptmeeting *meeting;
    person *speaker = cpersons::getPerson(QVariant(speakerId).toInt());
    QDate startDate = QDateTime::currentDateTime().date().addDays(-7);

    bool toDoListUpdated = false;

    // incoming talks: move to To Do List only if speaker is removed
    if (removed) {
        sql_items incomingTalks;

        incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

        if (!incomingTalks.empty()) {
            // move incoming talks
            for (unsigned int i = 0; i < incomingTalks.size(); i++) {
                int themeId = incomingTalks[i].value("theme_id").toInt();
                QDate scheduledDate = incomingTalks[i].value("date").toDate();
                cpttheme theme = cpt.getThemeById(themeId);

                todo t(true);
                t.setCongregation(speaker->congregationName());
                t.setSpeaker(speaker->fullname());
                t.setTheme(theme.theme);
                t.setNotes(tr("From %1; speaker removed", "From [scheduled date]; speaker removed").arg(scheduledDate.toString(Qt::ISODate)));
                t.save();

                meeting = cpt.getMeeting(scheduledDate);
                meeting->theme = cpttheme();
                meeting->setSpeaker(nullptr);
                meeting->save();

                toDoListUpdated = true;
            }
        }
    }

    sql_items outgoingTalks;
    outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

    if (!outgoingTalks.empty()) {
        for (unsigned int i = 0; i < outgoingTalks.size(); i++) {
            // move outgoing talks
            int outgoingId = outgoingTalks[i].value("id").toInt();
            QDate scheduledDate = outgoingTalks[i].value("date").toDate();
            meeting = cpt.getMeeting(scheduledDate);

            if (cpt.removeOutgoingSpeaker(outgoingId)) {
                int outCongId = outgoingTalks[i].value("congregation_id").toInt();
                ccongregation::congregation cong = c.getCongregationById(outCongId);
                int themeId = outgoingTalks[i].value("theme_id").toInt();
                cpttheme theme = cpt.getThemeById(themeId);

                todo t(false);
                t.setCongregation(cong.name);
                t.setSpeaker(speaker->fullname());
                t.setTheme(theme.theme);
                if (removed) {
                    // speaker removed
                    t.setNotes(tr("From %1; speaker removed", "From [scheduled date]; speaker removed").arg(scheduledDate.toString(Qt::ISODate)));
                } else {
                    // speaker moved to other congregation
                    t.setNotes(tr("From %1; speaker moved to %2", "From [scheduled date]; speaker moved to [new congregation]").arg(scheduledDate.toString(Qt::ISODate), speaker->congregationName()));
                }
                t.save();

                toDoListUpdated = true;
            }
        }
    }

    if (toDoListUpdated) {
        setDate(currentDate);
    }
}

void publictalkedit::updateScheduledTalks(int themeId)
{
    // move scheduled talks to To Do List
    // (when talk is discontinued)

    ccongregation c;
    auto cpt = new cpublictalks();
    cptmeeting *meeting;
    QDate startDate = QDate::currentDate().addDays(-7);

    bool toDoListUpdated = false;

    // check for scheduled talks

    sql_items incomingTalks;
    incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND theme_id = " + QVariant(themeId).toString() + " AND active");

    if (!incomingTalks.empty()) {
        // move incoming talks
        for (unsigned int i = 0; i < incomingTalks.size(); i++) {
            QDate scheduledDate = incomingTalks[i].value("date").toDate();
            cpttheme theme = cpt->getThemeById(themeId);

            int speakerId = incomingTalks[i].value("speaker_id").toInt();
            person *speaker = cpersons::getPerson(QVariant(speakerId).toInt());

            todo t(true);
            if (speaker != nullptr) {
                t.setCongregation(speaker->congregationName());
                t.setSpeaker(speaker->fullname());
            }
            t.setTheme(theme.theme);
            t.setNotes(tr("From %1; talk discontinued", "From [scheduled date]; talk discontinued").arg(scheduledDate.toString(Qt::ISODate)));
            t.save();

            meeting = cpt->getMeeting(scheduledDate);
            meeting->theme = cpttheme();
            meeting->setSpeaker(nullptr);
            meeting->save();

            toDoListUpdated = true;
        }
    }

    sql_items outgoingTalks;
    outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND theme_id = " + QVariant(themeId).toString() + " AND active");

    if (!outgoingTalks.empty()) {
        for (unsigned int i = 0; i < outgoingTalks.size(); i++) {
            // move outgoing talks
            int outgoingId = outgoingTalks[i].value("id").toInt();
            QDate scheduledDate = outgoingTalks[i].value("date").toDate();
            meeting = cpt->getMeeting(scheduledDate);

            if (cpt->removeOutgoingSpeaker(outgoingId)) {
                int outCongId = outgoingTalks[i].value("congregation_id").toInt();
                ccongregation::congregation cong = c.getCongregationById(outCongId);

                int speakerId = outgoingTalks[i].value("speaker_id").toInt();
                person *speaker = cpersons::getPerson(QVariant(speakerId).toInt());

                cpttheme theme = cpt->getThemeById(themeId);

                todo t(false);
                t.setCongregation(cong.name);
                t.setSpeaker(speaker->fullname());
                t.setTheme(theme.theme);
                t.setNotes(tr("From %1; talk discontinued", "From [scheduled date]; talk discontinued").arg(scheduledDate.toString(Qt::ISODate)));
                t.save();

                toDoListUpdated = true;
            }
        }
    }

    if (toDoListUpdated) {
        setDate(currentDate);
    }
    delete cpt;
}

void publictalkedit::on_comboChairman_activated(int index)
{
    QString id = ui->comboChairman->model()->index(index, 0).data(0).toString();
    qDebug() << "chairman id" << id;
    person *chairman = cpersons::getPerson(QVariant(id).toInt());
    currentMeeting->setChairman(chairman);
    currentMeeting->save();
}

void publictalkedit::on_comboReader_activated(int index)
{
    int id = QVariant(ui->comboReader->model()->index(index, 0).data(0).toString()).toInt();
    qDebug() << "wt reader id" << id;
    person *reader = cpersons::getPerson(id);
    currentMeeting->setWtReader(reader);
    currentMeeting->save();
}

void publictalkedit::on_btnPTClear_clicked()
{
    //ui->comboTheme->setCurrentText("");
    //ui->comboPTCong->setCurrentText("");
    //ui->comboPTSpeaker->setCurrentText("");
    ui->comboTheme->setCurrentIndex(-1);
    ui->comboPTCong->setCurrentIndex(-1);
    ui->comboPTSpeaker->setCurrentIndex(-1);
    currentMeeting->theme = cpttheme();
    currentMeeting->setSpeaker(nullptr);
    ptCongIdFilter = 0;
    currentMeeting->save();
}

void publictalkedit::beforeShowThemeCombo(QString cname, cEditableComboBox *combo, bool filterRequested)
{
    Q_UNUSED(cname);
    QString currentThemeName = combo->currentText();
    QString filter;
    if (filterRequested) {
        filter = currentThemeName;
    }
    QStandardItemModel *model = getThemesTable(filter, currentMeeting->speaker(), currentDate);
    QTableView *view = new QTableView(this);
    view->setModel(model);

    view->verticalHeader()->hide();
    view->setSelectionMode(QAbstractItemView::SingleSelection);
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setSortingEnabled(true);
    view->sortByColumn(1, Qt::AscendingOrder);
    view->verticalHeader()->hide();
    view->setColumnHidden(0, true);
    view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    view->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);

    combo->setModel(model);
    combo->setModelColumn(2);
    combo->setView(view);

    int currentRow = combo->findText(currentThemeName, Qt::MatchExactly);
    if (currentRow >= 0) {
        QFont boldfont(view->font().family(),
                       view->font().pointSize(),
                       QFont::Bold);
        model->item(currentRow, 2)->setFont(boldfont);
        combo->setCurrentText(currentThemeName);
    }
}

// theme combobox selected via dropdown
void publictalkedit::on_comboTheme_activated(int index)
{
    saveTheme(ui->comboTheme->model(), index);
    ui->comboTheme->setModelColumn(2);
    setLabelOK(ui->labelPTTheme, true);
}

// theme edited manually. Check for valid data.
void publictalkedit::on_comboTheme_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state)
{
    bool ok(true);
    if (state == cEditableComboBox::filtered) {
        QString currentThemeName = combo->currentText();
        // using the QStandardItemModel is not as efficient as querying the data directly, but preventing duplicate code is higher priority
        auto model = getThemesTable(currentThemeName, currentMeeting->speaker(), currentDate);
        if (model->rowCount() == 2) { // 2 because we have a blank row
            saveTheme(model, 1);
            combo->setCurrentText(model->index(1, 2).data(0).toString());
        } else {
            ok = false;
        }
    }
    setLabelOK(ui->labelPTTheme, ok);
}

QStandardItemModel *publictalkedit::getThemesTable(QString filter, person *speaker, QDate date)
{
    QStandardItemModel *model = cp.getThemesTable(filter, speaker, date, true);
    return model;
}

void publictalkedit::saveTheme(QAbstractItemModel *model, int index)
{
    int themeId = 0;
    cpttheme theme;
    if (index > 0) {
        themeId = model->index(index, 0).data(0).toInt();
        theme = cp.getThemeById(themeId);
    }
    currentMeeting->theme = theme;
    currentMeeting->save();
}

void publictalkedit::beforeShowPTCongCombo(QString cname, cEditableComboBox *combo, bool filterRequested)
{
    Q_UNUSED(cname);
    QString currentCongName = combo->currentText();
    QString filter;
    if (filterRequested) {
        filter = currentCongName;
    }
    QStandardItemModel *model = getCongregationTable(filter, true);
    QTableView *view = new QTableView(this);
    view->setModel(model);

    view->verticalHeader()->hide();
    view->setSelectionMode(QAbstractItemView::SingleSelection);
    view->setSelectionBehavior(QAbstractItemView::SelectRows);

    view->setSortingEnabled(false);
    //view->sortByColumn(1,Qt::AscendingOrder);
    view->horizontalHeader()->hide();
    view->setColumnHidden(0, true);
    view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);

    combo->setModel(model);
    combo->setModelColumn(1);
    combo->setView(view);

    int currentRow = combo->findText(currentCongName, Qt::MatchExactly);
    if (currentRow >= 0) {
        combo->setCurrentText(currentCongName);
    }
}

void publictalkedit::on_comboPTCong_activated(int index)
{
    if (index < 1)
        ptCongIdFilter = 0;
    else
        saveCongregation(ui->comboPTCong->model(), index);
    setLabelOK(ui->labelPTCong, true);
}

void publictalkedit::on_comboPTCong_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state)
{
    bool ok(true);
    if (state == cEditableComboBox::filtered) {
        QString currentCongName = combo->currentText();
        // using the QStandardItemModel is not as efficient as querying the data directly, but preventing duplicate code is higher priority
        QStandardItemModel *model = getCongregationTable(currentCongName, false);
        if (model->rowCount() == 2) { // 2 because we have a blank row
            saveCongregation(model, 1);
            combo->setCurrentText(model->index(1, 1).data(0).toString());
        } else {
            ptCongIdFilter = 0;
            ok = false;
        }
    }
    setLabelOK(ui->labelPTCong, ok);
}

QStandardItemModel *publictalkedit::getCongregationTable(QString filter, bool allowOwnCongregation)
{
    QString defaultItem = ui->comboPTCong->currentText();
    auto model = new QStandardItemModel(0, 2);
    model->setHorizontalHeaderItem(0, new QStandardItem(""));
    model->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("Congregation Name")));

    // blank row
    {
        QList<QStandardItem *> cells;
        cells.append(new QStandardItem(""));
        cells.append(new QStandardItem(""));
        model->appendRow(cells);
    }

    bool filtered = !filter.isEmpty();
    for (ccongregation::congregation const &cong : allCongregations) {
        if (!filtered || cong.name.contains(filter, Qt::CaseInsensitive)) {
            QList<QStandardItem *> cells;
            QStandardItem *item = new QStandardItem();
            item->setData(cong.id, Qt::DisplayRole);
            cells.append(item);
            item = new QStandardItem(cong.name);
            if (!allowOwnCongregation && cong.id == myCongregation.id) {
                // disable own congregation
                item->setFlags(Qt::NoItemFlags);
            } else if (cong.name == defaultItem) {
                // bold
                item->font().setBold(true);
            }
            cells.append(item);
            model->appendRow(cells);
        }
    }
    return model;
}

void publictalkedit::saveCongregation(QAbstractItemModel *model, int index)
{
    // this is just a filter for the speaker list
    ptCongIdFilter = model->index(index, 0).data(0).toInt();
}

void publictalkedit::beforeShowSpeakerCombo(QString cname, cEditableComboBox *combo, bool filterRequested)
{
    Q_UNUSED(cname);
    // get speakers for comboBox
    QString currentSpeakerName = combo->currentText();
    QString filter;
    if (filterRequested) {
        filter = currentSpeakerName;
    }
    QStandardItemModel *model = cp.getSpeakers(currentMeeting->theme.id, ptCongIdFilter, filter);
    QTableView *view = new QTableView(combo);
    view->setModel(model);

    view->verticalHeader()->hide();
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setSelectionMode(QAbstractItemView::SingleSelection);

    view->setColumnHidden(0, true);
    view->setColumnWidth(6, 25);
    view->setSortingEnabled(true);
    view->sortByColumn(1, Qt::AscendingOrder);
    view->setColumnWidth(3, 0);
    view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    view->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Fixed);
    view->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    view->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);

    combo->setModel(model);
    combo->setModelColumn(3);
    combo->setView(view);
    int currentRow = combo->findText(currentSpeakerName, Qt::MatchExactly);
    if (currentRow >= 0) {
        QFont boldfont(view->font().family(),
                       view->font().pointSize(),
                       QFont::Bold);
        model->item(currentRow, 3)->setFont(boldfont);
        combo->setCurrentText(currentSpeakerName);
    }
}

void publictalkedit::on_comboPTSpeaker_activated(int index)
{
    savePTSpeaker(ui->comboPTSpeaker->model(), index);
    setLabelOK(ui->labelPTSpeaker, true);
}

void publictalkedit::on_comboPTSpeaker_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state)
{
    bool ok(true);
    if (state == cEditableComboBox::filtered) {
        QString currentSpeakerName(combo->currentText());
        int congID(0);
        QString congName(ui->comboPTCong->currentText());
        if (!congName.isEmpty()) {
            for (ccongregation::congregation const &cong : allCongregations) {
                if (cong.name.contains(congName, Qt::CaseInsensitive)) {
                    congID = cong.id;
                    break;
                }
            }
        }
        // using the QStandardItemModel is not as efficient as querying the data directly, but preventing duplicate code is higher priority
        QStandardItemModel *model = cp.getSpeakers(0, congID, currentSpeakerName);
        int itemsFound(model->rowCount());
        if (itemsFound == 2) { // 2 because we have a blank row
            savePTSpeaker(model, 1);
            combo->setCurrentText(model->index(1, 3).data(0).toString());
        } else {
            ok = false;
        }
    }
    setLabelOK(ui->labelPTSpeaker, ok);
}

void publictalkedit::savePTSpeaker(QAbstractItemModel *model, int index)
{
    int id = model->index(index, 0).data(0).toInt();
    person *speaker = cpersons::getPerson(id);
    currentMeeting->setSpeaker(speaker);
    currentMeeting->save();
}

void publictalkedit::fillTodo()
{
    QList<todo *> todolist(todo::getList());
    todoModel->removeRows(0, todoModel->rowCount());

    foreach (todo *t, todolist) {
        QList<QStandardItem *> cells;
        QStandardItem *item = new QStandardItem();
        item->setData(qVariantFromValue((void *)t), Qt::DisplayRole);
        cells.append(item);
        cells.append(new QStandardItem(t->getIsIncoming() ? tr("In", "Incoming") : tr("Out", "Outgoing")));
        cells.append(new QStandardItem(t->getDate().toString()));
        cells.append(new QStandardItem(t->getSpeaker()));
        cells.append(new QStandardItem(t->getCongregation()));
        cells.append(new QStandardItem(t->getTheme()));
        cells.append(new QStandardItem(t->getNotes()));
        todoModel->appendRow(cells);
    }
}

void publictalkedit::populateCongregationCombo(QComboBox *combo)
{
    combo->clear();
    combo->addItem("");
    QStandardItemModel *model = qobject_cast<QStandardItemModel *>(combo->model());

    for (ccongregation::congregation const &cong : allCongregations) {
        combo->addItem(cong.name);

        if (cong.id == myCongregation.id) {
            // disable own congregation
            QStandardItem *item = model->item(combo->count() - 1, 0);
            item->setFlags(Qt::NoItemFlags);
        }
    }
}

void publictalkedit::beforeShowOutCongCombo(QString cname, cPersonComboBox *combo)
{
    Q_UNUSED(cname);
    populateCongregationCombo(combo);
}

void publictalkedit::beforeShowOutSpeakerCombo(QString cname, cPersonComboBox *combo)
{
    Q_UNUSED(cname);
    QStandardItemModel *model = cp.getSpeakers(0, 0, "", true, true, currentDate);

    QTableView *view = new QTableView(combo);
    view->verticalHeader()->hide();
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setSelectionMode(QAbstractItemView::SingleSelection);
    combo->setModel(model);
    combo->setView(view);
    combo->setModelColumn(1);

    view->setColumnHidden(0, true);
    view->setColumnHidden(3, true);
    view->setColumnHidden(4, true);
    view->setColumnHidden(5, true);

    view->setColumnWidth(6, 25);
    view->setMinimumWidth(400);
    view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
}

void publictalkedit::beforeShowOutThemeCombo(QString cname, cPersonComboBox *combo)
{
    Q_UNUSED(cname);
    person *spkr = cpersons::getPerson(ui->comboOutSpeaker->model()->index(ui->comboOutSpeaker->currentIndex(), 0).data(0).toInt());
    QStandardItemModel *model = cp.getThemesTable("", spkr, currentDate);

    QTableView *view = new QTableView(combo);
    view->verticalHeader()->hide();
    view->setSelectionBehavior(QAbstractItemView::SelectRows);
    view->setSelectionMode(QAbstractItemView::SingleSelection);
    combo->setModel(model);
    combo->setView(view);
    combo->setModelColumn(1);
    view->setColumnHidden(0, true);
    view->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    int w = view->style()->pixelMetric(QStyle::PM_ScrollBarExtent);
    for (int i = 0; i < model->columnCount(); i++) {
        w += view->columnWidth(i);
    }
    view->setMinimumWidth(w);
}

void publictalkedit::setLabelOK(QLabel *lbl, bool dataOk)
{
    if (dataOk) {
        lbl->setFont(okFont);
        lbl->setStyleSheet("");
    } else {
        lbl->setFont(filterFont);
        lbl->setStyleSheet("background-color: rgb(255, 0, 0)");
    }
}

void publictalkedit::on_comboWtSource_currentIndexChanged(const int index)
{
    qDebug() << "wt source combo clicked" << index;

    currentMeeting->wtsource = index < 1 ? "" : QString::number(index);
    currentMeeting->save();
}

void publictalkedit::on_editWTTheme_editingFinished()
{
    currentMeeting->wttheme = ui->editWTTheme->text();
    currentMeeting->save();
}

void publictalkedit::on_editFinalTalk_editingFinished()
{
    currentMeeting->setFinalTalk(ui->editFinalTalk->text());
    currentMeeting->save();
}

void publictalkedit::on_buttonAdd_clicked()
{
    // add a new outgoing row
    if (ui->comboOutSpeaker->currentText() == "" || ui->comboOutCong->currentIndex() == 0 || ui->comboOutTheme->currentText() == "")
        return;

    int speakerId = ui->comboOutSpeaker->model()->index(ui->comboOutSpeaker->currentIndex(), 0).data(0).toInt();
    int themeId = ui->comboOutTheme->model()->index(ui->comboOutTheme->currentIndex(), 0).data(0).toInt();
    int congId = allCongregations[ui->comboOutCong->currentIndex() - 1].id;

    if (cp.getOutgoingBySpeaker(currentDate, speakerId, true).size() > 0) {
        if (QMessageBox::question(this, "",
                                  tr("The selected speaker has already public talk on this calendar month. Do you want to add?"), QMessageBox::No, QMessageBox::Yes)
            == QMessageBox::No) {
            return;
        }
    }

    cpoutgoing *out = cp.addOutgoingSpeaker(currentDate, speakerId, themeId, congId);

    if (out) {
        ccongregation::meeting_dayandtime const &mtg = out->getCongregation().getPublicmeeting(currentDate);
        QDate outDate = mtg.getMeetingDate(currentDate);
        // the following logic will put the day in parenthesis if it's a different day than the current congregation
        QString displaytime = mtg.getMeetingtime() + (outDate == ptDate ? "" : " (" + QDate::shortDayName(outDate.dayOfWeek()) + ")");

        ui->tableOutgoing->insertRow(ui->tableOutgoing->rowCount());
        ui->tableOutgoing->setItem(ui->tableOutgoing->rowCount() - 1, 0, new QTableWidgetItem(QVariant(out->getId()).toString()));
        ui->tableOutgoing->setItem(ui->tableOutgoing->rowCount() - 1, 1, new QTableWidgetItem(out->getSpeaker()->fullname()));
        ui->tableOutgoing->setItem(ui->tableOutgoing->rowCount() - 1, 2, new QTableWidgetItem(out->getCongregation().name));
        ui->tableOutgoing->setItem(ui->tableOutgoing->rowCount() - 1, 3, new QTableWidgetItem(displaytime));
        ui->tableOutgoing->setItem(ui->tableOutgoing->rowCount() - 1, 4, new QTableWidgetItem(out->getTheme().theme));
        ui->tableOutgoing->setItem(ui->tableOutgoing->rowCount() - 1, 5, new QTableWidgetItem(QVariant(out->getTheme().number).toString()));
    }

    // clear selections
    ui->comboOutSpeaker->setCurrentText("");
    ui->comboOutCong->setCurrentIndex(0);
    ui->comboOutTheme->setCurrentText("");
}

void publictalkedit::on_buttonRemove_clicked()
{
    // remove outgoing row

    if (ui->tableOutgoing->selectedItems().count() < 1)
        return;

    QTableWidgetItem *selecteditem = ui->tableOutgoing->selectedItems().first();

    int id = ui->tableOutgoing->item(selecteditem->row(), 0)->text().toInt();
    if (cp.removeOutgoingSpeaker(id)) {
        ui->tableOutgoing->removeRow(selecteditem->row());
    }
}

void publictalkedit::on_comboOutSpeaker_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "") {
        ui->comboOutTheme->setCurrentText("");
    }
    ui->comboOutTheme->setEnabled(arg1 != "");
}

void publictalkedit::on_comboConductor_activated(int index)
{
    int id = QVariant(ui->comboConductor->model()->index(index, 0).data(0).toString()).toInt();
    person *conductor = cpersons::getPerson(id);
    currentMeeting->setWtConductor(conductor);
    currentMeeting->save();
}

void publictalkedit::on_spin_song_talk_valueChanged(int arg1)
{
    currentMeeting->song_talk = arg1;
    currentMeeting->save();
}

void publictalkedit::on_spin_song_wt_start_valueChanged(int arg1)
{
    currentMeeting->song_wt_start = arg1;
    currentMeeting->save();
}

void publictalkedit::on_spin_song_wt_end_valueChanged(int arg1)
{
    currentMeeting->song_wt_end = arg1;
    currentMeeting->save();
}

void publictalkedit::createCalendarPopup()
{
    calPopup = new QFrame(this, Qt::Popup);
    calPopup->setFrameShape(QFrame::Box);
    QHBoxLayout *layout = new QHBoxLayout;
    oDate = new QDateEdit();
    oDate->setCalendarPopup(true);
    oDate->setDate(QDate::currentDate());
    QCalendarWidget *oCal = oDate->calendarWidget();
    oCal->setFirstDayOfWeek(Qt::Monday);
    layout->addWidget(oCal);
    calPopup->setLayout(layout);

    QObject::connect(oDate, SIGNAL(dateChanged(QDate)), this, SLOT(calendarClicked(QDate)));
}

void publictalkedit::showCalendarPopup(bool isForIncoming)
{
    isCalendarPoppingUpForIncoming = isForIncoming;
    oDate->setDate(ptDate);
    QPoint curpos = QCursor::pos();
    QRect screen = QGuiApplication::screenAt(curpos)->availableGeometry();

    int xpos, ypos;
    xpos = curpos.x() + calPopup->sizeHint().width() > screen.width() ? screen.width() - calPopup->sizeHint().width() : curpos.x();
    ypos = curpos.y() + calPopup->sizeHint().height() > screen.height() ? screen.height() - calPopup->sizeHint().height() : curpos.y();
    calPopup->move(xpos, ypos);
    calPopup->show();
}

void publictalkedit::calendarClicked(QDate date)
{
    QDate otherdate = date.addDays(1 - date.dayOfWeek());
    calPopup->close();
    if (currentDate == otherdate)
        return; // nothing to move

    if (isCalendarPoppingUpForIncoming) {
        moveIncoming(otherdate);
    } else {
        moveOutgoing(otherdate);
    }
}

void publictalkedit::moveIncoming(QDate date)
{
    cptmeeting *othermtg = cp.getMeeting(date);
    if (othermtg->id > 0) {
        // conflict
        QMessageBox msgbox(this);
        msgbox.setText(tr("The destination date already has a talk scheduled. What to do?"));
        QAbstractButton *swap = msgbox.addButton(tr("&Swap Talks"), QMessageBox::AcceptRole);
        QAbstractButton *othertodo = msgbox.addButton(tr("&Move other talk to To Do List"), QMessageBox::AcceptRole);
        msgbox.addButton(tr("&Cancel"), QMessageBox::RejectRole);
        msgbox.exec();
        if (msgbox.clickedButton() == swap) {
            cptmeeting temp;
            temp.theme = currentMeeting->theme;
            temp.setSpeaker(currentMeeting->speaker());
            currentMeeting->theme = othermtg->theme;
            currentMeeting->setSpeaker(othermtg->speaker());
            othermtg->theme = temp.theme;
            othermtg->setSpeaker(temp.speaker());
            currentMeeting->save();
            othermtg->save();
        } else if (msgbox.clickedButton() == othertodo) {
            todo newtodo(true);
            newtodo.setTheme(othermtg->theme.theme + " (" + QVariant(othermtg->theme.number).toString() + ")");
            newtodo.setSpeaker(othermtg->speaker()->fullname());
            newtodo.setCongregation(othermtg->speaker()->congregationName());
            newtodo.setNotes(tr("From %1").arg(date.toString(Qt::ISODate)));
            newtodo.save();
            othermtg->theme = currentMeeting->theme;
            othermtg->setSpeaker(currentMeeting->speaker());
            othermtg->save();
            currentMeeting->theme = cpttheme();
            currentMeeting->setSpeaker(nullptr);
            currentMeeting->save();
        } else
            return;
    } else {
        // no conflict
        othermtg->theme = currentMeeting->theme;
        othermtg->setSpeaker(currentMeeting->speaker());
        othermtg->save();
    }
    setDate(currentDate);
}

void publictalkedit::moveOutgoing(QDate date)
{
    if (ui->tableOutgoing->selectedItems().count() < 1)
        return;

    QTableWidgetItem *selecteditem = ui->tableOutgoing->selectedItems().first();
    QString cong = ui->tableOutgoing->item(selecteditem->row(), 2)->text();
    cpoutgoing *currentout = cp.getOutgoingByCongregation(currentDate, cong);
    cpoutgoing *otherout = cp.getOutgoingByCongregation(date, cong);
    if (otherout) {
        // conflict
        QMessageBox msgbox(this);
        msgbox.setText(tr("The destination date already has a talk scheduled. What to do?"));
        QAbstractButton *swap = msgbox.addButton(tr("&Swap Talks"), QMessageBox::AcceptRole);
        QAbstractButton *othertodo = msgbox.addButton(tr("&Move other talk to To Do List"), QMessageBox::AcceptRole);
        msgbox.addButton(tr("&Cancel"), QMessageBox::RejectRole);
        msgbox.exec();
        if (msgbox.clickedButton() == swap) {
            cp.addOutgoingSpeaker(currentDate, otherout->getSpeaker()->id(), otherout->getTheme().id, otherout->getCongregation().id);
            cp.addOutgoingSpeaker(date, currentout->getSpeaker()->id(), currentout->getTheme().id, currentout->getCongregation().id);
            cp.removeOutgoingSpeaker(currentout->getId());
            cp.removeOutgoingSpeaker(otherout->getId());
        } else if (msgbox.clickedButton() == othertodo) {
            todo newtodo(false);
            newtodo.setSpeaker(otherout->getSpeaker()->fullname());
            newtodo.setCongregation(otherout->getCongregation().name);
            newtodo.setTheme(otherout->getTheme().theme + " (" + QVariant(otherout->getTheme().number).toString() + ")");
            newtodo.setNotes(tr("From %1").arg(date.toString(Qt::ISODate)));
            newtodo.save();
            cp.addOutgoingSpeaker(date, currentout->getSpeaker()->id(), currentout->getTheme().id, currentout->getCongregation().id);
            cp.removeOutgoingSpeaker(currentout->getId());
            cp.removeOutgoingSpeaker(otherout->getId());
        } else
            return;
    } else {
        // no confict
        cp.addOutgoingSpeaker(date, currentout->getSpeaker()->id(), currentout->getTheme().id, currentout->getCongregation().id);
        cp.removeOutgoingSpeaker(currentout->getId());
    }
    setDate(currentDate);
}

void publictalkedit::on_buttonMoveTodo_clicked()
{
    QModelIndex index = ui->todoTable->currentIndex();
    todo *t = (todo *)index.sibling(index.row(), 0).data().value<void *>();
    if (!t)
        return;
    QString msg = t->moveToSchedule();
    if (!msg.isEmpty()) {
        if (msg == tr("Date Already Scheduled")) {
            QMessageBox msgbox(this);
            msgbox.setText(tr("The destination date already has a talk scheduled. What to do?"));
            QAbstractButton *othertodo = msgbox.addButton(tr("&Move other talk to To Do List"), QMessageBox::AcceptRole);
            msgbox.addButton(tr("&Cancel"), QMessageBox::RejectRole);
            msgbox.exec();
            if (msgbox.clickedButton() == othertodo) {
                cptmeeting *othermtg = cp.getMeeting(t->get_date());
                todo newtodo(t->getIsIncoming());
                if (t->getIsIncoming()) {
                    newtodo.setTheme(othermtg->theme.theme + " (" + QVariant(othermtg->theme.number).toString() + ")");
                    newtodo.setSpeaker(othermtg->speaker()->fullname());
                    newtodo.setCongregation(othermtg->speaker()->congregationName());
                    newtodo.setNotes(tr("From %1").arg(t->get_date().toString(Qt::ISODate)));
                    othermtg->theme = cp.getThemeById(t->get_themeId());
                    othermtg->setSpeaker(t->get_speaker());
                    othermtg->save();
                } else {
                    cpoutgoing *otherout = cp.getOutgoingByCongregation(t->get_date(), t->getCongregation());
                    newtodo.setSpeaker(otherout->getSpeaker()->fullname());
                    newtodo.setCongregation(otherout->getCongregation().name);
                    newtodo.setTheme(otherout->getTheme().theme + " (" + QVariant(otherout->getTheme().number).toString() + ")");
                    newtodo.setNotes(tr("From %1").arg(t->get_date().toString(Qt::ISODate)));
                    cp.removeOutgoingSpeaker(otherout->getId());
                    cp.addOutgoingSpeaker(t->get_date(), t->get_speaker()->id(), t->get_themeId(), t->get_congregationId());
                }
                newtodo.save();
                t->deleteme();
            } else
                return;
        } else {
            QMessageBox::information(this, tr("Error"), tr("Cannot schedule this item until these fields are fixed: %1").arg(msg));
            return;
        }
    }
    setDate(currentDate);
}

void publictalkedit::on_btnPTTodo_clicked()
{
    todo t(true);
    t.setCongregation(ui->comboPTCong->currentText());
    t.setSpeaker(ui->comboPTSpeaker->currentText());
    t.setTheme(ui->comboTheme->currentText());
    t.setNotes(tr("From %1").arg(currentDate.toString(Qt::ISODate)));
    t.save();
    currentMeeting->theme = cpttheme();
    currentMeeting->setSpeaker(nullptr);
    currentMeeting->save();
    setDate(currentDate);
}

void publictalkedit::on_btnPTMove_clicked()
{
    showCalendarPopup(true);
}

void publictalkedit::on_btnOutMove_clicked()
{
    if (ui->tableOutgoing->selectedItems().count() < 1)
        return;
    showCalendarPopup(false);
}

void publictalkedit::on_btnOutTodo_clicked()
{
    if (ui->tableOutgoing->selectedItems().count() < 1)
        return;

    QTableWidgetItem *selecteditem = ui->tableOutgoing->selectedItems().first();

    int id = ui->tableOutgoing->item(selecteditem->row(), 0)->text().toInt();
    if (cp.removeOutgoingSpeaker(id)) {
        todo t(false);
        t.setCongregation(ui->tableOutgoing->item(selecteditem->row(), 2)->text());
        t.setSpeaker(ui->tableOutgoing->item(selecteditem->row(), 1)->text());
        t.setTheme(ui->tableOutgoing->item(selecteditem->row(), 4)->text());
        t.setNotes(tr("From %1").arg(currentDate.toString(Qt::ISODate)));
        t.save();
        setDate(currentDate);
    }
}

void publictalkedit::on_buttonAddTodoIn_clicked()
{
    todo t(true);
    fillTodo();
}

void publictalkedit::on_buttonAddTodoOut_clicked()
{
    todo t(false);
    fillTodo();
}

void publictalkedit::on_buttonRemoveTodo_clicked()
{
    QModelIndex index = ui->todoTable->currentIndex();
    if (index.row() < 0)
        return;
    todo *t = (todo *)index.sibling(index.row(), 0).data().value<void *>();
    if (!t)
        return;
    t->deleteme();
    fillTodo();
}

void publictalkedit::beforeShowTodoPopup(QString cname, cEditableComboBox *combo, bool filterRequested)
{
    Q_UNUSED(cname);
    todo *t = (todo *)combo->property("todo").value<void *>();
    if (!t)
        return;
    QString currentText = combo->currentText();
    QString filter;
    if (filterRequested) {
        filter = currentText;
    }
    int colIdx(combo->property("colid").toInt());
    int modelColIdx(0);
    QStandardItemModel *model = nullptr;
    QTableView *view = nullptr;

    switch (colIdx) {
    case 2: // date
        // future: show date picker
        break;

    case 3: // speaker
    {
        bool onlyOwnCongregation(!t->getIsIncoming());
        int _congregationId(0);
        if (t->getIsIncoming()) {
            ccongregation c;
            if (!t->getCongregation().isEmpty()) {
                _congregationId = c.getCongregationIDByPartialName(t->getCongregation());
            }
        }
        model = cp.getSpeakers(0, _congregationId, filter, onlyOwnCongregation);
    }
        modelColIdx = 3;

        view = new QTableView(combo);
        view->setModel(model);

        view->verticalHeader()->hide();
        view->setSelectionBehavior(QAbstractItemView::SelectRows);
        view->setSelectionMode(QAbstractItemView::SingleSelection);

        view->setColumnHidden(0, true);
        view->setColumnWidth(6, 25);
        view->setSortingEnabled(true);
        view->sortByColumn(3, Qt::AscendingOrder);
        view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        view->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
        view->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        view->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
        view->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
        break;

    case 4: // congregation
        model = getCongregationTable(filter, true);
        modelColIdx = 1;

        view = new QTableView(this);
        view->setModel(model);

        view->verticalHeader()->hide();
        view->setSelectionMode(QAbstractItemView::SingleSelection);
        view->setSelectionBehavior(QAbstractItemView::SelectRows);

        view->setSortingEnabled(false);
        //view->sortByColumn(1,Qt::AscendingOrder);
        view->horizontalHeader()->hide();
        view->setColumnHidden(0, true);
        view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        break;

    case 5: // theme
    {
        person *speaker(nullptr);
        bool onlyOwnCongregation(!t->getIsIncoming());
        model = cp.getSpeakers(0, 0, t->getSpeaker(), onlyOwnCongregation);
        if (model->rowCount() == 2) { // 2 because we have a blank row
            speaker = cpersons::getPerson(model->index(1, 0).data(0).toInt());
        }

        model = getThemesTable(filter, speaker);
    }
        modelColIdx = 2;

        view = new QTableView(this);
        view->setModel(model);

        view->verticalHeader()->hide();
        view->setSelectionMode(QAbstractItemView::SingleSelection);
        view->setSelectionBehavior(QAbstractItemView::SelectRows);
        view->setSortingEnabled(true);
        view->sortByColumn(1, Qt::AscendingOrder);
        view->verticalHeader()->hide();
        view->setColumnHidden(0, true);
        view->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        view->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
        break;
    }

    combo->setModel(model);
    combo->setModelColumn(modelColIdx);
    combo->setView(view);

    int w = view->style()->pixelMetric(QStyle::PM_ScrollBarExtent);
    for (int i = 1; i < model->columnCount(); i++) {
        w += view->columnWidth(i);
    }
    view->setMinimumWidth(w);

    int currentRow = combo->findText(currentText, Qt::MatchExactly);
    if (currentRow >= 0) {
        QFont boldfont(view->font().family(),
                       view->font().pointSize(),
                       QFont::Bold);
        model->item(currentRow, modelColIdx)->setFont(boldfont);
        combo->setCurrentText(currentText);
    }
}

void publictalkedit::on_todoCombo_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state)
{
    if (state == cEditableComboBox::filtered) {
        bool okSave(false);
        todo *t = (todo *)combo->property("todo").value<void *>();
        if (!t)
            return;
        int colIdx(combo->property("colid").toInt());
        QStandardItemModel *model;
        QString currentText = combo->currentText();

        switch (colIdx) {
        case 2: // date
            break;

        case 3: // speaker
        {
            bool onlyOwnCongregation(!t->getIsIncoming());
            int _congregationId(0);
            if (t->getIsIncoming()) {
                ccongregation c;
                if (!t->getCongregation().isEmpty()) {
                    _congregationId = c.getCongregationIDByPartialName(t->getCongregation());
                }
            }
            model = cp.getSpeakers(0, _congregationId, currentText, onlyOwnCongregation);
        }
            if (model->rowCount() == 2) { // 2 because we have a blank row
                okSave = true;
                currentText = model->index(1, 3).data(0).toString();
                t->setSpeaker(currentText);
            }
            break;

        case 4: // congregation
            model = getCongregationTable(currentText, true);
            if (model->rowCount() == 2) { // 2 because we have a blank row
                okSave = true;
                currentText = model->index(1, 1).data(0).toString();
                t->setCongregation(currentText);
            }
            break;

        case 5: // theme
            model = getThemesTable(currentText, nullptr);
            if (model->rowCount() == 2) { // 2 because we have a blank row
                okSave = true;
                currentText = model->index(1, 2).data(0).toString();
                t->setTheme(currentText);
            }
            break;
        }

        if (okSave) {
            t->save();
            fillTodo(); // this is a bad idea, but it's the best so far. Without this, the view doesn't update.
            //int rowIdx(combo->property("rowid").toInt());
            //combo->setCurrentText(currentText);
            //model->setData(model->index(rowIdx, colIdx), QVariant(currentText));
        }
    }
}
