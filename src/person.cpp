/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "person.h"

person::person(QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    this->setId(-1);
    this->setGender(Male);
    this->setServant(0);
    this->setCongregationid(0);
    this->setUsefor(0);
    this->m_uuid = "";
    this->m_congregationname = "";
}

person::person(int id, QString uuid, QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    this->setId(id);
    this->setGender(Male);
    this->setServant(0);
    this->setCongregationid(0);
    this->setUsefor(0);
    this->m_uuid = uuid;
    this->m_congregationname = "";
}

// id
int person::id()
{
    return m_id;
}

void person::setId(int id)
{
    if (m_id == id)
        return;
    m_id = id;
    emit idChanged(id);
}

bool person::isNew()
{
    return m_id < 1;
}

// firstname
QString person::firstname() const
{
    return m_firstname;
}

void person::setFirstname(QString arg)
{
    if (m_firstname == arg)
        return;
    m_firstname = arg;
    setDirtyFlag(true);
    emit firstnameChanged(arg);
}

// lastname
QString person::lastname() const
{
    return m_lastname;
}

void person::setLastname(QString arg)
{
    if (m_lastname == arg)
        return;
    m_lastname = arg;
    setDirtyFlag(true);
    emit lastnameChanged(arg);
}

// fullname
QString person::fullname(QString format)
{
    // FORMAT:
    // "FirstName LastName" = DEFAULT
    // "LastName FirstName"
    // "LastName, FirstName"
    format = format.replace("FirstName", "%1");
    format = format.replace("LastName", "%2");
    QString fullname = QString(format).arg(this->firstname()).arg(this->lastname());
    return (fullname);
}

// gender
person::Gender person::gender() const
{
    return m_gender;
}

void person::setGender(Gender gender)
{
    if (m_gender == gender)
        return;
    m_gender = gender;
    emit genderChanged(m_gender);
    setDirtyFlag(true);
}

// phone
QString person::phone() const
{
    return m_phone;
}

void person::setPhone(QString value)
{
    if (m_phone == value)
        return;
    m_phone = value;
    setDirtyFlag(true);
    emit phoneChanged(value);
}

// mobile
QString person::mobile() const
{
    return m_mobile;
}

void person::setMobile(QString value)
{
    if (m_mobile == value)
        return;
    m_mobile = value;
    setDirtyFlag(true);
    emit mobileChanged(value);
}

// email
QString person::email() const
{
    return m_email.simplified();
}

void person::setEmail(QString value)
{
    if (m_email == value)
        return;
    m_email = value;
    setDirtyFlag(true);
    emit emailChanged(value);
}

// servant
bool person::servant() const
{
    return m_servant;
}

void person::setServant(bool value)
{
    if (m_servant == value)
        return;
    m_servant = value;
    setDirtyFlag(true);
    emit servantChanged(value);
}

// use for
int person::usefor() const
{
    return m_usefor;
}

void person::setUsefor(int value)
{
    if (m_usefor == value)
        return;
    m_usefor = value;
    setDirtyFlag(true);
    emit useforChanged(value);
}

// info
QString person::info() const
{
    return m_info;
}

void person::setInfo(QString value)
{
    if (m_info == value)
        return;
    m_info = value;
    setDirtyFlag(true);
}

// congregation
int person::congregationid() const
{
    return m_congregationid;
}

void person::setCongregationid(int id)
{
    if (m_congregationid == id)
        return;
    m_congregationid = id;
    setDirtyFlag(true);
}

bool person::dirtyFlag()
{
    return m_dirty;
}

QString person::uuid() const
{
    return m_uuid;
}

void person::setDirtyFlag(bool dirty)
{
    m_dirty = dirty;
}

QString person::congregationName()
{
    if (m_congregationname.isEmpty()) {
        sql_items c = sql->selectSql("congregations", "id", QString::number(this->congregationid()), "");
        if (!c.empty()) {
            m_congregationname = c[0].value("name").toString();
        }
    }
    return m_congregationname;
}

sql_items person::congregationInfo()
{
    return sql->selectSql("congregations", "id", QString::number(this->congregationid()), "");
}

QList<QPair<QDate, QDate>> person::getUnavailabilities()
{
    // list person's all unavailabilities
    QList<QPair<QDate, QDate>> list;
    sql_item s;
    s.insert("person_id", this->id());
    sql_items unavailabilities = sql->selectSql("SELECT * FROM unavailables WHERE person_id=:person_id AND active ORDER BY start_date DESC", &s);
    if (!unavailabilities.empty()) {
        for (unsigned int i = 0; i < (unavailabilities.size()); i++) {
            sql_item u = unavailabilities[i];
            QDate start = u.value("start_date").toDate();
            QDate end = u.value("end_date").toDate();
            list.append(qMakePair(start, end));
        }
    }
    return list;
}

void person::setUnavailability(QDate start, QDate end)
{
    // add new unvailability for person
    sql_item s;
    s.insert("person_id", this->id());
    s.insert("start_date", start);
    s.insert("end_date", end);
    sql->insertSql("unavailables", &s, "id");
}

bool person::removeUnavailability(QDate start, QDate end)
{
    // remove unavailability
    sql_item s;
    s.insert("start_date", start);
    s.insert("end_date", end);
    s.insert("active", 0);
    return sql->execSql("UPDATE unavailables SET active = :active, time_stamp = strftime('%s','now') WHERE start_date = :start_date AND end_date = :end_date", &s, true);
}

bool person::update()
{
    if (!dirtyFlag())
        return true;
    // save person's information to database
    sql_item s;
    // now that sql_class::insertSql uses parameters, the apostrophes are not an issue
    s.insert("lastname", this->lastname());
    s.insert("firstname", this->firstname());
    s.insert("phone", this->phone());
    s.insert("mobile", this->mobile());
    s.insert("email", this->email());
    s.insert("servant", this->servant());
    s.insert("usefor", this->usefor());
    s.insert("gender", this->gender() == Male ? "B" : "S");
    s.insert("congregation_id", this->congregationid());
    s.insert("info", this->info());

    setDirtyFlag(false);
    if (this->isNew()) {
        int newID = sql->insertSql("persons", &s, "id");
        if (newID < 1)
            return false;
        setId(newID);
        setDirtyFlag(false);
        return true;
    }
    return sql->updateSql("persons", "id", QString::number(this->id()), &s);
}
