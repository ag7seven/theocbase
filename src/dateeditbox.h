#ifndef DATEEDITBOX_H
#define DATEEDITBOX_H

#include <QLayout>
#include <QItemDelegate>
#include <QDateEdit>
#include <QHBoxLayout>
#include <QToolButton>

class DateEditBox : public QItemDelegate {
    Q_OBJECT
public:
    enum { DateRole = Qt::UserRole+1 };
    explicit DateEditBox(QWidget *parent, QString dateFormat = QLocale::system().dateFormat(QLocale::ShortFormat));
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:
    bool mClearValue;
    QString mDateFormat;
private slots:
    void closeAndClearEditor();
};
#endif // DATEEDITBOX_H
