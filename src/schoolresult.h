/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOLRESULT_H
#define SCHOOLRESULT_H

#include <QDialog>
#include "sql_class.h"
#include "school.h"
#include "school_item.h"
#include "schoolstudy.h"
#include "person.h"
#include "cpersons.h"
#include "ccongregation.h"

namespace Ui {
    class schoolresult;
}

/**
 * @brief The schoolresult is the class to showing user interface for school assignment result
 *
 */
class schoolresult : public QDialog {
    Q_OBJECT
public:
    schoolresult(school_item &prog, QWidget *parent = 0);
    ~schoolresult();

    school_item &getProgram() const;

protected:
    void changeEvent(QEvent *e);

private:
    void initInformation();

    Ui::schoolresult *ui;
    QList<person*> allusers;
    QList<int> m_volunteerIds;
    QList<school_setting*> allsettings;
    school_item &currentprog;
    schoolStudentStudy *currentStudy;
    schoolStudentStudy *nextStudy;
    sql_class *sql;
    ccongregation c;

private slots:
    /**
     * @brief on_checkBoxDone_toggled - Done CheckBox clicked
     * @param checked
     */
    void on_checkBoxDone_toggled(bool checked);

    /**
     * @brief on_buttonCancel_clicked - Cancel Button clicked
     */
    void on_buttonCancel_clicked();

    /**
     * @brief on_pushButton_clicked - OK Button clicked
     */
    void on_pushButton_clicked();
    void on_checkBox_clicked();
    void on_comboBoxStudyStart_currentIndexChanged(int index);
    void on_checkBoxVolunteer_clicked(bool checked);
};

#endif // SCHOOLRESULT_H
