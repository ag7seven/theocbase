#include "theocbaseoauth.h"

TheocBaseOAuth::TheocBaseOAuth(QObject *parent) :
    QAbstractOAuth2(parent)
{
    this->setNetworkAccessManager(new QNetworkAccessManager(this));
    setStatus(Status::NotAuthenticated);
    setAccessTokenUrl(QUrl("https://api.theocbase.net/v2/api.php"));

    QSettings settings;
    setToken(settings.value("cloud/token","").toString());   

    _refreshToken = settings.value("cloud/refresh","").toString();
    _expiresAt = settings.value("cloud/expires").toDateTime();
}

void TheocBaseOAuth::setCredentials(const QString username, const QString password)
{
    _username = username;
    _password = password;
}

bool TheocBaseOAuth::isTokenValid()
{
    if (token().isEmpty())
        return false;
    if (_expiresAt < QDateTime::currentDateTime())
        return false;
    return true;
}

int TheocBaseOAuth::userId() const
{
    // get user id from JWT
    int userid = -1;
    if (!token().isEmpty()) {
        QStringList jwtparts = token().split(".");
        if (jwtparts.length() > 1) {
            QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromBase64(jwtparts[1].toUtf8()));
            QJsonObject obj = doc.object();
            bool ok;
            int id = obj.value("id").toString().toInt(&ok);
            if (ok)
                userid = id;
        }
    }
    return userid;
}

void TheocBaseOAuth::grant()
{
    // write authentication flow here
    qDebug() << "grant??" << "token:" << token() <<
                "refresh_token:" << _refreshToken <<
                "expiresat:" << _expiresAt <<
                "currenttime:" << QDateTime::currentDateTime();

    if (accessTokenUrl().isEmpty()) {
        qWarning() << "No request access token Url set";
        return;
    }

    // is it current token valid?
    if (!token().isEmpty()) {
        if (_expiresAt > QDateTime::currentDateTime()) {
            if (status() != Status::Granted) {
                setStatus(Status::Granted);
                emit granted();
            }
            return;
        }
        // is it possible to refresh token?
        if (!_refreshToken.isEmpty()) {
            refreshAccessToken();
            return;
        }
    }
    if (_username.isEmpty() || _password.isEmpty()) {
        emit authorizeWithBrowser(QUrl(""));
        return;
    }
    setStatus(Status::TemporaryCredentialsReceived);
    QVariantMap parameters;
    parameters.insert("oauth_token","");
    parameters.insert("grant_type", QStringLiteral("password"));
    parameters.insert("username", _username);
    QString md5psw = QString(QCryptographicHash::hash(_password.toLatin1(),QCryptographicHash::Md5).toHex());
    parameters.insert("password", md5psw);

    QUrlQuery query = createQuery(parameters);

    QUrl url = accessTokenUrl();
    url.setQuery(query);
    QNetworkRequest request(url);
    qDebug() << url;
    auto reply = networkAccessManager()->get(request);

    QEventLoop event;
    connect(reply,&QNetworkReply::finished,&event,&QEventLoop::quit);
    event.exec();
    accessTokenRequestFinished(reply);
}

void TheocBaseOAuth::revoke()
{
    qDebug() << "revoke";
    setToken("");
    _refreshToken = "";
    QSettings settings;
    settings.setValue("cloud/token","");
    settings.setValue("cloud/refresh","");   
    _username = "";
    _password = "";
}

void TheocBaseOAuth::refreshAccessToken()
{
    qDebug() << "refresh token";

    if (_refreshToken.isEmpty()) {
        qWarning() << "No refresh token set";
        return;
    }
    if (this->status() == Status::RefreshingToken) {
        qWarning() << "Cannot refresh access token. "
                      "Refreh Access Token is already in progress";
        return;
    }
    setStatus(Status::RefreshingToken);

    QVariantMap parameters;
    parameters.insert("oauth_token", "");
    parameters.insert("grant_type", QStringLiteral("refresh_token"));
    parameters.insert("refresh_token", _refreshToken);
    QUrlQuery query = createQuery(parameters);

    QUrl url = accessTokenUrl();
    url.setQuery(query);
    QNetworkRequest request(url);

    auto reply = networkAccessManager()->get(request);
    QEventLoop event;
    connect(reply,&QNetworkReply::finished,&event,&QEventLoop::quit);
    event.exec();
    accessTokenRequestFinished(reply);
}

QNetworkReply *TheocBaseOAuth::get(const QUrl &url, const QVariantMap &parameters)
{
    if (status() == Status::RefreshingToken) {
        qDebug() << "Refreh Access Token is in progress";
    }
    if (!isTokenValid())
        grant();
    return QAbstractOAuth2::get(url,parameters);;
}

QNetworkReply *TheocBaseOAuth::post(const QUrl &url, const QVariantMap &parameters)
{
    if (status() == Status::RefreshingToken) {
        qDebug() << "Refreh Access Token is in progress";
    }
    if (!isTokenValid())
        grant();
    return QAbstractOAuth2::post(url,parameters);
}

QString TheocBaseOAuth::responseType() const
{
    return QStringLiteral("code");
}

QUrl TheocBaseOAuth::accessTokenUrl() const
{
    return _accessTokenUrl;
}

void TheocBaseOAuth::setAccessTokenUrl(const QUrl &accessTokenUrl)
{
    _accessTokenUrl = accessTokenUrl;
}

QUrlQuery TheocBaseOAuth::createQuery(const QVariantMap &parameters)
{
    QUrlQuery query;
    for (auto it = parameters.begin(), end = parameters.end(); it != end; ++it)
        query.addQueryItem(it.key(), it.value().toString());
    return query;
}

void TheocBaseOAuth::accessTokenRequestFinished(QNetworkReply *reply)
{
    if (!reply)
        return;

    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject obj = doc.object();

    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "access token finished error" << doc;
        QSettings settings;
        settings.setValue("cloud/refresh", "");
        setStatus(Status::NotAuthenticated);
        if (obj["error"].toString() == "Authorization Required")
            emit authorizeWithBrowser(QUrl(""));
        reply->close();
        return;
    }

    const QString accessToken = obj["access_token"].toString();
    qDebug() << "access_token" << accessToken;
    if (accessToken.isEmpty()) {
        qWarning() << "Access token not received";
        setStatus(Status::NotAuthenticated);
        return;
    }
    setToken(accessToken);

    // refresh token
    _refreshToken = obj["refresh_token"].toString();

    // expires
    int expiresIn = obj["expires_in"].toInt();

    const QDateTime currentDateTime = QDateTime::currentDateTime();
    if (expiresIn > 0 && currentDateTime.secsTo(_expiresAt) != expiresIn) {
        _expiresAt = currentDateTime.addSecs(expiresIn);
        emit expirationAtChanged(_expiresAt);
    }

    QSettings settings;
    settings.setValue("cloud/token",accessToken);
    settings.setValue("cloud/refresh", _refreshToken);
    settings.setValue("cloud/expires", _expiresAt);

    setStatus(Status::Granted);
    emit granted();

    reply->deleteLater();
}
