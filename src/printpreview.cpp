#include "printpreview.h"
#include "ui_printpreview.h"

PrintPreview::PrintPreview(QWidget *parent)
    : QWidget(parent), ui(new Ui::PrintPreview)
{
    ui->setupUi(this);
    mPdf.clear();
    pageLoaded = false;
    ui->webengineview->page()->setBackgroundColor(Qt::transparent);
    connect(ui->webengineview, &QWebEngineView::loadFinished, this, &PrintPreview::htmlLoaded);
    ui->webengineview->setUrl(QUrl("qrc:/pdfjs/pdfjs-viewer.html"));
    channel = new QWebChannel(ui->webengineview->page());
    ui->webengineview->page()->setWebChannel(channel);
    channel->registerObject(QString("preview"), this);

    movie = new QMovie(":images/wait.gif", QByteArray(), this);
    movie->setScaledSize(QSize(16, 16));

    sql = &Singleton<sql_class>::Instance();
    pdfLoadingStarted();
}

PrintPreview::~PrintPreview()
{
    delete ui;
    delete channel;
}

void PrintPreview::setPdf(const QString pdffile)
{
    QFile file(pdffile);
    if (!file.exists())
        return;
    if (!file.open(QIODevice::ReadOnly))
        return;
    mPdf = file.readAll();
    if (pageLoaded)
        callPdfJavaScript();
    else
        qDebug() << "html is not yet loaded...";
}

void PrintPreview::setPdf(const QByteArray &pdf)
{
    ui->sliderZoom->setEnabled(false);
    ui->labelPages->setMovie(movie);
    movie->start();
    mPdf = pdf;
    if (pageLoaded)
        callPdfJavaScript();
    else
        qDebug() << "html is not yet loaded";
}

void PrintPreview::pdfLoaded(int current, int pages)
{
    qDebug() << "pdf loaded";
    currentPage = current;
    totalPages = pages;
    if (!clearBusy())
        emit previewReady();
}

void PrintPreview::pdfLoadingStarted()
{
    setBusy();
}

void PrintPreview::htmlLoaded(bool ok)
{
    qDebug() << "hml page loaded" << ok;
    pageLoaded = true;
    if (mPdf.size() > 0)
        callPdfJavaScript();
}

void PrintPreview::callPdfJavaScript()
{
    qreal previewZoom = sql->getSetting("preview_zoom").toFloat();
    if (previewZoom > 0) {
        ui->sliderZoom->blockSignals(true);
        ui->sliderZoom->setValue(previewZoom * 100);
        ui->sliderZoom->blockSignals(false);
    } else {
        previewZoom = 0.75;
    }
    ui->webengineview->page()->runJavaScript(QString("showPdfFile('%1',%2)")
                                                     .arg(QString::fromUtf8(mPdf.toBase64()), QVariant(previewZoom).toString()));
    mPdf.clear();
}

void PrintPreview::on_buttonPrevious_clicked()
{
    ui->webengineview->page()->runJavaScript("prevPage()");
}

void PrintPreview::on_buttonNext_clicked()
{
    ui->webengineview->page()->runJavaScript("nextPage()");
}

void PrintPreview::on_sliderZoom_sliderReleased()
{
    sliderPressed = false;
    qreal f = (double(ui->sliderZoom->value()) / 100);
    sql->saveSetting("preview_zoom", QVariant(f).toString());
    qDebug() << "slider released" << f;
    setBusy();
    ui->webengineview->page()->runJavaScript(QString("zoom(%1)").arg(f));
}

void PrintPreview::on_buttonFontSizeReset_clicked()
{
    emit textSizeReset();
    emit mapSizeReset();
}

void PrintPreview::on_buttonTextSizeDecrease_clicked()
{
    emit textSizeDecrease();
}

void PrintPreview::on_buttonTextSizeIncrease_clicked()
{
    emit textSizeIncrease();
}

void PrintPreview::on_buttonMapSizeDecrease_clicked()
{
    emit mapSizeDecrease();
}

void PrintPreview::on_buttonMapSizeIncrease_clicked()
{
    emit mapSizeIncrease();
}

void PrintPreview::on_sliderZoom_sliderPressed()
{
    sliderPressed = true;
}

void PrintPreview::on_sliderZoom_valueChanged(int value)
{
    Q_UNUSED(value);
#ifndef Q_OS_MAC
    if (!sliderPressed)
        on_sliderZoom_sliderReleased();
#endif
}

void PrintPreview::setBusy()
{
    ui->buttonNext->setEnabled(false);
    ui->buttonPrevious->setEnabled(false);
    ui->buttonTextSizeDecrease->setEnabled(false);
    ui->buttonFontSizeReset->setEnabled(false);
    ui->buttonTextSizeIncrease->setEnabled(false);
    ui->sliderZoom->setEnabled(false);
    ui->labelPages->setMovie(movie);
    movie->start();
}

bool PrintPreview::clearBusy()
{
    if (andZoom) {
        andZoom = false;
        on_sliderZoom_sliderReleased();
        return true; // preview again
    } else {
        ui->buttonNext->setEnabled(totalPages > currentPage);
        ui->buttonPrevious->setEnabled(currentPage > 1);
        ui->buttonTextSizeDecrease->setEnabled(true);
        ui->buttonFontSizeReset->setEnabled(true);
        ui->buttonTextSizeIncrease->setEnabled(true);
        ui->sliderZoom->setEnabled(true);
        ui->labelPages->setText(QVariant(currentPage).toString() + "/" + QVariant(totalPages).toString());
        movie->stop();
        return false;
    }
}
