/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "startup.h"
#include "ui_startup.h"

startup::startup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::startup)
{
    ui->setupUi(this);

    sql = &Singleton<sql_class>::Instance();

    //Put the dialog in the screen center
    const QRect screen = QApplication::desktop()->screenGeometry();
    this->move( screen.center() - this->rect().center() );

    // check if exist for native language
    // qrc:/startup/default.html
    QString langCode = sql->getSetting("theocbase_language");
    QString filename = ":/startup/default_" + langCode + ".html";
    qDebug() << filename;

    if (QFile::exists(filename)){
        ui->webView->setUrl(QUrl("qrc" + filename));
    }else{
        ui->webView->setUrl(QUrl("qrc:/startup/default.html"));
    }
}

startup::~startup()
{
    sql->saveSetting("screen","false");
    delete ui;
}

void startup::on_webView_loadFinished(bool arg1)
{
    qDebug() << "Page loaded" << arg1;
    // Hide scrollbars
    ui->webView->page()->runJavaScript("document.body.style.overflow='hidden';");
}
