#ifndef TODOTABLEVIEW_H
#define TODOTABLEVIEW_H

#include <QItemDelegate>
#include <QLineEdit>
#include <ceditablecombobox.h>
#include <todo.h>

class todoTableViewDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit todoTableViewDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &index) const;

signals: // only those we currently care about in publictalkedit
    void beforeShowPopup(QString cname, cEditableComboBox *object, bool filterRequested);
    void activated(int index);
    void focusOut(cEditableComboBox *object, const cEditableComboBox::states state);

private slots:
    void beforeShowComboPopup(QString cname, cEditableComboBox *combo, bool filterRequested);
    void on_combo_activated(int index);
    void on_combo_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state);

};

#endif // TODOTABLEVIEW_H
