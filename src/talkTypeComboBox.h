#ifndef TALKTYPECOMBOBOX_H
#define TALKTYPECOMBOBOX_H

#include <QItemDelegate>
#include <QComboBox>
#include <QTextEdit>
#include <QModelIndex>
#include <QTimer>
#include "lmm_schedule.h"

class talkTypeComboBox : public QItemDelegate
{
    Q_OBJECT

public:
    talkTypeComboBox(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option, const QModelIndex &) const;
};

class talkTypeTextEdit : public QItemDelegate
{
    Q_OBJECT
public:
    talkTypeTextEdit(QObject *parent = nullptr);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option, const QModelIndex &) const;
};

#endif // TALKTYPECOMBOBOX_H
