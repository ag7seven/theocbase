/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRINTUI_H
#define PRINTUI_H

#include <QDialog>
#include <QtGui>
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include <QMenu>
#include <QTextEdit>
#include <QPrintPreviewWidget>
#include <QPrintDialog>
#include <QPrinterInfo>
#include <QThread>
#include <QLocale>
#include <QList>
#include <QRegularExpression>
#include "school.h"
#include "ccongregation.h"
#include "cpublictalks.h"
#include "ccloud.h"
#include "printingconditionals.h"
#include "pdfform.h"
#include "slipscanner.h"
#include "printchannel.h"
#include "cloud/cloud_controller.h"

#include <QWebEnginePage>
#include <QWebEngineView>
#include <QtWebChannel>
#include <QWebEngineSettings>
#include <QPdfWriter>

namespace Ui {
class printui;
}

class printui : public QDialog
{
    Q_OBJECT
public:
    printui(QDate date, QWidget *parent = nullptr);
    ~printui();
    int schoolday;
    int publicmeetingday;

    void setCloud(cloud_controller *c);

    class weekInfo
    {
    public:
        weekInfo(QDate week, QDate mtgDate);
        QDate week;
        QDate mtgDate;
        int weeks;
        int fullweeknum;
        int weeknum;
        bool newmonth;
        bool endmonth;
        bool isFirst;
        bool isLast;
    };

    class extraOptions
    {
    public:
        extraOptions(QString name, QString tagName);

        QString name;
        QString tagName;
    };

    class templateData
    {
    public:
        enum templateTypes {
            notSet,
            midweekScheduleTemplate,
            midweekWorksheetTemplate,
            midweekSlipTemplate,
            combinationTemplate,
            publicMeetingTemplate,
            publicMeetingWorksheet,
            outgoingScheduleTemplate,
            outgoingSlipTemplate,
            territoryAssignmentRecordTemplate,
            territoryCardTemplate,
            territoryMapCardTemplate,
            hospitalityTemplate
        };

        templateData(templateTypes templateType, QString settingName, QString currentTemplate, QString fileFilter, QString notFileFilter = "");
        templateTypes getTemplateType();
        QString getTemplateName();
        void setTemplateName(QString name);

        // returns error message, if any
        QString setPaperSize(QString size = "");

        void loadMeta(QString html);
        QString metaValue(QString name, QString defaultValue = "");
        QString optionValue(QString name, QString defaultValue = "");
        void setOptionValue(QString name, QString value, bool okAdd = false, bool persist = false);

        sql_class *sql;
        QString fileFilter;
        QString notFileFilter;
        QString paperSize;
        QPrinter::PaperSize standardPaperSize;
        QSizeF customPaperSize;
        QPrinter::Unit customPaperUnit;

    private:
        templateTypes templateType;
        QString settingName;
        QString templateName;
        bool metaLoaded;
        QHash<QString, QString> meta;
        QHash<QString, QString> options;
    };

protected:
    void changeEvent(QEvent *e);
    void resizeEvent(QResizeEvent *);

private:
    Ui::printui *ui;
    sql_class *sql;
    AccessControl *ac;
    QDate fromDate;
    QDate thruDate;
    QString territoryNumberList = "1";
    //QPrintPreviewWidget *pv;
    QPrinter *pr;
    QString templatePath1;
    QString templatePath2;
    QString activeHtml;
    QAction *acsv;
    QPoint origo;
    int pagenumber;
    QMarginsF pmlist;
    QStringList paperSizes;
    QPrinter::Orientation po;
    QString qrcodepath;
    QList<QString> slipSettings;
    QSize customsize;
    qreal textSizeFactory;
    qreal textSizeFactorySlips;
    qreal defaultZoomFactor;
    int mapSizeFactory = 1;
    ccongregation c;
    ccongregation::congregation myCongregation;
    QList<weekInfo *> weekList;
    QHash<templateData::templateTypes, templateData *> templates;
    bool isLoaded;
    QString slipWorkingPath;
    bool individualSlips;
    int ptCounter;
    QByteArray bContent;
    QString sharedLink = "";
    QWebEnginePage *mPage;
    QWebEngineView *mWebView;
    QTemporaryFile tmpHtmlFile;
    QWebChannel *channel;
    PrintChannel printChannel;    
    cloud_controller *cloud;

    void printPublicMeetingWorkSheet();
    //QPoint getSchoolSlipSetting(QString settingname);

    //    void drawAssigmentSlipText(QPainter *painter, QString fullname,
    //                               QString fullassistantname, QString numbername, QString sourcetext, QString subject,
    //                               QString study,
    //                               QDate date, int schoolnumber, int settingid, QString settingname, int number);
    //    void drawAssignmentSlipLines(QPainter *painter);

    void applyAuthorizationRules();
    void printLMMSchedule();
    void printLMMWorkSheet();
    void printLMMAssignmentSlipsPreview();
    void printLMMAssignmentSlips_NoFormFields(bool debugBoxes, bool forceRescan, bool usePrinter = false);
    void printLMMAssignmentSlips(bool debugBoxes, bool findBoxNames);
    void printLMMCombination();
    void printPublicMeetingSchedule();
    void printOutgoingSpeakersList(bool bySpeaker = false);
    void drawCheckbox(QPainter *painter, int x, int y);

    void printTerritoryAssignmentRecord();
    void printTerritoryCard();
    void printTerritoryMapCardPreview();
    QString encodePointValue(double value);
    void printTerritoryMapCard(bool forceRescan, bool usePrinter = false);

    QString fillTemplateBlockLMMOverall(QString context);
    QString fillTemplateBlockLMM(QString context, bool worksheet);
    QString fillTemplateBlockPM(QString context);
    QString fillTemplateBlockPTOUT(QString context);
    QString fillTemplateBlockPTOUTSlips(person *speaker, QDate fromdate, QDate todate, QString context);
    QString fillTemplateBlockPTOUTTitle(QString context, QString speakername = "");
    QString fillTemplateBlockTerritoryOverall(QString context);
    QString fillCommonItems(QString context, bool nomeeting, ccongregation::exceptions ex);
    QString fillDate(QString context, ccongregation::meetings meetingtype, QString VarName);
    QString fillExceptionDate(QString context, QString tag);
    QString replaceDateTag(QString context, QString tag, QDate date, QString wrapperText = "%1");
    QString replaceTimeTag(QString context, QString tag, QTime time);
    QString replaceIntTag(QString context, QString tag, int number);
    QPair<QString, QString> templateGetSection(QString context, const QString start, const QString end, const QString classname);
    void setHTML(QString html, bool waitForLoadedEvent = false);
    QString addQrCode(QString context);
    QString addHtmlImage(QString path, QString tagname);
    QString addCssPageBreak();
    QString uploadSharedFile();
    void handleCurrentTemplateMeta(QString html);
    void showCurrentTemplateOptions();
    QString initPrinter(QString html);
    QPrinter::Orientation getOrientation();
    QMarginsF getMargins();
    QString getTemplate(bool returnFileName = false);
    void resetZooming();
    void fillWeekList(int offsetFromMonday);
    void fillWeekList_SetWeeks(weekInfo *previous);
    QString intListJoin(QList<int> list, QString delim);
    void finalizeSlip(QString pdftk, QString templateSlipPath, pdfform *pdf, int *slippagenum, QStringList *pdfs, QStringList *deleteMe);
    void saveSlipBoxFields(QString templateName);
    bool generateSlipPdfToJpg(QString pdfpath, QString jpgpath);
    void fillCustomPaperSizes();
    printingconditionals ifthen;
    weekInfo *currentWeek;
    QStringList templateList;
    templateData *currentTemplateData;
    int NoUIEvents;
    void on_resizeTimer_triggered();
    void setBusy();
    void clearBusy();

private slots:
    //void allReady();
    void printRequest();
    void pickTemplateEditor();
    void radioButtonClicked();
    void on_toolButtonCopy_clicked();
    void toolButtonPdf_clicked();
    void toolButtonHtml_clicked();
    void on_toolButtonPrint_clicked();
    void toolButtonOdf_clicked();
    void on_checkBoxQRCode_clicked(bool checked);
    void on_editDateRangeFrom_dateChanged(const QDate &date);
    void on_editDateRangeThru_dateChanged(const QDate &date);
    void on_lineEditTerritoryNumber_returnPressed();
    void increaseTextSize();
    void decreaseTextSize();
    void resetTextSize();
    void on_cboTemplate_activated(const QString &arg);
    void on_cboPaperSize_activated(int index);
    void on_ckOptionShowSongTitles_clicked(bool checked);
    void on_ckOptionShowWorkbookNumber_clicked(bool checked);
    void on_ckOptionShowWatchtowerNumber_clicked(bool checked);
    void on_ckOptionIncludeCounselText_clicked(bool checked);
    void on_ckOptionSectionTitles_clicked(bool checked);
    void on_rbOptionShowTime_clicked(bool checked);
    void on_rbOptionShowDuration_clicked(bool checked);
    void on_btnAdditionalOptions_clicked(bool checked);
    void on_txtMWTitle_editingFinished();
    void on_txtWETitle_editingFinished();
    void on_txtMW_OC_Title_editingFinished();
    void on_txtMW_RPA_Title_editingFinished();
};

#endif // PRINTUI_H
