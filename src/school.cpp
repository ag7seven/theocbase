/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "school.h"

school::school()
{
    this->m_ClassesQty = -1;
    this->m_schoolday = -1;
    sql = &Singleton<sql_class>::Instance();
}

school::~school()
{
}

bool school::getProgram(std::vector<std::unique_ptr<school_item> > &dest,const QDate &date)
{
    if (dest.size() > 0) dest.clear();

    sql_items otherweek;
    sql_items q = sql->selectSql("school_schedule","date", date.toString(Qt::ISODate),"number");

    if(q.empty()){
        return false;
    }else{
        ccongregation::exceptions ex1 = c.isException(date);
        ccongregation::exceptions ex2 = c.isException(date.addDays(-7));
        if(ex1 == ccongregation::circuit_overseer_visit || (date.year() < 2015 && ex1 == ccongregation::convention)){
            // this week is circuit overseer visit or convention
            for(unsigned int i = 0;i<q.size();i++){
                if(q[i].value("number").toInt() == 10){
                    // review = true;
                    q.erase(q.begin()+i);
                    otherweek = sql->selectSql("school_schedule","date", date.addDays(7).toString(Qt::ISODate),"number");
                    for(unsigned int iAdd = 0;iAdd<otherweek.size();iAdd++){
                        if(otherweek[iAdd].value("number").toInt() > 0){
                            otherweek[iAdd].insert("date", date); // was .toString(Qt::ISODate)
                            q.push_back(otherweek[iAdd]);
                        }
                    }
                    break;
                }
            }
        }else if(ex2 == ccongregation::circuit_overseer_visit || (date.year() < 2015 && ex2 == ccongregation::convention)){
            // previous week was circuit overseer visit or convention
            otherweek = sql->selectSql("school_schedule","date",date.addDays(-7).toString(Qt::ISODate),"number");
            for(unsigned int i = 0;i<otherweek.size();i++){
                if(otherweek[i].value("number").toInt() == 10){
                    // review should be this week
                    q.erase(q.begin()+1,q.end());
                    otherweek[i].insert("date", date); // .toString(Qt::ISODate)
                    q.push_back(otherweek[i]);
                    break;
                }
            }
        }

        for(unsigned int i = 0;i<q.size();i++){

            sql_items kv = sql->selectSql("school","school_schedule_id",q[i].value("id").toString(),"classnumber");

            for(int allschools = 0;allschools<this->getClassesQty();allschools++)
            {
                school_item* temp = new school_item();

                temp->setTheme(q[i].value("theme").toString());
                temp->setSource(q[i].value("source").toString());
                temp->setAssignmentNumber( q[i].value("number").toInt() );

                temp->setScheduleId( q[i].value("id").toInt() );
                temp->setDate( q[i].value("date").toDate() );
                temp->setStudent( 0 );
                temp->setAssistant( 0 );
                temp->setVolunteer( 0 );
                temp->setSetting(-1);
                temp->setDone( false );
                temp->setOnlyBrothers( q[i].value("onlybrothers").toBool() );
                if (temp->getOnlyBrothers()) {
                    // add * if needed
                    if (temp->getTheme().left(1) != "*") temp->setTheme( "* " + temp->getTheme() );
                }

                temp->setClassNumber( allschools+1 );

                int schoolindex = -1;
                if(!kv.empty())
                {
                    for(unsigned int schoolcheck=0;schoolcheck<kv.size();schoolcheck++)
                    {
                        if(kv[schoolcheck].value("classnumber").toInt() == allschools+1)
                        {
                            schoolindex = schoolcheck;
                            break;
                        }
                    }
                }

                if(schoolindex > -1){
                    int personid;
                    int assistantid;
                    cpersons cp;

                    temp->setId( kv[schoolindex].value("id").toInt() );
                    personid = kv[schoolindex].value("student_id").toInt();
                    temp->setStudent( cp.getPerson(personid) );

                    assistantid = kv[schoolindex].value("assistant_id").toInt();
                    temp->setAssistant( cp.getPerson(assistantid) );

                    temp->setNote( kv[schoolindex].value("note").toString() );
                    temp->setTiming( kv[schoolindex].value("timing").toString() );
                    temp->setDone( kv[schoolindex].value("completed").toInt() == 1 );

                    int setting = kv[schoolindex].value("setting_id").toInt();
                    if(setting != 0) temp->setSetting(setting);

                    // replacement student
                    int replacement = kv[schoolindex].value("volunteer_id").toInt();
                    temp->setVolunteer( cp.getPerson(replacement) );

                    // school number
                    temp->setClassNumber( kv[schoolindex].value("classnumber").toInt() );
                }else{
                    // add empty program
                }
                dest.push_back(std::unique_ptr<school_item> (temp));

                if(temp->getAssignmentNumber() < 1 || temp->getAssignmentNumber() > 3) break;
            }
        }
    }
    return true;
}

static int getrealtasknofrassignmentno(int assignmentnumber) {
    switch (assignmentnumber) {
    case person::Highlights:  return 0;
    case person::No1:      return 1;
    case person::No2:      return 2;
    case person::No3:      return 3;
    case person::WtReader: return 10;
    }
    abort();
}

QStandardItemModel *school::getStudents(int assignmentnumber, QDate date, int classnumber, QString selected, bool onlybrothers)
{
    QDate meetingdate = date.addDays(QVariant(sql->getSetting("school_day")).toInt()-1);

    int realtaskno = getrealtasknofrassignmentno(assignmentnumber);

    //    bool assignmentDateCol = !onlybrothers && (realtaskno == 2 || realtaskno == 3);

    QString query; // we run the query 3 times to get past some unknown inefficiency with QSqlQuery
    sql_items sMain;

    // main data set
    query.append("SELECT p.id AS id,"
                 "p.firstname || ' ' || p.lastname AS fullname,"
                 "p.date AS date1,"
                 "p.number AS number1,"
                 "p.classnumber AS class1,"
                 "p.student_id AS student1,"
                 "s2.date AS date2,"
                 "s2.number AS number2,"
                 "s2.classnumber AS class2, "
                 "s3.date AS date3,"
                 "s3.number AS number3,"
                 "s3.classnumber AS class3, "
                 "ABS(CASE WHEN p.date IS NULL THEN 100000 ELSE "
                 "CASE WHEN julianday(p.date) < julianday('%2') THEN "
                 "0 ELSE .5 END + "
                 "julianday(p.date) - julianday('%2') end) AS woffset, "
                 "(SELECT COUNT(*) FROM school WHERE (student_id = p.id OR assistant_id = p.id) AND date = '%1') AS used,"
                 "(SELECT COUNT(*) FROM unavailables WHERE person_id = p.id AND start_date <='%2' AND end_date >= '%2' AND active) AS away, "
		 "mfm.result multiused_family, "
		 "mmp.result multiused_person "
                 "FROM personschoolhistory_mt AS p "
                 "LEFT JOIN (SELECT * "
                 "FROM personschoolhistory_mt "
                 "WHERE id = student_id AND volunteer_id = -1 AND number = %3 "
                 "GROUP BY student_id "
                 "HAVING MIN("
                 "ABS(CASE WHEN date IS NULL THEN date ELSE "
                 "CASE WHEN julianday(date) < julianday('%2') THEN "
                 "0 ELSE .5 END + "
                 "julianday(date) - julianday('%2') END)"
                 ")) AS s2 "
                 "ON (p.id = s2.student_id AND s2.volunteer_id = -1) AND s2.number = %3 "
                 "LEFT JOIN (SELECT * "
                 "FROM personschoolhistory_mt "
                 "WHERE (id = student_id AND volunteer_id = -1) AND (number = 2 OR number = 3) "
                 "GROUP BY student_id "
                 "HAVING MIN("
                 "ABS(CASE WHEN date IS NULL THEN date ELSE "
                 "CASE WHEN julianday(date) < julianday('%2') THEN "
                 "0 ELSE .5 END + "
                 "julianday(date) - julianday('%2') END)"
                 ")) AS s3 "
                 "ON (p.id = s3.student_id AND s3.volunteer_id = -1) AND (s3.number = 2 OR s3.number = 3) "

		 "left join (select distinct 1 result,p.date,f2.person_id "
		 "from personmidweek p, families f,families f2 WHERE mtype = 'TMS' and f.person_id = p.person_id and f.family_head <> -1 and f.family_head = f2.family_head"
		 ") mfm "
         "ON (p.id = mfm.person_id and mfm.date = '%1')"
		 "left join (select distinct 1 result,person_id,date "
		 "from (select distinct person_id,date,mtype "
		 "from personmidweek) "
		 "group by date,person_id ) mmp "
         "ON (p.id = mmp.person_id and mmp.date = '%1')"
                 "WHERE p.active AND p.usefor & %4 AND NOT (p.usefor & %5) AND p.congregation_id = %6 "
                 "%7 "
                 "GROUP BY p.id "
                 "HAVING MIN(woffset) "
                 "ORDER BY woffset DESC");

    QString options = "";
    if (onlybrothers){
        options.append("AND p.gender = 'B'");
        // servant option can be as true/false string or 1/0 integer value
        options.append(QVariant(sql->getSetting("school_no3option")).toBool() ? " AND (p.servant or p.servant = 'true') ": " ");
    }
    if (assignmentnumber > 0){
        options.append("AND NOT (p.usefor & " + QVariant( classnumber == 1 ? person::SchoolAux : person::SchoolMain ).toString() + ")");
    }
    query.replace("%1", date.toString(Qt::ISODate), Qt::CaseSensitive);
    query.replace("%2", meetingdate.toString(Qt::ISODate), Qt::CaseSensitive);
    query.replace("%3", QVariant(realtaskno).toString(), Qt::CaseSensitive);
    query.replace("%4", QVariant(assignmentnumber).toString(), Qt::CaseSensitive);
    query.replace("%5", QVariant(person::IsBreak).toString(), Qt::CaseSensitive);
    query.replace("%6", sql->getSetting("congregation_id"), Qt::CaseSensitive);
    query.replace("%7", options, Qt::CaseInsensitive);
    sMain = sql->selectSql(query);


    // merge all three into the model
    QStandardItemModel *table = new QStandardItemModel();

    table->setHorizontalHeaderItem(1,new QStandardItem(assignmentnumber == person::Assistant ? QObject::tr("Assistant") : QObject::tr("Student") ));
    table->setHorizontalHeaderItem(2,new QStandardItem(QObject::tr("All")));
    QString s = assignmentnumber == person::Highlights ? QObject::tr( "Highlights") :
      assignmentnumber == person::No1 ? QObject::tr( "#1"):
      assignmentnumber == person::No2 ? QObject::tr( "#2"):
      assignmentnumber == person::No3 ? QObject::tr( "#3"):
      assignmentnumber == person::WtReader ? QObject::tr( "WT Reader"):
      assignmentnumber == person::Assistant ? QObject::tr("Assistant"):
      QObject::tr( "Selected");

    table->setHorizontalHeaderItem(3,new QStandardItem(s));

    table->setHorizontalHeaderItem(4,new QStandardItem(getShortAssignmentName(2) + " / " + getShortAssignmentName(3)));
    table->setHorizontalHeaderItem(5,new QStandardItem(""));

    table->setRowCount(sMain.size()+1);
    QStandardItem *emptyitem = new QStandardItem();
    emptyitem->setText("");
    table->setItem(0,0,emptyitem);
    for(unsigned int i = 1;i<(sMain.size()+1);i++){
        sql_item hMain = sMain[i-1];
        sql_item h2 = sMain[i-1];
        sql_item h3 = sMain[i-1];

        table->setItem(i,0,new QStandardItem(hMain.value("p.id").toString()));
        QStandardItem *nameitem = new QStandardItem;
        nameitem->setText( hMain.value("fullname").toString() );
        // bolded font for selected text
        if(nameitem->text() == selected){
            QFont boldfont(nameitem->font().family(),
                           nameitem->font().pointSize(),
                           QFont::Bold);
            nameitem->setFont(boldfont);
        }
        table->setItem(i,0, new QStandardItem( hMain.value("id").toString() ));
        table->setItem(i,1,nameitem);
        table->setData(table->index(i,1,QModelIndex()),nameitem->text(),Qt::UserRole);

        table->setItem(i,2, new QStandardItem(
                           hMain.value("date1").toString() == "" ? "" :
                                                               QString("%1 %2 (%3)").arg(
                                                                   QDate::fromString(hMain.value("date1").toString(),Qt::ISODate).toString(Qt::DefaultLocaleShortDate),
                                                                   getShortAssignmentName(hMain.value("number1").toInt(), hMain.value("id").toInt() != hMain.value("student1").toInt()),
                                                                   hMain.value("class1").toString())));
        table->setData(table->index(i,2,QModelIndex()),hMain.value("date1").toString(),Qt::UserRole);

        table->setItem(i,3, new QStandardItem(
                           h2.value("date2").toString() == "" ? "" :
                                                               QString("%1 %2 (%3)").arg(
                                                                   QDate::fromString(h2.value("date2").toString(),Qt::ISODate).toString(Qt::DefaultLocaleShortDate),
                                                                   getShortAssignmentName(h2.value("number2").toInt()),
                                                                   h2.value("class2").toString())));
        table->setData(table->index(i,3,QModelIndex()),h2.value("date2").toString(),Qt::UserRole);

        table->setItem(i,4, new QStandardItem(
                           h3.value("date3").toString() == "" ? "" :
                                                               QString("%1 %2 (%3)").arg(
                                                                   QDate::fromString(h3.value("date3").toString(),Qt::ISODate).toString(Qt::DefaultLocaleShortDate),
                                                                   getShortAssignmentName(h3.value("number3").toInt()),
                                                                   h3.value("class3").toString())));
        table->setData(table->index(i,4,QModelIndex()),h3.value("date3").toString(),Qt::UserRole);

        table->setItem(i,5, new QStandardItem(""));
        int icon_width = 18;
        if (hMain.value("used").toInt() > 0){
             table->item(i,5)->setData(QImage(":/icons/warning-already-assigned.png")
                                       .scaledToWidth(icon_width),Qt::DecorationRole);
             table->item(i,5)->setToolTip(QObject::tr("Assignment already has been made"));
        } else if (hMain.value("multiused_person").toInt() > 0){
             table->item(i,5)->setData(QImage(":/icons/warning-other-meeting-parts.png")
                                       .scaledToWidth(icon_width),Qt::DecorationRole);
             table->item(i,5)->setToolTip(QObject::tr("Assigned student has other meeting parts"));
        } else if(hMain.value("away").toInt() > 0){
             table->item(i,5)->setData(QImage(":/icons/warning-unavailable.png")
                                       .scaledToWidth(icon_width),Qt::DecorationRole);
             table->item(i,5)->setToolTip(QObject::tr("Student unavailable"));
        } else if (hMain.value("multiused_family").toInt()) {
             table->item(i,5)->setData(QImage(":/icons/warning-family-member.png")
                                       .scaledToWidth(icon_width),Qt::DecorationRole);
             table->item(i,5)->setToolTip(QObject::tr("Student part of family"));
        }
        table->item(i,2)->setFlags(Qt::NoItemFlags);
        table->item(i,3)->setFlags(Qt::NoItemFlags);
        table->item(i,4)->setFlags(Qt::NoItemFlags);
        table->item(i,5)->setFlags(Qt::NoItemFlags);
    }
    table->setSortRole(Qt::UserRole);
    return table;
}


QStandardItemModel *school::getAssistant(int assignmentnumber, QDate date,int studentid, int classnumber, QString selected,
                                         bool brother)
{
    QDate meetingdate = date.addDays(this->getSchoolDay()-1);
    QString query;
    sql_items sMain;

    // main data set
    query.append("SELECT p.id AS id,p.firstname || ' ' || p.lastname AS fullname, "
                 "p.date AS date1, "
                 "p.student_id AS student1, "
                 "p.classnumber AS class1, "
                 "p.number AS number1, "
                 "s2.date AS date2, "
                 "s2.classnumber AS class2, "
                 "s3.date AS date3, "
                 "s3.classnumber AS class3, "
                 "s3.number AS number3, "
                 "s4.date AS date4, "
                 "s4.number AS number4, "
                 "s4.classnumber AS class4, "
                 "(SELECT COUNT(*) FROM school WHERE (student_id = p.id OR assistant_id = p.id) AND date = '%1') AS used,"
                 "(SELECT COUNT(*) FROM unavailables WHERE person_id = p.id AND start_date <='%2' AND end_date >= '%2' AND active) AS away,"
		 "mfm.result multiused_family, "		 
                 // family
                 "p.gender AS gender,"
                 "f.family_head AS family, "
		 "MIN(abs(case when p.date is null then 100000 else case when julianday(p.date) < julianday('%2') then "
		 "0 else .5 end + julianday(p.date) - julianday('%2') end)) woffset from personschoolhistory_mt as p "
                 //
                 "LEFT JOIN (SELECT p.date,p.classnumber,p.number,p.assistant_id "
		 "FROM personschoolhistory_mt p "
		 "WHERE p.id = p.assistant_id "
		 "GROUP BY p.assistant_id  "
		 "HAVING MIN(ABS(CASE WHEN julianday(p.date) < julianday('%2') THEN 0 ELSE .5 END + "
		 "julianday(p.date) - julianday('%2')))"
		 ") AS s2 ON p.id = s2.assistant_id "
                 "LEFT JOIN (SELECT p.date,p.classnumber,p.number,p.assistant_id "
		 "FROM personschoolhistory_mt p WHERE student_id = %6 AND volunteer_id = -1 AND "
		 "id = assistant_id AND assistant_id <> student_id "
		 "GROUP BY p.assistant_id "
		 "HAVING MIN(abs(case when julianday(p.date) < julianday('%2') then 0 else .5 end + "
		 "julianday(p.date) - julianday('%2'))) "
		 ") AS s3 ON p.id = s3.assistant_id "
                 "LEFT JOIN (SELECT p.date,p.classnumber,p.number,p.volunteer_id,p.student_id "
		 "FROM personschoolhistory_mt p WHERE assistant_id=%6 AND "
		 "student_id <> assistant_id "
		 "GROUP BY p.student_id "
		 "HAVING MIN(abs(case when julianday(p.date) < julianday('%2') then 0 else .5 end + "
		 "julianday(p.date) - julianday('%2'))) "
		 ") AS s4 ON (p.id = s4.student_id AND s4.volunteer_id = -1) "
                 // assistant for brother
                 "LEFT JOIN families AS f ON p.id = f.person_id "


		 "left join (select distinct 1 result,p.date,f2.person_id "
		 "from personmidweek p, families f,families f2 WHERE mtype = 'TMS' and f.person_id = p.person_id and f.family_head <> -1 and f.family_head = f2.family_head"
		 ") mfm "
		 "ON (p.student_id = mfm.person_id and mfm.date = '%1')"
                 //
                 "WHERE p.active AND (%3) AND NOT (p.usefor & %4) AND p.congregation_id = %5 "
                 "%7 "
                 "GROUP BY p.id ORDER BY woffset desc");
    query.replace("%1", date.toString(Qt::ISODate), Qt::CaseSensitive);
    query.replace("%2", meetingdate.toString(Qt::ISODate), Qt::CaseSensitive);
    query.replace("%3", brother ? QString("(p.usefor & %1 AND family = (SELECT family_head FROM families LEFT JOIN persons ON families.person_id = persons.id WHERE person_id = %2)) OR p.gender = 'B'").arg(QVariant(person::Assistant).toString(),QVariant(studentid).toString()) :
                                  QString("p.usefor & %1").arg(QVariant(person::Assistant).toString()) , Qt::CaseSensitive);
    query.replace("%4", QVariant(person::IsBreak).toString(), Qt::CaseSensitive);
    query.replace("%5", sql->getSetting("congregation_id"), Qt::CaseSensitive);
    query.replace("%6", QVariant(studentid).toString(), Qt::CaseSensitive);
    query.replace("%7", QString("AND NOT (p.usefor & %1)").arg(QVariant( classnumber == 1 ? person::SchoolAux : person::SchoolMain ).toString()), Qt::CaseSensitive);
    sMain = sql->selectSql(query);


    QStandardItemModel *table = new QStandardItemModel();

    table->setHorizontalHeaderItem(1,new QStandardItem(QObject::tr("Assistant")));
    table->setHorizontalHeaderItem(2,new QStandardItem(QObject::tr("All")));
    QString s = assignmentnumber == person::Highlights ? QObject::tr( "Highlights") :
      assignmentnumber == person::No1 ? QObject::tr( "#1"):
      assignmentnumber == person::No2 ? QObject::tr( "#2"):
      assignmentnumber == person::No3 ? QObject::tr( "#3"):
      assignmentnumber == person::WtReader ? QObject::tr( "WT Reader"):
      assignmentnumber == person::Assistant ? QObject::tr( "Assistant"):
      QObject::tr( "Selected");
    
    table->setHorizontalHeaderItem(3,new QStandardItem(s));
    table->setHorizontalHeaderItem(4,new QStandardItem(QObject::tr("Recently together")));
    table->setHorizontalHeaderItem(5,new QStandardItem(""));

    table->setRowCount(sMain.size()+1);
    table->setItem(0,0,new QStandardItem(""));
    for(unsigned int i = 1; i < (sMain.size() + 1); i++){
        sql_item item = sMain[i-1];

        // person id
        table->setItem(i,0,new QStandardItem(item.value("id").toString()));
        // fullname
        QStandardItem *nameitem = new QStandardItem( item.value("fullname").toString() );
        // bolded font for selected text
        if(nameitem->text() == selected){
            QFont boldfont(nameitem->font().family(),
                           nameitem->font().pointSize(),
                           QFont::Bold);
            nameitem->setFont(boldfont);
        }
        table->setItem(i,1, nameitem);
        table->setData(table->index(i,1,QModelIndex()),nameitem->text(),Qt::UserRole);

        table->setItem(i,2,new QStandardItem( item.value("date1").toString() == "" ? "" :
                                                                                     QString("%1 %2 (%3)").arg(QDate::fromString(item.value("date1").toString(),Qt::ISODate).toString(Qt::DefaultLocaleShortDate),
                                                                                                                getShortAssignmentName(item.value("number1").toInt(),item.value("student1").toInt() != item.value("id").toInt()),
                                                                                                                item.value("class1").toString()) ));
        table->setData(table->index(i,2,QModelIndex()),item.value("date1").toString(),Qt::UserRole);

        table->setItem(i,3,new QStandardItem( item.value("date2").toString() == "" ? "" :
                                                                                     QString("%1 A (%2)").arg(
                                                                                         QDate::fromString(item.value("date2").toString(),Qt::ISODate).toString(Qt::DefaultLocaleShortDate),
                                                                                         item.value("class2").toString()) ));
        table->setData(table->index(i,3,QModelIndex()),item.value("date2").toString(),Qt::UserRole);

        QDate d1 = item.value("date3").toDate();
        QDate d2 = item.value("date4").toDate();

        if (d1 < d2){
            // swapped roles
            table->setItem(i,4,new QStandardItem(QString("%1 #%2 (%3)").arg( d2.toString(Qt::DefaultLocaleShortDate), item.value("number4").toString(), item.value("class4").toString() )));
            table->setData(table->index(i,4,QModelIndex()),item.value("date4").toString(),Qt::UserRole);
        }else{
            table->setItem(i,4,new QStandardItem( !d1.isValid() ? "" :
                                                                  QString("%1 A (%2)").arg( d1.toString(Qt::DefaultLocaleShortDate), item.value("class3").toString() )));
            table->setData(table->index(i,4,QModelIndex()),item.value("date3").toString(),Qt::UserRole);
        }
        table->setItem(i,5,new QStandardItem(""));
        int icon_width = 18;

        if (item.value("used").toInt() > 0){
            table->item(i,5)->setData(QImage(":/icons/warning-already-assigned.png")
                                      .scaledToWidth(icon_width),Qt::DecorationRole);
            table->item(i,5)->setToolTip(QObject::tr("Assignment already has been made"));
        } else if (item.value("multiused_person").toInt() > 0){
            table->item(i,5)->setData(QImage(":/icons/warning-other-meeting-parts.png")
                                      .scaledToWidth(icon_width),Qt::DecorationRole);
            table->item(i,5)->setToolTip(QObject::tr("Assigned student has other meeting parts"));
        } else if(item.value("away").toInt() > 0){
            table->item(i,5)->setData(QImage(":/icons/warning-unavailable.png")
                                      .scaledToWidth(icon_width),Qt::DecorationRole);
            table->item(i,5)->setToolTip(QObject::tr("Student unavailable"));
        } else if (item.value("multiused_family").toInt()) {
            table->item(i,5)->setData(QImage(":/icons/warning-family-member.png")
                                      .scaledToWidth(icon_width),Qt::DecorationRole);
            table->item(i,5)->setToolTip(QObject::tr("Family member used in another TMS assignment"));
        }

        table->item(i,2)->setFlags(Qt::NoItemFlags);
        table->item(i,3)->setFlags(Qt::NoItemFlags);
        table->item(i,4)->setFlags(Qt::NoItemFlags);
        table->item(i,5)->setFlags(Qt::NoItemFlags);

    }
    table->setSortRole(Qt::UserRole);
    return table;
}

QList<schoolStudy *> school::getStudies()
{
    QList<schoolStudy *> all;
    sql_items query;

    // get all studies
    query = sql->selectSql("SELECT * FROM studies WHERE active ORDER BY study_number");

    for(unsigned int i = 0;i<query.size();i++){
        schoolStudy *s = new schoolStudy();
        s->setId(query[i].value("id").toInt());
        s->setNumber(query[i].value("study_number").toInt());
        s->setName(query[i].value("study_name").toString());
        s->setReading(query[i].value("reading").toBool());
        s->setDemonstration(query[i].value("demonstration").toBool());
        s->setDiscourse(query[i].value("discource").toBool());

        all.push_back(s);
    }

    return all;
}

QList<schoolStudentStudy *> school::getStudentStudies(int personid, int studynumber)
{
    QList<schoolStudentStudy *> all;
    sql_items query;

    // get person's studies
    QString querystring = "SELECT student_studies.id AS ss_id, studies.*, student_studies.student_id, "
            "student_studies.start_date, student_studies.end_date, student_studies.exercises "
            "FROM studies "
            "LEFT OUTER JOIN student_studies "
            "ON studies.study_number = student_studies.study_number AND "
            "student_studies.student_id = " + QVariant(personid).toString() +
            " AND student_studies.active";
    querystring.append(" WHERE studies.active");
    if (studynumber > 0){
        querystring.append(" AND studies.study_number = " + QVariant(studynumber).toString());
    }
    querystring.append(" ORDER BY study_number");
    query = sql->selectSql(querystring);

    for(unsigned int i = 0;i<query.size();i++){
        schoolStudentStudy *s = new schoolStudentStudy();
        s->setName(query[i].value("study_name").toString());
        s->setReading(query[i].value("reading").toBool());
        s->setDemonstration(query[i].value("demonstration").toBool());
        s->setDiscourse(query[i].value("discource").toBool());
        s->setStartdate(query[i].value("start_date").toDate());
        s->setEndDate(query[i].value("end_date").toDate());
        s->setExercise(query[i].value("exercises").toBool());

        s->setId(query[i].value("ss_id").toInt());
        s->setNumber(query[i].value("study_number").toInt());
        s->setStudentId(personid);

        all.push_back(s);
    }

    return all;
}

schoolStudentStudy * school::addStudentStudy(int personid, schoolStudentStudy *st)
{
    sql_item s;
    s.insert("student_id", personid);
    s.insert("study_number", st->getNumber());
    s.insert("start_date",st->getStartdate());
    s.insert("end_date",st->getEndDate());
    s.insert("exercises",st->getExercise());

    // insert into database
    int id = sql->insertSql("student_studies",&s,"id");
    // fill id value to object
    st->setId(id);

    return st;
}

schoolStudentStudy * school::getActiveStudyPoint(int personid, school_item::AssignmentType type, bool createnew, QDate meetingdDate)
{
    QString stype = "";
    switch (type)
    {
    case school_item::Reading:
        stype = "reading";
        break;
    case school_item::Demonstration:
        stype = "demonstration";
        break;
    case school_item::Discource:
        stype = "discource";
        break;
    default:
        stype = "reading";
        break;
    }

    schoolStudentStudy *study = new schoolStudentStudy();
    study->setStudentId(personid);

    sql_items s;
    QDate firstdayofweek(meetingdDate.addDays(this->getSchoolDay()-1));

    if (meetingdDate.year() > 2018) {

        s = sql->selectSql("select s.id, sch.study_number, s.study_name "
                           "from lmm_schedule sch "
                           "left join lmm_studies s  on s.study_number = sch.study_number and s.lang = (select value from settings where name = 'theocbase_language') "
                           "where sch.date = '" + firstdayofweek.toString(Qt::ISODate) + "'");
        if(s.empty()) {
            study->setNumber(1);
            study->setStartdate(QDate::currentDate());
            study->setExercise(true);
        } else {
            study->setId(s[0].value("id").toInt());
            study->setNumber(s[0].value("study_number").toInt());
            study->setName(s[0].value("study_name").toString());
            study->setStartdate(firstdayofweek);
            study->setEndDate(firstdayofweek.addDays(7));
            study->setExercise(true);
        }
    } else {

        // student_studies.id AS ss_id
        s = sql->selectSql("SELECT student_studies.id AS ss_id, student_studies.*, studies.* FROM student_studies INNER JOIN studies ON "
                          "student_studies.study_number = studies.study_number AND studies.active WHERE student_id = " +
                           QVariant(personid).toString() + " AND " + stype + " = 1 " +
                           (meetingdDate.isValid() ? "AND start_date <= '" + meetingdDate.addDays(this->getSchoolDay()-1).toString(Qt::ISODate) + "' " : "") +
                           "AND (end_date IS NULL OR end_date = '') AND student_studies.active ORDER BY study_number DESC");
        if(s.empty()){
            // get last study point
            s = sql->selectSql("SELECT * FROM studies WHERE study_number > (SELECT studies.study_number FROM student_studies INNER JOIN studies ON "
                              "student_studies.study_number = studies.study_number AND studies.active  WHERE student_id = " +
                              QVariant(personid).toString() + " AND " + stype + " = 1 AND student_studies.active ORDER BY end_date DESC LIMIT 1) AND " +
                              stype + " = 1 LIMIT 1");
            sql_item si;
            si.insert("student_id", personid);
            si.insert("start_date", QDate::currentDate());
            si.insert("exercises", false);

            study->setStartdate(QDate::currentDate());
            study->setExercise(false);
            if(s.empty()){
                // ==> 1
                study->setNumber(1);
                s = sql->selectSql("SELECT study_name FROM studies WHERE study_number = 1 AND active");
                if (!s.empty()) study->setName(s[0].value("study_name").toString());
                si.insert("study_number",1);
            }else{
                study->setNumber(s[0].value("study_number").toInt());
                study->setName(s[0].value("study_name").toString());
                si.insert("study_number", s[0].value("study_number"));
            }
            if (createnew){
                // insert new for student_studies -table
                study->setId(sql->insertSql("student_studies",&si,"id"));
            }
        }else{
            study->setId(s[0].value("ss_id").toInt());
            study->setNumber(s[0].value("study_number").toInt());
            study->setName(s[0].value("study_name").toString());
            study->setStartdate(s[0].value("start_date").toDate());
            study->setEndDate(s[0].value("end_date").toDate());
            study->setExercise(s[0].value("exercises").toBool());
        }
    }
    return study;
}

schoolStudentStudy *school::getStudyUsed(int personid, QDate firstdayofweek, school_item::AssignmentType type)
{
    schoolStudentStudy *study = nullptr;
    sql_items s;

    if (firstdayofweek.year() > 2018) {
        study = new schoolStudentStudy();
        s = sql->selectSql("select s.id, sch.study_number, s.study_name "
                           "from lmm_schedule sch "
                           "left join lmm_studies s  on s.study_number = sch.study_number and s.lang = (select value from settings where name = 'theocbase_language') "
                           "where sch.date = '" + firstdayofweek.toString(Qt::ISODate) + "'");
        if(!s.empty()) {
            study->setName(s[0].value("study_name").toString());
            study->setReading(true);
            study->setDemonstration(true);
            study->setDiscourse(true);

            study->setStartdate(firstdayofweek);
            study->setEndDate(firstdayofweek.addDays(7));
            study->setExercise(true);

            study->setId(s[0].value("id").toInt());
            study->setNumber(s[0].value("study_number").toInt());
            study->setStudentId(personid);
        }
    } else {

        QString stype;
        switch (type)
        {
        case school_item::Reading:
            stype = "reading";
            break;
        case school_item::Demonstration:
            stype = "demonstration";
            break;
        case school_item::Discource:
            stype = "discource";
            break;
        default:
            return study;
        }
        s = sql->selectSql("SELECT student_studies.id AS ss_id,student_studies.start_date,student_studies.end_date,"
                           "student_studies.exercises,studies.* FROM student_studies INNER JOIN studies ON "
                           "student_studies.study_number = studies.study_number AND studies.active WHERE student_id = " +
                           QVariant(personid).toString() + " AND " + stype + " = 1 AND "
                           "(start_date <= '" + firstdayofweek.addDays(4).toString(Qt::ISODate) +
                           "' AND (end_date IS NULL OR end_date = '' OR end_date >= '" + firstdayofweek.toString(Qt::ISODate) + "')) "
                           "AND student_studies.active "
                           "ORDER BY exercises DESC,IFNULL(end_date,'') ASC, study_number DESC");
        if(!s.empty()) {
            study = new schoolStudentStudy();
            study->setName(s[0].value("study_name").toString());
            study->setReading(s[0].value("reading").toBool());
            study->setDemonstration(s[0].value("demonstration").toBool());
            study->setDiscourse(s[0].value("discource").toBool());

            study->setStartdate(s[0].value("start_date").toDate());
            study->setEndDate(s[0].value("end_date").toDate());
            study->setExercise(s[0].value("exercises").toBool());

            study->setId(s[0].value("ss_id").toInt());
            study->setNumber(s[0].value("study_number").toInt());
            study->setStudentId(personid);
        }
    }
    return study;
}

int school::getTheme(QDate date, int number){

    int ret = -1;
    sql_items e = sql->selectSql("school_schedule WHERE date='" +
                                            date.toString(Qt::ISODate) +
                                            "' AND number='" +
                                            QVariant(number).toString() + "'","","","");
    if (!e.empty()){
        ret = e[0].value("id").toInt();
    }
    return ret;
}

int school::addTheme(QDate date, int number, QString theme, QString source, bool onlybrothers)
{
    int ret = -1;
    sql_item sp;
    sp.insert("date",date.toString(Qt::ISODate));
    sp.insert("number",QVariant(number).toString());
    sp.insert("theme",theme);
    sp.insert("source",source);
    sp.insert("onlybrothers",onlybrothers);
    ret = sql->insertSql("school_schedule",&sp,"id");
    return ret;
}

int school::getClassesQty()
{
    // Get quantity of classes. Value is stored to settings table in the database
    if (m_ClassesQty == -1){
        QString value = sql->getSetting("schools_qty");
        m_ClassesQty = ((value == "") ? 1 : QVariant(value).toInt());
    }

    return m_ClassesQty;
}

void school::setClassesQty(int value)
{
    // Save quantity of classes.
    m_ClassesQty = value;
    // Save value to settings table in the database
    sql->saveSetting("schools_qty",QVariant(value).toString());
}

int school::getSchoolDay()
{
    // Get school day. Value is stored to settings table in the database.
    if (m_schoolday == -1){
        m_schoolday = QVariant(sql->getSetting("school_day")).toInt();
    }
    return m_schoolday;
}

void school::setSchoolDay(int value)
{
    // Save schoolday
    m_schoolday = value;
    // Save value to settings table in the database
    sql->saveSetting("school_day",QVariant(m_schoolday).toString());
}

bool school::getNoSchool(QDate date, int classno)
{
    sql_items noschool = sql->selectSql("SELECT * FROM school_break WHERE date = '" +
                                                  date.toString(Qt::ISODate) + "' AND classnumber = '" +
                                                  QVariant(classno).toString() + "'");
    return (!noschool.empty());
}

void school::setNoSchool(QDate date, int classno, bool noschool)
{
    if (noschool){
        sql_item si;
        si.insert("date",date);
        si.insert("classnumber",classno);
        sql->insertSql("school_break",&si,"id");
        sql->removeSql("school","date = '" +
                      date.toString(Qt::ISODate) +
                      "' AND classnumber = '" + QVariant(classno).toString() + "'");
    }else{
        qDebug() << "no checked";
        sql->removeSql("school_break","date = '" +
                      date.toString(Qt::ISODate) +
                      "' AND classnumber = '" + QVariant(classno).toString() + "'");
    }
}

QString school::getShortAssignmentName(int number, bool assistant)
{
    if (assistant){
        return "A";
    }else{
        switch(number){
        case 0:
            return QObject::tr("H","abbreviation of the 'highlights'");
            break;
        case 1:
        case 2:
        case 3:
            return "#" + QVariant(number).toString();
            break;
        case 10:
            return QObject::tr("R","abbreviation of the 'reader'");
            break;
        default:
            return QVariant(number).toString();
            break;
        }
    }
}

QList<school_setting *> school::settingsQuery(QString sqlQuery)
{
    QList<school_setting *> all;
    // Do query from
    sql_items s = sql->selectSql(sqlQuery);
    if (!s.empty()){
        foreach (sql_item sitem, s) {
            school_setting *setting = new school_setting();
            setting->setName(sitem.value("name").toString());
            setting->setNumber(sitem.value("number").toInt());
            setting->setOnlyBrothers(sitem.value("brothers").toBool());
            all.append(setting);
        }
    }
    return all;
}

QList<school_setting *> school::getAllSettings()
{
    return settingsQuery("SELECT * FROM school_settings GROUP BY number");
}

QList<school_setting *> school::getAllSettings(person::Gender g)
{
    return settingsQuery(QString("SELECT * FROM school_settings WHERE %1brothers GROUP BY number").arg(
                             g == person::Male ? "" : "NOT "));
}

/**
 * @brief school::deleteAllSettings Delete all school settings from database
 * @return true or false
 */
bool school::deleteAllSettings()
{
    // Get all setting
    sql_items s = sql->selectSql("school_settings","","","");
    if(!s.empty()){
        // loop each setting
        foreach (sql_item st, s){
            // delete row (return false value if failed)
            if (!sql->removeSql("school_settings","id = " + st.value("id").toString()))
                return false;
        }
    }
    return true;
}
