#ifndef MIDWEEKMEETINGAVAILABILITYCHECKER_H
#define MIDWEEKMEETINGAVAILABILITYCHECKER_H

#ifndef AVAILABILITYCHECKER_H
    #include "availabilitychecker.h"
#endif

#include "../lmm_schedule.h"

namespace tbAvailability
{

// Use MidWeekMeetingAvailabilityChecker to gather information about availability of
// bros and sisters for parts on a meeting.
//
// Example Usage:
//
//  MidWeekMeetingAvailabilityChecker checker(meetingDate, weekCommencingDate);
//  Availability a = checker.Get();
//
//  for(int n=0; n<a.Count(); ++n)
//  {
//      AvailabilityItem *ai = a.GetItem(index);
//      qDebug() << ai->DisplayName;
//      qDebug() << ai->OnHoliday? "On holiday" : "";
//      qDebug() << ai->HasRole(person::LMM_Chairman)? "Can be chairman" : "Can't be chairman";
//      qDebug() << "Last = " << GetDateLastAssigned(person::LMM_LivingTalks).toString(Qt::SystemLocaleShortDate);
//      qDebug() << ai->HasAssignmentsOtherThan(person::LMM_Chairman, weekCommencingDate)? "Yes" : "No";
//  }
//

class MidWeekMeetingAvailabilityChecker : public AvailabilityChecker
{
private:
    virtual QString GenerateAssignedPersonsSql(QString dataSourceId);
    virtual void PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons);

    person::UseFor TalkIdToAssignment(int talkId);
    bool IsStudentAssignment(person::UseFor assignment);

public:
    MidWeekMeetingAvailabilityChecker(const QDate &meetingDate, const QDate &weekCommencingDate);
};

class UnknownTalkIException : public std::invalid_argument
{
public:
    UnknownTalkIException(int talkId);
};

} //namespace tbAvailability

#endif // MIDWEEKMEETINGAVAILABILITYCHECKER_H
