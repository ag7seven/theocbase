/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TODOMODEL_H
#define TODOMODEL_H

#include <QAbstractTableModel>
#include "todo.h"

class TodoModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
public:
    explicit TodoModel(QObject *parent = nullptr);
    ~TodoModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    Q_INVOKABLE void loadList();
    Q_INVOKABLE int addRow(const bool inComing);
    Q_INVOKABLE void removeRow(const int rowIndex);
    Q_INVOKABLE void saveRow(const int rowIndex);
    Q_INVOKABLE void editRow(const int row, const QDate date, const QString speaker, const QString congregation, const QString notes);
    Q_INVOKABLE QVariantMap get(int row) const;
    Q_INVOKABLE QString moveToSchedule(const int row);

signals:
    void modelChanged();

private:
    enum Roles {        
        IdRole = Qt::UserRole,
        CongregationRole,
        SpeakerRole,
        DateRole,
        ThemeRole,
        NotesRole,
        IsIncomingRole
    };
    QList<todo*> _list;
};

#endif // TODOMODEL_H
