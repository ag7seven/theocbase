/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "schoolresult.h"
#include "ui_schoolresult.h"

schoolresult::schoolresult(school_item &prog, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::schoolresult),
    currentprog(prog)
{
    ui->setupUi(this);
    sql = &Singleton<sql_class>::Instance();

    initInformation();
}

schoolresult::~schoolresult()
{
    delete ui;
}

void schoolresult::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void schoolresult::initInformation()
{
    school sclass;

    QDate currentdate = currentprog.getDate();

    QString tasknr = "";
    if (currentprog.getAssignmentNumber() == 0){
        tasknr = tr("Bible highlights");
    }else if(currentprog.getAssignmentNumber() == 10){
        tasknr = tr("reading");
    }else{
        tasknr = QVariant(currentprog.getAssignmentNumber()).toString();
    }
    this->setWindowTitle(tr("Assignment result") + " " + tasknr);
    ui->comboBoxStudyEnd->setEnabled(false);
    ui->comboBoxStudyStart->setEnabled(false);    
    ui->pushButton->setEnabled(true);
    ui->groupBox->setEnabled(false);

    ui->checkBoxDone->setChecked(currentprog.getDone());
    ui->plainTextEdit->setPlainText(currentprog.getNote());
    ui->lineEditTime->setText(currentprog.getTiming());
    qDebug() << "timing" << currentprog.getTiming() << currentprog.getId();
    ui->labelName->setText(currentprog.getStudent()->fullname());

    cpersons cp;
    allusers = cp.getAllPersons(0);
    ui->comboBoxUsers->clear();
    ui->comboBoxUsers->addItem("");

    // Set volunteer ComboBox
    m_volunteerIds.clear();
    foreach(person *p, allusers){

        switch(currentprog.getAssignmentNumber()){
        case 0:
            // Bible highlights - Show only limited brothers
            if (!(p->usefor() & person::Highlights)) continue;
            break;
        case 1:
            // No 1 - Do not show sisters
            if (p->gender() != person::Male) continue;
            break;
        case 10:
            // Review reader - Show only wt readers
            if (!(p->usefor() & person::WtReader)) continue;
            break;
        default:
            break;
        }

        m_volunteerIds.append(p->id());
        ui->comboBoxUsers->addItem(p->fullname());
    }

    if (currentprog.getVolunteer() && m_volunteerIds.contains(currentprog.getVolunteer()->id())){
        ui->comboBoxUsers->setEnabled(true);
        ui->checkBoxVolunteer->setChecked(true);
        ui->comboBoxUsers->setCurrentIndex(m_volunteerIds.indexOf(currentprog.getVolunteer()->id())+1);
    }

    ui->checkBoxVolunteer->setEnabled(ui->checkBoxDone->isChecked());

    currentStudy = 0;
    nextStudy = 0;

    if(currentprog.getAssignmentNumber() > 0 && currentprog.getAssignmentNumber() < 10 && !currentprog.getVolunteer()){
        ui->groupBox->setEnabled(true);

        // studies
        if (currentprog.getDone()){
            currentStudy = sclass.getStudyUsed(currentprog.getStudent()->id(),
                                               currentdate,currentprog.getType());
        }else{
            currentStudy = sclass.getActiveStudyPoint(currentprog.getStudent()->id(),currentprog.getType(),false);
        }

        ui->comboBoxStudyEnd->addItem(QIcon(QPixmap("://images/dialog-information.png")),
                                      tr("Do not assign the next study"));

        QList<schoolStudentStudy *> studies = sclass.getStudentStudies(currentprog.getStudent()->id());
        foreach (schoolStudentStudy *study, studies){
            // is next study?
            if (!currentStudy->getEndDate().isNull() && study->getStartdate() == currentStudy->getEndDate().addDays(1)){
                // the next day of current study's end date should be start date of next study
                nextStudy = study;
            }

            QString studyfullname = QString::number(study->getNumber()) + " " + study->getName();
            // do not add duplicate studies
            if (studyfullname == ui->comboBoxStudyEnd->itemText(ui->comboBoxStudyEnd->count()-1)) continue;

            // Add study to start combobox. Show icon if student has already worked with this study
            if (study->getStartdate().isNull()){
                ui->comboBoxStudyStart->addItem(QIcon(QPixmap(":/images/draw-rectangle.png")),studyfullname);
            }else{
                ui->comboBoxStudyStart->addItem(QIcon(QPixmap(":/images/dialog-ok-apply.png")),studyfullname);
            }
            // disable start items
            if ((currentprog.getAssignmentNumber() == 1 && !study->getReading()) ||
                    (currentprog.getType() == school_item::Demonstration && (!study->getDemonstration()))){
                QStandardItemModel *model = qobject_cast<QStandardItemModel *>(ui->comboBoxStudyStart->model());
                QStandardItem *item = model->item(ui->comboBoxStudyStart->count()-1,0);
                item->setFlags(Qt::NoItemFlags);
            }

            // Add study to end combobox.
            QString studyname = currentStudy->getNumber() == study->getNumber() ? tr("Leave on current study") + " - " : "";
            studyname.append(studyfullname);
            if (study->getStartdate().isNull()){
                ui->comboBoxStudyEnd->addItem(QIcon(QPixmap(":/images/draw-rectangle.png")),studyname);
            }else{
                ui->comboBoxStudyEnd->addItem(QIcon(QPixmap(":/images/dialog-ok-apply.png")),studyname);
            }
            // disable end items
            if (currentprog.getStudent()->gender() == person::Female && !study->getDemonstration()){
                QStandardItemModel *model = qobject_cast<QStandardItemModel *>(ui->comboBoxStudyEnd->model());
                QStandardItem *item = model->item(ui->comboBoxStudyEnd->count()-1,0);
                item->setFlags(Qt::NoItemFlags);
            }
        }

        if (currentStudy){
            // activate start study is no active
            ui->comboBoxStudyStart->setEnabled(currentStudy->getId() < 0);
            ui->comboBoxStudyStart->setCurrentIndex(currentStudy->getNumber() - 1);
            ui->checkBox->setChecked(currentStudy->getExercise());
        }
        if (nextStudy){
            ui->comboBoxStudyEnd->setCurrentIndex(nextStudy->getNumber());
        }else{
            // next study not found
            if (currentStudy->getEndDate().isValid()){
                // current study ended but next study not assigned
                ui->comboBoxStudyEnd->setCurrentIndex(0);
            }else{
                // current study is not completed (end date is null)
                ui->comboBoxStudyEnd->setCurrentIndex(currentStudy->getNumber());
            }
        }
        ui->comboBoxStudyEnd->setEnabled(ui->checkBoxDone->isChecked() && ui->checkBox->isChecked());
    }

    // Get settings if demonstration (TMS2015 change -> also for brother)
    if(currentprog.getType() == school_item::Demonstration){
        // Init ComboBox
        ui->comboBoxSetting->clear();
        ui->comboBoxSetting->setEnabled(true);
        // Add empty value. Is used to clear setting selection.
        ui->comboBoxSetting->addItem("");
        // Get all settings
        int currentsetting = -1;
        allsettings = sclass.getAllSettings(currentprog.getStudent()->gender());
        foreach (school_setting *setting, allsettings){
            ui->comboBoxSetting->addItem(QVariant(setting->getNumber()).toString() + " " + setting->getName());
            if (setting->getNumber() == currentprog.getSetting()->getNumber())
                currentsetting = ui->comboBoxSetting->count()-1;
        }
        // Set active setting
        ui->comboBoxSetting->setCurrentIndex(currentsetting);
    }else{
        // ComboBox disabled because not demonstration
        ui->comboBoxSetting->clear();
        ui->comboBoxSetting->setEnabled(false);
    }
}

// OK button
void schoolresult::on_pushButton_clicked()
{
    QDate fd = currentprog.getDate();
    QDate date = fd.addDays( c.getMeetingDay(fd,ccongregation::tms) -1 );

    // is completed?
    currentprog.setDone( ui->checkBoxDone->isChecked() );

    // setting
    currentprog.setSetting(ui->comboBoxSetting->currentIndex() < 1 ? -1 : allsettings.at(ui->comboBoxSetting->currentIndex()-1)->getNumber());

    // save note
    currentprog.setNote(ui->plainTextEdit->toPlainText());

    if(ui->checkBoxDone->isChecked()){
        // assignment completed

        // check setting if assistant was used
        if(currentprog.getAssistant() && ui->comboBoxSetting->currentIndex() < 0 && !ui->checkBoxVolunteer->isChecked()){
            QMessageBox::information(this,"",tr("Select setting"));
            return;
        }

        // save timing
        if(ui->lineEditTime->text() == ""){
            // ask to save without timing
            if(QMessageBox::question(this,tr("Timing"),
                                     tr("The timing is empty. Save?"),
                                     QMessageBox::No, QMessageBox::Yes) == QMessageBox::No) return;
        }

        // is volunteer used?
        if(ui->checkBoxVolunteer->isChecked()){
            if (ui->comboBoxUsers->currentIndex() > 0){
                foreach (person *p, allusers){
                    if (p->id() == m_volunteerIds.at(ui->comboBoxUsers->currentIndex()-1)){
                        currentprog.setVolunteer( p );
                        if (p->gender() == person::Male) currentprog.setAssistant( 0 );
                        break;
                    }
                }
            }
        }
    }

    // studies
    if (currentprog.getAssignmentNumber() > 0 && currentprog.getAssignmentNumber() < 10 && !ui->checkBoxVolunteer->isChecked()){
        if (currentStudy && currentStudy->getId() < 1){
            // create a new start study
            currentStudy = new schoolStudentStudy();
            currentStudy->setStudentId(currentprog.getStudent()->id());
            currentStudy->setNumber(ui->comboBoxStudyStart->currentIndex()+1);
            currentStudy->setStartdate(date);
            currentStudy->setExercise(ui->checkBox->isChecked());
            if (!ui->checkBoxDone->isChecked()) currentStudy->update();
        }
        if (ui->checkBoxDone->isChecked()){
            // marked completed
            // current study changes
            currentStudy->setExercise(ui->checkBox->isChecked());
            // end date
            currentStudy->setEndDate(ui->comboBoxStudyStart->currentIndex()+1 == ui->comboBoxStudyEnd->currentIndex() ? QDate() : date);
            currentStudy->update();

            // next study
            if(nextStudy &&
                    (nextStudy->getNumber() != ui->comboBoxStudyEnd->currentIndex() ||
                     ui->comboBoxStudyEnd->currentIndex() == 0) ){
                // next study changed or not assigned -> remove previous next study
                nextStudy->remove();
                nextStudy = 0;
            }
            if (!nextStudy && ui->comboBoxStudyEnd->currentIndex() > 0 && ui->comboBoxStudyStart->currentIndex()+1 != ui->comboBoxStudyEnd->currentIndex()){
                // add new next study
                schoolStudentStudy *newNextStudy = new schoolStudentStudy();
                newNextStudy->setStudentId(currentprog.getStudent()->id());
                newNextStudy->setExercise(false);
                newNextStudy->setNumber(ui->comboBoxStudyEnd->currentIndex());
                newNextStudy->setStartdate(date.addDays(1));
                newNextStudy->update();
            }
        }else{
            // remove end date from current study if exists
            if (currentStudy->getEndDate().isValid()){
                currentStudy->setEndDate(QDate());
                currentStudy->update();
            }
            // remove next study if exists
            if (nextStudy){
                nextStudy->remove();
                nextStudy = 0;
            }
        }
    }

    currentprog.setTiming( ui->lineEditTime->text() );

    // ask to save the changes
    //        if(QMessageBox::question(this,tr("Saving"),
    //                                    tr("Assignment marked completed. This action cannot undo. Continue?"),
    //                                    QMessageBox::No, QMessageBox::Yes) == QMessageBox::No) return; 

    // save the assignment changes to database
    currentprog.save();

    this->accept();
    this->close();
    return;
}

// Cancel button
void schoolresult::on_buttonCancel_clicked()
{
    this->reject();
    this->close();
}

school_item &schoolresult::getProgram() const
{
    return this->currentprog;
}

void schoolresult::on_checkBoxDone_toggled(bool checked)
{
    // assignment has been marked to completed

    // enable or disable exercises checkbox
    ui->checkBox->setEnabled(checked && !ui->checkBoxVolunteer->isChecked());
    // enable or disable study end date combobox
    ui->comboBoxStudyEnd->setEnabled(checked && ui->checkBox->isChecked());
    // reset next study combobox when assignemnt is not completed
    if (!checked) ui->comboBoxStudyEnd->setCurrentIndex(ui->comboBoxStudyStart->currentIndex()+1);
    // enable or disable timing field
    ui->lineEditTime->setEnabled(checked);
    // clear timing field when assignemnt not completed
    if (!checked) ui->lineEditTime->setText("");
    if (!checked) ui->checkBoxVolunteer->setChecked(false);
}

void schoolresult::on_checkBox_clicked()
{
    ui->comboBoxStudyEnd->setCurrentIndex(ui->comboBoxStudyStart->currentIndex()+1);
}

void schoolresult::on_comboBoxStudyStart_currentIndexChanged(int index)
{
    QStandardItemModel *model = qobject_cast<QStandardItemModel *>(ui->comboBoxStudyEnd->model());
    for (int i = 1 ; i < model->rowCount() ; i++ ){
        QStandardItem *item = model->item(i);
        item->setText( (i==index+1 ? tr("Leave on current study") + " - ": "") + ui->comboBoxStudyStart->itemText(i-1));
    }
    ui->comboBoxStudyEnd->setCurrentIndex(index+1);
}

void schoolresult::on_checkBoxVolunteer_clicked(bool checked)
{
    // disable study field when volunteer is used
    ui->comboBoxStudyStart->setEnabled(!checked && currentStudy && currentStudy->getId() < 1);
    ui->checkBox->setEnabled(!checked && currentprog.getAssignmentNumber() > 0 && currentprog.getAssignmentNumber() < 10);
    ui->comboBoxStudyEnd->setEnabled(!checked && ui->checkBox->isChecked());

}
