/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HISTORYTABLE_H
#define HISTORYTABLE_H

#include <QDialog>
#include "sql_class.h"
#include "cpersons.h"
#include <QSettings>
#include "ccongregation.h"
#include "school.h"
#include "lmm_meeting.h"
#include "general.h"

namespace Ui {
class historytable;
}

class historytable : public QDialog
{
    Q_OBJECT
    
public:
    explicit historytable(QWidget *parent = 0);
    ~historytable();

    QDate firstdayofweek;
    void refreshHistory(bool publicmeeting = false);
    bool windowMinimized();
    int minimizedWidth();    
public slots:
    void showHistory();

private slots:
    void on_toolButton_clicked();

    void on_spinBoxGreyPeriod_valueChanged(int arg1);

    void on_btn_filter_am_clicked();

    void on_btn_filter_sg_clicked();

    void on_btn_filter_cl_clicked();

    void on_btn_filter_pt_clicked();

    void on_btn_filter_wt_clicked();

    void on_btn_close_clicked();
    void on_btn_open_clicked();

signals:
    void visibleChanged(bool visible);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:
    Ui::historytable *ui;
    int retreiveRow(int userid);
    void updateCellPixmap(int row, int col);
    void addToCell(int row, int col, Talk_ID sort_index, QString const& text, QColor fg, QColor bg = QColor());
    void addColors(int row, int col);
    void getPublicMeetingHistory();
    bool showpublictalk;
    sql_class *sql;
    ccongregation c;
    QPoint oldPos;
    bool mousePressed = false;
    bool mWindowMinimized;
};

//
class FormatedString
{
    QString m_text;
    QString m_iconName;
    QColor m_foregroundColor;
    QColor m_backgroundColor;
    bool m_customFG = false;
    bool m_customBG = false;
    bool m_strikeOut = false;
    bool m_italic = false;
    bool m_bold = false;    
public:
    FormatedString(QString = QString());
    FormatedString(char* utf8);

    void setText(QString const&);
    QString text() const;

    void setIconName(QString const&);
    QString iconName() const;

    void setForeground(QColor);
    void setBackground(QColor);

    QColor foreground() const;
    QColor background() const;
    bool customForeground() const;
    bool customBackground() const;

    void setStrikeOut(bool);
    bool strikeOut() const;

    void setItalic(bool);
    bool italic() const;

    void setBold(bool);
    bool bold() const;

    bool operator < (FormatedString const&) const;
    bool operator > (FormatedString const&) const;
};

//
template <class T>
class Array2
{
    std::vector<std::vector<T>> m_values;

public:
    typedef std::vector<T> RowType;
    void setRowCount(int n)
    {
        m_values.resize(n);
    }
    int rowCount() const
    {
        return static_cast<int>(m_values.size());
    }

    T& item(int row, int col)
    {
        if (row >= static_cast<int>(m_values.size()))
            m_values.resize(row+1);
        auto& theRow = m_values[row];
        if (col >= static_cast<int>(theRow.size()))
            theRow.resize(col+1);
        return theRow[col];
    }

// APC, July 2017. Commented out fn that returns a reference to a temp!
// Not used
//    T const& item(int row, int col) const
//    {
//        if (row >= m_values)
//            return T();
//        auto& theRow = m_values[row];
//        if (col >= theRow.size())
//            return T();
//        return theRow[col];
//    }

    void setItem(int row, int col, T const& value)
    {
        item(row,col) = value;
    }

    void sortItems(int col, Qt::SortOrder order)
    {
        if (order == Qt::AscendingOrder)
            std::sort(m_values.begin(), m_values.end(),
                  [col](RowType const& a, RowType const& b) { return a[col] < b[col]; });
        else
            std::sort(m_values.begin(), m_values.end(),
                  [col](RowType const& a, RowType const& b) { return a[col] > b[col]; });
    }
};

// column name reminder: (0) date (1) num (2) source (3) note (4) time (5) assistant (6) SettingNum (7) SettingDesc
// text are formated so they are always displayed with the same look...
Array2<FormatedString> getSchoolHistory(int assigneeID, QColor defaultFG, QColor defaultBG, bool includeAsAssistant = true);

#endif // HISTORYTABLE_H
