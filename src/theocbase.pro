# -------------------------------------------------
# Project created by QtCreator 2009-10-23T18:49:48
# -------------------------------------------------
lessThan(QT_MAJOR_VERSION, 4): error('Qt 5.8 required')
lessThan(QT_MAJOR_VERSION, 5): lessThan(QT_MINOR_VERSION, 8): error('Qt 5.8 required')

QT += widgets \
    sql \
    network \
    svg \
    printsupport \
    quickwidgets \
    webchannel \
    webenginewidgets \
    positioning \
    location \
    xmlpatterns \
    networkauth \
    help
TARGET = theocbase
TEMPLATE = app
VERSION = 2019.09.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
DEFINES += TB_LIBRARY

CONFIG -= qtquickcompiler
CONFIG += c++11

Release:DESTDIR = $${OUT_PWD}/release
Debug:DESTDIR = $${OUT_PWD}/debug
# QMAKE_CXX = ccache g++

macx {
QT += macextras concurrent dbus sensors
    QMAKE_FRAMEWORK_BUNDLE_NAME = TheocBase
    TARGET = TheocBase
}

SOURCES += main.cpp \
    mainwindow.cpp \
    sql_class.cpp \
    settings.cpp \
    school.cpp \
    schoolresult.cpp \
    printui.cpp \
    person.cpp \
    sqlcombo.cpp \
    cpersons.cpp \
    personsui.cpp \
    checkupdates.cpp \
    ccongregation.cpp \
    generatexml.cpp \
    csync.cpp \
    cpersoncombobox.cpp \
    historytable.cpp \
    single_application.cpp \
    cpublictalks.cpp \
    startup.cpp \
    logindialog.cpp \
    importwizard.cpp \
    reminders.cpp \
    publictalkedit.cpp \
    speakersui.cpp \
    ccloud.cpp \
    school_setting.cpp \
    school_item.cpp \
    importwintm.cpp \
    schoolstudy.cpp \
    schoolreminder.cpp \
    sync_cloud.cpp \
    printingconditionals.cpp \
    family.cpp \
    simplecrypt.cpp \
    googlemediator.cpp \
    googlemail.cpp \
    ceditablecombobox.cpp \
    todo.cpp \
    todotableview.cpp \
    file.cpp \
    lmm_assignment.cpp \
    lmm_assignment_ex.cpp \    
    lmm_meeting.cpp \
    lmm_schedule.cpp \
    lmm_assignmentcontoller.cpp \
    publicmeeting_controller.cpp \
    pdfform.cpp \
    lmmworksheetregex.cpp \
    talkTypeComboBox.cpp \
    slipscanner.cpp \
    ical.cpp \
    importTa1ks.cpp \
    importkhs.cpp \
    territorymanagement.cpp \
    territory.cpp \
    territoryassignment.cpp \
    dateeditbox.cpp \
    general.cpp \
    printpreview.cpp \
    cterritories.cpp \
    territoryaddress.cpp \
    territorystreet.cpp \
    availability/availabilitychecker.cpp \
    availability/tb_availability.cpp \
    availability/weekendmeetingavailabilitychecker.cpp \
    availability/midweekmeetingavailabilitychecker.cpp \
    lmmtalktypeedit.cpp \
    accesscontrol.cpp \
    cloud/cloud_controller.cpp \
    cloud/theocbaseoauth.cpp \
    cloud/dropbox.cpp \
    helpviewer.cpp \
    printchannel.cpp \
    dropboxsyncbutton.cpp \
    todomodel.cpp \
    outgoingspeakersmodel.cpp \
    xml_reader.cpp \
    applyyourselfimport.cpp \
    epub.cpp \
    zipper.cpp \
    zip/qzip.cpp \
    wtimport.cpp \
    mapSettings.cpp \
    importlmmworkbook.cpp \
    jwpub.cpp \
    internet.cpp
macx {
    LIBS += -framework AppKit
    LIBS += -lz
    HEADERS += macos/machelper.h
    OBJECTIVE_SOURCES += macos/machelper.mm
    QMAKE_INFO_PLIST = Info.plist
    OTHER_FILES += Info.plist
    MacTemplates.files = ../templates \
                        translations/theocbase_en.qm \
                        translations/theocbase_ru.qm \
                        translations/theocbase_hu.qm \
                        translations/theocbase_pt.qm \
                        translations/theocbase_el.qm \
                        translations/theocbase_de.qm \
                        translations/theocbase_fr.qm \
                        translations/theocbase_nl.qm \
                        translations/theocbase_it.qm \
                        translations/theocbase_es.qm \
                        translations/theocbase_et.qm \
                        translations/theocbase_ro.qm \
                        translations/theocbase_af.qm \
                        translations/theocbase_sv.qm \
                        translations/theocbase_hr.qm \
                        translations/theocbase_pl.qm \
                        translations/theocbase_da.qm \
                        translations/theocbase_sk.qm \
                        translations/theocbase_cs.qm \
                        translations/theocbase_ka.qm \
                        translations/theocbase_fi.qm \
                        translations/theocbase_lt.qm \
                        translations/theocbase_no.qm \
                        translations/theocbase_ht.qm \
                        translations/theocbase_bg.qm \
                        translations/theocbase_uk.qm \
                        translations/theocbase_pt_BR.qm \
                        translations/theocbase_my.qm \
                        translations/theocbase_sl.qm \
                        translations/theocbase_zh.qm \
                        translations/theocbase_gn.qm \
                        translations/theocbase_sr.qm \
                        translations/theocbase_th.qm \
                        translations/theocbase_gcf.qm \
                        translations/theocbase_hy.qm \
                        translations/theocbase_ne.qm
    MacTemplates.path = Contents/Resources
    QMAKE_BUNDLE_DATA += MacTemplates

    exists(../bitrock/autoupdate-mac.app) {
        message("Add autoupdater...")
        MacAutoUpdater.files = ../bitrock/autoupdate-mac.app \
                               ../update.ini
        MacAutoUpdater.path = Contents/MacOS
        QMAKE_BUNDLE_DATA += MacAutoUpdater
    }
    message("Add doc files...")
    docFiles.files = $$files(../docs/*.qhc)
    docFiles.files += $$files(../docs/*.qch)
    docFiles.path = Contents/Resources/docs
    QMAKE_BUNDLE_DATA += docFiles
} else {
    templFiles.files = ../templates/*
    templFiles.path = $${DESTDIR}/templates
    INSTALLS += templFiles
    docFiles.files = ../docs/*
    docFiles.path = $${DESTDIR}/docs
    INSTALLS += docFiles
    languageFiles.files = *.qm
    languageFiles.path = $${DESTDIR}
    INSTALLS += languageFiles
}
unix: LIBS += -lz

HEADERS += mainwindow.h \
    sharedlib_global.h \
    sql_class.h \
    settings.h \
    school.h \
    schoolresult.h \
    printui.h \
    school_item.h \
    person.h \
    cpersons.h \
    personsui.h \
    checkupdates.h \
    ccongregation.h \
    generatexml.h \
    csync.h \
    constants.h \
    cpersoncombobox.h \
    historytable.h \
    single_application.h \
    cpublictalks.h \
    startup.h \
    logindialog.h \
    importwizard.h \    
    reminders.h \
    publictalkedit.h \
    speakersui.h \
    ccloud.h \
    school_setting.h \
    importwintm.h \    
    schoolstudy.h \
    schoolreminder.h \
    singleton.h \
    sync_cloud.h \
    printingconditionals.h \
    family.h \
    simplecrypt.h \
    googlemediator.h \
    googlemail.h \
    ceditablecombobox.h \
    todo.h \
    todotableview.h \
    file.h \
    lmm_assignment.h \
    lmm_assignment_ex.h \
    lmm_meeting.h \
    lmm_schedule.h \
    lmm_assignmentcontoller.h \
    sortfilterproxymodel.h \
    publicmeeting_controller.h \
    pdfform.h \
    lmmworksheetregex.h \
    talkTypeComboBox.h \
    slipscanner.h \
    ical.h \
    importTa1ks.h \
    importkhs.h \
    territorymanagement.h \
    territory.h \
    territoryassignment.h \
    dateeditbox.h \
    general.h \
    printpreview.h \
    cterritories.h \
    territoryaddress.h \
    territorystreet.h \
    availability/availabilitychecker.h \
    availability/tb_availability.h \
    availability/weekendmeetingavailabilitychecker.h \
    availability/midweekmeetingavailabilitychecker.h \
    lmmtalktypeedit.h \
    accesscontrol.h \
    cloud/cloud_controller.h \
    cloud/theocbaseoauth.h \
    cloud/dropbox.h \
    helpviewer.h \
    printchannel.h \
    dropboxsyncbutton.h \
    todomodel.h \
    outgoingspeakersmodel.h \
    xml_reader.h \
    applyyourselfimport.h \
    epub.h \
    zipper.h \
    zip/qzipreader_p.h \
    zip/qzipwriter_p.h \
    wtimport.h \
    mapSettings.h \
    importlmmworkbook.h \
    jwpub.h \
    internet.h
FORMS += mainwindow.ui \
    settings.ui \
    schoolresult.ui \
    printui.ui \
    personsui.ui \
    historytable.ui \
    startup.ui \
    logindialog.ui \
    importwizard.ui \
    reminders.ui \
    publictalkedit.ui \
    speakersui.ui \
    lmmworksheetregex.ui \
    territorymanagement.ui \
    printpreview.ui \
    lmmtalktypeedit.ui
RESOURCES += images.qrc \
    database.qrc \
    startup.qrc \
    qml.qrc \
    fonts/fonts.qrc
ICON = images/theocbase.icns
RC_FILE = theocbase.rc
TRANSLATIONS = translations/theocbase_en.ts \
    translations/theocbase_ru.ts \
    translations/theocbase_hu.ts \
    translations/theocbase_pt.ts \
    translations/theocbase_el.ts \
    translations/theocbase_de.ts \
    translations/theocbase_fr.ts \
    translations/theocbase_nl.ts \
    translations/theocbase_it.ts \
    translations/theocbase_es.ts \
    translations/theocbase_et.ts \
    translations/theocbase_ro.ts \
    translations/theocbase_af.ts \
    translations/theocbase_sv.ts \
    translations/theocbase_hr.ts \
    translations/theocbase_pl.ts \
    translations/theocbase_da.ts \
    translations/theocbase_sk.ts \
    translations/theocbase_cs.ts \
    translations/theocbase_ka.ts \
    translations/theocbase_fi.ts \
    translations/theocbase_lt.ts \
    translations/theocbase_no.ts \
    translations/theocbase_ht.ts \
    translations/theocbase_bg.ts \
    translations/theocbase_uk.ts \
    translations/theocbase_pt_BR.ts \
    translations/theocbase_my.ts \
    translations/theocbase_sl.ts \
    translations/theocbase_zh.ts \
    translations/theocbase_gn.ts \
    translations/theocbase_sr.ts \
    translations/theocbase_th.ts \
    translations/theocbase_gcf.ts \
    translations/theocbase_hy.ts \
    translations/theocbase_ne.ts
OTHER_FILES += \
    changelog.txt \
    ../update.xml \
    ../update.ini \

lupdate_only{
HEADERS = \
    *.h
SOURCES = \    
    *.cpp \
    qml/*.qml
}
# SMTP Client
include(smtp/smtpclient.pri)

unix:!macx {
    LIBS += -L$$PWD/../lib/ -lgdal
}
macx {
    LIBS += -F/Library/Frameworks/ -framework GDAL
    INCLUDEPATH += /Library/Frameworks/GDAL.framework/Headers    
}

win32 {
    # GDAL for Windows
    !contains(QMAKE_TARGET.arch, x86_64) {
        # 32-bit
        LIBS += -L$$PWD/../lib/gdal32/lib/ -lgdal_i
        INCLUDEPATH += $$PWD/../lib/gdal32/include
        DEPENDPATH += $$PWD/../lib/gdal32/include
    } else {
        # 64-bit
        LIBS += -L$$PWD/../lib/gdal64/lib/ -lgdal_i
        INCLUDEPATH += $$PWD/../lib/gdal64/include
        DEPENDPATH += $$PWD/../lib/gdal64/include
    }
}
