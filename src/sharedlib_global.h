#ifndef SHAREDLIB_GLOBAL_H
#define SHAREDLIB_GLOBAL_H

#include <QtCore/QtGlobal>

#if defined(TB_LIBRARY)
#  define TB_EXPORT Q_DECL_EXPORT
#else
#  define TB_EXPORT Q_DECL_IMPORT
#endif

#endif // SHAREDLIB_GLOBAL_H
