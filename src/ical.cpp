#include "ical.h"

iCal::iCal()
{

}


iCal::Item::Item(QString name)
{
    this->name = name;
}

QString iCal::Item::getName()
{
    return this->name;
}

void iCal::Item::setName(QString name)
{
    this->name = name;
}

QString iCal::Item::getValueType()
{
    return this->valueType;
}

void iCal::Item::setValueType(QString valueType)
{
    this->valueType = valueType;
}

bool iCal::Item::getHidden()
{
    return hidden;
}

void iCal::Item::setHidden(bool hidden)
{
    this->hidden = hidden;
}

QString iCal::Item::toString()
{
    if (valueType.isEmpty())
        return name + ":" + dataAsString();
    else
        return name + ";VALUE=" + valueType + ":" + dataAsString();
}



iCal::StringItem::StringItem(QString name, QString data) : Item(name) {
    this->data = data;
}

QString iCal::StringItem::dataAsString() {
    return data.replace("\r\n", "\\n").replace("\n", "\\n");
}

void iCal::StringItem::setDataAsString(QString data) {
    this->data = data;
}



iCal::BeginEndItem::BeginEndItem(QString name) : Item("") {
    beginning = new StringItem("BEGIN");
    end = new StringItem("END");
    setName(name);
}

iCal::Item *iCal::BeginEndItem::getItemByName(QString name) {
    for (Item *i : items) {
        if (!i->getName().compare(name, Qt::CaseInsensitive))
            return i;
    }
    return 0;
}

QString iCal::BeginEndItem::getName()
{
    return beginning->dataAsString();
}

void iCal::BeginEndItem::setName(QString name)
{
    beginning->setDataAsString(name);
    end->setDataAsString(name);
}

QString iCal::BeginEndItem::toString() {
    QString results;

    results.append(beginning->toString());
    results.append("\n");
    for (Item *i : items) {
        if (!i->getHidden()) {
            results.append(i->toString());
            results.append("\n");
        }
    }
    results.append(end->toString());

    return results;
}



iCal::DateItem::DateItem(QString name, QDateTime data) : Item(name) {
    midnight.setHMS(0,0,0);
    setData(data);
}

QString iCal::DateItem::dataAsString() {
    if (data.isValid()) {
        if (isAllDay())
            return data.toString("yyyyMMdd");
        else
            return data.toString("yyyyMMddTHHmmss");
    } else {
        return "INVALIDDATE";
    }
}

QDateTime iCal::DateItem::getData() {
    return data;
}

void iCal::DateItem::setData(QDateTime data)
{
    this->data = data;
    if (isAllDay())
        valueType = "DATE";
    else
        valueType = "";
}

bool iCal::DateItem::isAllDay()
{
    return data.time() == midnight;
}

void iCal::DateItem::setDataAsString(QString sData) {
    this->data = QDateTime::fromString(sData, Qt::ISODate);
    if (!this->data.isValid())
        this->data = QDateTime::fromString(sData, "yyyyMMddTHHmmss");
    if (!this->data.isValid())
        this->data = QDateTime::fromString(sData,"yyyyMMdd");
}



iCal::TimeSpanItem::TimeSpanItem(QString name, int days, QTime time) : Item(name) {
    this->days = days;
    this->time = time;
    isValid = true;
}

QString iCal::TimeSpanItem::dataAsString() {
    if (isValid) {
        int workingDays(days);
        QString sgn;
        if (workingDays < 0) {
            sgn = "-";
            workingDays = -workingDays;
        }
        return
                sgn + "P" +
                QString::number(workingDays) + "DT" +
                QString::number(time.hour()) + "H" +
                QString::number(time.minute()) + "M" +
                QString::number(time.second()) + "S";
    } else {
        return "INVALIDTIMESPAN";
    }
}

void iCal::TimeSpanItem::setDataAsString(QString data) {
    QRegularExpression rx("([-]*)P*(\\d+)\\D+(\\d+)\\D+(\\d+)\\D+(\\d+)\\D+");
    QRegularExpressionMatch m = rx.match(data);
    isValid = m.hasMatch();
    if (isValid) {
        days = m.captured(2).toInt();
        if (!m.captured(1).isEmpty())
            days = -days;
        time.setHMS(m.captured(3).toInt(), m.captured(4).toInt(), m.captured(5).toInt());
    }
}


iCal::VEVENT::VEVENT() : BeginEndItem("VEVENT") {
    items.append(new DateItem("DTSTART", QDateTime::fromMSecsSinceEpoch(0)));
    items.append(new DateItem("DTEND", QDateTime::fromMSecsSinceEpoch(0)));
    items.append(new DateItem("DTSTAMP", QDateTime::currentDateTime()));
    items.append(new DateItem("CREATED", QDateTime::currentDateTime()));
    items.append(new StringItem("DESCRIPTION"));
    items.append(new DateItem("LAST-MODIFIED", QDateTime::currentDateTime()));
    items.append(new StringItem("LOCATION"));
    items.append(new StringItem("STATUS", "CONFIRMED"));
    items.append(new StringItem("SUMMARY"));
    items.append(new StringItem("TRANSP", "OPAQUE"));
    items.append(new StringItem("UID"));
}

iCal::VEVENT::VEVENT(QString Summary, QDateTime Start, QDateTime End, QDateTime Stamp, QString Description, QString Location) : BeginEndItem("VEVENT") {
    items.append(new DateItem("DTSTART", Start));
    items.append(new DateItem("DTEND", End));
    items.append(new DateItem("DTSTAMP", Stamp));
    items.append(new DateItem("CREATED", Stamp));
    items.append(new StringItem("DESCRIPTION", Description));
    items.append(new DateItem("LAST-MODIFIED", Stamp));
    items.append(new StringItem("LOCATION", Location));
    items.append(new StringItem("STATUS", "CONFIRMED"));
    items.append(new StringItem("SUMMARY", Summary));
    items.append(new StringItem("TRANSP", "OPAQUE"));
    items.append(new StringItem("UID"));
}

void iCal::VEVENT::addAlarm(QString description, int minutesBefore) {
    items.append(new VALARM(description, 0, QTime::fromMSecsSinceStartOfDay(-minutesBefore * 1000)));
}

void iCal::VEVENT::addAlarm(QString description, QDateTime trigger) {
    QDateTime work = start();
    items.append(new VALARM(description,
                            trigger.date().toJulianDay() - work.date().toJulianDay(),
                            QTime::fromMSecsSinceStartOfDay(trigger.time().msecsSinceStartOfDay() - work.time().msecsSinceStartOfDay())));
}

QString iCal::VEVENT::summary() {
    return ((StringItem*)getItemByName("SUMMARY"))->dataAsString();
}

void iCal::VEVENT::setSummary(QString data) {
    ((StringItem*)getItemByName("SUMMARY"))->setDataAsString(data);
}

iCal::DateItem *iCal::VEVENT::startItem()
{
    return (DateItem*)getItemByName("DTSTART");
}

QDateTime iCal::VEVENT::start() {
    return startItem()->getData();
}

void iCal::VEVENT::setStart(QDateTime data) {
    ((DateItem*)getItemByName("DTSTART"))->setData(data);
}

iCal::DateItem *iCal::VEVENT::endItem()
{
    return (DateItem*)getItemByName("DTEND");
}

QDateTime iCal::VEVENT::end() {
    return endItem()->getData();
}

void iCal::VEVENT::setEnd(QDateTime data) {
    ((DateItem*)getItemByName("DTEND"))->setData(data);
}

QDateTime iCal::VEVENT::stamp() {
    return ((DateItem*)getItemByName("DTSTAMP"))->getData();
}

void iCal::VEVENT::setStamp(QDateTime data) {
    ((DateItem*)getItemByName("DTSTAMP"))->setData(data);
    ((DateItem*)getItemByName("CREATED"))->setData(data);
    ((DateItem*)getItemByName("LAST-MODIFIED"))->setData(data);
}

QString iCal::VEVENT::description() {
    return ((StringItem*)getItemByName("DESCRIPTION"))->dataAsString();
}

void iCal::VEVENT::setDescription(QString data) {
    ((StringItem*)getItemByName("DESCRIPTION"))->setDataAsString(data);
}

QString iCal::VEVENT::location() {
    return ((StringItem*)getItemByName("LOCATION"))->dataAsString();
}

void iCal::VEVENT::setLocation(QString data) {
    ((StringItem*)getItemByName("LOCATION"))->setDataAsString(data);
}

QString iCal::VEVENT::uid() {
    return getItemByName("UID")->dataAsString();
}

void iCal::VEVENT::setUid(QString uid) {
    getItemByName("UID")->setDataAsString(uid);
}

QString iCal::VEVENT::status()
{
    return getItemByName("STATUS")->dataAsString();
}

void iCal::VEVENT::setStatus(QString status)
{
    getItemByName("STATUS")->setDataAsString(status);
}

QString iCal::VEVENT::toString()
{
    endItem()->setHidden(startItem()->isAllDay());
    return BeginEndItem::toString();
}

iCal::VCALENDAR::VCALENDAR(QString CalendarName): BeginEndItem("VCALENDAR") {
    items.append(new StringItem("VERSION", "2.0"));
    items.append(new StringItem("CALSCALE", "GREGORIAN"));
    items.append(new StringItem("METHOD", "PUBLISH"));
    items.append(new StringItem("X-WR-CALNAME", CalendarName));
    //items.append(new StringItem("X-WR-TIMEZONE", "America/New_York"));
    lastEvent = 0;
}

void iCal::VCALENDAR::AddEvent(iCal::VEVENT *vevent) {
     items.append(vevent);
     lastEvent = vevent;
}

iCal::VEVENT *iCal::VCALENDAR::LastEvent()
{
    return lastEvent;
}
