/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOLSTUDY_H
#define SCHOOLSTUDY_H

#include "sql_class.h"
#include <QObject>
#include <QString>
#include <QDebug>

/**
 * @brief The schoolStudy class - School study class
 *                                This class represent a one school study (counsel point)
 */
class schoolStudy : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int number READ getNumber WRITE setNumber)
    Q_PROPERTY(QString name READ getName WRITE setName)
    Q_PROPERTY(int id READ getId WRITE setId)
public:
    schoolStudy(QObject *parent = 0);

    /**
     * @brief getId - Get id number in database
     * @return
     */
    int getId() const;

    /**
     * @brief setId - Set id number in database
     * @param value
     */
    void setId(int value);

    /**
     * @brief getNumber - Get study number (1-53)
     * @return
     */
    int getNumber() const;

    /**
     * @brief setNumber - Set study number (1-53)
     * @param value
     */
    void setNumber(int value);

    /**
     * @brief getName - Get name of study
     * @return
     */
    QString getName() const;

    /**
     * @brief setName - Set name of study
     * @param value
     */
    void setName(const QString &value);

    /**
     * @brief getReading - Is study used for reading
     * @return - true or false
     */
    bool getReading() const;

    /**
     * @brief setReading - Set study for reading
     * @param value
     */
    void setReading(bool value);

    /**
     * @brief getDiscourse - Is study used for discource
     * @return
     */
    bool getDiscourse() const;

    /**
     * @brief setDiscourse - Set study for discource
     * @param value
     */
    void setDiscourse(bool value);

    /**
     * @brief getDemonstration - Is study used for demonstration
     * @return - true or false
     */
    bool getDemonstration() const;

    /**
     * @brief setDemonstration - Set to use this study for demostration
     * @param value - true or false
     */
    void setDemonstration(bool value);

    /**
     * @brief update - Save changes to database or insert a new row if id is -1
     * @return
     */
    virtual bool update();

    /**
     * @brief remove - Remove study from database
     * @return
     */
    virtual bool remove();

private:
    int m_studyid;
    int m_number;
    QString m_name;
    bool m_reading;
    bool m_discourse;
    bool m_demonstration;

protected:
    sql_class *sql;
};

/**
 * @brief The schoolStudentStudy class - Student's study class
 *                                       This class represent a one student's study (counsel point)
 *                                       Save changes to database using update() function
 */
class schoolStudentStudy : public schoolStudy
{
    Q_OBJECT
    Q_PROPERTY(QDate startdate READ getStartdate WRITE setStartdate)
    Q_PROPERTY(QDate enddate READ getEndDate WRITE setEndDate)
    Q_PROPERTY(bool exercise READ getExercise WRITE setExercise)
public:
    schoolStudentStudy(QObject *parent = 0);

    /**
     * @brief getStartdate - Get start date of study
     * @return
     */
    QDate getStartdate() const;

    /**
     * @brief setStartdate - Set start date of study
     * @param value
     */
    void setStartdate(const QDate &value);

    /**
     * @brief getEndDate - Get end date of study
     * @return
     */
    QDate getEndDate() const;

    /**
     * @brief setEndDate - Set end date of study (used when study completed)
     * @param value
     */
    void setEndDate(const QDate &value);

    /**
     * @brief getExercise - Is exercise done or undone
     * @return - true or false
     */
    bool getExercise() const;

    /**
     * @brief setExercise - Set exercise to done or undone
     * @param value - true or false
     */
    void setExercise(bool value);

    /**
     * @brief getStudentId - Get student's id
     * @return - id number in database
     */
    int getStudentId() const;

    /**
     * @brief setStudentId - Set student's id
     * @param id - id number in database
     */
    void setStudentId(int id);

    /**
     * @brief updateStudy - Save the changes to database or insert a new row if id is -1.
     * @return - success or failure
     */
    bool update();

    /**
     * @brief remove - Remove student's study from database
     * @return - success or failure
     */
    bool remove();

private:
    QDate m_startdate;
    QDate m_enddate;
    bool m_exercise;
    int m_studentid;    
};


#endif // SCHOOLSTUDY_H
