#ifndef MAPSETTINGS_H
#define MAPSETTINGS_H

#include <QObject>
#include <QString>
#include "sql_class.h"

class MapSettings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double markerScale READ markerScale NOTIFY notification)

public:
    explicit MapSettings(QObject *parent = nullptr);

    double markerScale();

signals:
    void notification();

private:
};

#endif // MAPSETTINGS_H
