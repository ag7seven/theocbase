/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "generatexml.h"
#include <QMap>
#include <QFile>
#include <QMessageBox>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QMapIterator>

generateXML::generateXML()
{
    sql = &Singleton<sql_class>::Instance();
}
// code for destructor
 //   GenerateXML::~GenerateXML()
 //   {

 //   }

void generateXML::writeInfo(QXmlStreamWriter *xWriter,int version)
{
    ccongregation c;
    xWriter->writeStartElement("info");
    xWriter->writeTextElement("version",QVariant(version).toString());
    ccongregation::congregation mine = c.getMyCongregation();
    xWriter->writeTextElement("default_congregation",mine.name);

    ccongregation::meeting_dayandtime const& daytime_now = mine.getPublicmeeting_now();
    QString yr = QString::number(daytime_now.getOfyear());
    xWriter->writeTextElement("meeting_day_" + yr, QVariant(daytime_now.getMeetingday()).toString());
    xWriter->writeTextElement("meeting_time_" + yr, daytime_now.getMeetingtime());

    auto const& daytime = mine.getPublicmeeting_next();
    yr = QString::number(daytime.getOfyear());
    xWriter->writeTextElement("meeting_day_" + yr, QVariant(daytime.getMeetingday()).toString());
    xWriter->writeTextElement("meeting_time_" + yr, daytime.getMeetingtime());

    xWriter->writeEndElement();// info
}


void generateXML::writePersons(QXmlStreamWriter *xWriter, bool publishers, bool speakers){
    cpersons *cp = new cpersons();
    int types = 0;
    if (publishers && speakers){
        types = 2;
    }else if(speakers){
        types = 1;
    }
    QList<person*> allusers = cp->getAllPersons(types);
    foreach(person *p, allusers){
        xWriter->writeStartElement("person");  // <person>
        xWriter->writeTextElement("firstname",p->firstname());
        xWriter->writeTextElement("lastname",p->lastname());
        xWriter->writeTextElement("usefor",QString::number(p->usefor()));
        xWriter->writeTextElement("congregation",p->congregationName());
        xWriter->writeTextElement("email",p->email());
        xWriter->writeTextElement("phone",p->phone());
        xWriter->writeTextElement("gender",p->gender() == person::Male ? "B" : "S");
        xWriter->writeTextElement("servant", QVariant(p->servant()).toString());
        xWriter->writeTextElement("uuid",p->uuid());
        xWriter->writeEndElement();  // </person>
    }
}

void generateXML::writeFamilies(QXmlStreamWriter *xWriter)
{
   QList<family *>flist = family::getFamilies();
   foreach(family *f, flist){
       person *familyHead = cpersons::getPerson(f->getHeadId());
       foreach(person *p, f->getMembers()){
           if (!p) continue;
           xWriter->writeStartElement("family");
           xWriter->writeTextElement("head_firstname",familyHead ? familyHead->firstname() : "");
           xWriter->writeTextElement("head_lastname",familyHead ? familyHead->lastname() : "");
           xWriter->writeTextElement("member_firstname", p->firstname() );
           xWriter->writeTextElement("member_lastname", p->lastname() );
           xWriter->writeEndElement();
       }
   }
   qDeleteAll(flist);
   flist.clear();
}

void generateXML::writePublicMeetings(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox){
    cpersons cp;

    if (sql->getLanguageDefaultId() == 0){
        QMessageBox::warning(0,"",QObject::tr("Default language not selected!"));
        return;
    }
    cpublictalks c;
    if (spinbox >= 0){
        for(int weeks = 0;weeks < spinbox+1; weeks++){
            QSharedPointer<cptmeeting> cm(c.getMeeting(tempdate));
            if (cm->id > 0){
                xWriter->writeStartElement("public_meeting");
                xWriter->writeTextElement("date",tempdate.toString(Qt::ISODate));
                xWriter->writeTextElement("theme_id",QVariant(cm->theme.number).toString());
                writeName(xWriter, "speaker", cm->speaker());
                writeName(xWriter, "chairman",cm->chairman());
                writeName(xWriter, "reader",cm->wtReader());
                xWriter->writeTextElement("song_pt", QVariant(cm->song_talk).toString());
                xWriter->writeTextElement("song_wt_start",QVariant(cm->song_wt_start).toString());
                xWriter->writeTextElement("song_wt_end",QVariant(cm->song_wt_end).toString());
                xWriter->writeEndElement(); // </public_meeting>
            }
            tempdate = tempdate.addDays(7);
        }
    }

    // all themes
    sql_items esitelmat = sql->selectSql("SELECT * FROM publictalks");
    for(unsigned int j = 0; j < esitelmat.size(); j++){
        xWriter->writeStartElement("themes");
        xWriter->writeTextElement("theme_id", esitelmat[j].value("theme_number").toString());
        xWriter->writeTextElement("theme", esitelmat[j].value("theme_name").toString());
        xWriter->writeTextElement("lang_id", esitelmat[j].value("lang_id").toString());
        xWriter->writeEndElement();
    }
    sql_items p_esitelmat = sql->selectSql("SELECT speaker_publictalks.*,publictalks.theme_number FROM "
                                           "speaker_publictalks "
                                           "LEFT JOIN publictalks "
                                           "ON speaker_publictalks.theme_id = publictalks.id");
    for(unsigned int k = 0; k < p_esitelmat.size(); k++){
        xWriter->writeStartElement("speaker_themes");
        person *speaker = cp.getPerson(p_esitelmat[k].value("speaker_id").toInt());
        if(speaker){
            xWriter->writeTextElement("speaker_firstname", speaker->firstname());
            xWriter->writeTextElement("speaker_lastname",speaker->lastname());
        }else{
            xWriter->writeTextElement("speaker_firstname", "");
            xWriter->writeTextElement("speaker_lastname","");
        }
        xWriter->writeTextElement("theme_id", p_esitelmat[k].value("theme_number").toString());
        xWriter->writeTextElement("lang_id", p_esitelmat[k].value("lang_id").toString());
        xWriter->writeEndElement();
    }
}

void generateXML::writeOutgoing(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox){
    Q_UNUSED(xWriter);
    Q_UNUSED(tempdate);
    Q_UNUSED(spinbox);
}

void generateXML::writeSchool(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox){
    school s;
    xWriter->writeTextElement("qty",QVariant(s.getClassesQty()).toString());

    for(int weeks = 0;weeks < spinbox+1; weeks++){

        std::vector<std::unique_ptr<school_item> > schoolprog;
        if (s.getProgram(schoolprog,tempdate)){
            for(unsigned int i = 0;i<schoolprog.size();i++){
                school_item &task = *schoolprog[i];

                xWriter->writeStartElement("school");
                xWriter->writeTextElement("schoolnumber",QVariant(task.getClassNumber()).toString());
                xWriter->writeTextElement("date",QVariant(task.getDate()).toString());
                xWriter->writeTextElement("number",QVariant(task.getAssignmentNumber()).toString());
                xWriter->writeTextElement("theme",QVariant(task.getTheme()).toString());
                xWriter->writeTextElement("src_material",QVariant(task.getSource()).toString());

                xWriter->writeTextElement("student_firstname",task.getStudent() ? task.getStudent()->firstname() : "" );
                xWriter->writeTextElement("student_lastname",task.getStudent() ? task.getStudent()->lastname() : "" );

                xWriter->writeTextElement("assistant_firstname",task.getAssistant() ? task.getAssistant()->firstname() : "");
                xWriter->writeTextElement("assistant_lastname",task.getAssistant() ? task.getAssistant()->lastname() : "");

                xWriter->writeTextElement("volunteer_firstname",task.getVolunteer() ? task.getVolunteer()->firstname() : "");
                xWriter->writeTextElement("volunteer_lastname",task.getVolunteer() ? task.getVolunteer()->lastname() : "");

                xWriter->writeTextElement("settingnumber",QVariant(task.getSetting()->getNumber()).toString());
                xWriter->writeTextElement("done",QVariant(task.getDone()).toString());
                xWriter->writeTextElement("note",QVariant(task.getNote()).toString());
                xWriter->writeTextElement("time",QVariant(task.getTiming()).toString());

                xWriter->writeEndElement();
            }
        }

        tempdate = tempdate.addDays(7);
    }
}

void generateXML::writeMidweekMeeting(QXmlStreamWriter *xWriter, QDate tempdate, int spinbox){
    school s;
    xWriter->writeTextElement("qty",QVariant(s.getClassesQty()).toString());

    LMM_Meeting* mtg = new LMM_Meeting();
    for(int weeks = 0;weeks < spinbox+1; weeks++){
        if (mtg->loadMeeting(tempdate, false)) {

            xWriter->writeStartElement("midweekmeeting");
            xWriter->writeTextElement("date", mtg->date().toString(Qt::ISODate));
            xWriter->writeTextElement("bible_reading", mtg->bibleReading());
            writeName(xWriter, "chairman", mtg->chairman());
            writeName(xWriter, "counselor2", mtg->counselor2());
            writeName(xWriter, "counselor3", mtg->counselor3());
            writeName(xWriter, "prayer_beginning", mtg->prayerBeginning());
            writeName(xWriter, "prayer_end", mtg->prayerEnd());
            xWriter->writeTextElement("song_beginning", QString::number(mtg->songBeginning()));
            xWriter->writeTextElement("song_middle", QString::number(mtg->songMiddle()));
            xWriter->writeTextElement("song_end", QString::number(mtg->songEnd()));

            QList<LMM_Assignment *> prog = mtg->getAssignments();
            for (LMM_Assignment* asgn: prog) {
                if (asgn->theme() == "") continue;
                xWriter->writeStartElement("talk");
                xWriter->writeTextElement("talk_id", QString::number(asgn->dbTalkId()));
                xWriter->writeTextElement("theme", asgn->theme());
                xWriter->writeTextElement("source", asgn->source().replace("{br}", "\r\n"));
                xWriter->writeTextElement("time", QString::number(asgn->time()));
                xWriter->writeTextElement("class", QString::number(asgn->classnumber()));
                writeName(xWriter, "speaker", asgn->speaker());
                if (asgn->canCounsel()) {
                    LMM_Assignment_ex *studenttalk((LMM_Assignment_ex*)asgn);
                    if (asgn->canHaveAssistant())
                        writeName(xWriter, "assistant", studenttalk->assistant());
                    writeName(xWriter, "volunteer", studenttalk->volunteer());
                    xWriter->writeTextElement("completed", QVariant(studenttalk->completed()).toString());
                } else if (asgn->talkId() == LMM_Schedule::TalkType_CBS) {
                    LMM_Assignment_ex *cbs((LMM_Assignment_ex*)asgn);
                    writeName(xWriter, "reader", cbs->assistant());
                }
                xWriter->writeEndElement();
            }

            xWriter->writeEndElement();
        }
        tempdate = tempdate.addDays(7);
    }
    delete mtg;
}

void generateXML::writeName(QXmlStreamWriter *xWriter, QString elementName, person *person)
{
    if (person) {
        xWriter->writeTextElement(elementName + "_firstname", person->firstname());
        xWriter->writeTextElement(elementName + "_lastname", person->lastname());
    } else {
        xWriter->writeTextElement(elementName + "_firstname", "");
        xWriter->writeTextElement(elementName + "_lastname", "");
    }
}

void generateXML::writeStudyHistory(QXmlStreamWriter *xWriter)
{
    school s;

    cpersons *cp = new cpersons();
    QList<person*> allusers = cp->getAllPersons(0);
    foreach(person *p, allusers){
        QList<schoolStudentStudy *> slist = s.getStudentStudies(p->id());

        for(int i = 0;i<slist.size();i++){
            schoolStudentStudy *st = slist[i];
            if (st->getStartdate().isNull()) continue;
            xWriter->writeStartElement("study");  // <study>
            xWriter->writeTextElement("firstname",p->firstname());
            xWriter->writeTextElement("lastname",p->lastname());
            xWriter->writeTextElement("study_number",QVariant(st->getNumber()).toString());
            xWriter->writeTextElement("date_assigned",st->getStartdate().toString(Qt::ISODate));
            if (st->getEndDate().isNull()){
                xWriter->writeTextElement("date_completed","");
            }else{
                xWriter->writeTextElement("date_completed",st->getEndDate().toString(Qt::ISODate));
            }
            xWriter->writeTextElement("exercises", QVariant(st->getExercise()).toString());
            xWriter->writeEndElement();  // </study>
        }
    }
    delete cp;
}
