/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CCONGREGATION_H
#define CCONGREGATION_H

#include <QString>
#include <QList>
#include <QPair>
#include "sql_class.h"

class ccongregation : public QObject
{   
    Q_OBJECT

public:
    class congregation;

    class meeting_dayandtime
    {
    public:
        meeting_dayandtime(); // should only be used in the one "blank" object in congregation

        int getId() const;
        void setId(int value);

        int getOfyear() const;
        void setOfyear(int value);

        int getMeetingday() const;
        void setMeetingday(int value);

        QString getMeetingtime() const;
        void setMeetingtime(const QString &value);

        /**
         * @brief ccongregation::meeting_dayandtime::getMeetingDate - translates weekof to the actual date of the talk
         * @param weekOf
         * @return date of the talk
         */
        QDate getMeetingDate(QDate weekOf) const;

        bool getIsvalid() const;
        void setIsvalid(bool value);

        bool getIsdirty() const;
        void setIsdirty(bool value);

    private:
        int id;
        bool isdirty;
        int ofyear;
        int meetingday;
        QString meetingtime;
    };

    class congregation
    {
    public:
        congregation();

        // returns true if the congregation is not 'None' (so one that comes from the Database)
        bool isValid() const;

        int id;
        QString name;
        QString time_meeting1;
        QString address;
        QString circuit;
        QString info;
        bool save();

        meeting_dayandtime const& getPublicmeeting(QDate onDate) const;
        meeting_dayandtime const& getPublicmeeting(int onYear) const;
        meeting_dayandtime const& getPublicmeeting_now() const;
        meeting_dayandtime const& getPublicmeeting_next() const;

        meeting_dayandtime& getPublicmeeting(QDate onDate);
        meeting_dayandtime& getPublicmeeting(int onYear);
        meeting_dayandtime& getPublicmeeting_now();
        meeting_dayandtime& getPublicmeeting_next();

    private:
        meeting_dayandtime blank;
        meeting_dayandtime publicmeeting_now;
        meeting_dayandtime publicmeeting_next;
    };

    enum exceptions {
        none,
        circuit_overseer_visit,
        convention,
        other
    };

    enum meetings {
        cbs,
        tms,
        sm,
        pm,
        wt
    };
    Q_ENUM(meetings)
    Q_ENUM(exceptions)

    ccongregation(QObject *parent = nullptr);

    /**
     * @brief getAllCongregations - retrieves all congregations' information except meeting day/times
     *                            - so far, no code that calls this makes use of it
     *                            - no need to pull it from the database unless it's used
     * @param groupedByCircuit
     * @return list of congregations
     */
    QList<congregation> getAllCongregations(bool groupedByCircuit = false);

    int getCongregationId(QString name);
    ccongregation::congregation getOrAddCongregation(QString name);
    ccongregation::congregation addCongregation(QString name);
    bool removeCongregation(int id);

    QString getCongregationName(int id);
    ccongregation::congregation getCongregationById(int id);
    int getCongregationIDByPartialName(QString name);
    ccongregation::congregation getMyCongregation();

    /**
     * @brief isException - Get exceptions in the specific week
     * @param date - The first day of week
     * @return - exception or exceptions::none
     */
    Q_INVOKABLE exceptions isException(QDate date);

    /**
     * @brief noMeeting - is there a meeting on 'date'
     * @param date - The first day of week
     * @return - true if no midweek or/and weekend meeting for that week
     */
    Q_INVOKABLE bool noMeeting(QDate date);

    /**
     * @brief getExceptionText - Get exception text (ex. 'Circuit overseer's visit')
     * @param date - First day of week
     * @return - Exception text
     */
    Q_INVOKABLE QString getExceptionText(QDate date);

    /**
     * @brief getStandardExceptionText - Get untranslated 'plain' exception text (ex. 'Circuit overseer's visit')
     * @param date - First day of week
     * @param isReview - if no other exception found, and isReview, return 'Review'
     * @return - Exception text
     */
    QString getStandardExceptionText(QDate date, bool isReview);

    /**
     * @brief getExceptionDates - Get exception date range
     * @param weekDate - First day of week
     * @param date1 - start date of exception
     * @param date2 - end date of exception
     * @return - true if exception found
     */
    bool getExceptionDates(const QDate weekDate, QDate &date1, QDate &date2);

    /**
     * @brief getMeetingDay - Get day of meeting
     * @param date - First date of week
     * @param meetingtype - meetingtypes are cbs, tms, sm, pm and wt
     * @return - Day of week
     */
    Q_INVOKABLE int getMeetingDay(QDate date, meetings meetingtype);

    /**
     * @brief clearExceptionCache - clear memorized exceptions. Useful when database table has changed
     */
    Q_INVOKABLE void clearExceptionCache();

private:
    sql_class *sql;
    QHash<QDate, sql_item> cachedExceptions;
    QDate minCachedException;
    void updateExCache(QDate date);
};

#endif // CCONGREGATION_H
