/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PUBLICTALKEDIT_H
#define PUBLICTALKEDIT_H

#include <QWidget>
#include <QLabel>
#include <QCalendarWidget>
#include <QDateEdit>
#include <QGuiApplication>
#include <QScreen>
#include "cpublictalks.h"
#include "cpersoncombobox.h"
#include "ceditablecombobox.h"
#include "sql_class.h"
#include "todo.h"
#include "todotableview.h"

extern ccongregation::congregation myCongregation;

namespace Ui {
class publictalkedit;
}

class publictalkedit : public QWidget
{
    Q_OBJECT

    enum comboStates {
        idle,
        popup,
        filtered,
        leaving
    };

public:
    explicit publictalkedit(QWidget *parent = 0);
    ~publictalkedit();
    void setDate(QDate date);

public slots:
    void updateScheduledTalks(int speakerId, int oldCongregationId, int newCongregationId, bool removed);
    void updateScheduledTalks(int themeId);

private:
    QList<ccongregation::congregation> allCongregations;
    QStandardItemModel *getThemesTable(QString filter, person *speaker, QDate date = QDate::currentDate());
    void saveTheme(QAbstractItemModel *model, int index);
    QStandardItemModel *getCongregationTable(QString filter, bool allowOwnCongregation);
    void saveCongregation(QAbstractItemModel *model, int index);
    void savePTSpeaker(QAbstractItemModel *model, int index);
    void fillTodo();
    void createCalendarPopup();
    void showCalendarPopup(bool isForIncoming);
    void setLabelOK(QLabel *lbl, bool dataOk);
    void populateCongregationCombo(QComboBox *combo);
    bool isCalendarPoppingUpForIncoming;

private slots:
    void on_comboChairman_activated(int index);
    void on_comboReader_activated(int index);

    void beforeShowThemeCombo(QString cname, cEditableComboBox *combo, bool filterRequested);
    void on_comboTheme_activated(int index);
    void on_comboTheme_focusOut(cEditableComboBox *, const cEditableComboBox::states state);

    void beforeShowPTCongCombo(QString cname, cEditableComboBox *combo, bool filterRequested);
    void on_comboPTCong_activated(int index);
    void on_comboPTCong_focusOut(cEditableComboBox *, const cEditableComboBox::states state);

    void beforeShowSpeakerCombo(QString cname, cEditableComboBox *combo, bool filterRequested);
    void on_comboPTSpeaker_activated(int index);
    void on_comboPTSpeaker_focusOut(cEditableComboBox *, const cEditableComboBox::states state);

    void beforeShowOutCongCombo(QString cname, cPersonComboBox *combo);
    void beforeShowOutSpeakerCombo(QString cname, cPersonComboBox *combo);
    void beforeShowOutThemeCombo(QString cname, cPersonComboBox *combo);

    void on_buttonAdd_clicked();

    void on_buttonRemove_clicked();

    void on_comboOutSpeaker_currentIndexChanged(const QString &arg1);
    void on_comboWtSource_currentIndexChanged(const int index);
    void on_editWTTheme_editingFinished();
    void on_editFinalTalk_editingFinished();

    void on_comboConductor_activated(int index);

    void on_spin_song_talk_valueChanged(int arg1);

    void on_spin_song_wt_start_valueChanged(int arg1);

    void on_spin_song_wt_end_valueChanged(int arg1);

    void on_btnPTMove_clicked();

    void calendarClicked(QDate date);
    void moveIncoming(QDate date);
    void moveOutgoing(QDate date);

    void on_buttonAddTodoIn_clicked();
    void on_buttonAddTodoOut_clicked();
    void on_buttonRemoveTodo_clicked();
    void on_buttonMoveTodo_clicked();
    void beforeShowTodoPopup(QString cname, cEditableComboBox *combo, bool filterRequested);
    void on_todoCombo_focusOut(cEditableComboBox *combo, const cEditableComboBox::states state);

    void on_btnPTTodo_clicked();

    void on_btnOutMove_clicked();

    void on_btnOutTodo_clicked();

    void on_btnPTClear_clicked();

private:
    Ui::publictalkedit *ui;
    sql_class *sql;
    cpublictalks cp;
    QSharedPointer<cptmeeting> currentMeeting;
    QStandardItemModel *todoModel;
    todoTableViewDelegate *todoViewDelegate;
    QDate currentDate;
    QDate ptDate;
    int ptCongIdFilter;
    QFrame *calPopup;
    QDateEdit *oDate;
    QFont okFont;
    QFont filterFont;
};

#endif // PUBLICTALKEDIT_H
