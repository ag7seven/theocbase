#ifndef LMMWORKSHEETREGEX_H
#define LMMWORKSHEETREGEX_H

#include <QDialog>
#include <QStringListModel>
#include <QTemporaryDir>
#include <QDesktopServices>
#include <QProcess>
#include <QClipboard>
#include "importlmmworkbook.h"

namespace Ui {
class lmmWorksheetRegEx;
}

class lmmWorksheetRegEx : public QDialog
{
    Q_OBJECT

public:
    explicit lmmWorksheetRegEx(QWidget *parent = nullptr);
    ~lmmWorksheetRegEx();

private slots:
    void on_txtEPUB_editingFinished();

    void on_rxDate1_editingFinished();

    void on_rxDate2_editingFinished();

    void on_rxSong_editingFinished();

    void on_txtA1_editingFinished();

    void on_txtA2_editingFinished();

    void on_txtA3_editingFinished();

    void on_txtA4_editingFinished();

    void on_txtA5_editingFinished();

    void on_btnToSQL_clicked();

    void on_lstTOC_clicked(const QModelIndex &index);

    void on_lstParts_clicked(const QModelIndex &index);

    void on_rxTiming_editingFinished();

    void on_ckAddPDFLanguage_clicked();

private:
    Ui::lmmWorksheetRegEx *ui;
    importlmmworkbook* import;
    QStringListModel *tocModel;
    QStringListModel *mtgModel;
    QTemporaryDir tempdir;
    sql_class *sql;
    QFont okFont;
    QFont errFont;
    void importEPUB(bool loadRegEx = false);
    void copyRegexToImport();
    void ImportDate(bool reloadRex);

    // After hitting Save, the clipboard will now have the SQL to paste into the upgrade file
    void addToClipboardAndRun(QString *clipboard, QString cmd);
};

#endif // LMMWORKSHEETREGEX_H
