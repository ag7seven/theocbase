﻿INSERT INTO lmm_workbookregex (lang, key, value) VALUES ("pt|TPO","date","^(?<fromday>\d+)\sa\s(?<thruday>\d+)\sde\s(?<month>\D+)");
INSERT INTO lmm_workbookregex (lang, key, value) VALUES ("pt|TPO","song","Cântico\s*(\d+)(.*)");
INSERT INTO lmm_workbookregex (lang, key, value) VALUES ("pt|TPO","timing","[(](?<timing>\d+)\smin[.]*(?<timingextra>[^)]*)[)]");
INSERT INTO lmm_workbookregex (lang, key, value) VALUES ("pt|TPO","assignment1","•(?<theme>[^•]+)•*(?<source>.*)");
