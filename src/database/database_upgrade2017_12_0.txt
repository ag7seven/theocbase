﻿delete from lmm_workbookregex where lang = 'ro'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'date1', '^[^\s+]+\s(?<fromday>\d+)-(?<thruday>\d+)\s*(?<month1>\D*)'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'date2', '^[^\s+]+\s(?<fromday>\d+)\s+(?<month1>\D*)\s*–\s*(?<thruday>\d+)\s+(?<month2>\D*)'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'song', 'Cântarea\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'timing', '[(](?<timingextra>\D*)(?<timing>\d+)\smin\.*[)]'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from settings where name like '%s-89%' and length(value) > 100

#ifversion = 2017.11.0
update lmm_assignment set time_stamp = strftime('%s','now') where time_stamp < strftime('%s','2017-12-05T06:00','utc') and not exists(select value from settings where name = 'platform' and value = 'android' or value = 'ios')
#endversion

#ifversion = 2017.10.0
update lmm_assignment set time_stamp = strftime('%s','now') where time_stamp < strftime('%s','now') and not exists(select value from settings where name = 'platform' and value = 'android' or value = 'ios')
#endversion

#ifversion < 2017.10.0
update lmm_assignment set time_stamp = strftime('%s','now') where not exists(select value from settings where name = 'platform' and value = 'android' or value = 'ios')
#endversion

update lmm_schedule set time_stamp = strftime('%s','now') where talk_id > 7 and not exists(select value from settings where name = 'platform' and value = 'android' or value = 'ios')
update lmm_assignment set time_stamp = strftime('%s','now') where time_stamp < strftime('%s','2017-12-05T06:00','utc') and not exists(select value from settings where name = 'platform' and value = 'android' or value = 'ios')
update settings set value = 'true' where name = 'local_changes' and not exists(select value from settings where name = 'platform' and value = 'android' or value = 'ios')
