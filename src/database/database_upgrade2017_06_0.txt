ALTER TABLE territory_city RENAME TO territory_city_old;
CREATE TABLE territory_city (id INTEGER, city TEXT, lang_id INT, wkt_geometry VARCHAR, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT, PRIMARY KEY(id));
DELETE FROM territory_city;
INSERT INTO territory_city (id, city, lang_id, time_stamp, active, uuid) SELECT id, city, lang_id, time_stamp, active, uuid FROM territory_city_old;
DROP TABLE territory_city_old;

ALTER TABLE territory RENAME TO territory_old;
CREATE TABLE territory (id INTEGER, territory_number INTEGER, locality TEXT, city_id INTEGER, type_id INTEGER, priority INTEGER, remark VARCHAR, wkt_geometry VARCHAR, lang_id INTEGER, time_stamp INTEGER DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT, PRIMARY KEY(id));
DELETE FROM territory;
INSERT INTO territory (id, territory_number, locality, city_id, type_id, priority, lang_id, time_stamp, active, uuid) SELECT id, territory_number, locality, city_id, type_id, priority, lang_id, time_stamp, active, uuid FROM territory_old;
DROP TABLE territory_old;

CREATE TABLE spatial_ref_sys (srid INTEGER UNIQUE, auth_name TEXT, auth_srid TEXT, srtext TEXT);
CREATE TABLE geometry_columns (f_table_name VARCHAR, f_geometry_column VARCHAR, geometry_type INTEGER, coord_dimension INTEGER, srid INTEGER,     geometry_format VARCHAR );

INSERT INTO spatial_ref_sys (srid, auth_name, auth_srid, srtext) VALUES (4326, 'epsg', 4326, 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]');
INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, geometry_format) VALUES ('territory', 'wkt_geometry', 3, 2, 4326, 'WKT'), ('territory_city', 'wkt_geometry', 3, 2, 4326, 'WKT'), ('territories', 'wkt_geometry', 3, 2, 4326, 'WKT');

delete from lmm_workbookregex where lang = 'sw'
insert into lmm_workbookregex (lang, key, value) select 'sw', 'date1', '^(?<month>\D+)\s(?<fromday>\d+),*\s*\d*\d*\d*\d*\D*\s*(?<thruday>\d+),*\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'sw', 'song', 'Wimbo\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'sw', 'timing', '[(](?<timingextra>[^)]*)\s*dak.\s+(?<timing>\d+)[)]'
insert into lmm_workbookregex (lang, key, value) select 'sw', 'assignment1', '�(?<theme>[^�]+)�*(?<source>.*)'

delete from lmm_workbookregex where lang = 'nds'
insert into lmm_workbookregex (lang, key, value) select 'nds', 'date1', '^(?<fromday>\d+)[.]\s*(?<month>\D*)\s*\d*\d*\d*\d*\s*[-�]\s*(?<thruday>\d+)[.]*\s(?<month2>\D*)\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'nds', 'song', 'Leet\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'nds', 'timing', '[(](?<timingextra>[^)]*)(?<timing>\d+)\smin.[)]'
insert into lmm_workbookregex (lang, key, value) select 'nds', 'assignment1', '�(?<theme>[^�]+)�*(?<source>.*)'
