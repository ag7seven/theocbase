﻿delete from lmm_workbookregex where lang = 'lv'
insert into lmm_workbookregex (lang, key, value) select 'lv', 'date1', '^(?<fromday>\d+)[.]\s*(?<month>\D*)—(?<thruday>\d+)[.]\s(?<month1>\D*)'
insert into lmm_workbookregex (lang, key, value) select 'lv', 'song', '(\d+)[.]\sdziesma.+'
insert into lmm_workbookregex (lang, key, value) select 'lv', 'timing', '[(](?<timingextra>[^\d]*)(?<timing>\d+) min[)]'
insert into lmm_workbookregex (lang, key, value) select 'lv', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'yue-hans|CNS'
insert into lmm_workbookregex (lang, key, value) select 'yue-hans|CNS', 'date1', '^(?<month>\d+)月(?<fromday>\d+)-(?<thruday>\d+)日'
insert into lmm_workbookregex (lang, key, value) select 'yue-hans|CNS', 'song', '唱诗(\d+)首'
insert into lmm_workbookregex (lang, key, value) select 'yue-hans|CNS', 'timing', '（(?<timingextra>\D*)(?<timing>\d+)分钟）'
insert into lmm_workbookregex (lang, key, value) select 'yue-hans|CNS', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'yue-hant|CHC'
insert into lmm_workbookregex (lang, key, value) select 'yue-hant|CHC', 'date1', '^(?<month>\d+)月(?<fromday>\d+)-(?<thruday>\d+)日'
insert into lmm_workbookregex (lang, key, value) select 'yue-hant|CHC', 'song', '唱詩(\d+)首'
insert into lmm_workbookregex (lang, key, value) select 'yue-hant|CHC', 'timing', '（(?<timingextra>\D*)(?<timing>\d+)分鐘）'
insert into lmm_workbookregex (lang, key, value) select 'yue-hant|CHC', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'zh-cn|CHS'
insert into lmm_workbookregex (lang, key, value) select 'zh-cn|CHS', 'date1', '^(?<month>\d+\D)(?<fromday>\d+)日*-(?<thruday>\d+)'
insert into lmm_workbookregex (lang, key, value) select 'zh-cn|CHS', 'song', '唱诗第(\d+)首'
insert into lmm_workbookregex (lang, key, value) select 'zh-cn|CHS', 'timing', '[（]\D*(?<timing>\d+)\D*[）]'
insert into lmm_workbookregex (lang, key, value) select 'zh-cn|CHS', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'zh-Hant|CH'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant|CH', 'date1', '^(?<month>\d+)月(?<fromday>\d+)-(?<thruday>\d+)日'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant|CH', 'song', '唱詩第(\d+)首'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant|CH', 'timing', '[（]\D*(?<timing>\d+)\D*[）]'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant|CH', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'ja'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'date1', '^(?<month>\d+\D)月*(?<fromday>\d+)[‐–]\d*\d*\d*\d*年*(?<thruday>\d+)日'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'date2', '^\d*\d*\d*\d*年*(?<month>\d+\D)月*(?<fromday>\d+)日*[‐–]\d*\d*\d*\d*年*(?<thruday>\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'song', '(\d+)番の歌'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'timing', '（(?<timing>\d+)分(?<timingextra>\D*)）'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'gn'
insert into lmm_workbookregex (lang, key, value) select 'gn', 'date1', '^(?<fromday>\d+)-*(?<thruday>\d*)\sde\s(?<month>\D+).+$'
insert into lmm_workbookregex (lang, key, value) select 'gn', 'date2', '19-25 de diciembre'
insert into lmm_workbookregex (lang, key, value) select 'gn', 'song', 'Purahéi\s(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'gn', 'timing', '[(](?<timing>\d+)\smin.(?<timingextra>[^)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'gn', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'

DROP TABLE IF EXISTS territory_addresstype;
CREATE TABLE territory_addresstype (id INTEGER PRIMARY KEY, addresstype_number INTEGER, addresstype_name TEXT, color TEXT, lang_id INTEGER, time_stamp INTEGER DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DROP TABLE IF EXISTS territory_address;
CREATE TABLE territory_address (id INTEGER PRIMARY KEY, territory_id INTEGER, country TEXT, state TEXT, county TEXT, city TEXT, district TEXT, street TEXT, housenumber TEXT, postalcode TEXT, wkt_geometry TEXT, name TEXT, addresstype_number INTEGER, lang_id INTEGER, time_stamp INTEGER DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DROP VIEW IF EXISTS territoryaddresses;
CREATE VIEW territoryaddresses AS SELECT a.id, a.territory_id, t.territory_number, t.locality, a.country, a.state, a.county, a.city, a.district, a.street, a.housenumber, a.postalcode, a.wkt_geometry, a.name, a.addresstype_number, a_t.addresstype_name, a_t.color, a.lang_id FROM territory_address a LEFT JOIN territory_addresstype a_t ON a.addresstype_number = a_t.addresstype_number AND a_t.lang_id = (SELECT id FROM languages WHERE languages.code = (SELECT value FROM settings WHERE name = "theocbase_language") AND languages.active) LEFT JOIN territory t ON a.territory_id = t.id WHERE a.active;
INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, geometry_format) VALUES ('territory_address', 'wkt_geometry', 1, 2, 4326, 'WKT');
INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, geometry_format) VALUES ('territoryaddresses', 'wkt_geometry', 1, 2, 4326, 'WKT');

DROP VIEW IF EXISTS territories;
CREATE VIEW territories AS SELECT territory_city.city, territory.*, territory_type.type_name, assignedTerritories.person_id, persons.firstname || ' ' || persons.lastname publisher, CASE WHEN workedthrough.elapseddays <= 187 THEN 1 WHEN workedthrough.elapseddays <= 365 THEN 2 WHEN workedthrough.elapseddays > 365 THEN 3 END AS workedthrough, workedthrough.lastworked_date FROM territory LEFT JOIN territory_city ON territory.city_id = territory_city.id LEFT JOIN territory_type ON territory.type_id = territory_type.id LEFT JOIN (SELECT territory_id, person_id FROM territory_assignment WHERE territory_assignment.checkedbackin_date IS NULL AND territory_assignment.active) assignedTerritories ON territory.id = assignedTerritories.territory_id LEFT JOIN persons ON assignedTerritories.person_id = persons.id LEFT JOIN (SELECT territory_assignment.territory_id, julianday(datetime('now')) - julianday(max(territory_assignment.checkedbackin_date)) AS elapseddays, max(territory_assignment.checkedbackin_date) AS lastworked_date FROM territory_assignment WHERE territory_assignment.checkedbackin_date IS NOT NULL AND territory_assignment.active GROUP BY territory_assignment.territory_id) workedthrough ON territory.id = workedthrough.territory_id WHERE (territory_city.active OR territory_city.active IS NULL) AND (territory_type.active OR territory_type.active IS NULL) AND territory.active;

ALTER TABLE persons RENAME TO persons_old;
CREATE TABLE persons(id INTEGER PRIMARY KEY, firstname VARCHAR(255), lastname VARCHAR(255), servant BOOLEAN DEFAULT 0, elder BOOLEAN DEFAULT 0, publisher INT DEFAULT 2, gender CHAR(1) DEFAULT B, usefor INT, congregation_id INT, phone VARCHAR(255), phone2 VARCHAR(255), email VARCHAR(255), info VARCHAR(255), time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DELETE FROM persons;
INSERT INTO persons (id, firstname, lastname, servant, elder, publisher, gender, usefor, congregation_id, phone, phone2, email, info, time_stamp, active, uuid) SELECT id, firstname, lastname, servant, usefor & 147968 > 0, 2, gender, usefor, congregation_id, phone, '', email, info, time_stamp, active, uuid FROM persons_old;
DROP TABLE persons_old;

ALTER TABLE congregations RENAME TO congregations_old;
CREATE TABLE congregations(id INTEGER PRIMARY KEY, name VARCHAR(255), address VARCHAR(255), meeting1_time VARCHAR(255), circuit VARCHAR(255), info VARCHAR(255), talkcoordinator_id INT, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DELETE FROM congregations;
INSERT INTO congregations (id, name, address, meeting1_time, circuit, info, time_stamp, active, uuid) SELECT id, name, address, meeting1_time, circuit, info, time_stamp, active, uuid FROM congregations_old;
DROP TABLE congregations_old;
