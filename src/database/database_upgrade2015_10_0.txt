﻿#okNextError No query
CREATE TABLE lmm_schedule (id INTEGER PRIMARY KEY NOT NULL, date DATE, talk_id INT, theme TEXT, source TEXT, time INT, time_stamp INT DEFAULT (0), uuid TEXT);
#okNextError No query
CREATE TABLE lmm_meeting (id INTEGER PRIMARY KEY NOT NULL, date DATE, chairman INT, prayer_beginning INT DEFAULT (- 1), song_beginning INT, song_middle INT, song_end INT, prayer_end INT DEFAULT (- 1), bible_reading TEXT, time_stamp INT DEFAULT (0), active BOOLEAN DEFAULT (1), uuid TEXT);
#okNextError No query
CREATE TABLE lmm_assignment (id INTEGER PRIMARY KEY NOT NULL, lmm_schedule_id INT DEFAULT (- 1), classnumber INT DEFAULT (1), assignee_id INT DEFAULT (- 1), volunteer_id INT DEFAULT (- 1), assistant_id INT DEFAULT (- 1), date DATE, completed BOOLEAN DEFAULT (0), note VARCHAR (255), timing VARCHAR (255), setting VARCHAR(200), time_stamp INT DEFAULT (0), uuid TEXT);
#okNextError No query
CREATE TABLE lmm_workbookregex (lang varchar(20), key varchar(20), value varchar(20), names varchar(20))
delete from lmm_workbookregex 
INSERT INTO lmm_workbookregex VALUES ("en","song","Song\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("en","date","^(\D+)\s(\d+)-(\d+)$","month;fromday;thruday");
INSERT INTO lmm_workbookregex VALUES ("en","assignment1","([^:]+):([^()]*)[(](\d+)\smin.([^[)]*)[)](.*)","theme;source1;timing;timingextra;source2");
INSERT INTO lmm_workbookregex VALUES ("fi","date","^(\d+)[.]–(\d+)[.]\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("fi","song","Laulu\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("fi","assignment1","(”[^”]+”)([^()]*)[(]([^[)\d]*)(\d+)\smin[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("fi","assignment2","([^:()]+):*([^()]*)[(]([^[)\d]*)(\d+)\smin[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("de","date","^(\d+)[.]–(\d+)[.]\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("de","song","Lied\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("de","assignment1","(„[^“]+“)([^()]*)[(]([^[)\d]*)(\d+)\sMin.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("de","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\sMin.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("fr","date","^(\d+)-(\d+)\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("fr","song","Cantique\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("fr","assignment1","(«[^»]+»)([^()]*)[(]([^[)\d]*)(\d+)\smin[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("fr","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\smin([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("ru","date","^(\d+)—(\d+)\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("ru","song","Песня\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("ru","assignment1","(«[^»]+»)([^()]*)[(]([^[)\d]*)(\d+)\sмин[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("ru","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\sмин([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("nl","date","^(\d+)-(\d+)\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("nl","song","Lied\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("nl","assignment1","(‘[^’]+’)([^()]*)[(]([^[)\d]*)(\d+)\smin.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("nl","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\smin.([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("es","date","^(\d+)-(\d+)\sde\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("es","song","Canción\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("es","assignment1","(“[^”]+”)([^()]*)[(]([^[)\d]*)(\d+)\smins.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("es","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\smins.([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("pt","date","^(\d+)-(\d+)\sde\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("pt","song","Cântico\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("pt","assignment1","(“[^”]+”)([^()]*)[(]([^[)\d]*)(\d+)\smin[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("pt","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\smin([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("it","date","^(\d+)-(\d+)\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("it","song","Cantico\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("it","assignment1","(“[^”]+”)([^()]*)[(]([^[)\d]*)(\d+)\smin[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("it","assignment2","([^:()]+):*([^()]*)[(]([^)\d]*)(\d+)\smin([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("el","date","^(\d+)-(\d+)\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("el","song","Ύμνος\s(\d+)","");
INSERT INTO lmm_workbookregex VALUES ("el","assignment1","(«[^»]+»):*([^()]*)[(](\d+)\sλεπτά([^[)]*)[)](.*)","theme;source1;timing1;timingextra;source2");
INSERT INTO lmm_workbookregex VALUES ("el","assignment2","([^:()]+):*([^()]*)[(](\d+)\sλεπτά([^[)]*)[)](.*)","theme;source1;timing;timingextra;source2");
INSERT INTO lmm_workbookregex VALUES ("sv","date","^(\d+)–(\d+)\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("sv","song","Sång\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("sv","assignment1","(”[^”]+”)([^()]*)[(]([^[)\d]*)(\d+)\smin.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("sv","assignment2","([^:()]+):*([^()]*)[(]([^[)\d]*)(\d+)\smin.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("no","date","^(\d+)[.]–(\d+)[.]\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("no","song","Sang nr.\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("no","assignment1","(«[^«]+»)([^()]*)[(]([^[)\d]*)(\d+)\smin[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("no","assignment2","([^:()]+):*([^()]*)[(](\d+)\smin([^[)]*)[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("da","date","^(\d+)[.]-(\d+)[.]\s(\D+)$","fromday;thruday;month");
INSERT INTO lmm_workbookregex VALUES ("da","song","Sang nr.\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("da","assignment1","(“[^“]+”)([^()]*)[(]([^[)\d]*)(\d+)\smin.[)](.*)","theme;source1;timingextra;timing;source2");
INSERT INTO lmm_workbookregex VALUES ("da","assignment2","([^:()]+):*([^()]*)[(]([^[)\d]*)(\d+)\smin.[)](.*)","theme;source1;timingextra;timing;source2");
#okNextError No query
CREATE TABLE song (song_id int, title varchar(200))
#okNextError No query
CREATE VIEW personidlmmhistory_mt as select p.id,sh1.date,sh1.talk_id,sh1.classnumber,case when sh1.assignee_id is null then null when p.id = sh1.assignee_id then 0 else 1 end capacity_id from persons p left join ( SELECT lmm_assignment.assignee_id, lmm_assignment.assistant_id,lmm_assignment.volunteer_id,lmm_assignment.date, lmm_schedule.talk_id, lmm_assignment.classnumber FROM lmm_assignment INNER JOIN lmm_schedule ON lmm_assignment.lmm_schedule_id = lmm_schedule.id WHERE lmm_assignment.date IN (SELECT sss.date FROM lmm_schedule sss WHERE sss.date NOT IN (SELECT ss.date FROM lmm_schedule AS ss, exceptions AS e WHERE e.schoolday = 0 AND 0 <= julianday(e.date) - julianday(ss.date) AND julianday(e.date) - julianday(ss.date) < 7 and e.active))) sh1 ON (p.id = sh1.assignee_id and sh1.volunteer_id = -1 or sh1.assistant_id = p.id) where p.active;
