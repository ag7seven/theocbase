/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPERSONS_H
#define CPERSONS_H

#include <QMessageBox>
#include <QList>
#include "person.h"
#include "sql_class.h"

/**
 * @brief The cpersons class - This a base class for handling persons.
 *        You can get list of existing persons, add a new one or remove.
 */
class cpersons : public QObject
{
    Q_OBJECT
private:
    static person *createPerson(const sql_item &item, sql_class *sql);

public:
    /**
     * @brief cpersons - Constructor
     */
    cpersons(QObject *parent = nullptr);

    person *test();

    /**
     * @brief getAllPersons - Get all persons from database
     * @param type - 0 = All persons from current congregation
     *               1 = Only public talk speakers
     *               2 = Only public talk speakers in own congregation
     *               3 = All persons who may share in public meeting
     * @return - List of persons
     */
    QList<person *> getAllPersons(int type);

    /**
     * @brief addPerson - Add a new person to database
     * @param p - Filled person object
     * @return - New storage id or -1 if failure
     */
    static int addPerson(person *p);

    /**
     * @brief getPersons - Get all person objects by name
     * @param name - Name of person
     * @param format - Name format. Default id FirstName LastName (eg Joe Love).
     *                 Format options are:
     *                  + FirstName LastName (eg Joe Love)
     *                  + LastName FirstName (eg Love Joe)
     *                  + LastName, FirstName (eg Love, Joe)
     *                  + Partial (name can be a partial first name, a partial last name, or partial first and partial last)
     * @param congregationId - if > 0, makes sure persons comes from this congregation
     * @param publicSpeakersOnly - if true, makes sure persons are public speakers
     * @return - person objects (can be empty)
     */
    static QList<person *> getPersons(QString name, QString format = "FirstName LastName", int congregationId = 0, bool publicSpeakersOnly = false);

    /**
     * @brief getPerson - Get a person object by name
     * @param name - Name of person
     * @param format - Name format. Default id FirstName LastName (eg Joe Love).
     *                 Format options are:
     *                  + FirstName LastName (eg Joe Love)
     *                  + LastName FirstName (eg Love Joe)
     *                  + LastName, FirstName (eg Love, Joe)
     *                  + Partial (name can be a partial first name, a partial last name, or partial first and partial last)
     * @param congregationId - if > 0, makes sure person comes from this congregation
     * @return - person object or 0 if not exist
     */
    Q_INVOKABLE static person *getPerson(QString name, QString format = "FirstName LastName", int congregationId = 0, bool publicSpeakersOnly = false);

    /**
     * @brief getPerson - Get a person object by email
     * @param email - Email of person
     * @return - person object or 0 if not exist
     */
    Q_INVOKABLE static person *getPersonByEmail(QString email, int congregationId = 0);

    /**
     * @brief getPerson - Get a person object (public speaker) by name
     * @param name - Name of person
     * @param format - Name format. Default id FirstName LastName (eg Joe Love).
     *                 Format options are:
     *                  + FirstName LastName (eg Joe Love)
     *                  + LastName FirstName (eg Love Joe)
     *                  + LastName, FirstName (eg Love, Joe)
     *                  + Partial (name can be a partial first name, a partial last name, or partial first and partial last)
     * @param congregationId - if > 0, makes sure person comes from this congregation
     * @return - person object or 0 if not exist
     */
    static person *getPublicSpeaker(QString name, QString format = "FirstName LastName", int congregationId = 0);

    /**
     * @brief getPerson - Get a person object by id
     * @param id - Id in database
     * @return - person object or 0 if not exist
     */
    Q_INVOKABLE static person *getPerson(int id);

    /**
     * @brief removePerson - Remove person from the database
     * @param id - Person's id in the database
     * @return - success or failure
     */
    static bool removePerson(int id);
};

#endif // CPERSONS_H
