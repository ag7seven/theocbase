#ifndef MACHELPER_H
#define MACHELPER_H

#include <QObject>
#include <QMacToolBar>
#include <QWindow>

class MacHelper : public QObject
{
    Q_OBJECT

public:
    static void initToolbar(QMacToolBar *toolbar);
    static void colorizeTitleBar(QWindow *qtwindow);
private:

};

#endif // MACHELPER_H
