#include "dateeditbox.h"

DateEditBox::DateEditBox(QWidget *parent, QString dateFormat) : QItemDelegate(parent)
{
    mClearValue = false;
    mDateFormat = dateFormat;
}

QWidget *DateEditBox::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    Q_UNUSED(option);
    QWidget *editorWidget = new QWidget(parent);
    QHBoxLayout *layout = new QHBoxLayout(editorWidget);
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(0);
    QDateEdit *dateEdit = new QDateEdit(editorWidget);
    dateEdit->setDisplayFormat(mDateFormat);
    QToolButton *button = new QToolButton(editorWidget);
    connect(button,&QToolButton::clicked,this,&DateEditBox::closeAndClearEditor);
    button->setText("-");
    button->setMaximumWidth(15);
    layout->addWidget(dateEdit);
    layout->addWidget(button);
    return editorWidget;
}

void DateEditBox::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QDate value = index.model()->data(index,DateRole).toDate();
    if (!value.isValid() && index.model()->data(index, Qt::EditRole).toString() != "")
        value = QDate::fromString(index.model()->data(index, Qt::EditRole).toString(), mDateFormat);
    QDateEdit *dateEdit = static_cast<QDateEdit*>(editor->layout()->itemAt(0)->widget());
    dateEdit->setDate(value);
}

void DateEditBox::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QDateEdit *dateEdit = static_cast<QDateEdit*>(editor->layout()->itemAt(0)->widget());
    dateEdit->interpretText();
    QDate value = dateEdit->date();
    if (mClearValue)
        value = QDate();
    QMap<int,QVariant> roles;
    roles[DateRole] = value;
    roles[Qt::DisplayRole] = value.isValid() ? value.toString(mDateFormat) : "";
    model->setItemData(index,roles);
}

void DateEditBox::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
}

void DateEditBox::closeAndClearEditor()
{
    QToolButton *button = qobject_cast<QToolButton *>(sender());
    QWidget *editor = button->parentWidget();
    mClearValue = true;
    emit commitData(editor);
    emit closeEditor(editor);
    mClearValue = false;
}
