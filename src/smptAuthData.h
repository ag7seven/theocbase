/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2013, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMPTAUTHDATA_H
#define SMPTAUTHDATA_H

#include <QString>

class SmtpAuthData
{

public:
    struct Data
    {
        Data();
        QString email;
        QString login;
        QString name;
        QString password;
        QString port;
        bool enableSsl;

    };

private:
    Data data;
    int id;


public:
    SmtpAuthData();
    // set allmost all data in smtpAuth
    void rewriteData(const Data& newData);
    // return smtpAuthData
    const Data& get_SmtpAuth() const { return data; }

    const QString& get_email() const { return data.email; }
    const QString& get_name() const { return data.name; }
    const QString& get_port() const { return data.port; }

    int get_id() const { return id; }
    void set_id(int rowId) { id = rowId; }

    bool isEmpty() const;


};

#endif // SMPTAUTHDATA_H
