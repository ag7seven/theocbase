/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "territoryassignment.h"
#include <QDebug>

TerritoryAssignment::TerritoryAssignment(int territoryId)
    : m_id(0), m_territoryId(territoryId), m_personId(0), m_publisher(""), m_isDirty(false)
{
}

TerritoryAssignment::TerritoryAssignment(const int id, const int territoryId, const int personId, const QString publisher, const QDate checkedOutDate, const QDate checkedBackInDate)
    : m_id(id), m_territoryId(territoryId), m_personId(personId), m_publisher(publisher), m_checkedOutDate(checkedOutDate), m_checkedBackInDate(checkedBackInDate), m_isDirty(false)
{
}

int TerritoryAssignment::id() const
{
    return m_id;
}

int TerritoryAssignment::territoryId() const
{
    return m_territoryId;
}

int TerritoryAssignment::personId() const
{
    return m_personId;
}

void TerritoryAssignment::setPersonId(const int value)
{
    if (m_personId != value) {
        m_personId = value;

        cpersons persons;
        person *publisher = persons.getPerson(m_personId);
        if (publisher)
            m_publisher = publisher->fullname();

        setIsDirty(true);
    }
}

QString TerritoryAssignment::publisher() const
{
    return m_publisher;
}

//void TerritoryRecordItem::setPublisher(const QString &value)
//{
//    publisher = value;
//}

QDate TerritoryAssignment::checkedOutDate() const
{
    return m_checkedOutDate;
}

void TerritoryAssignment::setCheckedOutDate(const QDate value)
{
    if (m_checkedOutDate != value) {
        m_checkedOutDate = value;
        setIsDirty(true);
    }
}

QDate TerritoryAssignment::checkedBackInDate() const
{
    return m_checkedBackInDate;
}

void TerritoryAssignment::setCheckedBackInDate(const QDate value)
{
    if (m_checkedBackInDate != value) {
        m_checkedBackInDate = value;
        setIsDirty(true);
    }
}

void TerritoryAssignment::setIsDirty(bool value)
{
    m_isDirty = value;
}

bool TerritoryAssignment::save()
{
    if (personId() < 1)
        return false;

    // save changes to database
    sql_class *sql = &Singleton<sql_class>::Instance();

    int lang_id = sql->getLanguageDefaultId();

    sql_item queryitems;
    queryitems.insert(":id", m_id);
    int assignmentId = m_id > 0 ? sql->selectScalar("SELECT id FROM territory_assignment WHERE id = :id", &queryitems, -1).toInt() : -1;

    sql_item insertItems;
    insertItems.insert("territory_id", territoryId());
    insertItems.insert("person_id", personId());
    insertItems.insert("checkedout_date", checkedOutDate());
    insertItems.insert("checkedbackin_date", checkedBackInDate());
    insertItems.insert("lang_id", lang_id);

    bool ret = false;
    if (assignmentId > 0) {
        // update
        ret = sql->updateSql("territory_assignment", "id", QString::number(assignmentId), &insertItems);
    } else {
        // insert new row
        int newId = sql->insertSql("territory_assignment", &insertItems, "id");
        ret = newId > 0;
        if (newId > 0)
            m_id = newId;
    }
    setIsDirty(!ret);

    return ret;
}

TerritoryAssignmentModel::TerritoryAssignmentModel()
{
}

TerritoryAssignmentModel::TerritoryAssignmentModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QHash<int, QByteArray> TerritoryAssignmentModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[AssignmentIdRole] = "id";
    items[PersonIdRole] = "person_id";
    items[PublisherRole] = "publisher";
    items[CheckedOutRole] = "checkedOutDate";
    items[CheckedBackInRole] = "checkedBackInDate";
    return items;
}

int TerritoryAssignmentModel::rowCount(const QModelIndex & /*parent*/) const
{
    return territoryAssignments.count();
}

int TerritoryAssignmentModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 5;
}

QVariantMap TerritoryAssignmentModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QVariant TerritoryAssignmentModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > territoryAssignments.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return territoryAssignments[index.row()].id();
        case 1:
            return territoryAssignments[index.row()].personId();
        case 2:
            return territoryAssignments[index.row()].publisher();
        case 3:
            return QDateTime(territoryAssignments[index.row()].checkedOutDate());
        case 4:
            return QDateTime(territoryAssignments[index.row()].checkedBackInDate());
        }
    }

    switch (role) {
    case AssignmentIdRole:
        return territoryAssignments[index.row()].id();
    case PersonIdRole:
        return territoryAssignments[index.row()].personId();
    case PublisherRole:
        return territoryAssignments[index.row()].publisher();
    case CheckedOutRole:
        return QDateTime(territoryAssignments[index.row()].checkedOutDate());
    case CheckedBackInRole:
        return QDateTime(territoryAssignments[index.row()].checkedBackInDate());
    }

    return QVariant();
}

bool TerritoryAssignmentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TerritoryAssignment *currTerritoryAssignment = &territoryAssignments[index.row()];

    if (role == Qt::EditRole) {
        int column = index.column();

        switch (column) {
        case 0:
            //assignmentId
            break;
        case 1:
            currTerritoryAssignment->setPersonId(value.toInt());
            break;
        case 2:
            currTerritoryAssignment->setPersonId(value.toInt());
            break;
        case 3: {
            QString dateFormat = QLocale().dateFormat(QLocale::ShortFormat);
            QDate date = QDate::fromString(value.toString(), dateFormat);
            if (!dateFormat.contains("yyyy"))
                date = date.addYears(99).year() > QDate::currentDate().year() ? date : date.addYears(100);
            currTerritoryAssignment->setCheckedOutDate(date);
            break;
        }
        case 4: {
            QString dateFormat = QLocale().dateFormat(QLocale::ShortFormat);
            QDate date = QDate::fromString(value.toString(), dateFormat);
            if (!dateFormat.contains("yyyy"))
                date = date.addYears(99).year() > QDate::currentDate().year() ? date : date.addYears(100);
            currTerritoryAssignment->setCheckedBackInDate(date);
            break;
        }
        default:
            break;
        }

        emit editCompleted();
    } else {
        switch (role) {
        case Roles::PersonIdRole:
            currTerritoryAssignment->setPersonId(value.toInt());
            emit dataChanged(this->index(index.row(), 1), this->index(index.row(), 2));
            break;
        case Roles::CheckedOutRole:
            currTerritoryAssignment->setCheckedOutDate(value.toDate());
            emit dataChanged(this->index(index.row(), 3), this->index(index.row(), 3));
            break;
        case Roles::CheckedBackInRole:
            currTerritoryAssignment->setCheckedBackInDate(value.toDate());
            emit dataChanged(this->index(index.row(), 4), this->index(index.row(), 4));
            break;
        default:
            break;
        }
    }

    return true;
}

Qt::ItemFlags TerritoryAssignmentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

QModelIndex TerritoryAssignmentModel::index(int row, int column, const QModelIndex &parent)
        const
{
    if (hasIndex(row, column, parent)) {
        return createIndex(row, column);
    }
    return QModelIndex();
}

QModelIndex TerritoryAssignmentModel::getAssignmentIndex(int assignmentId) const
{
    for (int row = 0; row < this->rowCount(); ++row) {
        QModelIndex rowIndex = this->index(row, 0);
        if (rowIndex.data(AssignmentIdRole) == assignmentId)
            return rowIndex;
    }
    return QModelIndex();
}

bool TerritoryAssignmentModel::addAssignment(const TerritoryAssignment &territoryAssignment)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    territoryAssignments << territoryAssignment;
    endInsertRows();
    return true;
}

bool TerritoryAssignmentModel::addAssignment(int territoryId)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    TerritoryAssignment *newTerritoryAssignment = new TerritoryAssignment(territoryId);

    if (!territoryAssignments.isEmpty()) {
        // sort by checked out date
        std::sort(territoryAssignments.begin(), territoryAssignments.end(),
                  [](TerritoryAssignment const &a, TerritoryAssignment const &b) {
                      return a.checkedOutDate() < b.checkedOutDate();
                  });

        TerritoryAssignment *lastAssignment = &territoryAssignments.last();
        if (!lastAssignment->checkedBackInDate().isValid()) {
            QModelIndex lastAssignmentIndex = this->index(territoryAssignments.count() - 1, 0);
            this->setData(lastAssignmentIndex, QDate::currentDate(), Roles::CheckedBackInRole);
        }

        newTerritoryAssignment->setPersonId(lastAssignment->personId());
        newTerritoryAssignment->setCheckedOutDate(lastAssignment->checkedBackInDate().addDays(1));
    }

    territoryAssignments << *newTerritoryAssignment;
    endInsertRows();
    return true;
}

void TerritoryAssignmentModel::removeAssignment(int id)
{
    if (id > 0) {
        sql_class *sql = &Singleton<sql_class>::Instance();

        sql_item s;
        s.insert(":id", id);
        s.insert(":active", 0);
        if (sql->execSql("UPDATE territory_assignment SET active = :active WHERE id = :id", &s, true)) {
            QModelIndex assignmentIndex = getAssignmentIndex(id);
            if (assignmentIndex.isValid()) {
                int row = assignmentIndex.row();
                beginRemoveRows(QModelIndex(), row, row);
                territoryAssignments.erase(std::next(territoryAssignments.begin(), row));
                endRemoveRows();
            }
        }
    }
}

bool TerritoryAssignmentModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    if (row < 0 || count < 1 || (row + count) > territoryAssignments.size())
        return false;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    for (int i = 0; i < count; i++) {
        territoryAssignments.removeAt(row);
    }
    endRemoveRows();
    return true;
}

void TerritoryAssignmentModel::loadAssignments(int territoryId)
{
    removeRows(0, territoryAssignments.count());
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_items territoryAssignmentRows = sql->selectSql("SELECT * FROM territoryassignments WHERE territory_id = " + QVariant(territoryId).toString() + " ORDER BY checkedout_date");

    if (!territoryAssignmentRows.empty()) {
        for (unsigned int i = 0; i < territoryAssignmentRows.size(); i++) {
            sql_item s = territoryAssignmentRows[i];

            addAssignment(TerritoryAssignment(s.value("id").toInt(),
                                              s.value("territory_id").toInt(),
                                              s.value("person_id").toInt(),
                                              s.value("firstname").toString() + " " + s.value("lastname").toString(),
                                              s.value("checkedout_date").toDate(),
                                              s.value("checkedbackin_date").toDate()));
        }
    }
}

void TerritoryAssignmentModel::saveAssignments()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();
    for (int i = 0; i < territoryAssignments.count(); i++) {
        TerritoryAssignment t = territoryAssignments[i];
        if (t.isDirty()) {
            t.save();
        }
    }
    sql->commitTransaction();
}

SortFilterProxyModel *TerritoryAssignmentModel::getPublisherList()
{
    QHash<int, QByteArray> roles;
    roles[MySortFilterProxyModel::MyRoles::id] = "id";
    roles[MySortFilterProxyModel::MyRoles::name] = "name";
    roles[MySortFilterProxyModel::MyRoles::date] = "date";
    roles[MySortFilterProxyModel::MyRoles::icon] = "icon";

    QStandardItemModel *itemmodel = new QStandardItemModel(0, 3, this);
    itemmodel->setItemRoleNames(roles);
    itemmodel->setHorizontalHeaderItem(0, new QStandardItem("Id"));
    itemmodel->setHorizontalHeaderItem(1, new QStandardItem("Name"));
    itemmodel->setHorizontalHeaderItem(2, new QStandardItem("Date"));
    itemmodel->setHorizontalHeaderItem(3, new QStandardItem("Icon"));

    sql_class *sql = &Singleton<sql_class>::Instance();
    QString w("SELECT firstname"
              ",lastname"
              ",id"
              ",1000000 date_offset"
              ",NULL date "
              "FROM persons "
              "WHERE active AND (congregation_id IS NULL OR congregation_id = %1);");
    w.replace("%1", sql->getSetting("congregation_id"));
    sql_items items = sql->selectSql(w);

    // empty row
    itemmodel->setRowCount(1);

    for (unsigned int i = 0; i < items.size(); i++) {
        QStandardItem *item = new QStandardItem();
        item->setData(items[i].value("id").toInt(), MySortFilterProxyModel::MyRoles::id);
        item->setData(items[i].value("firstname").toString() + " " + items[i].value("lastname").toString(), MySortFilterProxyModel::MyRoles::name);
        item->setData(items[i].value("date").toDate().toString(Qt::DefaultLocaleShortDate), MySortFilterProxyModel::MyRoles::date);
        item->setData(items[i].value("firstname"), MySortFilterProxyModel::MyRoles::h_firstname);
        item->setData(items[i].value("lastname"), MySortFilterProxyModel::MyRoles::h_lastname);
        item->setData(items[i].value("date_offset"), MySortFilterProxyModel::MyRoles::h_offset);

        itemmodel->setRowCount(itemmodel->rowCount() + 1);
        itemmodel->setItem(i + 1, item);
    }
    SortFilterProxyModel *sortmodel = new MySortFilterProxyModel(this, itemmodel, MySortFilterProxyModel::MyRoles::name, "brother");
    return sortmodel;
}

bool TerritoryAssignmentModel::MySortFilterProxyModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    int row1 = source_left.row();
    int row2 = source_right.row();

    if (row1 == 0 || row2 == 0)
        return row1 < row2;
    bool ret = false;

    switch (this->sortRole()) {
    case MySortFilterProxyModel::MyRoles::name:
        break;
    case MySortFilterProxyModel::MyRoles::date: {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_offset);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_offset);
        float f1 = val1.toFloat();
        float f2 = val2.toFloat();
        if (f1 > f2)
            return false;
        ret = f1 < f2;
        break;
    }
    default:
        return SortFilterProxyModel::lessThan(source_left, source_right);
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_lastname);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_lastname);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_firstname);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_firstname);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::id);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::id);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    return ret;
}
