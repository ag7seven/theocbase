<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>TheocBase</source>
        <translation>TheocBase</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1143"/>
        <source>Subject</source>
        <translation>Subject</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="219"/>
        <location filename="mainwindow.cpp" line="220"/>
        <source>(No meeting)</source>
        <translation>(No meeting)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="227"/>
        <source>No meeting</source>
        <translation>No meeting</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <location filename="mainwindow.cpp" line="295"/>
        <source>Nothing to display</source>
        <translation>Nothing to display</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Prayer:</source>
        <translation>Prayer:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Conductor:</source>
        <translation>Conductor:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>Reader:</source>
        <translation>Reader:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="409"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="410"/>
        <source>TheocBase team</source>
        <translation>TheocBase team</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="412"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Qt libraries licensed under the GPL.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <location filename="mainwindow.cpp" line="770"/>
        <source>TheocBase data exchange</source>
        <translation>TheocBase data exchange</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Hello</source>
        <translation>Hello</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Best Regards</source>
        <translation>Best Regards</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="688"/>
        <source>Transfer file created.</source>
        <translation>Transfer file created.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="690"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="691"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="702"/>
        <source>Enter e-mail address.</source>
        <translation>Enter e-mail address.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="712"/>
        <source>Save file</source>
        <translation>Save file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="723"/>
        <source>Saved.</source>
        <translation>Saved.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="734"/>
        <source>Error sending e-mail</source>
        <translation>Error sending e-mail</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="736"/>
        <source>E-mail sent successfully</source>
        <translation>E-mail sent successfully</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="745"/>
        <source>Open file</source>
        <translation>Open file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="760"/>
        <source>Save unsaved data?</source>
        <translation>Save unsaved data?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="768"/>
        <source>Import file?</source>
        <translation>Import file?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="820"/>
        <source>Subject is empty!</source>
        <translation>Subject is empty!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="826"/>
        <source>Description is empty!</source>
        <translation>Description is empty!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="832"/>
        <source>E-mail field is empty!</source>
        <translation>E-mail field is empty!</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="761"/>
        <location filename="mainwindow.ui" line="1018"/>
        <location filename="mainwindow.ui" line="1054"/>
        <location filename="mainwindow.ui" line="1065"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="731"/>
        <source>School assignment</source>
        <translation>School assignment</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <source>Congregation Bible Study:</source>
        <translation>Congregation Bible Study:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <source>Theocratic Ministry School:</source>
        <translation>Theocratic Ministry School:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="298"/>
        <source>Service Meeting:</source>
        <translation>Service Meeting:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="630"/>
        <location filename="mainwindow.ui" line="990"/>
        <source>Congregation Bible Study</source>
        <translation>Congregation Bible Study</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <location filename="mainwindow.ui" line="969"/>
        <source>Theocratic Ministry School</source>
        <translation>Theocratic Ministry School</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <location filename="mainwindow.ui" line="997"/>
        <source>Service Meeting</source>
        <translation>Service Meeting</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="741"/>
        <location filename="mainwindow.ui" line="807"/>
        <location filename="mainwindow.ui" line="1082"/>
        <location filename="mainwindow.ui" line="1127"/>
        <source>&lt;--</source>
        <translation>&lt;--</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="857"/>
        <location filename="mainwindow.ui" line="902"/>
        <location filename="mainwindow.ui" line="931"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="866"/>
        <location filename="mainwindow.ui" line="1173"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="872"/>
        <source>Recipient:</source>
        <translation>Recipient:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="886"/>
        <source>Subject: </source>
        <translation>Subject: </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="909"/>
        <source>Territories</source>
        <translation>Territories</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="919"/>
        <source>Students</source>
        <translation>Students</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="944"/>
        <source>previous weeks</source>
        <translation>previous weeks</translation>
    </message>
    <message>
        <source>
      </source>
        <translation type="obsolete">
      </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="976"/>
        <source>Public talks</source>
        <translation>Public talks</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1012"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1046"/>
        <source>info</source>
        <translation>info</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1093"/>
        <location filename="mainwindow.ui" line="1453"/>
        <source>Data exhange</source>
        <translation>Data exhange</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1114"/>
        <source>Report bug</source>
        <translation>Report bug</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1153"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1166"/>
        <location filename="mainwindow.cpp" line="689"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1268"/>
        <source>Timeline</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1320"/>
        <source>Print...</source>
        <translation>Print...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1323"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1352"/>
        <source>Publishers...</source>
        <translation>Publishers...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1355"/>
        <source>Publishers</source>
        <translation>Publishers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1422"/>
        <source>Speakers...</source>
        <translation>Speakers...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1425"/>
        <source>Speakers</source>
        <translation>Speakers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1450"/>
        <source>Data exhange...</source>
        <translation>Data exhange...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1462"/>
        <source>TheocBase help...</source>
        <translation>TheocBase help...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1467"/>
        <source>Meetings for field ministry</source>
        <translation>Meetings for field ministry</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1479"/>
        <source>History</source>
        <translation>History</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1491"/>
        <location filename="mainwindow.ui" line="1494"/>
        <source>Full Screen</source>
        <translation>Full Screen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="772"/>
        <source>Organizing public talks</source>
        <translation>Organizing public talks</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1209"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1215"/>
        <source>Tools</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1227"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1364"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1376"/>
        <source>Report bug...</source>
        <translation>Report bug...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1388"/>
        <source>Send feedback...</source>
        <translation>Send feedback...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1407"/>
        <source>About TheocBase...</source>
        <translation>About TheocBase...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1435"/>
        <source>Check updates...</source>
        <translation>Check updates...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1287"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1299"/>
        <source>Next</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1308"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1340"/>
        <source>Settings...</source>
        <oldsource>Settings ...</oldsource>
        <translation>Settings...</translation>
    </message>
    <message>
        <source>Students ...</source>
        <translation type="obsolete">Students ...</translation>
    </message>
    <message>
        <source>Report bug ...</source>
        <translation type="obsolete">Report bug ...</translation>
    </message>
    <message>
        <source>Send feedback</source>
        <translation type="obsolete">Send feedback</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1397"/>
        <source>TheocBase website</source>
        <oldsource>TheocBase website</oldsource>
        <translation>TheocBase website</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <source>(Circuit overseer visit)</source>
        <translation>(Circuit overseer visit)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Convention week (no meeting) </source>
        <translation>Convention week (no meeting) </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="209"/>
        <location filename="mainwindow.cpp" line="214"/>
        <location filename="mainwindow.cpp" line="226"/>
        <location filename="mainwindow.cpp" line="229"/>
        <location filename="mainwindow.cpp" line="236"/>
        <source>Public Talk:</source>
        <translation>Public Talk:</translation>
    </message>
    <message>
        <source>Tehtävän suorittajaa ei ole määritetty!</source>
        <translation type="obsolete">Tehtävän suorittajaa ei ole määritetty!</translation>
    </message>
</context>
<context>
    <name>PublicTalks</name>
    <message>
        <location filename="publictalks.cpp" line="33"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="34"/>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="35"/>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="36"/>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="37"/>
        <source>Reader</source>
        <translation>Reader</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="144"/>
        <source>Convention week (no meeting) </source>
        <translation>Convention week (no meeting) </translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="151"/>
        <source>No meeting</source>
        <translation>No meeting</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="476"/>
        <source>Theme:</source>
        <translation>Theme:</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="477"/>
        <source>Speaker:</source>
        <translation>Speaker:</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="478"/>
        <source>Chairman:</source>
        <translation>Chairman:</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="479"/>
        <source>Reader:</source>
        <translation>Reader:</translation>
    </message>
    <message>
        <location filename="publictalks.cpp" line="506"/>
        <source>Show Details...</source>
        <translation>Show Details...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="931"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location filename="main.cpp" line="961"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="main.cpp" line="1031"/>
        <source>Wrong usernam and/or password</source>
        <translation>Wrong usernam and/or password</translation>
    </message>
    <message>
        <location filename="main.cpp" line="1651"/>
        <source>Database not found!</source>
        <translation>Database not found!</translation>
    </message>
    <message>
        <location filename="main.cpp" line="1681"/>
        <source>Choose database</source>
        <translation>Choose database</translation>
    </message>
    <message>
        <location filename="main.cpp" line="1681"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>SQLite files (*.sqlite)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="1891"/>
        <source>Database restoring failed</source>
        <translation>Database restoring failed</translation>
    </message>
    <message>
        <location filename="main.cpp" line="2141"/>
        <location filename="mainwindow.cpp" line="181"/>
        <source>Save changes?</source>
        <translation>Save changes?</translation>
    </message>
    <message>
        <location filename="main.cpp" line="1611"/>
        <source>Database copied to </source>
        <translation>Database copied to </translation>
    </message>
    <message>
        <location filename="sql_class.cpp" line="21"/>
        <source>Database file not found! Searching path =</source>
        <translation>Database file not found! Searching path =</translation>
    </message>
    <message>
        <location filename="sql_class.cpp" line="35"/>
        <source>Database Error</source>
        <translation>Database Error</translation>
    </message>
    <message>
        <location filename="sql_class.cpp" line="458"/>
        <location filename="sql_class.cpp" line="459"/>
        <source>Congregation</source>
        <translation>Congregation</translation>
    </message>
    <message>
        <location filename="sql_class.cpp" line="484"/>
        <source>Database updated</source>
        <translation>Database updated</translation>
    </message>
    <message>
        <location filename="cpersoncombobox.cpp" line="93"/>
        <location filename="publictalks.cpp" line="263"/>
        <location filename="school.cpp" line="257"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="cpersoncombobox.cpp" line="94"/>
        <location filename="publictalks.cpp" line="264"/>
        <source>Last</source>
        <translation>Last</translation>
    </message>
    <message>
        <location filename="school.cpp" line="258"/>
        <source>Last (all)</source>
        <translation>Last (all)</translation>
    </message>
    <message>
        <location filename="school.cpp" line="259"/>
        <source>Last (selected)</source>
        <translation>Last (selected)</translation>
    </message>
    <message>
        <location filename="school_item.h" line="34"/>
        <source>Bible highlights:</source>
        <translation>Bible highlights:</translation>
    </message>
    <message>
        <location filename="school_item.h" line="37"/>
        <source>No. 1:</source>
        <translation>No. 1:</translation>
    </message>
    <message>
        <location filename="school_item.h" line="40"/>
        <source>No. 2:</source>
        <translation>No. 2:</translation>
    </message>
    <message>
        <location filename="school_item.h" line="43"/>
        <source>No. 3:</source>
        <translation>No. 3:</translation>
    </message>
    <message>
        <location filename="school_item.h" line="46"/>
        <source>Reader:</source>
        <translation>Reader:</translation>
    </message>
    <message>
        <location filename="csync.cpp" line="668"/>
        <location filename="generatexml.cpp" line="66"/>
        <source>Default language not selected!</source>
        <translation>Default language not selected!</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="119"/>
        <source>Confirm password!</source>
        <translation>Confirm password!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="settings.ui" line="109"/>
        <location filename="settings.ui" line="112"/>
        <source>Time/Date</source>
        <translation>Time/Date</translation>
    </message>
    <message>
        <location filename="settings.ui" line="129"/>
        <location filename="settings.ui" line="132"/>
        <location filename="settings.ui" line="630"/>
        <source>Exceptions</source>
        <translation>Exceptions</translation>
    </message>
    <message>
        <location filename="settings.ui" line="159"/>
        <location filename="settings.ui" line="162"/>
        <location filename="settings.ui" line="1116"/>
        <source>Public Talks</source>
        <translation>Public Talks</translation>
    </message>
    <message>
        <location filename="settings.ui" line="174"/>
        <source>Printing</source>
        <translation>Printing</translation>
    </message>
    <message>
        <location filename="settings.ui" line="94"/>
        <location filename="settings.ui" line="97"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1311"/>
        <source>Theocratic ministry school</source>
        <translation>Theocratic ministry school</translation>
    </message>
    <message>
        <location filename="settings.ui" line="275"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="settings.ui" line="281"/>
        <source>Database backup</source>
        <translation>Database backup</translation>
    </message>
    <message>
        <location filename="settings.ui" line="313"/>
        <source>Save as...</source>
        <translation>Save as...</translation>
    </message>
    <message>
        <location filename="settings.ui" line="291"/>
        <source>Restore backup database</source>
        <translation>Restore backup database</translation>
    </message>
    <message>
        <location filename="settings.ui" line="144"/>
        <location filename="settings.ui" line="147"/>
        <source>Theocratic Ministry School</source>
        <translation>Theocratic Ministry School</translation>
    </message>
    <message>
        <location filename="settings.ui" line="199"/>
        <location filename="settings.cpp" line="973"/>
        <source>Congregation</source>
        <translation>Congregation</translation>
    </message>
    <message>
        <location filename="settings.ui" line="205"/>
        <source>Current congregation</source>
        <translation>Current congregation</translation>
    </message>
    <message>
        <location filename="settings.ui" line="215"/>
        <source>Assign a Prayer</source>
        <translation>Assign a Prayer</translation>
    </message>
    <message>
        <location filename="settings.ui" line="222"/>
        <source>Use the same reader throughout the week</source>
        <translation>Use the same reader throughout the week</translation>
    </message>
    <message>
        <location filename="settings.ui" line="232"/>
        <source>User interface</source>
        <translation>User interface</translation>
    </message>
    <message>
        <location filename="settings.ui" line="241"/>
        <source>User interface language</source>
        <translation>User interface language</translation>
    </message>
    <message>
        <location filename="settings.ui" line="301"/>
        <source>Restore file...</source>
        <translation>Restore file...</translation>
    </message>
    <message>
        <location filename="settings.ui" line="344"/>
        <source>Security</source>
        <translation>Security</translation>
    </message>
    <message>
        <location filename="settings.ui" line="352"/>
        <source>Enable password</source>
        <translation>Enable password</translation>
    </message>
    <message>
        <location filename="settings.ui" line="362"/>
        <source>Enable database encryption</source>
        <translation>Enable database encryption</translation>
    </message>
    <message>
        <location filename="settings.ui" line="373"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location filename="settings.ui" line="383"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="settings.ui" line="404"/>
        <source>Confirm</source>
        <translation>Confirm</translation>
    </message>
    <message>
        <location filename="settings.ui" line="446"/>
        <location filename="settings.ui" line="793"/>
        <source>Cong.Bible Study, school and service meeting</source>
        <translation>Cong.Bible Study, school and service meeting</translation>
    </message>
    <message>
        <location filename="settings.ui" line="454"/>
        <location filename="settings.ui" line="550"/>
        <source>Day of week</source>
        <translation>Day of week</translation>
    </message>
    <message>
        <location filename="settings.ui" line="461"/>
        <location filename="settings.ui" line="533"/>
        <source>Start time</source>
        <translation>Start time</translation>
    </message>
    <message utf8="both">
        <source>
      </source>
        <translation type="obsolete">
      </translation>
    </message>
    <message>
        <location filename="settings.ui" line="482"/>
        <location filename="settings.ui" line="561"/>
        <location filename="settings.ui" line="755"/>
        <location filename="settings.ui" line="813"/>
        <source>Mo</source>
        <translation>Mo</translation>
    </message>
    <message>
        <location filename="settings.ui" line="487"/>
        <location filename="settings.ui" line="566"/>
        <location filename="settings.ui" line="760"/>
        <location filename="settings.ui" line="818"/>
        <source>Tu</source>
        <translation>Tu</translation>
    </message>
    <message>
        <location filename="settings.ui" line="492"/>
        <location filename="settings.ui" line="571"/>
        <location filename="settings.ui" line="765"/>
        <location filename="settings.ui" line="823"/>
        <source>We</source>
        <translation>We</translation>
    </message>
    <message>
        <location filename="settings.ui" line="497"/>
        <location filename="settings.ui" line="576"/>
        <location filename="settings.ui" line="770"/>
        <location filename="settings.ui" line="828"/>
        <source>Th</source>
        <translation>Th</translation>
    </message>
    <message>
        <location filename="settings.ui" line="502"/>
        <location filename="settings.ui" line="581"/>
        <location filename="settings.ui" line="775"/>
        <location filename="settings.ui" line="833"/>
        <source>Fr</source>
        <translation>Fr</translation>
    </message>
    <message>
        <location filename="settings.ui" line="507"/>
        <location filename="settings.ui" line="586"/>
        <location filename="settings.ui" line="780"/>
        <location filename="settings.ui" line="838"/>
        <source>Sa</source>
        <translation>Sa</translation>
    </message>
    <message>
        <location filename="settings.ui" line="512"/>
        <location filename="settings.ui" line="591"/>
        <location filename="settings.ui" line="785"/>
        <location filename="settings.ui" line="843"/>
        <source>Su</source>
        <translation>Su</translation>
    </message>
    <message>
        <location filename="settings.ui" line="525"/>
        <location filename="settings.ui" line="800"/>
        <source>Public Talk and Watchtower study</source>
        <translation>Public Talk and Watchtower study</translation>
    </message>
    <message>
        <location filename="settings.ui" line="679"/>
        <source>Circuit Overseer&apos;s visit</source>
        <translation>Circuit Overseer&apos;s visit</translation>
    </message>
    <message>
        <location filename="settings.ui" line="684"/>
        <source>Convention</source>
        <translation>Convention</translation>
    </message>
    <message>
        <location filename="settings.ui" line="689"/>
        <source>Memorial</source>
        <translation>Memorial</translation>
    </message>
    <message>
        <location filename="settings.ui" line="694"/>
        <source>Zone overseer&apos;s talk</source>
        <translation>Zone overseer&apos;s talk</translation>
    </message>
    <message>
        <location filename="settings.ui" line="699"/>
        <source>Other exception</source>
        <translation>Other exception</translation>
    </message>
    <message>
        <location filename="settings.ui" line="707"/>
        <location filename="settings.cpp" line="574"/>
        <source>Start date</source>
        <translation>Start date</translation>
    </message>
    <message>
        <location filename="settings.ui" line="728"/>
        <location filename="settings.cpp" line="575"/>
        <source>End date</source>
        <translation>End date</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1045"/>
        <location filename="settings.cpp" line="805"/>
        <source>Settings</source>
        <comment>for sisters assignment</comment>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1628"/>
        <source>Lines</source>
        <translation>Lines</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1671"/>
        <source>Backgroud image</source>
        <translation>Backgroud image</translation>
    </message>
    <message>
        <location filename="settings.ui" line="636"/>
        <location filename="settings.ui" line="647"/>
        <location filename="settings.ui" line="964"/>
        <location filename="settings.ui" line="988"/>
        <location filename="settings.ui" line="1009"/>
        <location filename="settings.ui" line="1033"/>
        <location filename="settings.ui" line="1054"/>
        <location filename="settings.ui" line="1078"/>
        <location filename="settings.ui" line="1162"/>
        <location filename="settings.ui" line="1263"/>
        <location filename="settings.ui" line="1651"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="settings.ui" line="750"/>
        <location filename="settings.ui" line="808"/>
        <source>No meeting</source>
        <translation>No meeting</translation>
    </message>
    <message>
        <location filename="settings.ui" line="854"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="settings.ui" line="881"/>
        <source>Classes</source>
        <translation>Classes</translation>
    </message>
    <message>
        <location filename="settings.ui" line="887"/>
        <source>Number of classes</source>
        <translation>Number of classes</translation>
    </message>
    <message>
        <location filename="settings.ui" line="930"/>
        <source>No 3 themes with asterisk</source>
        <translation>No 3 themes with asterisk</translation>
    </message>
    <message>
        <location filename="settings.ui" line="937"/>
        <source>All brothers</source>
        <translation>All brothers</translation>
    </message>
    <message>
        <location filename="settings.ui" line="944"/>
        <source>Only servants</source>
        <translation>Only servants</translation>
    </message>
    <message>
        <location filename="settings.ui" line="955"/>
        <location filename="settings.cpp" line="200"/>
        <source>School program</source>
        <translation>School program</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1000"/>
        <location filename="settings.cpp" line="788"/>
        <source>Studies</source>
        <translation>Studies</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1587"/>
        <source>Field</source>
        <translation>Field</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1592"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1597"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1563"/>
        <source>Modified</source>
        <translation>Modified</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1556"/>
        <source>Assignment Slips</source>
        <translation>Assignment Slips</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1110"/>
        <source>Add subjects</source>
        <translation>Add subjects</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1177"/>
        <location filename="settings.ui" line="1183"/>
        <source>Add subject one at a time</source>
        <translation>Add subject one at a time</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1212"/>
        <source>Public talk number</source>
        <translation>Public talk number</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1219"/>
        <source>Public talk subject</source>
        <translation>Public talk subject</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1229"/>
        <location filename="settings.cpp" line="976"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1305"/>
        <source>Templates</source>
        <translation>Templates</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1320"/>
        <location filename="settings.ui" line="1384"/>
        <source>Modern</source>
        <translation>Modern</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1327"/>
        <location filename="settings.ui" line="1370"/>
        <location filename="settings.ui" line="1515"/>
        <location filename="settings.ui" line="1545"/>
        <source>Custom Template</source>
        <translation>Custom Template</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1334"/>
        <location filename="settings.ui" line="1360"/>
        <location filename="settings.ui" line="1501"/>
        <location filename="settings.ui" line="1531"/>
        <source>Classic</source>
        <translation>Classic</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1354"/>
        <source>Combination</source>
        <translation>Combination</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1394"/>
        <source>Paper</source>
        <translation>Paper</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1475"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1485"/>
        <source>Landscape</source>
        <translation>Landscape</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1525"/>
        <source>Public Talk</source>
        <translation>Public Talk</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1568"/>
        <source>Finnish</source>
        <translation>Finnish</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1573"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1247"/>
        <source>Add congregations and speakers</source>
        <translation>Add congregations and speakers</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1700"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="1169"/>
        <source>Papersize</source>
        <translation>Papersize</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="1169"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="361"/>
        <location filename="settings.cpp" line="975"/>
        <location filename="settings.cpp" line="1169"/>
        <source>Number</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="243"/>
        <location filename="settings.cpp" line="1169"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="1169"/>
        <source>Assistant</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="246"/>
        <location filename="settings.cpp" line="1170"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="245"/>
        <location filename="settings.cpp" line="362"/>
        <location filename="settings.cpp" line="1170"/>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="201"/>
        <location filename="settings.cpp" line="423"/>
        <source>Remove selected row?</source>
        <translation>Remove selected row?</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="244"/>
        <location filename="settings.cpp" line="309"/>
        <location filename="settings.cpp" line="334"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="247"/>
        <source>Only brothers</source>
        <translation>Only brothers</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="310"/>
        <location filename="settings.cpp" line="1170"/>
        <source>Study</source>
        <translation>Study</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="335"/>
        <location filename="settings.cpp" line="1170"/>
        <source>Setting</source>
        <translation>Setting</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="363"/>
        <source>Language id</source>
        <translation>Language id</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="462"/>
        <source>Public talk number missing</source>
        <translation>Public talk number missing</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="465"/>
        <source>Public talk subject missing</source>
        <translation>Public talk subject missing</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="474"/>
        <source>Public talk is already saved!</source>
        <translation>Public talk is already saved!</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="483"/>
        <source>Public talk added to database</source>
        <translation>Public talk added to database</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="490"/>
        <source>Adding failed</source>
        <translation>Adding failed</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="573"/>
        <source>Exception</source>
        <translation>Exception</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="576"/>
        <source>Meeting 1</source>
        <translation>Meeting 1</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="577"/>
        <source>Meeting 2</source>
        <translation>Meeting 2</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="767"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Database restored. The program will be restarted.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="789"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>Remove ALL studies? (Use only to remove invalid data from database)</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="806"/>
        <source>Remove ALL settings? (Use only to remove invalid data from database)</source>
        <translation>Remove ALL settings? (Use only to remove invalid data from database)</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="872"/>
        <location filename="settings.cpp" line="1102"/>
        <location filename="settings.cpp" line="1119"/>
        <location filename="settings.cpp" line="1126"/>
        <location filename="settings.cpp" line="1143"/>
        <source>Choose file</source>
        <translation>Choose file</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="873"/>
        <source>Image files</source>
        <translation>Image files</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="971"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="972"/>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="974"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="1103"/>
        <location filename="settings.cpp" line="1120"/>
        <location filename="settings.cpp" line="1127"/>
        <location filename="settings.cpp" line="1144"/>
        <source>HTML files</source>
        <translation>HTML files</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="1171"/>
        <source>Class 1</source>
        <translation>Class 1</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="1171"/>
        <source>Class 2</source>
        <translation>Class 2</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="737"/>
        <source>Save database</source>
        <translation>Save database</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="742"/>
        <source>Database backuped</source>
        <translation>Database backuped</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="750"/>
        <source>Select restored file</source>
        <translation>Select restored file</translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <location filename="cpersoncombobox.cpp" line="122"/>
        <source>Person unavailable</source>
        <translation>Person unavailable</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="checkupdates.cpp" line="45"/>
        <source>New version available</source>
        <translation>New version available</translation>
    </message>
    <message>
        <location filename="checkupdates.cpp" line="47"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="checkupdates.cpp" line="50"/>
        <source>New version...</source>
        <translation>New version...</translation>
    </message>
    <message>
        <location filename="checkupdates.cpp" line="61"/>
        <source>No new update available</source>
        <translation>No new update available</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="csync.cpp" line="94"/>
        <source>File reading failed</source>
        <translation>File reading failed</translation>
    </message>
    <message>
        <location filename="csync.cpp" line="498"/>
        <source>XML file genarated in the wrong version.</source>
        <translation>XML file genarated in the wrong version.</translation>
    </message>
    <message>
        <location filename="csync.cpp" line="536"/>
        <source>Territories - updated </source>
        <translation>Territories - updated </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="542"/>
        <source>Territories - added </source>
        <translation>Territories - added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="588"/>
        <source>Persons - added </source>
        <translation>Persons - added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="607"/>
        <source>Persons - updated </source>
        <translation>Persons - updated </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="633"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>Theocratic ministry school - schedule added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="635"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>Theocratic ministry school - schedule updated </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="687"/>
        <source>Public talks - theme added </source>
        <translation>Public talks - theme added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="728"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>Public Talk and WT - schedule updated </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="734"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>Public Talk and WT - schedule added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="765"/>
        <source>Congregation Bible Study - schedule added </source>
        <translation>Congregation Bible Study - schedule added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="767"/>
        <source>Congregation Bible Study - schedule updated </source>
        <translation>Congregation Bible Study - schedule updated </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="808"/>
        <source>Service Meeting - schedule added </source>
        <translation>Service Meeting - schedule added </translation>
    </message>
    <message>
        <location filename="csync.cpp" line="810"/>
        <source>Service Meeting - schedule updated </source>
        <translation>Service Meeting - schedule updated </translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="historytable.ui" line="193"/>
        <source>All</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="historytable.ui" line="223"/>
        <source>Congregation Bible Study</source>
        <translation>Congregation Bible Study</translation>
    </message>
    <message>
        <location filename="historytable.ui" line="253"/>
        <source>Theocratic Ministry School</source>
        <translation>Theocratic Ministry School</translation>
    </message>
    <message>
        <location filename="historytable.ui" line="283"/>
        <source>Service Meeting</source>
        <translation>Service Meeting</translation>
    </message>
    <message>
        <location filename="historytable.ui" line="331"/>
        <source>weeks</source>
        <translation>weeks</translation>
    </message>
    <message>
        <location filename="historytable.cpp" line="1045"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="historytable.cpp" line="1063"/>
        <location filename="historytable.cpp" line="1180"/>
        <source>CBS</source>
        <translation>CBS</translation>
    </message>
    <message>
        <location filename="historytable.cpp" line="1162"/>
        <location filename="historytable.cpp" line="1414"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="historytable.cpp" line="1405"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="historytable.cpp" line="1432"/>
        <source>TMS</source>
        <translation>TMS</translation>
    </message>
    <message>
        <location filename="historytable.cpp" line="1684"/>
        <source>SM</source>
        <translation>SM</translation>
    </message>
</context>
<context>
    <name>http_report</name>
    <message>
        <location filename="http_report.cpp" line="48"/>
        <source>Message sent successfully!</source>
        <translation>Message sent successfully!</translation>
    </message>
    <message>
        <location filename="http_report.cpp" line="49"/>
        <source>Subject: </source>
        <translation>Subject: </translation>
    </message>
    <message>
        <location filename="http_report.cpp" line="50"/>
        <source>Description: </source>
        <translation>Description: </translation>
    </message>
    <message>
        <location filename="http_report.cpp" line="89"/>
        <source>Message sending failed</source>
        <translation>Message sending failed</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="importwizard.ui" line="14"/>
        <source>Import data</source>
        <translation>Import data</translation>
    </message>
    <message>
        <source>
      </source>
        <translation type="obsolete">
      </translation>
    </message>
    <message>
        <location filename="importwizard.ui" line="59"/>
        <location filename="importwizard.cpp" line="736"/>
        <source>Next &gt;</source>
        <translation>Next &gt;</translation>
    </message>
    <message>
        <location filename="importwizard.ui" line="66"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="importwizard.ui" line="73"/>
        <source>&lt; Back</source>
        <translation>&lt; Back</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="42"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="43"/>
        <source>Check schedule</source>
        <translation>Check schedule</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="46"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="47"/>
        <source>Check studies</source>
        <translation>Check studies</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="50"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="51"/>
        <source>Check settings</source>
        <translation>Check settings</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="54"/>
        <source>Add public talk&amp;apos;s subjects. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <oldsource>Add public talk&apos;s subjects. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</oldsource>
        <translation>Add public talk&apos;s subjects. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="55"/>
        <source>Check subjects</source>
        <translation>Check subjects</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="58"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="59"/>
        <source>Check data</source>
        <translation>Check data</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="70"/>
        <source>No schedule to import.</source>
        <translation>No schedule to import.</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="121"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="122"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>Please add start date YYYY-MM-DD (eg. 2011-01-03)</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="657"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="660"/>
        <source>Congregation</source>
        <translation>Congregation</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="663"/>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="666"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="669"/>
        <source>Public talks</source>
        <translation>Public talks</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="672"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="689"/>
        <location filename="importwizard.cpp" line="690"/>
        <source>First name</source>
        <translation>First name</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="689"/>
        <location filename="importwizard.cpp" line="690"/>
        <source>Last name</source>
        <translation>Last name</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="151"/>
        <source>Import subjects</source>
        <translation>Import subjects</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="152"/>
        <source>Choose language</source>
        <translation>Choose language</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="166"/>
        <source>Save to database</source>
        <translation>Save to database</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="185"/>
        <source>date</source>
        <translation>date</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="188"/>
        <source>number</source>
        <translation>number</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="191"/>
        <source>subject</source>
        <translation>subject</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="194"/>
        <source>material</source>
        <translation>material</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="337"/>
        <source>setting</source>
        <translation>setting</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="389"/>
        <source>study</source>
        <translation>study</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="449"/>
        <source>Subject</source>
        <translation>Subject</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="640"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>Public talk themes not found. Add themes and try again!</translation>
    </message>
    <message>
        <location filename="importwizard.cpp" line="644"/>
        <source> rows added</source>
        <translation> rows added</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="personsui.ui" line="7376"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="17583"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="18940"/>
        <location filename="personsui.cpp" line="236"/>
        <source>First name</source>
        <translation>First name</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="19353"/>
        <location filename="personsui.cpp" line="235"/>
        <source>Last name</source>
        <translation>Last name</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="19766"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="20179"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="20592"/>
        <source>Congregation</source>
        <translation>Congregation</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="21241"/>
        <source>Sister</source>
        <translation>Sister</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="21654"/>
        <source>Brother</source>
        <translation>Brother</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="22067"/>
        <source>Servant</source>
        <translation>Servant</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="7907"/>
        <source>Use for school</source>
        <translation>Use for school</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="1299"/>
        <source>Publishers</source>
        <translation>Publishers</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="8261"/>
        <source>Bible highlights</source>
        <translation>Bible highlights</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="8674"/>
        <source>No. 1</source>
        <translation>No. 1</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="9087"/>
        <source>No. 2</source>
        <translation>No. 2</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="9500"/>
        <source>No. 3</source>
        <translation>No. 3</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="9913"/>
        <location filename="personsui.ui" line="32215"/>
        <source>Assistant</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="10326"/>
        <source>Break</source>
        <translation>Break</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="11388"/>
        <source>Use for Sunday meeting</source>
        <translation>Use for Sunday meeting</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="11742"/>
        <location filename="personsui.ui" line="45136"/>
        <source>Public talks</source>
        <translation>Public talks</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="12155"/>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="12568"/>
        <source>Watchtower reader</source>
        <translation>Watchtower reader</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="13925"/>
        <source>Other settings</source>
        <translation>Other settings</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="14279"/>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="14692"/>
        <source>Cong. Bible Study</source>
        <translation>Cong. Bible Study</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="15105"/>
        <source>Cong. Bible Study reader</source>
        <translation>Cong. Bible Study reader</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="15518"/>
        <source>Use for service meeting</source>
        <translation>Use for service meeting</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="15931"/>
        <source>Meeting for field ministry</source>
        <translation>Meeting for field ministry</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="30150"/>
        <source>History</source>
        <translation>History</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="30740"/>
        <location filename="personsui.ui" line="39708"/>
        <source>date</source>
        <translation>date</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="31035"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="31330"/>
        <source>material</source>
        <translation>material</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="31625"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="31920"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="32746"/>
        <source>Studies</source>
        <translation>Studies</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="35165"/>
        <location filename="personsui.ui" line="36522"/>
        <source>Study</source>
        <translation>Study</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="35460"/>
        <source>Date assigned</source>
        <translation>Date assigned</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="35755"/>
        <source>Exercises</source>
        <translation>Exercises</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="36050"/>
        <source>Date completed</source>
        <translation>Date completed</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="36876"/>
        <source>No 1</source>
        <translation>No 1</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="37289"/>
        <source>No 2</source>
        <translation>No 2</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="37702"/>
        <source>No 3</source>
        <translation>No 3</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="39118"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="40003"/>
        <source>setting</source>
        <translation>setting</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="40534"/>
        <source>Unavailable</source>
        <translation>Unavailable</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="28380"/>
        <location filename="personsui.ui" line="41950"/>
        <location filename="personsui.ui" line="42717"/>
        <location filename="personsui.ui" line="45667"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="44310"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="44605"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="3246"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="3895"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="personsui.ui" line="5134"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="47"/>
        <source>Speakers</source>
        <translation>Speakers</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="285"/>
        <source>Remove student?</source>
        <translation>Remove student?</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="286"/>
        <source>Remove student</source>
        <translation>Remove student</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="460"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="462"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="657"/>
        <source>Add new congregation</source>
        <translation>Add new congregation</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="658"/>
        <source>Congregation name:</source>
        <translation>Congregation name:</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="714"/>
        <source>Remove study</source>
        <translation>Remove study</translation>
    </message>
    <message>
        <location filename="personsui.cpp" line="715"/>
        <source>Remove exercise</source>
        <translation>Remove exercise</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="printui.ui" line="222"/>
        <source>Theocratic ministry school</source>
        <translation>Theocratic ministry school</translation>
    </message>
    <message>
        <source>
      </source>
        <translation type="obsolete">
      </translation>
    </message>
    <message>
        <location filename="printui.ui" line="215"/>
        <source>Public talks</source>
        <translation>Public talks</translation>
    </message>
    <message>
        <location filename="printui.ui" line="208"/>
        <location filename="printui.cpp" line="759"/>
        <source>Meetings for field ministry</source>
        <translation>Meetings for field ministry</translation>
    </message>
    <message>
        <location filename="printui.ui" line="14"/>
        <location filename="printui.ui" line="276"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="printui.ui" line="26"/>
        <source>Printing</source>
        <translation>Printing</translation>
    </message>
    <message>
        <location filename="printui.ui" line="29"/>
        <location filename="printui.ui" line="52"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="printui.ui" line="72"/>
        <source>Copy to the clipboard</source>
        <translation>Copy to the clipboard</translation>
    </message>
    <message>
        <location filename="printui.ui" line="75"/>
        <source>Copy</source>
        <translation>Copy</translation>
    </message>
    <message>
        <location filename="printui.ui" line="105"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="printui.ui" line="156"/>
        <location filename="printui.ui" line="166"/>
        <location filename="printui.ui" line="196"/>
        <source>buttonGroup_2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="printui.ui" line="173"/>
        <source>Assignment slip for assistant</source>
        <translation>Assignment slip for assistant</translation>
    </message>
    <message>
        <location filename="printui.ui" line="249"/>
        <location filename="printui.ui" line="265"/>
        <source>buttonGroup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="printui.ui" line="286"/>
        <source>previous weeks</source>
        <translation>previous weeks</translation>
    </message>
    <message>
        <location filename="printui.ui" line="341"/>
        <source>Combination</source>
        <translation>Combination</translation>
    </message>
    <message>
        <location filename="printui.ui" line="366"/>
        <location filename="printui.cpp" line="1446"/>
        <location filename="printui.cpp" line="1533"/>
        <location filename="printui.cpp" line="2042"/>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
    <message>
        <location filename="printui.ui" line="376"/>
        <location filename="printui.cpp" line="672"/>
        <location filename="printui.cpp" line="2035"/>
        <source>Congregation Bible Study</source>
        <translation>Congregation Bible Study</translation>
    </message>
    <message>
        <location filename="printui.ui" line="402"/>
        <location filename="printui.cpp" line="2085"/>
        <source>Service Meeting</source>
        <translation>Service Meeting</translation>
    </message>
    <message>
        <location filename="printui.ui" line="415"/>
        <location filename="printui.cpp" line="2136"/>
        <source>Public Talk</source>
        <translation>Public Talk</translation>
    </message>
    <message>
        <location filename="printui.ui" line="150"/>
        <location filename="printui.ui" line="240"/>
        <source>Schedule</source>
        <translation>Schedule</translation>
    </message>
    <message>
        <location filename="printui.ui" line="193"/>
        <location filename="printui.ui" line="259"/>
        <source>Worksheets</source>
        <translation>Worksheets</translation>
    </message>
    <message>
        <location filename="printui.ui" line="163"/>
        <source>Assignment Slips</source>
        <translation>Assignment Slips</translation>
    </message>
    <message>
        <location filename="printui.ui" line="183"/>
        <source>Print assigned only</source>
        <translation>Print assigned only</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="450"/>
        <location filename="printui.cpp" line="1473"/>
        <source>Bible highlights: </source>
        <translation>Bible highlights: </translation>
    </message>
    <message>
        <location filename="printui.cpp" line="455"/>
        <location filename="printui.cpp" line="1477"/>
        <location filename="printui.cpp" line="1897"/>
        <source>No </source>
        <translation>No </translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1297"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1299"/>
        <source>reading</source>
        <translation>reading</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="176"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="183"/>
        <location filename="printui.cpp" line="237"/>
        <location filename="printui.cpp" line="1016"/>
        <source>Save file</source>
        <translation>Save file</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="194"/>
        <location filename="printui.cpp" line="255"/>
        <location filename="printui.cpp" line="1127"/>
        <source>file created</source>
        <translation>file created</translation>
    </message>
    <message>
        <location filename="printui.ui" line="389"/>
        <location filename="printui.cpp" line="437"/>
        <location filename="printui.cpp" line="678"/>
        <location filename="printui.cpp" line="1036"/>
        <location filename="printui.cpp" line="1865"/>
        <source>Theocratic Ministry School</source>
        <translation>Theocratic Ministry School</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="504"/>
        <source>Exercises</source>
        <translation>Exercises</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="515"/>
        <location filename="printui.cpp" line="732"/>
        <source>Convention</source>
        <translation>Convention</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="536"/>
        <location filename="printui.cpp" line="1105"/>
        <source>Next week</source>
        <translation>Next week</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="574"/>
        <location filename="printui.cpp" line="609"/>
        <source>Territories</source>
        <translation>Territories</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="684"/>
        <source>Public talk:</source>
        <translation>Public talk:</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="693"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="693"/>
        <source>Group language is undefined!</source>
        <translation>Group language is undefined!</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="734"/>
        <location filename="printui.cpp" line="1433"/>
        <location filename="printui.cpp" line="1558"/>
        <location filename="printui.cpp" line="1709"/>
        <location filename="printui.cpp" line="1774"/>
        <location filename="printui.cpp" line="1881"/>
        <source>No meeting</source>
        <translation>No meeting</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="817"/>
        <source>Public talk</source>
        <translation>Public Talk</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="821"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="822"/>
        <source>Subject</source>
        <translation>Subject</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="823"/>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="824"/>
        <source>Congregation</source>
        <translation>Congregation</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="825"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="884"/>
        <source>Readers for bible study meeting and watchtower study &lt;br /&gt; and chairman for public meeting</source>
        <translation>Readers for bible study meeting and watchtower study &lt;br /&gt; and chairman for public meeting</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="887"/>
        <location filename="printui.cpp" line="1460"/>
        <location filename="printui.cpp" line="1576"/>
        <location filename="printui.cpp" line="2063"/>
        <location filename="printui.cpp" line="2169"/>
        <source>Reader</source>
        <translation>Reader</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="887"/>
        <location filename="printui.cpp" line="1572"/>
        <location filename="printui.cpp" line="2161"/>
        <source>Chairman</source>
        <translation>Chairman</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="943"/>
        <source>Public meeting </source>
        <translation>Public meeting </translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1058"/>
        <location filename="printui.cpp" line="1983"/>
        <location filename="printui.cpp" line="1985"/>
        <location filename="printui.cpp" line="1988"/>
        <source>Class</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1187"/>
        <source>Theocratic ministry school schedule</source>
        <translation>Theocratic ministry school schedule</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1300"/>
        <source>Review reader</source>
        <translation>Review reader</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1342"/>
        <source>Assist.</source>
        <translation>Assist.</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1415"/>
        <location filename="printui.cpp" line="1540"/>
        <source>(Circuit overseer visit)</source>
        <translation>(Circuit overseer visit)</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1431"/>
        <location filename="printui.cpp" line="1556"/>
        <location filename="printui.cpp" line="1707"/>
        <location filename="printui.cpp" line="1772"/>
        <location filename="printui.cpp" line="1879"/>
        <source>Convention week (no meeting) </source>
        <translation>Convention week (no meeting) </translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1451"/>
        <source>Congregation Bible Study:</source>
        <translation>Congregation Bible Study:</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1467"/>
        <source>Theocratic Ministry School:</source>
        <translation>Theocratic Ministry School:</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1518"/>
        <source>Service Meeting:</source>
        <translation>Service Meeting:</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1564"/>
        <source>Public Talk:</source>
        <translation>Public Talk:</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="1916"/>
        <source>Bible highlights:</source>
        <translation>Bible highlights:</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="2056"/>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <location filename="printui.cpp" line="753"/>
        <source>Watchtower study</source>
        <translation>Watchtower study</translation>
    </message>
</context>
<context>
    <name>school</name>
    <message>
        <location filename="school.cpp" line="20"/>
        <source>Theme</source>
        <translation>Theme</translation>
    </message>
    <message>
        <location filename="school.cpp" line="21"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="school.cpp" line="25"/>
        <source>Class</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="school.cpp" line="309"/>
        <source>Assignment already has been made</source>
        <translation>Assignment already has been made</translation>
    </message>
    <message>
        <location filename="school.cpp" line="349"/>
        <source>Student unavailable</source>
        <translation>Student unavailable</translation>
    </message>
    <message>
        <location filename="school.cpp" line="548"/>
        <source>Bible highlights:</source>
        <translation>Bible highlights:</translation>
    </message>
    <message>
        <location filename="school.cpp" line="552"/>
        <source>No. 1:</source>
        <translation>No. 1:</translation>
    </message>
    <message>
        <location filename="school.cpp" line="556"/>
        <source>No. 2:</source>
        <translation>No. 2:</translation>
    </message>
    <message>
        <location filename="school.cpp" line="560"/>
        <source>No. 3:</source>
        <translation>No. 3:</translation>
    </message>
    <message>
        <location filename="school.cpp" line="564"/>
        <source>Reader:</source>
        <translation>Reader:</translation>
    </message>
    <message>
        <location filename="school.cpp" line="612"/>
        <source>No school</source>
        <translation>No school</translation>
    </message>
    <message>
        <location filename="school.cpp" line="798"/>
        <source>No assignment has been made!</source>
        <translation>No assignment has been made!</translation>
    </message>
    <message>
        <location filename="school.cpp" line="803"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</translation>
    </message>
    <message>
        <location filename="school.cpp" line="918"/>
        <source>Nothing to display</source>
        <translation>Nothing to display</translation>
    </message>
    <message>
        <location filename="school.cpp" line="946"/>
        <source>Show Details...</source>
        <translation>Show Details...</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <source>
      </source>
        <translation type="obsolete">
      </translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>Volunteer</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>Study</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>Exercise Completed</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>Next study:</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <translation>Setting:</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>Completed</translation>
    </message>
    <message>
        <location filename="schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>Timing:</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="38"/>
        <source>Bible highlights</source>
        <translation>Bible highlights</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="40"/>
        <source>reading</source>
        <translation>reading</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="42"/>
        <source>Assignment result</source>
        <translation>Assignment result</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="267"/>
        <source>Select setting</source>
        <translation>Select setting</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="278"/>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="279"/>
        <source>The timing is empty. Save?</source>
        <translation>The timing is empty. Save?</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="300"/>
        <source>Saving</source>
        <translation>Saving</translation>
    </message>
    <message>
        <location filename="schoolresult.cpp" line="301"/>
        <source>Assignment marked completed. This action cannot undo. Continue?</source>
        <translation>Assignment marked completed. This action cannot undo. Continue?</translation>
    </message>
</context>
<context>
    <name>servicemeeting</name>
    <message>
        <location filename="servicemeeting.cpp" line="50"/>
        <location filename="servicemeeting.cpp" line="103"/>
        <source>Song</source>
        <translation>Song</translation>
    </message>
    <message>
        <location filename="servicemeeting.cpp" line="105"/>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
    <message>
        <source>Announcements.</source>
        <translation type="obsolete">Announcements.</translation>
    </message>
</context>
<context>
    <name>study</name>
    <message>
        <location filename="study.ui" line="115"/>
        <source>Song</source>
        <translation>Song</translation>
    </message>
    <message>
        <location filename="study.ui" line="217"/>
        <source>Prayer</source>
        <translation>Prayer</translation>
    </message>
    <message>
        <location filename="study.ui" line="277"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="study.ui" line="319"/>
        <source>Conductor</source>
        <translation>Conductor</translation>
    </message>
    <message>
        <location filename="study.ui" line="397"/>
        <source>Reader</source>
        <translation>Reader</translation>
    </message>
</context>
</TS>
