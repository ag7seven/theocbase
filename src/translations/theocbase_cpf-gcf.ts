<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cpf-gcf">
<context>
    <name>AssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Chwazi</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="40"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="60"/>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="75"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="75"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="86"/>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Chwazi</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="87"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tout</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="106"/>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
</context>
<context>
    <name>CBSDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Chwazi</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="39"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="51"/>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="66"/>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="72"/>
        <source>CBS conductor</source>
        <comment>Dropdown column title</comment>
        <translation>Kondiktè a EBL-la</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="73"/>
        <source>Any CL assignment</source>
        <comment>Dropdown column title</comment>
        <translation>Tout sijé a VK-la</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>Chwazi</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tout</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="86"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="105"/>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
</context>
<context>
    <name>ComboBoxTable</name>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="189"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="195"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>Montré adrès a lasanblé-la</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="76"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>Ès ou sèten ou vlé éfasé tout sé enfòwmasyon-la ki an klaoud-la?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="214"/>
        <source>Last synchronized: %1</source>
        <translation>I senkwonizé lè: %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="218"/>
        <source>Synchronize</source>
        <translation>Senkwonizé</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="230"/>
        <source>Delete Cloud Data</source>
        <translation>Éfasé sé ki adan klaoud-la</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>Èd pou itilizé TheocBase</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>Paj èd-la pa ka vin (%1)</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="42"/>
        <source>Notes</source>
        <translation>Sé nòt-la</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Please find below details of your assignment:</source>
        <translation>Gadé pliba ou ké vouè détay a sijé-aw:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Assignment</source>
        <translation>Sijé</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="184"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="185"/>
        <source>Source material</source>
        <translation>Référans-la</translation>
    </message>
</context>
<context>
    <name>LMM_AssignmentContoller</name>
    <message>
        <location filename="../lmm_assignmentcontoller.cpp" line="54"/>
        <source>Do not assign the next study</source>
        <translation>Pa bay pwochen lèson-la</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="179"/>
        <source>Please find below details of your assignment:</source>
        <translation>Gadé pliba ou ké vouè détay a sijé-aw:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="180"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="181"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="188"/>
        <source>Assignment</source>
        <translation>Sijé</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Study</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="194"/>
        <source>Source material</source>
        <translation>Rérérans-la</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="238"/>
        <source>See your be book</source>
        <comment>Counsel point is not known yet. See your &apos;Ministry School&apos; book.</comment>
        <translation>Gadé liv &quot;be&quot; la</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="607"/>
        <source>Enter source material here</source>
        <translation>Ba-y sé référans-la koté sit</translation>
    </message>
</context>
<context>
    <name>LMM_Schedule</name>
    <message>
        <location filename="../lmm_schedule.cpp" line="97"/>
        <location filename="../lmm_schedule.cpp" line="122"/>
        <source>Chairman</source>
        <comment>talk title</comment>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="98"/>
        <location filename="../lmm_schedule.cpp" line="123"/>
        <source>Treasures From God’s Word</source>
        <comment>talk title</comment>
        <translation>Trézò ki an Pawòl a Bondyé</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="99"/>
        <location filename="../lmm_schedule.cpp" line="124"/>
        <source>Digging for Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>An-nou fouyé pou jwenn trézò éspirityèl</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="100"/>
        <location filename="../lmm_schedule.cpp" line="125"/>
        <source>Bible Reading</source>
        <comment>talk title</comment>
        <translation>Lèkti a Labib</translation>
    </message>
    <message>
        <source>Prepare This Month’s Presentations</source>
        <comment>talk title</comment>
        <translation>Préparé prézantasyon a mwa lasa</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="103"/>
        <location filename="../lmm_schedule.cpp" line="128"/>
        <source>Initial Call</source>
        <comment>talk title</comment>
        <translation>Prèmyé kontak</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <comment>talk title</comment>
        <translation>Nouvèl vizit</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="101"/>
        <location filename="../lmm_schedule.cpp" line="126"/>
        <source>Sample Conversation Video</source>
        <comment>talk title</comment>
        <translation>Vidéo kijan fè on ti kozé</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="104"/>
        <location filename="../lmm_schedule.cpp" line="129"/>
        <source>First Return Visit</source>
        <comment>talk title</comment>
        <translation>Prèmyé nouvèl vizit</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="105"/>
        <location filename="../lmm_schedule.cpp" line="130"/>
        <source>Second Return Visit</source>
        <comment>talk title</comment>
        <translation>Dézyèm nouvèl vizit</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="106"/>
        <location filename="../lmm_schedule.cpp" line="131"/>
        <source>Third Return Visit</source>
        <comment>talk title</comment>
        <translation>Twazyèm nouvèl vizit</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="107"/>
        <location filename="../lmm_schedule.cpp" line="132"/>
        <source>Bible Study</source>
        <comment>talk title</comment>
        <translation>Étid biblik</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="108"/>
        <location filename="../lmm_schedule.cpp" line="133"/>
        <source>Talk</source>
        <comment>talk title</comment>
        <translation>Diskou</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="109"/>
        <location filename="../lmm_schedule.cpp" line="134"/>
        <source>Living as Christians Talk 1</source>
        <comment>talk title</comment>
        <translation>Vi a on krétyen Pati 1</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="110"/>
        <location filename="../lmm_schedule.cpp" line="135"/>
        <source>Living as Christians Talk 2</source>
        <comment>talk title</comment>
        <translation>Vi a on krétyen Pati 2</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="111"/>
        <location filename="../lmm_schedule.cpp" line="136"/>
        <source>Living as Christians Talk 3</source>
        <comment>talk title</comment>
        <translation>Vi a on krétyen Pati 3</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="112"/>
        <location filename="../lmm_schedule.cpp" line="137"/>
        <source>Congregation Bible Study</source>
        <comment>talk title</comment>
        <translation>Étid biblik a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="113"/>
        <location filename="../lmm_schedule.cpp" line="138"/>
        <source>Circuit Overseer&#x27;s Talk</source>
        <comment>talk title</comment>
        <translation>Diskou a sèwvitè a siwkonskripsyon-la</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="127"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <comment>talk title</comment>
        <translation>Konsantré-w si lèkti a-w é ansègnman a-w</translation>
    </message>
</context>
<context>
    <name>LifeMinistryMeetingSchedule</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>Trézò ki an Pawòl a Bondyé</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>Pran ministè-la oséryé</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VI A ON KRÉTYEN</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Enpòté pwogram-la...</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Konséyé</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 é priyè-la</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Pawòl pou koumansé</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Révizyon é sa nou ké vwè pwochen simenn-la</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <source>Main hall</source>
        <translation>Sal prensipal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 1</source>
        <translation>Dézyèm sal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 2</source>
        <translation>Twazyèm sal</translation>
    </message>
    <message>
        <source>Main Class</source>
        <translation>Sal prensipal</translation>
    </message>
    <message>
        <source>Second Class</source>
        <translation>Dézyèm sal</translation>
    </message>
    <message>
        <source>Third class</source>
        <translation>Twazyèm sal</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>Non a itilizatè-la ou imèl-ay</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>Modpas pèd</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>Konèkté</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>Modpas pèd</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>Kréyé on kont</translation>
    </message>
</context>
<context>
    <name>LoginForm.ui</name>
    <message>
        <source>TheocBase</source>
        <translation>TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Itilizatè</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konèkté</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>Services</source>
        <translation>Sèvis</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Hide %1</source>
        <translation>Kaché %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide Others</source>
        <translation>Kaché sé zòt la</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Show All</source>
        <translation>Vwè tout</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Preferences...</source>
        <translation>Préférans...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Quit %1</source>
        <translation>Kité %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>About %1</source>
        <translation>Biten si: %1</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="76"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="95"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>Konséyé adan ti sal-la</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="115"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>Konséyé adan ti sal-la 3</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="217"/>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TRÉZÒ KI AN PAWÒL A BONDYÉ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="225"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>PRAN MINISTÈ-LA OSÉRYÉ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="230"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VI A ON KRÉTYEN</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="243"/>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="255"/>
        <source>Import Schedule...</source>
        <translation>Enpòté pwgram-la...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="303"/>
        <location filename="../qml/MWMeetingModule.qml" line="504"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="304"/>
        <location filename="../qml/MWMeetingModule.qml" line="505"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>S2</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="305"/>
        <location filename="../qml/MWMeetingModule.qml" line="506"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>S3</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="314"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="314"/>
        <source>Counselor</source>
        <translation>Konséyé</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="340"/>
        <location filename="../qml/MWMeetingModule.qml" line="392"/>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 é priyè-la</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="355"/>
        <source>Opening Comments</source>
        <translation>Pawòl pou koumansé</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="374"/>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Révizyon é sa nou ké vwè pwochen simenn-la</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="469"/>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <source>Main hall</source>
        <translation>Sal prensipal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 1</source>
        <translation>Dézyèm sal</translation>
    </message>
    <message>
        <source>Auxiliary classroom 2</source>
        <translation>Twazyèm sal</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="490"/>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="492"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="44"/>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>(No meeting)</source>
        <translation>(Pani réinyon)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="418"/>
        <source>No meeting</source>
        <translation>Pani réinyon</translation>
    </message>
    <message>
        <source>Nothing to display</source>
        <translation>Pani ayen pou vwè</translation>
    </message>
    <message>
        <source>Prayer:</source>
        <translation>Priyè:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Conductor:</source>
        <translation>Kondiktè:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Reader:</source>
        <translation>Lèktè:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>Copyright</source>
        <translation>Dwa a otè-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="475"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Bibliyotèk Qt adan lisans GPL</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>TheocBase Team</source>
        <translation>Ékip a TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="278"/>
        <source>Last synchronized</source>
        <translation>Dènyé senkwonizasiyon-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Licensed under GPLv3.</source>
        <translation>I tin on lisans GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Versions of Qt libraries </source>
        <translation>Vèwsyon a sé bibliyotèk Qt-la </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="520"/>
        <location filename="../mainwindow.cpp" line="1345"/>
        <source>TheocBase data exchange</source>
        <translation>Pawtajé sé enfòwmasyon-la èvè sa ki ni TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Hello</source>
        <translation>Byen bonjou</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Best Regards</source>
        <translation>Nou enmé-w anlo,</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>New update available. Do you want to install?</source>
        <translation>Ni on mizajou ki disponib. Ou vlé enstalé-y?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="560"/>
        <source>No new update available</source>
        <translation>Pa ni pon mizajou ki disponib</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="616"/>
        <source>Enter e-mail address.</source>
        <translation>Mèt on imèl</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="595"/>
        <source>Save file</source>
        <translation>Anrèjistré fichyé-la</translation>
    </message>
    <message>
        <source>TheocBase cloud synchronizing...</source>
        <translation>Senkwonizé-y èvè klaoud-la..</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Ni sé menm chanjman la adan Klaoud-la (%1 ligne). Èskè ou vlé vrèman anrèjistré tout sé chanjman-la?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1717"/>
        <source>Select ePub file</source>
        <translation>Chwazi on fichyé ePub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1630"/>
        <source>Send e-mail reminders?</source>
        <translation>Voyé on imèl pou fè-y sonjé?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1664"/>
        <source>Updates available...</source>
        <translation>Ni on mizajou ki disponib...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="644"/>
        <location filename="../mainwindow.cpp" line="678"/>
        <location filename="../mainwindow.cpp" line="1248"/>
        <source>Error sending e-mail</source>
        <translation>Imèl pa pati, i kontré on érè</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="392"/>
        <source>WEEK STARTING %1</source>
        <translation>SIMENN KI KOUMANSÉ LÈ %1</translation>
    </message>
    <message>
        <source>min</source>
        <comment>Abbreviation of minutes</comment>
        <translation>mn</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="576"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>Zouti-la pou voyé sé oratè-la ki ka sòti pòkò paré, padon.</translation>
    </message>
    <message>
        <source>Exporting speakers to iCal not ready yet, sorry.</source>
        <translation>Dézolé ou péké pé voyé sé oratè-la adan iCal, i poko paré.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="580"/>
        <source>Emailing iCal is not ready yet, sorry.</source>
        <translation>Dézolé ou péké pé voyé imèl èvè iCal, i poko paré.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="584"/>
        <source>Exporting study history to iCal is not supported</source>
        <translation>Ou péké pé voyé sé lèson-la ki ja fèt adan iCal.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="599"/>
        <source>Save folder</source>
        <translation>Anrèjistré fichyé-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="680"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <source>E-mail sent successfully</source>
        <translation>Imèl byen pati</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="683"/>
        <source>Saved successfully</source>
        <translation>I byen anrèjistré</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="748"/>
        <source>Counselor-Class II</source>
        <translation>Konséyé-Klas II</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <location filename="../mainwindow.cpp" line="749"/>
        <source>Counselor-Class III</source>
        <translation>Konséyé-Klas III</translation>
    </message>
    <message>
        <source>Opening Prayer</source>
        <translation>Priyè pou komansé</translation>
    </message>
    <message>
        <source>Concluding Prayer</source>
        <translation>Priyè pou fin</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="764"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Moun-la ki ka fè-y èvè %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <location filename="../mainwindow.cpp" line="1046"/>
        <location filename="../mainwindow.cpp" line="1071"/>
        <source>Kingdom Hall</source>
        <translation>Sal di Wayòm</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <location filename="../mainwindow.cpp" line="747"/>
        <location filename="../mainwindow.cpp" line="837"/>
        <location filename="../mainwindow.cpp" line="848"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="771"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Lèktè a létid biblik a lasanblé la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="745"/>
        <location filename="../mainwindow.cpp" line="750"/>
        <location filename="../mainwindow.cpp" line="795"/>
        <location filename="../mainwindow.cpp" line="797"/>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="837"/>
        <location filename="../mainwindow.cpp" line="1002"/>
        <source>Public Talk</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="837"/>
        <source>Watchtower Study</source>
        <translation>Étid a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="854"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="859"/>
        <source>Watchtower Study Conductor</source>
        <translation>Kondiktè a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="861"/>
        <source>Watchtower reader</source>
        <translation>Lèktè a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation>Ni sé menm chanjman la adan Klaoud-la (%1 ligne). Èskè ou vlé vrèman anrèjistré tout sé chanjman-la?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1148"/>
        <source>The cloud data has now been deleted.</source>
        <translation>Sé enfòwmasyon-la ki té adan klaoud-la éfasé konnyéla.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1152"/>
        <source>Synchronize</source>
        <translation>Senkwonizé</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1154"/>
        <source>Sign Out</source>
        <translation>Dékonèktè-w</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1164"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>Sé enfòwmasyon-la ki adan klaoud la éfasé. Sé enfòwmasyon lokal la ka-y changé. Ou vlé kontinyé?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1259"/>
        <source>Open file</source>
        <translation>Ouvè fichyé-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1286"/>
        <source>Open directory</source>
        <translation>Ouvè on dosyé</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>Import Error</source>
        <translation>Ni on érè èvè enpòtasyon-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>Ou péké pé enpòté dépi Ta1ks. Ka manké sé fichyé-lasa:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1332"/>
        <source>Save unsaved data?</source>
        <translation>Anrèjistré sé enfòwmasyon ki chanjé?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1341"/>
        <source>Import file?</source>
        <translation>Fè vin on fichyé?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>School assignment</source>
        <translation>Sijé</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Congregation Bible Study:</source>
        <translation>Étid biblik a lasanblé-la:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>Theocratic Ministry School:</source>
        <translation>Lékòl pou Ministè Téokratik:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Service Meeting:</source>
        <translation>Réyinyon sèvis:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="806"/>
        <source>Congregation Bible Study</source>
        <translation>Étid biblik a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="833"/>
        <source>Theocratic Ministry School</source>
        <translation>Lékòl pou Ministè Téokratik</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="860"/>
        <source>Service Meeting</source>
        <translation>Réyinyon sèvis</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Export</source>
        <translation>Voyé</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1177"/>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>E-mail</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1406"/>
        <source>Recipient:</source>
        <translation>Voyé ba:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1392"/>
        <source>Subject: </source>
        <translation>Sijé: </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1303"/>
        <source>Public talks</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1457"/>
        <source>Import</source>
        <translation>Fè vin</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1491"/>
        <source>info</source>
        <translation>enfo</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2066"/>
        <source>Data exhange</source>
        <translation>Pawtajé sé enfòwmasyon-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2128"/>
        <source>TheocBase Cloud</source>
        <translation>TheocBase klaoud</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1650"/>
        <location filename="../mainwindow.ui" line="1683"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1882"/>
        <source>Timeline</source>
        <translation>Sa ki ja fèt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1943"/>
        <source>Print...</source>
        <translation>Enprésyon..</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1946"/>
        <source>Print</source>
        <translation>Enprésyon</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1955"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Réglaj-la...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1967"/>
        <source>Publishers...</source>
        <translation>Sé pwoklamatè-la..</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1246"/>
        <location filename="../mainwindow.ui" line="1970"/>
        <source>Publishers</source>
        <translation>Sé pwoklamatè-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2036"/>
        <source>Speakers...</source>
        <translation>Sé oratè-la...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1283"/>
        <location filename="../mainwindow.ui" line="2039"/>
        <source>Speakers</source>
        <translation>Sé oratè-la...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2063"/>
        <source>Data exhange...</source>
        <translation>Pawtajé sé enfòwmasyon-la..</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2075"/>
        <source>TheocBase help...</source>
        <translation>Èd pou itilizé TheocBase...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2090"/>
        <source>History</source>
        <translation>Sa ki ja fèt</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2102"/>
        <location filename="../mainwindow.ui" line="2105"/>
        <source>Full Screen</source>
        <translation>Mété-y si tout ékran-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2110"/>
        <source>Startup Screen</source>
        <translation>Paj akèy</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2119"/>
        <source>Reminders...</source>
        <translation>Sonjé...</translation>
    </message>
    <message>
        <source>&lt;--</source>
        <translation>&lt;--</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Theme:</source>
        <translation>Tèm:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="573"/>
        <source>Speaker:</source>
        <translation>Oratè:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Chairman:</source>
        <translation>Prézidan:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <source>Watchtower Study:</source>
        <translation>Étid a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="529"/>
        <source>Data exchange</source>
        <translation>Pawtajé sé enfòwmasyon-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1114"/>
        <source>Export Format</source>
        <translation>Adan ki fòwma pou voyé-y</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1120"/>
        <source>For sending data to another user</source>
        <translation>Pou voyé sé enfòwmasyon-la ba on dòt itilizatè</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1139"/>
        <source>For easy import to Calendar programs</source>
        <translation>Èvè-y i ké pli fasil mété-y adan on ajanda élèktwònik</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1171"/>
        <source>Export Method</source>
        <translation>Kijan pou voyé-y</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1190"/>
        <source>Save to File</source>
        <translation>Anrèjistré adan on fichyé</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1213"/>
        <source>Events grouped by date</source>
        <translation>Évènman ki sanblé sèlon dat-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1220"/>
        <source>All day events</source>
        <translation>Si ki fèt tout jouné-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1236"/>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1259"/>
        <source>Study History</source>
        <translation>Lèson ki ja travay</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1310"/>
        <source>Outgoing Talks</source>
        <translation>Diskou pou fè déwò</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1321"/>
        <source>Date Range</source>
        <translation>Kantité jou</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1328"/>
        <source>Previous Weeks</source>
        <translation>Simenn ki ja pasé</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1342"/>
        <source>From Date</source>
        <translation>Dèpi lè %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1359"/>
        <source>Thru Date</source>
        <translation>Jiska</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1836"/>
        <source>File</source>
        <translation>Fichyé</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1842"/>
        <source>Tools</source>
        <translation>Zouti</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1855"/>
        <source>Help</source>
        <translation>Èd</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1931"/>
        <source>Today</source>
        <translation>Jòdijou</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1979"/>
        <source>Exit</source>
        <translation>Sòti</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1991"/>
        <source>Report bug...</source>
        <translation>Di-y ni on pwoblèm...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2003"/>
        <source>Send feedback...</source>
        <translation>Bay lidé-aw...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2021"/>
        <source>About TheocBase...</source>
        <translation>Kaki TheocBase...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2048"/>
        <source>Check updates...</source>
        <translation>Vérifyé sé mizajou la...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2078"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2146"/>
        <source>Territories...</source>
        <translation>Téritwa...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2149"/>
        <source>Territories</source>
        <translation>Téritwa</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1910"/>
        <source>Back</source>
        <translation>Rètou</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1922"/>
        <source>Next</source>
        <translation>Apré</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2137"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2012"/>
        <source>TheocBase website</source>
        <translation>Sit a TheocBase</translation>
    </message>
    <message>
        <source>(Circuit overseer visit)</source>
        <translation>(Vizit a sèwvitè a siwkonskripsyon-la)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Convention week (no meeting) </source>
        <translation>Simenn a lasanblé (pani réinyon) </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="534"/>
        <source>Public Talk:</source>
        <translation>Diskou piblik:</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="70"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="94"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="121"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="147"/>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="159"/>
        <source>Address</source>
        <translation>Adrès</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="172"/>
        <source>Meeting day and time</source>
        <translation>Jou é lè a réinyon-la</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="163"/>
        <source>From %1</source>
        <translation>Dèpi lè %1</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="109"/>
        <source>Outgoing speakers</source>
        <translation>Oratè ki ka sòti</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="193"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 oratè ki ka sòti wikenn-lasa</numerusform>
            <numerusform>%1 oratè-la ki ka sòti wikenn-lasa</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="194"/>
        <source>No speakers away this weekend</source>
        <translation>Pani pon oratè ki ka sòti wikenn-lasa</translation>
    </message>
</context>
<context>
    <name>Permission</name>
    <message>
        <source>Edit Settings</source>
        <comment>Access Control</comment>
        <translation>Modifyé sé réglaj</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule</name>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 é priyè-la</translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation>Kantik é priyè</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISKOU PIBLIK</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ÉTID A TOU DÈ GAD LA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="48"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="72"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="94"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="128"/>
        <source>Mobile</source>
        <translation>Pòwtab</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="140"/>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="152"/>
        <source>Email</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="164"/>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="177"/>
        <source>Host</source>
        <translation>Moun ki ka risivouè</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Yes</source>
        <translation>Wi</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>&amp;Yes</source>
        <translation>&amp;Wi</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>No</source>
        <translation>Awa</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;No</source>
        <translation>&amp;Awa</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>Cancel</source>
        <translation>Anilé</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anilé</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Save</source>
        <translation>Anrèjistré</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;Save</source>
        <translation>&amp;Anrèjistré</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>Open</source>
        <translation>Ouvè-y</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="159"/>
        <source>Wrong username and/or password</source>
        <translation>Non a itilizatè-la pa bon/oswa modpas-la pa bon</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="267"/>
        <source>Database not found!</source>
        <translation>Bazdoné-la pala!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>Choose database</source>
        <translation>Chwazi bazdoné-la</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>Fichyé SQLite (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="294"/>
        <source>Database restoring failed</source>
        <translation>Nou pa rivè roumèt bazdoné-la</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="325"/>
        <location filename="../mainwindow.cpp" line="224"/>
        <source>Save changes?</source>
        <translation>Anrèjistré sé enfòwmasyon ki chanjé?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="263"/>
        <source>Database copied to </source>
        <translation>Bazdoné-la copyé adan </translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="37"/>
        <source>Database file not found! Searching path =</source>
        <translation>Bazdoné-la pala! Chèché la:</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="56"/>
        <source>Database Error</source>
        <translation>Ni on érè èvè bazdoné-la</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="706"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>Vèsyon a aplikasyon lasa (%1) pli vyé ki bazdoné-la (%2). Anlo mésaj érè pé parèt si ekran-la èvè é sé chanjman la ou fè pé pa anrèjistré kon dabitid. Tanprisouplé, téléchajé é enstalé dènyé vèsyon-la pou ni on bon rézilta.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="345"/>
        <source>Circuit</source>
        <translation>Siwkonskripsyon</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="748"/>
        <source>Database updated</source>
        <translation>Sé enfòwmasyon-la a jou</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="314"/>
        <location filename="../ccongregation.cpp" line="340"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>Vizit a sèwvitè a siwkonskripsyon-la</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="316"/>
        <location filename="../ccongregation.cpp" line="321"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (Pani réinyon)</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="316"/>
        <location filename="../ccongregation.cpp" line="342"/>
        <source>Convention week</source>
        <translation>Ni lasanblé simenn-lasa</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="318"/>
        <location filename="../ccongregation.cpp" line="344"/>
        <source>Memorial</source>
        <translation>Mémoryal</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="349"/>
        <source>Review</source>
        <comment>Theocratic ministry school review</comment>
        <translation>Révizyon</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="56"/>
        <location filename="../cpublictalks.cpp" line="341"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="57"/>
        <location filename="../cpublictalks.cpp" line="260"/>
        <location filename="../cpublictalks.cpp" line="342"/>
        <source>Last</source>
        <translation>Dènyé</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="257"/>
        <location filename="../school.cpp" line="424"/>
        <source>All</source>
        <translation>Tout</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="258"/>
        <location filename="../school.cpp" line="425"/>
        <source>Highlights</source>
        <translation>PILB</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="262"/>
        <location filename="../school.cpp" line="429"/>
        <source>WT Reader</source>
        <translation>Lèktè TG la</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="264"/>
        <location filename="../school.cpp" line="431"/>
        <source>Selected</source>
        <translation>Chwazi</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="323"/>
        <location filename="../school.cpp" line="486"/>
        <source>Assignment already has been made</source>
        <translation>I ja ba on moun</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="327"/>
        <location filename="../school.cpp" line="490"/>
        <source>Assigned student has other meeting parts</source>
        <translation>I ja ni on atribusyon pou réinyon-lasa</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="331"/>
        <location filename="../school.cpp" line="494"/>
        <source>Student unavailable</source>
        <translation>Zélèv-la pa disponib</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="335"/>
        <source>Student part of family</source>
        <translation>Élèv-lasa adan fanmi</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="434"/>
        <source>Recently together</source>
        <translation>Yo té ansanm pa ni lontan</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="498"/>
        <source>Family member used in another TMS assignment</source>
        <translation>I ni on fanmi ki ja ni on sijé</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="67"/>
        <source>I/O</source>
        <comment>Incoming/Outgoing</comment>
        <translation>L/D</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="72"/>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="564"/>
        <source>Congregation Name</source>
        <translation>Non a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="273"/>
        <location filename="../historytable.cpp" line="322"/>
        <location filename="../school.cpp" line="854"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>CBS</source>
        <translation>EBL</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="468"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="469"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="208"/>
        <source>Assignment</source>
        <translation>Sijé</translation>
    </message>
    <message>
        <source>date</source>
        <translation>dat</translation>
    </message>
    <message>
        <source>no</source>
        <translation>n°</translation>
    </message>
    <message>
        <source>material</source>
        <translation>référans</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="470"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="209"/>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="471"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="210"/>
        <source>Time</source>
        <translation>Lè-la</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <location filename="../school.cpp" line="263"/>
        <location filename="../school.cpp" line="423"/>
        <location filename="../school.cpp" line="430"/>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <source>Student</source>
        <translation>Zélèv</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="265"/>
        <source>Bible highlights:</source>
        <translation>PILB:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="268"/>
        <source>No. 1:</source>
        <translation>N°1:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="271"/>
        <source>No. 2:</source>
        <translation>N°2:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="274"/>
        <source>No. 3:</source>
        <translation>N°3:</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="277"/>
        <source>Reader:</source>
        <translation>Lèktè:</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="816"/>
        <location filename="../generatexml.cpp" line="107"/>
        <source>Default language not selected!</source>
        <translation>Ou pa chwazi lang-la!</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="937"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>Prèmyé lign a fichyé CSV-la pa bon.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="296"/>
        <source>Confirm password!</source>
        <translation>Konfiwmé modpas-la!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2249"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>Ja ni on moun ki ni mèm imèl-lasa</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2455"/>
        <source>All talks have been added to this week</source>
        <translation>Tout sé atribusyon-la fin ajouté pou simenn-lasa</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="259"/>
        <location filename="../mainwindow.cpp" line="1003"/>
        <location filename="../publictalkedit.cpp" line="71"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Import Error</source>
        <translation>Ni on érè èvè enpòtasyon-la</translation>
    </message>
    <message>
        <source>Unable to open database</source>
        <translation>Bazdoné-la pa ka ouvé</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import</source>
        <translation>Fè vin</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import Complete</source>
        <translation>Ou fè-y vin</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="192"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="199"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="209"/>
        <source>Theme (could be a theme this speaker does not give)</source>
        <comment>The todo list Theme cell is in error</comment>
        <translation>Tèm (sa pé on tèm kè oratè-lasa pa bay)</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="221"/>
        <location filename="../todo.cpp" line="232"/>
        <source>Date Already Scheduled</source>
        <translation>Dat-lasa ja adan pwogram-la</translation>
    </message>
    <message>
        <source>File reading failed</source>
        <translation>Nou pé pa li fichyé-la</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="85"/>
        <source>Unable to read new Workbook format</source>
        <translation>Nou pé pa li sa ki adan nouvo fòwma a Kayé-la</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="87"/>
        <source>Database not set to handle language &#x27;%1&#x27;</source>
        <translation>Bazdoné-la pa fèt pou sèvi èvè &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Nothing imported (no month names recognized)</source>
        <translation>Pa ni ayen ki enpòté (pa ni pon mwa)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="89"/>
        <source>Unable to find year/month</source>
        <translation>Pa ka touvé lanné-la/mwa-la</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="90"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>Pa ni ayen ki enpòté (pa ni pon dat)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="94"/>
        <location filename="../wtimport.cpp" line="31"/>
        <location filename="../wtimport.cpp" line="32"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>Enpòtasyon dè %1 simenne dèpi %2 jiska %3</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="397"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>Tanprisouplé chwazi non a sé diskou-la ki kay èvè Kayé Vi é Ministè la.</translation>
    </message>
    <message>
        <source>Imported weeks from %1 thru %2</source>
        <translation>Simenn-la ki kay dèpi %1 jiska %2 enpoté</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>Pr</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>PE</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#L</translation>
    </message>
    <message>
        <source>#M</source>
        <comment>history table: abbreviation for &apos;Prepare your monthly presentation&apos;</comment>
        <translation>#M</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>#A</source>
        <comment>history table: abbreviation for &apos;Apply yourself to Reading and Teaching&apos;</comment>
        <translation>#K</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="20"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="21"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="26"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="27"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="32"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>#3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="34"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>VK1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>VK2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>VK3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="41"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>LB-L</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>EB</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="28"/>
        <source>One-time Scanning of New Slip</source>
        <translation>Skané on sèl fwa Nouvo Fich atribusyon la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="820"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>Tout sé réinyon a wikenn-la pou</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="832"/>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="940"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>Tout sé diskou-la pou fè déwò pou</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="956"/>
        <location filename="../mainwindow.cpp" line="967"/>
        <source>Outgoing Talks</source>
        <translation>Diskou pou fè déwò</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1072"/>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="467"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="206"/>
        <location filename="../publictalkedit.cpp" line="68"/>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <source>Material</source>
        <comment>Source material</comment>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="473"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="212"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Ansanm</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="166"/>
        <source>Sister</source>
        <translation>Sè</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Brother</source>
        <translation>Frè</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="171"/>
        <source>Dear %1 %2</source>
        <translation>Zanmi %1 %2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>Gadé pliba ou ké vouè détay a pwochen sijé-aw:</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>Regards</source>
        <translation>Fè on bon ti préparasyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="970"/>
        <source>You have a new kind of slip. Please enter a request on the forum so the app can be updated.  Mention code %1</source>
        <translation>Ou ni on nouvo modèl a Fich atribusyon? Tranprisouplé voyé-nou on ti mésaj adan foròm a TheocBase èvè niméro-lasa: %1. Konsa nou ké pé mèt aplikasyon-la ajou.</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="203"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>Pwoklamatè</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="96"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Gadé pwogram a réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="98"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Modifiyé pwogram a réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="100"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>Vwè paramèt a réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="102"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>Modifiyé paramèt a réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="104"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>Fè yo sonjé réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="106"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Enprimé pwogram a réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="108"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>Enprimé atribusyon pou réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="110"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>Enprimé fèy pou travay pou réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="112"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Gadé pwogram a réinyon a fen a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="114"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Changé pwogram a réinyon a fen a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="116"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>Gadé paramèt a réinyon a fen a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="118"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>Modifiyé paramèt a réinyon a fen a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="120"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>Gadé list a sé diskou-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="122"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>Modifyé list a sé diskou-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="124"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>Pwogram pou risivouè</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="126"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Enprimé pwogram a réinyon a fen a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="128"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>Enprimé fèy pou travay pou réinyon a fen a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="130"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>Enprimé pwogram a sé oratè-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="132"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>Enprimé atribusyon a sé oratè-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="134"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>Enprimé Pwogram pou risivouè</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="136"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>Enprimé list a sé diskou-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="146"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>Vwè sé oratè-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="148"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>Modifiyé sé oratè-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="138"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>Tout sé pwoklamatè-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="140"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>Modifyé lis a sé pwoklamaté-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="142"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation>Vwè enfòwmasyon a sé zélèv-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="144"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation>Modifyé enfòwmasyon a sé zélèv-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="150"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation>Vwè sé privilèj-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="152"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation>Modifyé sé privilèj-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="154"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation>Vwè ki diskou ki ja fèt adan réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="156"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation>Vwè lè yo disponib</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="158"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation>Modifyé lè yo disponib</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="160"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>Vwè sé pèmisyon-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="162"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>Modifiyé sé pèmisyon-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="164"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>Gadé sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="166"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>Chanjé sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="168"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>Enprimé dosyé a sé Tèritwa la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="170"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>Enprimé kat a téritwa la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="172"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>Enprimé kat a téritwa-la é sé adrès-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="174"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>Vwè réglaj a sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="176"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>Modifiyé sé paramèt la pou sé téritwa la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="178"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>Vwè atribusyon a sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="180"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>Vwè adrès a sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="182"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>Vwè paramèt a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="184"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>Modifiyé paramèt a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="186"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>Gadé sé évènman èspésyal la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="188"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>Modifiyé sé évènman èspésyal la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="190"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>Gadé list a sé kantik-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="192"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>Modifyé list a sé kantik-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="194"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="205"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>Ansyen</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="207"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>Prézidan a réinyon Vi é Ministè a on krétyen</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>Rèsponsab a réinyon Vi é Ministè a on krétyen</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="211"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>Koòwdinatè a sé diskou piblik-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="213"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>Responsab a sé tèritwa-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>Sèkrétè</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>Rèsponsab a prédikasyon</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>Koòwdinatè a sé ansyen-la</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="221"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>Jèsyonè</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="174"/>
        <location filename="../settings.ui" line="177"/>
        <location filename="../settings.ui" line="1610"/>
        <source>Exceptions</source>
        <translation>Èksèpsyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="207"/>
        <source>Public Talks</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="773"/>
        <source>Custom templates folder</source>
        <translation>Dosyé a sé modèl-la ki pèrsonalizé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="917"/>
        <source>Open database location</source>
        <translation>Ouvè la bazdoné-la yé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Backup database</source>
        <translation>Fè on sòvgad a bazdoné-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Restore database</source>
        <translation>Roumèt bazdoné-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1454"/>
        <source>Names display order</source>
        <translation>Mannyè pou montré sé non-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1521"/>
        <source>Color palette</source>
        <translation>Sé koulè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1466"/>
        <source>By last name</source>
        <translation>Èvè sé non-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>By first name</source>
        <translation>Èvè sé tinon-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1533"/>
        <source>Light</source>
        <translation>Klè</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1545"/>
        <source>Dark</source>
        <translation>Fonsé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2497"/>
        <source>Public talks maintenance</source>
        <translation>Modifyé sé diskou piblik la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2589"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>Pwogram pou risivouè sé oratè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3105"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3111"/>
        <source>Street types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3361"/>
        <source>Map marker scale:</source>
        <translation>Zouti pou kat-la:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3387"/>
        <source>Geo Services</source>
        <translation>Jéolokalizasyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3395"/>
        <source>Google:</source>
        <translation>Google Mpas:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3405"/>
        <location filename="../settings.ui" line="3408"/>
        <source>API Key</source>
        <translation>Clé a API-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3415"/>
        <source>Here:</source>
        <translation>here.com:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3422"/>
        <source>Default:</source>
        <translation>Sélèksyon otomatik:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3432"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3437"/>
        <source>Google</source>
        <translation>Google Maps</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3442"/>
        <source>Here</source>
        <translation>here.com</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3457"/>
        <location filename="../settings.ui" line="3460"/>
        <source>App Id</source>
        <translation>ID a aplikasyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3467"/>
        <location filename="../settings.ui" line="3470"/>
        <source>App Code</source>
        <translation>Code aplikasyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3506"/>
        <source>Send E-Mail Reminders</source>
        <translation>Voyé on imèl pou fè-y sonjé?</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3756"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>Voyé sé rapèl-la lè ou ka fèmé TheocBase</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3528"/>
        <source>E-Mail Options</source>
        <translation>Òpsyon a sé imèl</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3562"/>
        <source>Sender&#x27;s e-mail</source>
        <translation>Imèl a moun-la ki ka voyé mésaj-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3572"/>
        <source>Sender&#x27;s name</source>
        <translation>Non a moun-la ki ka voyé-y</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3666"/>
        <source>Test Connection</source>
        <translation>Gadé konèksyon la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <source>Printing</source>
        <translation>Enprésyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3796"/>
        <source>Users</source>
        <translation>Sé itilizatè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3812"/>
        <source>E-mail:</source>
        <translation>Imèl:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3865"/>
        <source>Rules</source>
        <translation>Règ pou rèspekté</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <location filename="../settings.ui" line="162"/>
        <source>General</source>
        <translation>Prensipal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="839"/>
        <source>Backup</source>
        <translation>Sòvgad</translation>
    </message>
    <message>
        <source>Database backup</source>
        <translation>Sòvgadé bazdoné-la</translation>
    </message>
    <message>
        <source>Restore backup database</source>
        <translation>Roumèt bazdoné-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="468"/>
        <source>Current congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface</source>
        <translation>Anviwònman pou itilizatè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1386"/>
        <source>User interface language</source>
        <translation>Lang pou itilizatè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1063"/>
        <source>Security</source>
        <translation>Sékirité</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1242"/>
        <source>Enable password</source>
        <translation>Aktivé modpas-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>Enable database encryption</source>
        <translation>Kripté bazdoné-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1168"/>
        <source>Confirm</source>
        <translation>Konfiwmé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="189"/>
        <location filename="../settings.ui" line="192"/>
        <location filename="../settings.ui" line="1820"/>
        <source>Life and Ministry Meeting</source>
        <translation>Réinyon Vi é Ministè a on krétyen</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2013"/>
        <source>Remove Duplicates</source>
        <translation>Éfasé séla ki ka parèt plizyè fwa</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2020"/>
        <source>Meeting Items</source>
        <translation>Pati a réinyon-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2119"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>Pwogram</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2599"/>
        <source>Hide discontinued</source>
        <translation>Kaché sé diskou la nou arété bay</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2781"/>
        <source>Add songs</source>
        <translation>Ajouté sé kantik la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="2787"/>
        <source>Songs</source>
        <translation>Sé kantik-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2859"/>
        <location filename="../settings.ui" line="2865"/>
        <source>Add song one at a time</source>
        <translation>Ajouté on kantik yon apré lòt</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2880"/>
        <source>Song number</source>
        <translation>Niméro a kantik-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2887"/>
        <source>Song title</source>
        <translation>Tit a kantik-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2961"/>
        <source>Cities</source>
        <translation>Vil</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3033"/>
        <source>Territory types</source>
        <translation>Tip a téritwa-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3210"/>
        <source>Addresses</source>
        <translation>Sé adrès-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3216"/>
        <source>Address types</source>
        <translation>Tip a adrès-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3324"/>
        <source>Configuration</source>
        <translation>Konfigirasyon</translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation>Léchèl:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <location filename="../settings.cpp" line="2263"/>
        <source>Access Control</source>
        <translation>Kontwòlé sé aksè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3222"/>
        <source>Type number:</source>
        <translation>Niméro a tip-la:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3117"/>
        <location filename="../settings.ui" line="3232"/>
        <location filename="../settings.ui" line="3802"/>
        <source>Name:</source>
        <translation>Non:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3186"/>
        <location filename="../settings.ui" line="3242"/>
        <source>Color:</source>
        <translation>Koulè:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3127"/>
        <location filename="../settings.ui" line="3252"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3332"/>
        <source>Default address type:</source>
        <translation>Ègzanp a adrès tip:</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;#&quot;&gt;Forgot Password&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;#&quot;&gt;Modpas pèd&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Delete Cloud Data</source>
        <translation>Éfasé sé ki adan klaoud-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="228"/>
        <source>Territories</source>
        <translation>Téritwa</translation>
    </message>
    <message>
        <source>TheocBase Cloud</source>
        <translation>TheocBase klaoud</translation>
    </message>
    <message>
        <source>Open Database Location</source>
        <translation>Ouvè la bazdoné-la yé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="1101"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="435"/>
        <source>Current Circuit Overseer</source>
        <translation>Sèwvitè a siwkonskripsyon-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="668"/>
        <source>Click to edit</source>
        <translation>Kliké pou modifyé-y</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1111"/>
        <source>Username</source>
        <translation>Non a itilizatè-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <location filename="../settings.ui" line="3673"/>
        <source>Password</source>
        <translation>Modpas</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="529"/>
        <location filename="../settings.ui" line="1838"/>
        <location filename="../settings.ui" line="1902"/>
        <source>Mo</source>
        <translation>Lend</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="534"/>
        <location filename="../settings.ui" line="1843"/>
        <location filename="../settings.ui" line="1907"/>
        <source>Tu</source>
        <translation>Maw</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="539"/>
        <location filename="../settings.ui" line="1848"/>
        <location filename="../settings.ui" line="1912"/>
        <source>We</source>
        <translation>Mèr</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="544"/>
        <location filename="../settings.ui" line="1853"/>
        <location filename="../settings.ui" line="1917"/>
        <source>Th</source>
        <translation>Jéd</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.ui" line="1858"/>
        <location filename="../settings.ui" line="1922"/>
        <source>Fr</source>
        <translation>Van</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="554"/>
        <location filename="../settings.ui" line="1863"/>
        <location filename="../settings.ui" line="1927"/>
        <source>Sa</source>
        <translation>San</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="559"/>
        <location filename="../settings.ui" line="1868"/>
        <location filename="../settings.ui" line="1932"/>
        <source>Su</source>
        <translation>Dim</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1881"/>
        <source>Public Talk and Watchtower study</source>
        <translation>Diskou piblik é étid a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1754"/>
        <location filename="../settings.ui" line="1783"/>
        <location filename="../settings.ui" line="2210"/>
        <location filename="../settings.ui" line="2271"/>
        <location filename="../settings.ui" line="2321"/>
        <location filename="../settings.ui" line="2360"/>
        <location filename="../settings.ui" line="2420"/>
        <location filename="../settings.ui" line="2443"/>
        <location filename="../settings.ui" line="2530"/>
        <location filename="../settings.ui" line="2730"/>
        <location filename="../settings.ui" line="2808"/>
        <location filename="../settings.ui" line="2831"/>
        <location filename="../settings.ui" line="2906"/>
        <location filename="../settings.ui" line="2982"/>
        <location filename="../settings.ui" line="3008"/>
        <location filename="../settings.ui" line="3054"/>
        <location filename="../settings.ui" line="3080"/>
        <location filename="../settings.ui" line="3149"/>
        <location filename="../settings.ui" line="3175"/>
        <location filename="../settings.ui" line="3271"/>
        <location filename="../settings.ui" line="3297"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Custom Templates Folder</source>
        <translation>Dosyé a sé modèl-la ki pèrsonalizé</translation>
    </message>
    <message>
        <source>iCal Export</source>
        <translation>Èkspò iCal</translation>
    </message>
    <message>
        <source>Events grouped by date</source>
        <translation>Évènman ki sanblé sèlon dat-la</translation>
    </message>
    <message>
        <source>All day events</source>
        <translation>Si ki fèt tout jouné-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1652"/>
        <source>Circuit Overseer&#x27;s visit</source>
        <translation>Vizit a sèwvitè a siwkonskripsyon-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1657"/>
        <source>Convention</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1662"/>
        <source>Memorial</source>
        <translation>Mémoryal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1667"/>
        <source>Zone overseer&#x27;s talk</source>
        <translation>Diskou a rèprézantan a sièg mondial</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1672"/>
        <source>Other exception</source>
        <translation>Dòt èksèpsyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1685"/>
        <location filename="../settings.cpp" line="608"/>
        <source>Start date</source>
        <translation>Dat i koumansé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1716"/>
        <location filename="../settings.cpp" line="609"/>
        <source>End date</source>
        <translation>Dat i fin</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1833"/>
        <location filename="../settings.ui" line="1897"/>
        <source>No meeting</source>
        <translation>Pani réinyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1799"/>
        <source>Description</source>
        <translation>Dèskripsyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2053"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>Pòté kayé a réinyon Vi é Ministè a on krétyen-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1981"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>Pensipal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2132"/>
        <source>Year</source>
        <translation>Lanné</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="486"/>
        <location filename="../settings.ui" line="2170"/>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Érè&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2297"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>Pwogram a Réinyon simenn-la pou sa ki chwazi</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3584"/>
        <source>Custom settings</source>
        <translation>Opsyon</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3705"/>
        <source>Authorize</source>
        <translation>Otorizé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3715"/>
        <source>Status</source>
        <translation>Jan i yé</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2788"/>
        <source>Synchronize</source>
        <translation>Senkwonizé</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Non a itilizatè-la ou imèl-ay</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;http://register.theocbase.net/index.php&quot;&gt;Create an account&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;http://register.theocbase.net/index.php&quot;&gt;Kréyé on kont&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="237"/>
        <source>Reminders</source>
        <translation>Sonjé</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3932"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>Ègzékité on fichyé kè sèwvis tèknik voyé ba-w</translation>
    </message>
    <message>
        <source>Run Special Command</source>
        <translation>Ègzékité on kòmand èspésyal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Sé réglaj-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1987"/>
        <source>Number of classes</source>
        <translation>Kantité sal ki ni</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2741"/>
        <source>Studies</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <source>Add subjects</source>
        <translation>Ajouté sé tèm la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2613"/>
        <location filename="../settings.ui" line="2619"/>
        <source>Add subject one at a time</source>
        <translation>Ajouté on tèm alafwa</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2653"/>
        <source>Public talk number</source>
        <translation>Niméwo a sé diskou-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2660"/>
        <source>Public talk subject</source>
        <translation>Tèm a diskou piblik</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2670"/>
        <location filename="../settings.ui" line="2917"/>
        <source>Language</source>
        <translation>Lang-la</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2702"/>
        <source>Add congregations and speakers</source>
        <translation>Ajouté lasanblé é oratè</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fèmé</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="828"/>
        <location filename="../settings.cpp" line="1103"/>
        <location filename="../settings.cpp" line="1171"/>
        <source>Number</source>
        <translation>Niméwo</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2558"/>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="556"/>
        <source>Select a backup file</source>
        <translation>Chwazi fichyé-la pou roumèt</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="934"/>
        <location filename="../settings.cpp" line="1256"/>
        <location filename="../settings.cpp" line="1553"/>
        <location filename="../settings.cpp" line="1621"/>
        <location filename="../settings.cpp" line="1705"/>
        <location filename="../settings.cpp" line="1801"/>
        <source>Remove selected row?</source>
        <translation>Éfasé sa ki sélèksyoné ?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1032"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Ja ni on diskou piblik ki ni menm niméro-la!
Ès ou vlé arété sèvi èvè tala ki té ja la?

Sé diskou-la ki pou fèt ké adan &quot;Biten pou fè&quot;.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1172"/>
        <source>Title</source>
        <translation>Tit</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1272"/>
        <source>Song number missing</source>
        <translation>Ka manké niméro a kantik-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1275"/>
        <source>Song title missing</source>
        <translation>Ka manké tèm a kantik-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1285"/>
        <source>Song is already saved!</source>
        <translation>Kantik-lasa ja anrèjistré!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1294"/>
        <source>Song added to database</source>
        <translation>Kantik la adan bazdoné la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1331"/>
        <source>City</source>
        <translation>Vil</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1367"/>
        <location filename="../settings.cpp" line="1451"/>
        <source>Type</source>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1517"/>
        <source>City name missing</source>
        <translation>Ka manké non a vil-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1526"/>
        <source>City is already saved!</source>
        <translation>Vil-lasa ja anrèjistré !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1534"/>
        <source>City added to database</source>
        <translation>Vil-la ajouté adan bazdoné la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1585"/>
        <source>Territory type name missing</source>
        <translation>Ka manké tip a téritwa-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1594"/>
        <source>Territory type is already saved!</source>
        <translation>Tip a téritwa-la ja anrèjistré!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1602"/>
        <source>Territory type added to database</source>
        <translation>Tip a téritwa-la ajouté adan bazdoné la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1671"/>
        <source>Name of the street type is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1678"/>
        <source>Street type is already saved!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1686"/>
        <source>Street type added to database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2026"/>
        <source>This is no longer an option. Please request help in the forum if this is needed.</source>
        <translation>Ou pé pa sèvi èvè opsyon-lasa ankò. Tanprisouplé, si ou bizwen èd mandé-y si fowòm-la.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2263"/>
        <source>Remove permissions for the selected user?</source>
        <translation>Woté pèmisyon pou sé itilizatè kè ou chwazi la?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2495"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="829"/>
        <location filename="../settings.cpp" line="2557"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="831"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>Pibliyé lè</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="832"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>Paka sèvi èvè-y dèpi</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="985"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>Si nou pa bay diskou-lasa ankò, nou ké mèt séla ki téja prévwa é plan-la adan &quot;Biten pou fè&quot;. 

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1104"/>
        <source>Revision</source>
        <translation>Révizyon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1408"/>
        <location filename="../settings.cpp" line="1452"/>
        <location filename="../settings.cpp" line="2140"/>
        <location filename="../settings.cpp" line="2196"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1409"/>
        <location filename="../settings.cpp" line="1453"/>
        <source>Color</source>
        <translation>Koulè</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1758"/>
        <source>Number of address type is missing</source>
        <translation>Ka manké niméro a tip a adrès-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1762"/>
        <source>Name of address type is missing</source>
        <translation>Ka manké non a tip a adrès-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1771"/>
        <source>Address type is already saved!</source>
        <translation>Tip a adrès-la ja anrèjistré!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1781"/>
        <source>Address type added to database</source>
        <translation>Tip a adrès-la ajouté adan bazdoné la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1879"/>
        <source>Error sending e-mail</source>
        <translation>Imèl pa pati, i kontré on érè</translation>
    </message>
    <message>
        <source>Sign In</source>
        <translation>Konèkté-w</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2790"/>
        <source>Sign Out</source>
        <translation>Dékonèkté-w</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Konèksyon-la pa maché</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1931"/>
        <source>Authorized. (Re-authorization only needed if you are getting error messages when sending mail.)</source>
        <translation>Otorizé. (Ou bizwen on dòt otorizasyon si sèlman ou ka risivwè on mésaj dérè lè ou ka voyé on imel)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1933"/>
        <source>Click the button to authorize theocbase to send email on your behalf</source>
        <translation>Kliké si bouton-la pou ba TheocBase otorizasyon pou voye imèl ba-w.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2002"/>
        <location filename="../settings.cpp" line="2683"/>
        <source>Select ePub file</source>
        <translation>Chwazi on fichyé ePub</translation>
    </message>
    <message>
        <source>TMS History Copied (up to 2 years of assignments)</source>
        <translation>Sa ki ja fèt adan LMT kopyé (jiska 2 lanné)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2031"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>Tansyon-aw: Asiré-w kè fichyé-lasa ka vin don sous dè konfyans. Ès ou vlé kontinyé?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2035"/>
        <source>Command File</source>
        <translation>Fichyé ki ni sé komand la</translation>
    </message>
    <message>
        <source>Last synchronized</source>
        <translation>Dènyé senkwonizasiyon-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2416"/>
        <location filename="../settings.cpp" line="2466"/>
        <source>Meeting</source>
        <translation>Réinyon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2416"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>Éfasé tout sé réinyon-la? (Itilize sèlman pou éfasé sé doné-la ki pabon adan bazdone-la)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2458"/>
        <source>Enter source material here</source>
        <translation>Ba-y sé référans-la koté sit</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2466"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>Éfasé diskou-lasa? (Itilizé sèlman pou éfasé enfòwmasyon ki pa bon adan bazdone-la)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2496"/>
        <source>Bible Reading</source>
        <translation>Lèkti a Labib</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2497"/>
        <source>Song 1</source>
        <translation>Kantik 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2498"/>
        <source>Song 2</source>
        <translation>Kantik 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2499"/>
        <source>Song 3</source>
        <translation>Kantik 3</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2556"/>
        <source>Meeting Item</source>
        <translation>Pati a réinyon-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2559"/>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2703"/>
        <source>Study Number</source>
        <translation>Niméwo a lèson-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2704"/>
        <source>Study Name</source>
        <translation>Lèson-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2771"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>Ès ou sèten ou vlé éfasé tout sé enfòwmasyon-la ki an klaoud-la?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2784"/>
        <source>The cloud data has now been deleted.</source>
        <translation>Sé enfòwmasyon-la ki té adan klaoud-la éfasé konnyéla.</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="833"/>
        <location filename="../settings.cpp" line="1105"/>
        <location filename="../settings.cpp" line="1173"/>
        <location filename="../settings.cpp" line="1332"/>
        <location filename="../settings.cpp" line="1368"/>
        <location filename="../settings.cpp" line="1454"/>
        <source>Language id</source>
        <translation>ID a lang-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1004"/>
        <source>Public talk number missing</source>
        <translation>Ka manké niméwo a diskou piblik-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1007"/>
        <source>Public talk subject missing</source>
        <translation>Ka manké tèm a diskou piblik-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1028"/>
        <source>Public talk is already saved!</source>
        <translation>Diskou piblik-lasa ja anrèjistré!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1062"/>
        <source>Public talk added to database</source>
        <translation>Diskou piblik-la ajouté adan bazdoné la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1069"/>
        <location filename="../settings.cpp" line="1301"/>
        <location filename="../settings.cpp" line="1540"/>
        <location filename="../settings.cpp" line="1608"/>
        <location filename="../settings.cpp" line="1692"/>
        <location filename="../settings.cpp" line="1788"/>
        <source>Adding failed</source>
        <translation>Pon biten ajouté!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="607"/>
        <source>Exception</source>
        <translation>Èksèpsyon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="610"/>
        <source>Meeting 1</source>
        <translation>Réinyon 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="611"/>
        <source>Meeting 2</source>
        <translation>Réinyon 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="574"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Nou woumèt bazdoné-la. Konnyéla pwogram-la ka-y rikoumansé travay.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2742"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>Éfasé tout sé lèson-la? (Itilize sèlman pou éfasé sé doné-la ki pabon adan bazdone-la)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1100"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1099"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1102"/>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="538"/>
        <source>Save database</source>
        <translation>Anrèjistré bazdoné-la</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="546"/>
        <source>Database backuped</source>
        <translation>Konnyéla bazdoné-la sòvgadé</translation>
    </message>
</context>
<context>
    <name>SmtpClient</name>
    <message>
        <location filename="../smtp/smtpclient.cpp" line="392"/>
        <source>Sender&#x27;s name</source>
        <translation>Non a moun-la ki ka voyé-y</translation>
    </message>
    <message>
        <location filename="../smtp/smtpclient.cpp" line="397"/>
        <source>Sender&#x27;s email</source>
        <translation>Imèl a moun-la ki ka voyé mésaj-la</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="13"/>
        <source>Welcome to theocbase</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Zélèv</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tout</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>Èvè zélèv-la</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Moun-la ki ka fè sijè-la èvè-y dwèt mèm sèks ki-y.</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation>Kad</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rézilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ja fèt</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Lèson pou travay konnyéla</translation>
    </message>
    <message>
        <source>Exercise Completed</source>
        <translation>Ègzèsis-la fin</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen lèson</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="65"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="79"/>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="94"/>
        <source>Student</source>
        <translation>Zélèv</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="102"/>
        <location filename="../qml/StudentAssignmentPanel.qml" line="149"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>Tout</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="138"/>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="148"/>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>Èvè Zélèv-la</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="163"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Moun-la ki ka fè sijè-la èvè-y dwèt mèm sèks ki-y.</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="185"/>
        <source>Study point</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="202"/>
        <source>Result</source>
        <translation>Rézilta</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="208"/>
        <source>Completed</source>
        <translation>Ja fèt</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="225"/>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="260"/>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="277"/>
        <source>Current Study</source>
        <translation>Lèson pou travay konnyéla</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="293"/>
        <source>Exercise Completed</source>
        <translation>Ègzèsis-la fin</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="303"/>
        <source>Next Study</source>
        <translation>Prochen lèson-la</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="324"/>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="53"/>
        <source>Address</source>
        <translation>Adrès</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="115"/>
        <source>Street:</source>
        <translation>Lari:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="125"/>
        <source>Postalcode:</source>
        <translation>Kòd pòstal:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="147"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>Chèché</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="162"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Péyi</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="179"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Réjyon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="197"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Dépawtman</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="75"/>
        <source>Country:</source>
        <translation>Péyi:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="85"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>Réjyon:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="95"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>vil:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="105"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Réjyon:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="135"/>
        <source>No.:</source>
        <translation>N°:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="215"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Vil</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="232"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Réjyon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="249"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Lari</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="266"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Nim.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="283"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>Kòd pòstal</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="300"/>
        <source>Latitude</source>
        <translation>Latitid</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="317"/>
        <source>Longitude</source>
        <translation>Lonjitid</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="349"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="354"/>
        <source>Cancel</source>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="53"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="79"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="156"/>
        <source>Address</source>
        <translation>Adrès</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="197"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="256"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>Endéfini [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="299"/>
        <source>Edit address</source>
        <translation>Maké on adrès</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="347"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>Ajouté on adrès</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="348"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>Tanprisouplé, chwazi on adrès adan rézilta a chèché-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="407"/>
        <location filename="../qml/TerritoryAddressList.qml" line="417"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Chèché on adrès</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="408"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>Adrès-lasa pa ka ègzisté.</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="39"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>ID-Tèritwa</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="83"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Péyi</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="106"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Réjyon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="129"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Dépawtman</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="173"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>Réjyon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="151"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Vil</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="196"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>Lari</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="221"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>N°</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="245"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>Kòd pòstal</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="268"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>Koòwdoné GPS</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="290"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="300"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="325"/>
        <source>Add new address</source>
        <translation>Ajouté on nouvo adrès</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="336"/>
        <source>Edit selected address</source>
        <translation>Modifyé adrès-lasa</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="347"/>
        <source>Remove selected address</source>
        <translation>Éfasé adrès-la ki chwazi</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="78"/>
        <source>Filename:</source>
        <translation>Non a fichyé-la:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="133"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>Menm enfòwmasyon KML</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="55"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>Fen a tèritwa-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="63"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>Sé adrès-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="163"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>Dèskripsyon:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="180"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>Chèché èvè &quot;Dèskripsyon&quot; sianka i pa touvè-y èvè non-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="195"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>Tèritwa n°</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="201"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>Ki koté</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="207"/>
        <source>Remark</source>
        <translation>Rèmak</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>Menm enfòwmasyon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="255"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>Adrès:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="266"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>Non:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="304"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>Adrès tip:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="326"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>Non a fichyé la ki ni sé adrès-la ki pa bon:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="396"/>
        <source>Import</source>
        <translation>Enpòté</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="405"/>
        <source>Close</source>
        <translation>Fèmé</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="132"/>
        <source>Group by:</source>
        <translation>Sanblé pa:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="139"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>Vil</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="141"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>Tip</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="142"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>Ja travayé</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="242"/>
        <source>Add new territory</source>
        <translation>Ajouté on nouvo téritwa</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="257"/>
        <source>Remove selected territory</source>
        <translation>Éfasé téritwa-la ki chwazi</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="353"/>
        <source>Remark</source>
        <translation>Rèmak</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="372"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>Tip:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="399"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>Vil:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="464"/>
        <source>Assignments</source>
        <translation>Sé sijé-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="831"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="870"/>
        <source>Addresses</source>
        <translation>Sé adrès-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="298"/>
        <source>No.:</source>
        <translation>N°:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="309"/>
        <source>Number</source>
        <translation>Niméwo</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="320"/>
        <source>Locality:</source>
        <translation>Ki koté:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="330"/>
        <source>Locality</source>
        <translation>Ki koté</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="434"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>Kat</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="499"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="513"/>
        <source>Publisher-ID</source>
        <translation>ID-Pwoklamatè</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="627"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>Dat i sòti</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="633"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>Dat i rètouné</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="784"/>
        <source>Add new assignment</source>
        <translation>Ajouté on nouvo sijé</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="808"/>
        <source>Remove selected assignment</source>
        <translation>Éfasé sijé-la ki chwazi</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="592"/>
        <source>Publisher</source>
        <comment>Dropdown column title</comment>
        <translation>Pwoklamatè</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="334"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Chèché on adres</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="760"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="762"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#x27;No&#x27; if overlapping areas should remain in their territories and to add only the part, that doesn&#x27;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="796"/>
        <source>Add address to selected territory</source>
        <translation>Ajouté on adrès adan tèritwa-la ki chwazi</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="804"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>Bay a téritwa-la ki chwazi</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="812"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>Éfasé adrès-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="902"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>Yenki %1 dè %2 adrès ki enpòté.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="917"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>Enpòte limit a sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="927"/>
        <location filename="../qml/TerritoryMap.qml" line="938"/>
        <location filename="../qml/TerritoryMap.qml" line="953"/>
        <location filename="../qml/TerritoryMap.qml" line="962"/>
        <location filename="../qml/TerritoryMap.qml" line="995"/>
        <location filename="../qml/TerritoryMap.qml" line="1004"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>Enpòte enfòwmasyon a sé téritwa-la</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="928"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>Ni %n téritwa ki enpòté ou ajou.</numerusform>
            <numerusform>Ni %n téritwa ki enpòté ou ajou.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="954"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Tanprisouplé, chwazi koté-la pou maké adrès-la é non-la.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="963"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Ou pé pa chwazi menm-la.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="971"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>Enpòte adrès a sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="972"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>Nou ké ajouté sé adrès-la adan téritwa-la. Mè fò ou chwazi on tèritwa anvan.</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="996"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>Ni %n adrès ki enpòté.</numerusform>
            <numerusform>Ni %n adrès ki enpòté.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1006"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>Ou pa chwazi pon téritwa ki bon.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1008"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>Nou pé pa li fichyé-lasa.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1040"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>Adrès</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1042"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1071"/>
        <location filename="../qml/TerritoryMap.qml" line="1079"/>
        <source>Open file</source>
        <translation>Ouvè on fichyé</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1072"/>
        <location filename="../qml/TerritoryMap.qml" line="1073"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>Fichyé KML (*.kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1072"/>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>Tout sé fichyé-la (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1081"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <location filename="../qml/TerritoryMap.qml" line="1089"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>Fichyé CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>Fichyé tèks (*.txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1087"/>
        <source>Save file</source>
        <translation>Anrèjistré fichyé-la</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="65"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>Chèché</translation>
    </message>
    <message>
        <source>Import boundaries</source>
        <comment>Import territory boundaries from a file</comment>
        <translation>Enpòté sé limit-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="149"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>Enpòté enfòwmasyon</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="193"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>Modifyé-y</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="207"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="219"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>Éfasé sé limit-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="231"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="160"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>Vwè</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="170"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>Montré/kaché sé téritwa-la</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="181"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>Montré/kaché sé makè-la</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="209"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="269"/>
        <location filename="../qml/TerritoryStreetList.qml" line="280"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="270"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="38"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="60"/>
        <source>Territory-ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="166"/>
        <source>Add new street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="177"/>
        <source>Remove selected street</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 mwa</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 a 12 mwa</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 mwa</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>Pòkò travayé</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>Pòkò bay-li</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>tèritwa</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>téritwa</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="114"/>
        <source>To Do List</source>
        <translation>Biten pou fè</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="209"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>Ou péké pé pwogramé-y toutotan ou pa korijé sa: %1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Incoming</source>
        <comment>incoming speaker</comment>
        <translation>Vizitè</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Outgoing</source>
        <comment>outgoing speaker</comment>
        <translation>I ka sòti</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="10"/>
        <source>Text</source>
        <translation>Tèks</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="69"/>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="45"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>Dènyé diskou sèvis-la</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="66"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="97"/>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="109"/>
        <location filename="../qml/WEMeetingModule.qml" line="316"/>
        <source>Song and Prayer</source>
        <translation>Kantik é priyè</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="110"/>
        <location filename="../qml/WEMeetingModule.qml" line="317"/>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 é priyè-la</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="145"/>
        <source>PUBLIC TALK</source>
        <translation>DISKOU PIBLIK</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="163"/>
        <source>Send to To Do List</source>
        <translation>Mété-y adan &quot;Biten pou fè&quot;</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="169"/>
        <source>Move to different week</source>
        <translation>Mété-y adan on dòt simenn</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="178"/>
        <source>Clear Public Talk selections</source>
        <translation>Éfasé sé diskou piblik-la ki sélèksyoné</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="226"/>
        <source>WATCHTOWER STUDY</source>
        <translation>ÉTID A TOU DÈ GAD LA</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="235"/>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="256"/>
        <source>Import WT...</source>
        <translation>Enpòté TG...</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="284"/>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="285"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="25"/>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="46"/>
        <source>Watchtower Issue</source>
        <translation>Édisyon a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="68"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Awtik</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="80"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="99"/>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="121"/>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <source>Outside speaker</source>
        <translation>Oratè ki ka sòti</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="238"/>
        <source>Brother has other meeting parts</source>
        <translation>I ja ni on atribusyon pou réinyon-lasa</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="233"/>
        <source>Person unavailable</source>
        <translation>Moun lasa pa disponib</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="228"/>
        <source>Outgoing speaker</source>
        <translation>Oraté ki ka sòti</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="118"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(Dosyé-lasa pa disponib)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>Ni on nouvo vèwsyon ki disponib</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>Chajé-y</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>Nouvo vèwsyon...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>Pa ni pon mizajou ki disponib</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="112"/>
        <source>File reading failed</source>
        <translation>Nou pé pa li fichyé-la</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="450"/>
        <source>XML file generated in the wrong version.</source>
        <translation>Fichyé XML-la fèt adan on mové vèwsyon</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="517"/>
        <source>Persons - added </source>
        <translation>Sé moun-la - ajouté </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="555"/>
        <source>Persons - updated </source>
        <translation>Sé moun-la - ajou </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="586"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>Lékòl pou Ministè Téokratik - pwogram la disponib </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="588"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>Lékòl pou Ministè Téokratik - pwogram la ajou </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="784"/>
        <source>Theocratic Ministry School - Study updated</source>
        <translation>Lékòl pou Ministè Téokratik - Lèson ajou</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="798"/>
        <source>Theocratic Ministry School - Study added</source>
        <translation>Lékòl pou Ministè Téokratik - Lèson ajouté</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="834"/>
        <source>Public talks - theme added </source>
        <translation>Tèm a diskou piblik-la - ajouté </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="892"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>Diskou Piblik é Étid a TG - pwogram la a ajou </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="898"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>Diskou Piblik é Étid a TG - pwogram la a ajou </translation>
    </message>
    <message>
        <source>Congregation Bible Study - schedule added </source>
        <translation>Étid biblik a lasanblé-la - pwogram la ajouté </translation>
    </message>
    <message>
        <source>Congregation Bible Study - schedule updated </source>
        <translation>Étid biblik a lasanblé-la - pwogram la ajou </translation>
    </message>
    <message>
        <source>Service Meeting - schedule added </source>
        <translation>Réyinyon sèvis - pwgram-la ajouté </translation>
    </message>
    <message>
        <source>Service Meeting - schedule updated </source>
        <translation>Réyinyon sèvis - pwogram-la ajou </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="210"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>Pa vizité-y</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1512"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>Anrèjistré sé adrès-la ki pa bon</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1513"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>Ou pé sèlman li fichyé-lasa</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>Pòkò chwazi</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>Oké</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>Oké men pa ni JSON</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>Pwosésis otorizasyon...</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>Otorizasyon-la pa maché</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>Klian-la pa ni ID</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>Ka manké sèké klian-la</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>Fò mèt kòd otorizasyon la</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>Fò mèt ajou jèton-la. Parapòt a Gmail</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="196"/>
        <source>Number of weeks after selected date</source>
        <translation>Kantité simenn apré dat-la ki chwazi-la</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="246"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Kantite simenn pou mèt an gri apré on sijé</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="220"/>
        <source>weeks</source>
        <translation>Simenn</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="122"/>
        <source>Timeline</source>
        <translation>Sa ki ja fèt</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="180"/>
        <source>Number of weeks before selected date</source>
        <translation>Kantité simenn avan dat-la ki chwazi-la</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="418"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>Sé pwoklamatè-la</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="589"/>
        <source>CBS</source>
        <translation>EBL</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="684"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="589"/>
        <location filename="../historytable.cpp" line="614"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="611"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="623"/>
        <source>TMS</source>
        <translation>LMT</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="750"/>
        <source>SM</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="768"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>DP</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="774"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>Pr</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="783"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>TG-L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="999"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1000"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1001"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1002"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1003"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1004"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Next &gt;</source>
        <translation>Apré &gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>&lt; Back</source>
        <translation>&lt; Rètou</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="41"/>
        <source>Cancel</source>
        <translation>Anilé</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="67"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Enpòté pwogram a Lekòl ministè teyokratik la. Kopyé pwogram la dépi WT Library é kolé yo anba (Ctrl + V / cmd +V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="68"/>
        <source>Check schedule</source>
        <translation>Vérifyé pwogram-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="71"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Enpòté sé lèson la. Kopyé sé lèson la dépi WT Library é kolé yo anba (Ctrl + V / cmd +V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="72"/>
        <source>Check studies</source>
        <translation>Vérifyé sé lèson-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="75"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Enpòté sé kad la. Kopyé sé kad la dépi WT Library é kolé yo anba (Ctrl + V / cmd +V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="76"/>
        <source>Check settings</source>
        <translation>Vérifyé sé kad-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="80"/>
        <source>Check subjects</source>
        <translation>Vérifyé sé tèm la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="83"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Ajouté non a sé oraté la é sé lasanblé la. Kopyé sé enfòwmasyon la é kolé yo adan karé ki anba (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="84"/>
        <source>Check data</source>
        <translation>Vérifyé sé enfòwmasyon la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="87"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Ajouté sé kantik la. Kopyé sé enfòwmasyon la é kolé yo adan karé ki anba (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="88"/>
        <source>Check songs</source>
        <translation>Vérifyé sé kantik-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="100"/>
        <source>No schedule to import.</source>
        <translation>Pa ni pwogram pou enpòté.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="141"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="142"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>Tanprisouplé mété jou-la i ka koumansé LLLL-MM-JJ (eg. 2019-01-03)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="149"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>Dat-lasa pa prèmyé jou a simenn-la (Lendi)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="202"/>
        <source>Import songs</source>
        <translation>Enpòté sé kantik-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="244"/>
        <source>Only brothers</source>
        <translation>Frè sèlman</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="474"/>
        <source>Subject</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="517"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="520"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="523"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="526"/>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="529"/>
        <source>Public talks</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="532"/>
        <source>Language</source>
        <translation>Lang-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="549"/>
        <location filename="../importwizard.cpp" line="550"/>
        <source>First name</source>
        <translation>Tinon</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="549"/>
        <location filename="../importwizard.cpp" line="550"/>
        <source>Last name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="178"/>
        <source>Import subjects</source>
        <translation>Enpòté sé tèm-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="179"/>
        <location filename="../importwizard.cpp" line="203"/>
        <source>Choose language</source>
        <translation>Chwazi lang-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>Save to database</source>
        <translation>Mété-y adan bazdoné-la</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="79"/>
        <source>Add public talk&#x27;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>Ajouté tèm a sé diskou piblik la. Kopyé é kolé tèm yo anba (CTRL +V / cmd +V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="233"/>
        <source>date</source>
        <translation>dat</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="236"/>
        <source>number</source>
        <translation>niméwo</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="239"/>
        <source>subject</source>
        <translation>tèm</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="242"/>
        <source>material</source>
        <translation>référans</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="353"/>
        <source>setting</source>
        <translation>kad</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="414"/>
        <source>study</source>
        <translation>lèson</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="600"/>
        <source>Title</source>
        <translation>Tit</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="746"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Ja ni on diskou piblik ki ni menm niméro-la!
Ès ou vlé arété sèvi èvè tala ki té ja la?

Sé diskou-la ki pou fèt ké adan &quot;Biten pou fè&quot;.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="749"/>
        <source>Previous talk: </source>
        <translation>Diskou i bay avan: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="750"/>
        <source>New talk: </source>
        <translation>Nouvo diskou: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="871"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>Pa ni tèm a diskou piblik-la. Mété-y é éséyé ankò!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="898"/>
        <source> rows added</source>
        <translation> lign ajouté</translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <source>Tak Type Editor</source>
        <comment>dialog name</comment>
        <translation>Modifié tip a diskou-la</translation>
    </message>
    <message>
        <source>Continue</source>
        <comment>dialog button name</comment>
        <translation>Kontinyé</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="14"/>
        <source>Talk Name in the Workbook</source>
        <translation>Tèm a diskou-la adan Kayé Vi é Ministè</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="16"/>
        <source>Meeting Item</source>
        <translation>Pati a réinyon-la</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="42"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>Pa konèt</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>Koté pou modifyé tip a sé diskou-la</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>Non a itilizatè-la</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>Modpas</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>WEEK STARTING %1</source>
        <translation>SIMENN KI KOUMANSÉ LÈ %1</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="292"/>
        <source>Select an assignment on the left to edit</source>
        <translation>Chwazi on sijé ki an men gòch-aw pou modifyé-y</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="366"/>
        <source>Sister</source>
        <translation>Sè</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="390"/>
        <source>Brother</source>
        <translation>Frè</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="342"/>
        <source>Servant</source>
        <translation>Frè nomé</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>Sé pwoklamatè-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="782"/>
        <source>General</source>
        <translation>Prensipal</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="990"/>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1189"/>
        <source>Public talks</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="929"/>
        <location filename="../personsui.ui" line="1182"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1162"/>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1246"/>
        <source>Watchtower reader</source>
        <translation>Lèktè a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="414"/>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1120"/>
        <source>Cong. Bible Study reader</source>
        <translation>Lèktè a létid biblik a lasanblé la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1275"/>
        <source>Meeting for field ministry</source>
        <translation>Réinyon pou prédikasyon</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="542"/>
        <location filename="../personsui.cpp" line="551"/>
        <source>First name</source>
        <translation>Tinon</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="642"/>
        <location filename="../personsui.cpp" line="550"/>
        <source>Last name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="581"/>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="481"/>
        <source>E-mail</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1331"/>
        <source>Assignment</source>
        <translation>Sijé</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1448"/>
        <source>Current Study</source>
        <translation>Lèson pou travay konnyéla</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1716"/>
        <location filename="../personsui.ui" line="1739"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1212"/>
        <source>Watchtower Study Conductor</source>
        <translation>Kondiktè a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="354"/>
        <source>Family Head</source>
        <translation>Chèf a fanmi</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="426"/>
        <source>Family member linked to</source>
        <translation>I sé fanmi a</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="892"/>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1066"/>
        <source>Only Auxiliary Classes</source>
        <translation>Sèlman dézyèm sal-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1039"/>
        <source>Only Main Class</source>
        <translation>Sèlman sal prensipal-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="330"/>
        <source>Active</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="953"/>
        <source>Treasures From God&#x27;s Word</source>
        <translation>Trézò ki an Pawòl a Bondyé</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1004"/>
        <source>Initial Call</source>
        <translation>Prèmyé kontak</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="967"/>
        <source>Bible Reading</source>
        <translation>Lèkti a Labib</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1073"/>
        <source>All Classes</source>
        <translation>Tout sé sal-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="997"/>
        <source>Bible Study</source>
        <translation>Étid biblik</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1113"/>
        <source>Congregation Bible Study</source>
        <translation>Étid biblik a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1106"/>
        <source>Living as Christians Talks</source>
        <translation>Diskou a Vi a on krétyen</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Préparé prézantasyon a mwa lasa</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="960"/>
        <source>Digging for Spiritual Gems</source>
        <translation>An-nou fouyé pou jwenn trézò éspirityèl</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1011"/>
        <source>Return Visit</source>
        <translation>Nouvèl vizit</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="307"/>
        <source>Personal Info</source>
        <translation>Enfòwmasyon ki pèrsonèl</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="378"/>
        <source>Host for Public Speakers</source>
        <translation>Ka risivouè sé oratè-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="709"/>
        <source>Mobile</source>
        <translation>Pòwtab</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="751"/>
        <source>Details</source>
        <translation>Détay</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1086"/>
        <source>Talk</source>
        <translation>Diskou</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1287"/>
        <source>History</source>
        <translation>Sa ki ja fèt</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1321"/>
        <source>date</source>
        <translation>dat</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1326"/>
        <source>no</source>
        <translation>n°</translation>
    </message>
    <message>
        <source>material</source>
        <translation>référans</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1336"/>
        <source>Note</source>
        <translation>Maké</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1341"/>
        <source>Time</source>
        <translation>Lè-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1346"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Èvè</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1355"/>
        <source>Studies</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1420"/>
        <source>Study</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1425"/>
        <source>Date assigned</source>
        <translation>Jou a atribusyon-la</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1430"/>
        <source>Exercises</source>
        <translation>Ègzèsis</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1435"/>
        <source>Date completed</source>
        <translation>Dat i fin</translation>
    </message>
    <message>
        <source>Settings#S</source>
        <comment>for sisters assignment</comment>
        <translation>Kad</translation>
    </message>
    <message>
        <source>setting</source>
        <translation>kad</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1615"/>
        <source>Unavailable</source>
        <translation>Pa disponib</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1649"/>
        <source>Start</source>
        <translation>Koumansé</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1654"/>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="131"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="181"/>
        <location filename="../personsui.ui" line="1585"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="382"/>
        <source>A person with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>Ja ni on moun èvè menm non-la: &apos;%1&apos;. Ès ou vlé chanjé non-la?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="601"/>
        <source>Remove student?</source>
        <translation>Éfasé élèv la?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="578"/>
        <source>Remove student</source>
        <translation>Éfasé élèv la</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="405"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 ni diskou piblik a fè! Si ou woté-y adan lis a sé oratè-la, 
nou ké mèt sé diskou-lasa adan &quot;Biten pou fè&quot;. 
Ès ou sèten ou vlé woté-y?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="592"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 ni diskou piblik a fè! Si ou woté-y adan lis a sé zélèv-la, 
nou ké mèt sé diskou-lasa adan &quot;Biten pou fè&quot;.</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="1077"/>
        <source>Remove study</source>
        <translation>Éfasé lèson-la</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1098"/>
        <source>Copy to the clipboard</source>
        <translation>Kopyé-y adan prèspayé-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1101"/>
        <source>Copy</source>
        <translation>Kopyé</translation>
    </message>
    <message>
        <source>Assignment slip for assistant</source>
        <translation>Fich atribusyon pou moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="235"/>
        <location filename="../printui.ui" line="326"/>
        <source>Schedule</source>
        <translation>Pwogram</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>Enprésyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="390"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>Lis pou téléfoné é Pwogram pou risivouè</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="751"/>
        <source>Midweek Meeting Title</source>
        <translation>Tit a réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="732"/>
        <source>Weekend Meeting Title</source>
        <translation>Tit a réinyon a wikenn-la</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Montré lè-la</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Montré diré-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="674"/>
        <source>Show Section Titles</source>
        <translation>Montré tit a sé sèksyon-la</translation>
    </message>
    <message>
        <source>Generate QR Code
and upload file to TheocBase Cloud</source>
        <translation>Kréyé on QR Kòd
é mèt fichyé-la adan Klaoud a TheocBase</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="799"/>
        <source>Review/Preview/Announcements Title</source>
        <comment>See S-140</comment>
        <translation>Tit Sa nou vwè/Sa nou ké wwè/Anons</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="780"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>Tit pou Pawòl pou koumansé</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="258"/>
        <source>Assignment Slips for Assistants</source>
        <translation>Fich atribusyon pou moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="361"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>Pwogram a sé oratè-la ki ka sòti</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="377"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>Diskou a sé oraté-la ki ka sòti</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="403"/>
        <source>Talks List</source>
        <translation>List a sé diskou-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="545"/>
        <source>Map and Addresses Sheets</source>
        <translation>Kat-la èvè Lis a sé adrès-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="681"/>
        <source>Show duration</source>
        <translation>Montré diré-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="704"/>
        <source>Show time</source>
        <translation>Montré lè-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="806"/>
        <source>Show Workbook Issue no.</source>
        <translation>Montré Kayé-la niméro</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="813"/>
        <source>Show Watchtower Issue no.</source>
        <translation>Montré Tou dè Gad la niméro</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="957"/>
        <source>Territory number(s)</source>
        <translation>Niméwo a téritwa</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="964"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>Séparé èvè viwgil; pézé Entré pou mété-y ajou</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="970"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <source>Map and address sheets</source>
        <translation>Kat-la èvè Lis a sé adrès-la</translation>
    </message>
    <message>
        <source>Territory number</source>
        <translation>Niméwo a téritwa</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="522"/>
        <source>Territory Record</source>
        <translation>Dosyé a Tèritwa</translation>
    </message>
    <message>
        <source>Territory Card</source>
        <translation>Kat a tèritwa</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="859"/>
        <location filename="../printui.cpp" line="2232"/>
        <source>Template</source>
        <translation>Modèl</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="885"/>
        <source>Paper Size</source>
        <translation>Wòtè a papyé-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="947"/>
        <source>Print From Date</source>
        <translation>Enprimé dépi</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="905"/>
        <source>Print Thru Date</source>
        <translation>Enprimé jiska</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="778"/>
        <location filename="../printui.cpp" line="1302"/>
        <source>Slip Template</source>
        <translation>Modèl a on Fich atribusyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1028"/>
        <source>Printing</source>
        <translation>Enprésyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1031"/>
        <location filename="../printui.ui" line="1066"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="248"/>
        <source>Assignment Slips</source>
        <translation>Fich atribusyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="767"/>
        <source>Share in Dropbox</source>
        <translation>Mété-y adan Dropbox</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="278"/>
        <location filename="../printui.ui" line="345"/>
        <source>Worksheets</source>
        <translation>Fèy pou travay</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="565"/>
        <source>Meetings for field ministry</source>
        <translation>Sé réinyon la pou prédikasyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="423"/>
        <source>Combination</source>
        <translation>Ansanm</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2529"/>
        <location filename="../printui.cpp" line="2988"/>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1767"/>
        <location filename="../printui.cpp" line="1841"/>
        <location filename="../printui.cpp" line="2538"/>
        <source>Congregation Bible Study</source>
        <translation>Étid biblik a lasanblé-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1842"/>
        <source>Theocratic Ministry School</source>
        <translation>Lékòl pou Ministè Téokratik</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1843"/>
        <source>Service Meeting</source>
        <translation>Réyinyon sèvis</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="632"/>
        <source>Additional Options</source>
        <translation>Dòt òpsyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="667"/>
        <source>Show Song Titles</source>
        <translation>Montré tit a sé santik-la</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="660"/>
        <source>Show Counsel Text</source>
        <translation>Montré Teknik oratwa-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="730"/>
        <location filename="../printui.cpp" line="1678"/>
        <location filename="../printui.cpp" line="1765"/>
        <location filename="../printui.cpp" line="1839"/>
        <location filename="../printui.cpp" line="2980"/>
        <source>Public Talk</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="474"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>Pwogram a sé oratè-la ki ka sòti</translation>
    </message>
    <message>
        <source>Outgoing Speaker Assignments</source>
        <translation>Atribusyon a sé oraté-la ki ka sòti</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="268"/>
        <source>Print assigned only</source>
        <translation>Enprimé enki sé atribusyon-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="385"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>Kopyé-y. Kolé-y adan on pwogram pou maké sé mo-la (Ctrl+V/Cmd+V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="431"/>
        <location filename="../printui.cpp" line="457"/>
        <location filename="../printui.cpp" line="546"/>
        <source>file created</source>
        <translation>fichyé-la kréé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2550"/>
        <location filename="../printui.cpp" line="2551"/>
        <location filename="../printui.cpp" line="2552"/>
        <location filename="../printui.cpp" line="2845"/>
        <source>Class</source>
        <translation>Sal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="396"/>
        <location filename="../printui.cpp" line="442"/>
        <location filename="../printui.cpp" line="473"/>
        <location filename="../printui.cpp" line="526"/>
        <source>Save file</source>
        <translation>Anrèjistré fichyé-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2545"/>
        <source>Exercises</source>
        <translation>Ègzèsis</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territories</source>
        <translation>Téritwa</translation>
    </message>
    <message>
        <source>No meeting</source>
        <translation>Pani réinyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1769"/>
        <location filename="../printui.cpp" line="1784"/>
        <location filename="../printui.cpp" line="3175"/>
        <location filename="../printui.cpp" line="3203"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1700"/>
        <location filename="../printui.cpp" line="3174"/>
        <source>Outgoing Speakers</source>
        <translation>Oraté ki ka sòti</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3178"/>
        <source>Theme Number</source>
        <translation>Niméwo a tèm-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="728"/>
        <location filename="../printui.cpp" line="1670"/>
        <location filename="../printui.cpp" line="1759"/>
        <location filename="../printui.cpp" line="1837"/>
        <location filename="../printui.cpp" line="3031"/>
        <location filename="../printui.cpp" line="3179"/>
        <location filename="../printui.cpp" line="3197"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1674"/>
        <location filename="../printui.cpp" line="1764"/>
        <location filename="../printui.cpp" line="1781"/>
        <location filename="../printui.cpp" line="2533"/>
        <source>Reader</source>
        <translation>Lèktè:</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1672"/>
        <location filename="../printui.cpp" line="1762"/>
        <location filename="../printui.cpp" line="1779"/>
        <location filename="../printui.cpp" line="2530"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="731"/>
        <location filename="../printui.cpp" line="1679"/>
        <location filename="../printui.cpp" line="1766"/>
        <location filename="../printui.cpp" line="1840"/>
        <location filename="../printui.cpp" line="2982"/>
        <source>Watchtower Study</source>
        <translation>Étid a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="700"/>
        <source>Class </source>
        <translation>Sal </translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="732"/>
        <location filename="../printui.cpp" line="2535"/>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Trézò ki an Pawòl a Bondyé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="733"/>
        <location filename="../printui.cpp" line="2536"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>Pran ministè-la oséryé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="734"/>
        <location filename="../printui.cpp" line="2537"/>
        <source>Living as Christians</source>
        <translation>Vi a on krétyen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1174"/>
        <source>Click the print button to preview assignment slips</source>
        <translation>Kliké si bouton-la pou enprimé pou vwè a ki biten ka sanblé sé fèy atribusyon-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1174"/>
        <source>Print button is found at the bottom right corner</source>
        <translation>Ou ké touvé bouton-la pou enprimé anba an kwen-la adwat</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1905"/>
        <source>Territory Assignment Record</source>
        <translation>Anrejistrèman Tèritwa ki Distribiyé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1906"/>
        <source>Territory Coverage</source>
        <translation>Tèritwa nou travayé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3194"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation>Sé adrès-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3208"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3220"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation>Kat</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3227"/>
        <source>Territory</source>
        <translation>Tèritwa</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3228"/>
        <source>Terr. No.</source>
        <translation>Téri. N°</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1907"/>
        <source>Total number of territories</source>
        <translation>Kantité téritwa ki ni</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3219"/>
        <source>Locality</source>
        <translation>Ki koté</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3229"/>
        <source>Territory type</source>
        <translation>Tip a tèritwa-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3223"/>
        <source>Name of publisher</source>
        <translation>Non a pwoklamatè-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3200"/>
        <source>Date checked out</source>
        <translation>Dat i sòti</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3201"/>
        <source>Date checked back in</source>
        <translation>Dat i rètouné</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3202"/>
        <source>Date last worked</source>
        <translation>Dènyé fwa i travayé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3196"/>
        <source>Assigned to</source>
        <translation>Bay li a</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3224"/>
        <source>Remark</source>
        <translation>Rèmak</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1908"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 mwa</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1909"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 a 12 mwa</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1910"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 mwa</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1911"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation>Mwayèn anyèl</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3193"/>
        <source>Address</source>
        <translation>Adrès</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3205"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Péyi</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3225"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Réjyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3206"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Dépawtman</translation>
    </message>
    <message>
        <source>DistrictSublocality</source>
        <translation>DistrictSublocality</translation>
    </message>
    <message>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Vil</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3207"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Réjyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3226"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Lari</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3218"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>N°</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3222"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>Kòd pòstal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3221"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3195"/>
        <source>Address type</source>
        <translation>Adrès tip</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2157"/>
        <source>This Territory Map Card could not be converted to a JPG file</source>
        <translation>Ou pé pa mété kat a téritwa-lasa adan on fichyé JPG</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1081"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation>Lèson %1</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1770"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1771"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation>Moun ki ka risivouè</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2054"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>Kat a téritwa</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2055"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation>Kat a téritwa</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2056"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation>Lis a sé adrès-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2057"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation>Kat a téritwa èvè Lis a sé adrès-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2058"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2059"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2060"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation>Pa vizité-yo</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.cpp" line="2540"/>
        <location filename="../printui.cpp" line="3069"/>
        <source>No regular meeting</source>
        <translation>Pani réinyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2544"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2553"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>Konséyé adan dézyèm sal-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2554"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>Dézyèm sal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2555"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>Sal prensipal</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2556"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2557"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2558"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>Zélèv</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2561"/>
        <source>Review/Preview/Announcements</source>
        <comment>Customizable title for RPA notes</comment>
        <translation>Sa nou vwè/Sa nou ké wwè/Anons</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2562"/>
        <source>Today</source>
        <translation>Jòdijou</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2563"/>
        <source>Next week</source>
        <translation>Simenn pwochèn</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3182"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>ON TI MO: Frè, nou ka fè anlo éfò pou sa pa rivé men dètanzantan ni dé érè adan sé doné-la. Sé pou sa nou ka mandé-w,  toujou vérifyé sé enfòwmasyon anlè JW.ORG. Mèsi!</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1677"/>
        <location filename="../printui.cpp" line="1755"/>
        <source>Public Meeting</source>
        <translation>Réinyon piblik</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="368"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>Pèrsonalizé...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="727"/>
        <location filename="../printui.cpp" line="1669"/>
        <location filename="../printui.cpp" line="1758"/>
        <location filename="../printui.cpp" line="1836"/>
        <location filename="../printui.cpp" line="3030"/>
        <location filename="../printui.cpp" line="3199"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Lasanblé %1</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1254"/>
        <source>Converting %1 to JPG file</source>
        <translation>Mété %1 adan on fichyé JPG</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1659"/>
        <source>Select at least one option</source>
        <translation>Chwazi on opsyon</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="207"/>
        <location filename="../printui.ui" line="461"/>
        <location filename="../printui.cpp" line="1680"/>
        <location filename="../printui.cpp" line="2524"/>
        <location filename="../printui.cpp" line="3488"/>
        <source>Midweek Meeting</source>
        <translation>Réinyon a simenn-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1681"/>
        <location filename="../printui.cpp" line="1682"/>
        <location filename="../printui.cpp" line="1768"/>
        <location filename="../printui.cpp" line="2534"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Réinyon Vi é Ministè a on krétyen</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1697"/>
        <location filename="../printui.cpp" line="1698"/>
        <source>Combined Schedule</source>
        <translation>Pwogram konbiné</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1782"/>
        <source>WT Reader</source>
        <translation>Lèktè TG la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1785"/>
        <location filename="../printui.cpp" line="3177"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2525"/>
        <source>Worksheet</source>
        <translation>Fèy pou travay</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Révizyon é sa nou ké vwè pwochen simenn-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2539"/>
        <location filename="../printui.cpp" line="2983"/>
        <source>Circuit Overseer</source>
        <translation>Sèwvitè a siwkonskripsyon-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2541"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <source>Study</source>
        <comment>title for study point</comment>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2547"/>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3230"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3231"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3232"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3244"/>
        <source>Service Talk</source>
        <translation>Diskou sèvis</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3253"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>Koumansé a</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3497"/>
        <source>Closing Comments</source>
        <translation>Konklizyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3575"/>
        <source>Can&#x27;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>Nou pé pa li fichyé-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3836"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>Nouvo wòtè a papyé-la ki pèrsonalizé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3836"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>Fòwma: lajè x otè. Ou pé mèt Lajè-la é Otè-la an mm. Ègzanp: 210mm x 297mm</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2549"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>Kad</translation>
    </message>
    <message>
        <source>Print button is found at the bottom left corner</source>
        <translation>Ou ké touvé bouton-la pou enprimé anba an kwen-la agòch</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1242"/>
        <source>Unable to find information in this template: </source>
        <translation>Pa ni pon enfòwmasyon adan modèl-lasa: </translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1108"/>
        <location filename="../printui.cpp" line="1525"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>Dézyèm diskou</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1620"/>
        <source>Slips created at %1*.pdf</source>
        <translation>Sé Fich atribusyon la adan %1*.pdf</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1673"/>
        <location filename="../printui.cpp" line="2531"/>
        <source>Counselor</source>
        <translation>Konséyé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1676"/>
        <location filename="../printui.cpp" line="1763"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="298"/>
        <location filename="../printui.ui" line="448"/>
        <location filename="../printui.cpp" line="574"/>
        <location filename="../printui.cpp" line="1699"/>
        <location filename="../printui.cpp" line="1754"/>
        <location filename="../printui.cpp" line="3489"/>
        <source>Weekend Meeting</source>
        <translation>Réinyon a wikenn-la</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1711"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>Simenn ki koumansé lè %1</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3496"/>
        <source>Opening Comments</source>
        <translation>Pawòl pou koumansé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2528"/>
        <location filename="../printui.cpp" line="2991"/>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3066"/>
        <source>Watchtower Conductor</source>
        <translation>Kondiktè a Tou dè Gad la</translation>
    </message>
    <message>
        <source>No Meeting</source>
        <translation>Pani réinyon</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3173"/>
        <source>Talk</source>
        <translation>Diskou</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1675"/>
        <location filename="../printui.cpp" line="1761"/>
        <location filename="../printui.cpp" line="2532"/>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3570"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>Modèl-la pala</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2842"/>
        <source>Other schools</source>
        <translation>Sé zòt klas la</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2548"/>
        <source>Next study</source>
        <translation>Pwochen lèson:</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3180"/>
        <source>Start Time</source>
        <translation>Lè pou koumansé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3896"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>Inité a lajè-la pa korèsponn èvè inité a wòtè-la.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3924"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>Padon, men sa ou mèt pa bon.</translation>
    </message>
    <message>
        <source>buttonGroup_2</source>
        <translation>buttonGroup_2</translation>
    </message>
    <message>
        <source>buttonGroup</source>
        <translation>buttonGroup</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="202"/>
        <source>From %1</source>
        <translation>Dèpi lè %1</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="230"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>I ja ni on diskou jou-lasa.</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="235"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>Touné sé diskou-la</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="236"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>Anilé</translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="67"/>
        <source>Public Talk</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="257"/>
        <location filename="../publictalkedit.ui" line="592"/>
        <location filename="../publictalkedit.ui" line="681"/>
        <location filename="../publictalkedit.ui" line="881"/>
        <location filename="../publictalkedit.cpp" line="47"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="169"/>
        <location filename="../publictalkedit.ui" line="866"/>
        <location filename="../publictalkedit.cpp" line="48"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="144"/>
        <location filename="../publictalkedit.ui" line="328"/>
        <location filename="../publictalkedit.cpp" line="50"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="232"/>
        <location filename="../publictalkedit.ui" line="617"/>
        <location filename="../publictalkedit.ui" line="642"/>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="411"/>
        <source>Watchtower Study</source>
        <translation>Étid a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="472"/>
        <location filename="../publictalkedit.cpp" line="52"/>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="542"/>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="285"/>
        <location filename="../publictalkedit.ui" line="932"/>
        <source>Move to different week</source>
        <translation>Mété-y adan on dòt simenn</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="118"/>
        <location filename="../publictalkedit.ui" line="906"/>
        <source>Send to To Do List</source>
        <translation>Mété-y adan &quot;Biten pou fè&quot;</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="311"/>
        <source>Clear Public Talk selections</source>
        <comment>Clear=Delete</comment>
        <translation>Éfasé sé diskou piblik-la ki sélèksyoné</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="567"/>
        <location filename="../publictalkedit.ui" line="658"/>
        <location filename="../publictalkedit.cpp" line="51"/>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="497"/>
        <location filename="../publictalkedit.ui" line="671"/>
        <source>Service Talk</source>
        <translation>Diskou sèvis</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="759"/>
        <source>Outgoing Speakers This Weekend</source>
        <translation>Oraté ki ka sòti wiken-lasa</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="833"/>
        <source>Add to Outgoing List</source>
        <translation>Mété-y adan lis la a sé oratè-la ki ka sòti</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="876"/>
        <source>Start time</source>
        <translation>Lè pou koumansé</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="886"/>
        <source>Theme No.</source>
        <translation>N° a tèm-la</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="979"/>
        <source>To Do List</source>
        <translation>Biten pou fè</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1019"/>
        <source>Add item to schedule</source>
        <translation>Mété-y an pwogram-la</translation>
    </message>
    <message>
        <source>Add Outgoing To Do item</source>
        <comment>Add OUT item</comment>
        <translation>Mèt sé diskou-la pou fè déwò</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="793"/>
        <location filename="../publictalkedit.ui" line="1045"/>
        <source>Remove item</source>
        <translation>Éfasé sa</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1071"/>
        <source>Add Incoming To Do item</source>
        <translation>Mèt sé diskou-la pou fè adan lasanblé an-nou</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="207"/>
        <location filename="../publictalkedit.ui" line="871"/>
        <location filename="../publictalkedit.cpp" line="49"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="89"/>
        <source>Watchtower Study Edition</source>
        <translation>Édisyon a Tou dè Gad la</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="255"/>
        <location filename="../publictalkedit.cpp" line="290"/>
        <source>From %1; speaker removed</source>
        <comment>From [scheduled date]; speaker removed</comment>
        <translation>Dèpi lè %1; nou pa ka sèvi èvè-y</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="293"/>
        <source>From %1; speaker moved to %2</source>
        <comment>From [scheduled date]; speaker moved to [new congregation]</comment>
        <translation>Dépi %1, oratè-la chanjé lasanblé. Konnyéla i adan %2</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="339"/>
        <location filename="../publictalkedit.cpp" line="374"/>
        <source>From %1; talk discontinued</source>
        <comment>From [scheduled date]; talk discontinued</comment>
        <translation>Nou paka sèvi èvè diskou-lasa, dèpi lè %1</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="694"/>
        <source>In</source>
        <comment>Incoming</comment>
        <translation>Andidan</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="694"/>
        <source>Out</source>
        <comment>Outgoing</comment>
        <translation>Déwò</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="816"/>
        <source>The selected speaker has already public talk on this calendar month. Do you want to add?</source>
        <translation>Oratè-lasa ja ka bay on diskou piblik pwogram mwa-lasa. Ou vlé ajouté-y kan  mèm sa?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="944"/>
        <location filename="../publictalkedit.cpp" line="995"/>
        <location filename="../publictalkedit.cpp" line="1035"/>
        <source>The destination date already has a talk scheduled. What to do?</source>
        <translation>I ja ni on diskou jou-lasa. Alò, ka ou vlé fè ?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="945"/>
        <location filename="../publictalkedit.cpp" line="996"/>
        <source>&amp;Swap Talks</source>
        <translation>&amp;Touné sé diskou-la</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="946"/>
        <location filename="../publictalkedit.cpp" line="997"/>
        <location filename="../publictalkedit.cpp" line="1036"/>
        <source>&amp;Move other talk to To Do List</source>
        <translation>&amp;Mété lòt diskou-la adan &quot;Biten pou fè&quot;</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="947"/>
        <location filename="../publictalkedit.cpp" line="998"/>
        <location filename="../publictalkedit.cpp" line="1037"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anilé</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="964"/>
        <location filename="../publictalkedit.cpp" line="1010"/>
        <location filename="../publictalkedit.cpp" line="1046"/>
        <location filename="../publictalkedit.cpp" line="1055"/>
        <location filename="../publictalkedit.cpp" line="1077"/>
        <location filename="../publictalkedit.cpp" line="1110"/>
        <source>From %1</source>
        <translation>Dèpi lè %1</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1033"/>
        <source>Date Already Scheduled</source>
        <translation>Dat-lasa ja adan pwogram-la</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1064"/>
        <source>Error</source>
        <translation>Érè</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1064"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>Ou péké pé pwogramé-y toutotan ou pa korijé sa: %1</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>Sonjé</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="58"/>
        <source>Date range</source>
        <translation>Séri dat</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="159"/>
        <source>Send selected reminders</source>
        <translation>Voyé sé mésaj-la ki sélèksyoné pou fè yo sonjé</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="177"/>
        <source>From</source>
        <translation>Ki mou ki voyé-y</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="195"/>
        <source>To</source>
        <translation>Pou</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="247"/>
        <source>Details</source>
        <translation>Détay</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="298"/>
        <source>Date</source>
        <translation>Dat</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="303"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="313"/>
        <source>Subject</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="318"/>
        <source>Message</source>
        <translation>Mésaj</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="308"/>
        <source>E-Mail</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="95"/>
        <source>Email sending...</source>
        <translation>Imèl-la voyé...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Anilé</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="113"/>
        <source>Error sending e-mail</source>
        <translation>Imèl pa pati, i kontré on érè</translation>
    </message>
    <message>
        <source>Send</source>
        <comment>Email reminder</comment>
        <translation>Voyé</translation>
    </message>
    <message>
        <source>Resend</source>
        <comment>Email reminder</comment>
        <translation>Rouvoyé-y</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>Prézidan</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>Konséyé-Klas II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>Konséyé-Klas III</translation>
    </message>
    <message>
        <source>Opening Prayer</source>
        <translation>Priyè pou komansé</translation>
    </message>
    <message>
        <source>Concluding Prayer</source>
        <translation>Priyè pou fin</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>Priyè I</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>Priyè II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="161"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Anilé - Sijé adan réinyon Vi é Ministè a on krétyen</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="161"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Sijé adan réinyon Vi é Ministè a on krétyen</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="181"/>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="183"/>
        <source>Assignment</source>
        <translation>Sijé</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="186"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="188"/>
        <source>Source Material</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="202"/>
        <source>Assistant</source>
        <translation>Moun-la ki ka fè-y èvè-y</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="212"/>
        <location filename="../schoolreminder.cpp" line="214"/>
        <source>Study</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="219"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Lèktè a létid biblik a lasanblé la</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="221"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Moun-la ki ka fè-y èvè %1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="226"/>
        <source>Cancellation</source>
        <translation>Anilasyon</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <location filename="../schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>Lèson</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>Ègzèsis-la fin</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>Pwochen lèson:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <comment>for sisters assignment</comment>
        <translation>Kad:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>Anilé</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>Ja fèt</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>Kantité tan:</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="59"/>
        <source>Bible highlights</source>
        <translation>PILB</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="61"/>
        <source>reading</source>
        <translation>lèkti</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="65"/>
        <source>Assignment result</source>
        <translation>Rézilta a atribusyon-la</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="130"/>
        <source>Do not assign the next study</source>
        <translation>Pa bay pwochen lèson-la</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="159"/>
        <location filename="../schoolresult.cpp" line="369"/>
        <source>Leave on current study</source>
        <translation>Kontinyé èvè menm lèson-la</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="239"/>
        <source>Select setting</source>
        <translation>Chwazi kad-la</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="246"/>
        <source>Timing</source>
        <translation>Kantité tan</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="247"/>
        <source>The timing is empty. Save?</source>
        <translation>Ou pa mèt tan-la. Ès ou vlé kontinyé?</translation>
    </message>
</context>
<context>
    <name>schoolui</name>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <location filename="../schoolui.cpp" line="36"/>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="41"/>
        <source>Class</source>
        <translation>Sal</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="111"/>
        <source>Bible highlights:</source>
        <translation>PILB:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="115"/>
        <source>No. 1:</source>
        <translation>N°1:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="119"/>
        <source>No. 2:</source>
        <translation>N°2:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="123"/>
        <source>No. 3:</source>
        <translation>N°3:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="127"/>
        <source>Reader:</source>
        <translation>Lèktè:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="176"/>
        <source>No school</source>
        <translation>Pani lékòl</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="329"/>
        <source>Nothing to display</source>
        <translation>Pani ayen pou vwè</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="495"/>
        <source>No assignment has been made!</source>
        <translation>Pòkò ni pon sijé!</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="500"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>Tanprisouplé, enpòté nouvo pwogram a lekòl la dépi Watchtower Library (Réglaj-la-&gt;Lékòl pou Ministè Téokratik...)</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="542"/>
        <source>Show Details...</source>
        <translation>Montré détay-ay</translation>
    </message>
</context>
<context>
    <name>servicemeetingui</name>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>min</source>
        <comment>Abbreviation of minutes</comment>
        <translation>mn</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>Lasanblé é Oratè</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="400"/>
        <source>Congregation...</source>
        <translation>Lasanblé...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="371"/>
        <source>Speaker...</source>
        <translation>Oratè...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1332"/>
        <source>Toggle Talks Editable</source>
        <translation>Modifyé sé tèm-la</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1300"/>
        <source>Add Multiple Talks</source>
        <translation>Mèt plizyè diskou alafwa</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="95"/>
        <location filename="../speakersui.ui" line="136"/>
        <location filename="../speakersui.ui" line="180"/>
        <location filename="../speakersui.ui" line="246"/>
        <location filename="../speakersui.ui" line="1303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="494"/>
        <source>Select a Congregation or Speaker</source>
        <translation>Chwazi on Lasanblé oben on Oratè</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="725"/>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="792"/>
        <source>Address</source>
        <translation>Adrès</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.cpp" line="171"/>
        <source>Circuit</source>
        <translation>Siwkonskripsyon</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="667"/>
        <location filename="../speakersui.ui" line="1685"/>
        <location filename="../speakersui.cpp" line="179"/>
        <source>Congregation</source>
        <translation>Lasanblé</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="92"/>
        <source>Speakers</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="133"/>
        <source>Group by congregation</source>
        <translation>Gwoupé-y pa lasanblé</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="177"/>
        <source>Group by circuit</source>
        <translation>Gwoupé-y pa siwkonskripsyon</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="221"/>
        <source>Filter</source>
        <translation>Chwazi</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="243"/>
        <source>Configure Filter</source>
        <translation>Kijan pou chwazi</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="882"/>
        <source>Congregation Details</source>
        <translation>Détay si lasanblé-la</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="894"/>
        <source>Meeting Times</source>
        <translation>Lè a sé réinyon-la</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="999"/>
        <location filename="../speakersui.ui" line="1169"/>
        <source>Mo</source>
        <translation>Len</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1004"/>
        <location filename="../speakersui.ui" line="1174"/>
        <source>Tu</source>
        <translation>Maw</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1009"/>
        <location filename="../speakersui.ui" line="1179"/>
        <source>We</source>
        <translation>Mèk</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1014"/>
        <location filename="../speakersui.ui" line="1184"/>
        <source>Th</source>
        <translation>Jéd</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1019"/>
        <location filename="../speakersui.ui" line="1189"/>
        <source>Fr</source>
        <translation>Van</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1024"/>
        <location filename="../speakersui.ui" line="1194"/>
        <source>Sa</source>
        <translation>San</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1029"/>
        <location filename="../speakersui.ui" line="1199"/>
        <source>Su</source>
        <translation>Dim</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1363"/>
        <source>Personal Info</source>
        <translation>Enfòwmasyon ki pèrsonèl</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1786"/>
        <location filename="../speakersui.cpp" line="833"/>
        <source>First Name</source>
        <translation>Tinon</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1838"/>
        <source>Mobile</source>
        <translation>Pòwtab</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1576"/>
        <location filename="../speakersui.cpp" line="834"/>
        <source>Last Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1540"/>
        <source>Phone</source>
        <translation>Téléfòn</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1377"/>
        <source>Public Talks</source>
        <translation>Diskou piblik</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1646"/>
        <source>E-mail</source>
        <translation>Imèl</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1467"/>
        <source>Notes</source>
        <translation>Maké</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="187"/>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="361"/>
        <location filename="../speakersui.cpp" line="541"/>
        <source>Undefined</source>
        <translation>Endéfini</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="444"/>
        <location filename="../speakersui.cpp" line="449"/>
        <source>%1 Meeting Day/Time</source>
        <translation>Jou é Lè a sé réinyon la an %1</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="574"/>
        <source>A speaker with the same name already exists: &#x27;%1&#x27;. Do you want to change the name?</source>
        <translation>Ja ni on oratè ki ni menm non-la: &apos;%1&apos;. Ès ou vlé chanjé non-la?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="706"/>
        <source>The congregation has speakers!</source>
        <translation>Lasanblé-la ni oraté!</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="711"/>
        <source>Remove the congregation?</source>
        <translation>Éfasé lasanblé la?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="757"/>
        <source>Remove the speaker?</source>
        <translation>Éfasé oratè la?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="762"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>Oratè-lasa ni diskou pou fè! Si ou éfase non a-y,
diskou a-y ké ay adan &quot;Biten pou fè&quot;.</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="826"/>
        <source>Missing Information</source>
        <translation>I ka manké on enfòwmasyon</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="826"/>
        <source>Select congregation first</source>
        <translation>Koumansé mèt lasamblé-la</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="859"/>
        <source>New Congregation</source>
        <translation>Nouvo lansanblé</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="927"/>
        <source>Add Talks</source>
        <translation>Ajouté diskou</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="927"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>Antre niméwo a sé diskou la séparé swa èvè viwgil oswa èvè pwen</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="963"/>
        <source>Change congregation to &#x27;%1&#x27;?</source>
        <translation>Chanjé lasanblé an &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="965"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>Oratè lasa ni on diskou pou bay adan an dòt kongrégasyon.
Si ou chanjé kongregasyon-ay, diskou-ay kay adan &quot;Biten pou fè&quot;.</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>Paj akèy</translation>
    </message>
</context>
<context>
    <name>study</name>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Référans-la</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lèktè</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="240"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation>Pwoblèm a vèwsyon-la: Sé chanjman la adan Klaoud-la fèt èvè on vèwsyon tou nèf!</translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="246"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation>Pwoblèm a vèwsyon-la: Fò on moun ki ni dwa mèt a jou Klaoud-la èvè menm vèwsyon-la.</translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>Téritwa</translation>
    </message>
</context></TS>