/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQL_CLASS_H
#define SQL_CLASS_H

#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QUuid>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QString>
#include <QRegularExpression>
#include <QVariant>
#include <QFileDialog>
#include <vector>
#include <QDateTime>
#include <QPair>
#include <QList>
#include <QInputDialog>
#include "singleton.h"
#include "sharedlib_global.h"

extern QString theocbaseDirPath;
extern QString localdatabasedir;
extern bool transactionStarted;

/* sql_item
 *  instead of the sql_item class, use build-in QHash.
 *  major differences:
 *    * adding items is done with "insert", which handles new items or updates existing
 *    * value will be a variant instead of string. Should mean less casting back and forth overall
 *    * down side: previously getValue() would popup a messagebox if the requested name didn't exist. This doesn't happen with QHash; the item is added with a default value
 *  useful code:
 *     a standard iterator: for (sql_item v: values)
 */
typedef QHash<QString, QVariant> sql_item;
typedef std::vector<sql_item> sql_items;

//namespace Ui {
//    class personsui;
//}

class TB_EXPORT sql_class : public QObject
{
    Q_OBJECT
public:
    QString databasepath;
    sql_class();
    ~sql_class();
    bool createConnection();

    sql_items selectSql(QString tname, QString searchfield, QString value, QString orderbytable = "");
    sql_items selectSql(QString fullsqlquery);
    sql_items selectSql(QString fullsqlquery, sql_item *values);
    QPair<sql_items, QList<QString>> selectSql2(QString fullsqlquery, sql_item *values);
    template<class mo>
    std::vector<mo> selectSql(QString fullsqlquery, sql_item *values);
    QVariant selectScalar(QString fullsqlquery, sql_item *values);
    QVariant selectScalar(QString fullsqlquery, sql_item *values, QVariant defaultvalue);
    QVariant selectScalar(QString fullsqlquery, QVariant value, QVariant defaultvalue);
    int insertSql(QString tname, sql_item *values, QString idfield);
    bool updateSql(QString tname, QString idname, QString idvalue, sql_item *values);
    bool removeSql(QString tname, QString where, sql_item *values);
    bool removeSql(QString tname, QString where);
    bool execSql(QString query, QString okErr = QString::null);
    bool execSql(QString query, sql_item *values, bool valueAssistant = 0);
    bool fillAllUUID(QString tablename, QString idname);
    bool deleteNonActive();
    bool renameColumn(QString tableName, QString columnName, QString newColumnName);

    /**
     * @brief getPublicMeetingInfo - selects the closest record from congregationmeetingtimes for a congregation given a year.
     *        returns an empty sql_item if no records for that congregation exist
     */
    sql_item getPublicMeetingInfo(int congregation_id, QDate weekof);

    bool saveSetting(QString name, QString value);
    QString getSetting(QString name, QString defaultvalue = "");
    int getIntSetting(QString name, int defaultvalue = -1);

    /**
     * @brief updateThisYearMeetingTimes - makes sure congregationmeetingtimes table is filled for this year's times for all congregations
     *        will be called when creating a new congregation
     *        only creates records if needed
     */
    void updateThisYearMeetingTimes();

    /**
     * @brief updateNextYearMeetingTimes - makes sure congregationmeetingtimes table is filled for next year's times for all congregations
     *        will be called when creating a new congregation
     *        will be called upon launching the app each time (for when we transition to the next year)
     *        only creates records if needed
     */
    void updateNextYearMeetingTimes();

    void updateDatabase(QString version);
    void closeConnection();
    void startTransaction();
    void commitTransaction();
    void rollbackTransaction();
    void copyDatabase();
    void clearDatabase();

    QList<QPair<int, QString>> getLanguages();
    int getLanguageDefaultId();
    QString getLanguageCode(int id);
    int getLanguageId(QString code);

    QString EscapeQuotes(QString sql);
    int lastNumRowsAffected;

signals:
    void dbChanged(QString tablename = "");

private:
    QSqlDatabase *db;
    int getNextId(QString tablename, QString fieldname);
    QString getUuid(QString basedOn = "");
    int getCurrentTimeStamp();
    bool addColumn(QString tablename, QString columnname, QString type, QString defaultvalue = "");
    bool deleteTable(QString tablename, QString deleteifcolumnexists = "");
    bool updateTable(QString tablename, QString field, QString value);
    void createView(QString viewname, QString select);
    bool isTableExists(QString tablename);
    int updateDatabase_2014010();
    int runDatabaseBatch(QString resourcepath);

    void bindQueryValues(QSqlQuery *query, sql_item *values, bool valueAssistant = 0);

    QHash<QString, QString> *getSettings();
    QHash<QString, QString> cachedSettings;
};

#endif // SQL_CLASS_H
