#ifndef HELPVIEWER_H
#define HELPVIEWER_H

#include "sql_class.h"
#include <QCoreApplication>
#include <QString>

QT_BEGIN_NAMESPACE
class QProcess;
QT_END_NAMESPACE

extern QString theocbaseDirPath;

class HelpViewer
{
    Q_DECLARE_TR_FUNCTIONS(HelpViewer)

public:
    HelpViewer();
    ~HelpViewer();
    void showHelp(const QString &file);

private:
    bool startHelpViewer();
    QString langCode;
    QProcess *proc;
    sql_class *sql;
};

#endif // HELPVIEWER_H
