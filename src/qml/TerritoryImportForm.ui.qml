import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import QtQml.Models 2.2
import QtQuick.Dialogs 1.2

Item {
    id: item1
    anchors.fill: parent

    property alias importButton: importButton
    property alias closeButton: closeButton
    property alias searchByDescriptionCheckBox: searchByDescriptionCheckBox
    property alias fileNameTextField: fileNameTextField
    property alias selectFileButton: selectFileButton
    property int importOption: importOptionsGroup.current === boundariesRadioButton ? 1 : 2
    property alias boundaryNameComboBox: boundaryNameComboBox
    property alias boundaryDescriptionComboBox: boundaryDescriptionComboBox
    property alias addressComboBox: addressComboBox
    property alias addressNameComboBox: addressNameComboBox
    property alias csvFieldModel: csvFieldModel
    property alias addressTypeComboBox: addressTypeComboBox
    property bool displayProgress: false
    property alias progressLabel: progressLabel
    property alias failedFileNameTextField: failedFileNameTextField
    property alias selectFailedFileButton: selectFailedFileButton

    ColumnLayout {
        id: columnLayout
        spacing: 10
        anchors.fill: parent

        GridLayout {
            Layout.fillHeight: false
            rows: 5
            columns: 2
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            RowLayout {
                id: rowLayout3
                Layout.columnSpan: 2
                Layout.rowSpan: 1
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                ExclusiveGroup {
                    id: importOptionsGroup
                }

                RadioButton {
                    id: boundariesRadioButton
                    checked: true
                    exclusiveGroup: importOptionsGroup
                    text: qsTr("Territory boundaries",
                               "Territory data import option")
                    Layout.fillWidth: false
                }

                RadioButton {
                    id: addressesRadioButton
                    exclusiveGroup: importOptionsGroup
                    text: qsTr("Addresses", "Territory data import option")
                    checked: false
                }
            }

            RowLayout {
                id: rowLayout5
                Layout.columnSpan: 2
                Layout.fillWidth: true
                Layout.rowSpan: 1
                Layout.topMargin: 0
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                Label {
                    id: fileNameLabel
                    text: qsTr("Filename:")
                }

                RowLayout {
                    id: row1
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    TextField {
                        id: fileNameTextField
                        readOnly: true
                        Layout.fillWidth: true
                    }

                    ToolButton {
                        id: selectFileButton
                        Layout.minimumWidth: 24
                        Layout.minimumHeight: 24

                        iconSource: "qrc:///icons/browse_folder.svg"
                        opacity: parent.enabled ? 1.0 : 0.5
                    }
                }
            }

            ColumnLayout {
                id: column
                Layout.columnSpan: 2
                Layout.rowSpan: 1
                Layout.topMargin: 0
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                GridLayout {
                    id: boundariesGridLayout
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.fillWidth: true
                    Layout.leftMargin: 0
                    Layout.rightMargin: 0
                    visible: boundariesRadioButton.checked == true
                    columns: 1
                    rowSpacing: 20
                    columnSpacing: 20

                    ColumnLayout {
                        id: columnLayout1
                        Layout.fillWidth: true
                        spacing: 10

                        RowLayout {
                            id: rowLayout2
                            spacing: 5

                            GroupBox {
                                id: groupBox
                                title: qsTr("Match KML Fields",
                                            "Choose which data fields in kml-file correspond to which territory properties")

                                GridLayout {
                                    id: gridLayout1
                                    columnSpacing: 10
                                    rowSpacing: 10
                                    anchors.rightMargin: 5
                                    anchors.leftMargin: 5
                                    anchors.bottomMargin: 5
                                    anchors.topMargin: 10
                                    anchors.fill: parent
                                    rows: 3
                                    columns: 2

                                    Label {
                                        id: nameLabel
                                        text: qsTr("Name:",
                                                   "Territory data import KML Field")
                                    }

                                    ComboBox {
                                        id: boundaryNameComboBox

                                        model: cbItems
                                        currentIndex: 1
                                    }

                                    Label {
                                        id: descriptionLabel
                                        text: qsTr("Description:",
                                                   "Territory data import KML Field")
                                    }

                                    ComboBox {
                                        id: boundaryDescriptionComboBox

                                        model: cbItems
                                        currentIndex: 2
                                    }
                                }
                            }
                        }

                        CheckBox {
                            id: searchByDescriptionCheckBox

                            text: qsTr("Search by \"Description\" if territory is not found by \"Name\"")
                            Layout.fillWidth: false
                        }
                    }

                    ListModel {
                        id: cbItems
                        ListElement {
                            // @disable-check M16
                            text: ""
                            // @disable-check M16
                            fieldName: "none"
                        }
                        ListElement {
                            // @disable-check M16
                            text: qsTr("Territory No.", "Territory number")
                            // @disable-check M16
                            fieldName: "territory_number"
                        }
                        ListElement {
                            // @disable-check M16
                            text: qsTr("Locality", "Territory locality")
                            // @disable-check M16
                            fieldName: "locality"
                        }
                        ListElement {
                            // @disable-check M16
                            text: qsTr("Remark")
                            // @disable-check M16
                            fieldName: "remark"
                        }
                    }
                }

                GridLayout {
                    id: addressesGridLayout
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.fillWidth: true
                    Layout.rightMargin: 0
                    Layout.leftMargin: 0
                    visible: addressesRadioButton.checked == true
                    rows: 2
                    columns: 2
                    rowSpacing: 20
                    columnSpacing: 20

                    ColumnLayout {
                        id: columnLayout2
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        spacing: 10

                        RowLayout {
                            id: rowLayout4
                            spacing: 5

                            GroupBox {
                                id: groupBox1
                                title: qsTr("Match Fields",
                                            "Choose which fields in the import-file correspond to the address data")

                                GridLayout {
                                    id: gridLayout2
                                    columnSpacing: 10
                                    rowSpacing: 10
                                    anchors.rightMargin: 5
                                    anchors.leftMargin: 5
                                    anchors.bottomMargin: 5
                                    anchors.topMargin: 10
                                    anchors.fill: parent
                                    rows: 3
                                    columns: 2

                                    Label {
                                        id: addressLabel
                                        text: qsTr("Address:",
                                                   "Territory address import field")
                                    }

                                    ComboBox {
                                        id: addressComboBox
                                        model: csvFieldModel
                                    }

                                    Label {
                                        id: nameLabel1
                                        text: qsTr("Name:",
                                                   "Territory address import field")
                                    }

                                    GridLayout {

                                        ComboBox {
                                            id: addressNameComboBox
                                            model: csvFieldModel
                                        }
                                    }
                                }
                            }
                        }

                        //                RowLayout {
                        //                    Label {
                        //                        id: googleAPIkeyLabel
                        //                        text: qsTr("Google API key (optional):")
                        //                    }

                        //                    TextField {
                        //                        id: googleAPIkeyTextField
                        //                        Layout.fillWidth: true
                        //                    }
                        //                }
                    }

                    ListModel {
                        id: csvFieldModel
                    }

                    ColumnLayout {
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop

                        Label {
                            id: addressTypeLabel
                            text: qsTr("Address type:",
                                       "Address type for territory address import")
                        }

                        ComboBox {
                            id: addressTypeComboBox
                            textRole: "name"
                        }
                    }
                }
            }

            ColumnLayout {
                id: rowLayout6
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.columnSpan: 2
                visible: addressesRadioButton.checked == true
                spacing: 1

                Label {
                    id: failedFileNameLabel
                    text: qsTr("Output filename for failed addresses:",
                               "Territory address import")
                }

                RowLayout {
                    id: row2
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    TextField {
                        id: failedFileNameTextField
                        readOnly: true
                        Layout.fillWidth: true
                    }

                    ToolButton {
                        id: selectFailedFileButton
                        Layout.minimumWidth: 24
                        Layout.minimumHeight: 24

                        iconSource: "qrc:///icons/browse_folder.svg"
                        opacity: parent.enabled ? 1.0 : 0.5
                    }
                }
            }

            RowLayout {
                id: rectangle
                Layout.columnSpan: 2
                Layout.rowSpan: 1
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                visible: displayProgress

                ProgressBar {
                    id: progressBar
                    width: 150
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    indeterminate: true
                    minimumValue: 0
                    maximumValue: 100
                    value: 0
                    Layout.minimumWidth: 200
                    Layout.fillWidth: true
                    Layout.leftMargin: 0
                    Layout.rightMargin: 0
                }

                Label {
                    id: progressLabel
                    text: ""
                    horizontalAlignment: Text.AlignHCenter
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: true
                    Layout.leftMargin: 0
                    Layout.rightMargin: 0
                }
            }
        }

        RowLayout {
            id: rowLayout
            height: 33
            Layout.fillWidth: true
            spacing: 10
            Layout.columnSpan: 1
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.bottomMargin: 0

            Button {
                id: importButton
                text: qsTr("Import")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                //Layout.rightMargin: 90
                //                anchors.right: parent.right
                //                anchors.rightMargin: 90
            }

            Button {
                id: closeButton
                text: qsTr("Close")
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                //Layout.rightMargin: 0
            }
        }
    }
}



/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:48;anchors_width:150}
}
 ##^##*/
