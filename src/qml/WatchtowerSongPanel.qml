import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

ScrollView {
    id: wtSongEdit
    property string title: "Watchtower Song"
    width: 500
    clip: true

    property CPTMeeting meeting
    property bool startSong: true

    anchors.fill: parent

    ColumnLayout {
        id: layout
        x: 10        
        width: wtSongEdit.width - 20

        // WT Song
        Label {
            text: qsTr("Song")
            font.capitalization: Font.AllUppercase
            Layout.fillWidth: true
            Layout.topMargin: 10
        }
        NumberSelector {
            Layout.fillWidth: true
            Layout.preferredHeight: height
            Layout.bottomMargin: 10
            maxValue: 151
            selectedValue: startSong ? meeting.songWtStart : meeting.songWtEnd
            onSelectedValueChanged: {
                if (startSong) {
                    if (selectedValue === meeting.songWtStart)
                        return
                    meeting.songWtStart = selectedValue
                } else {
                    if (selectedValue === meeting.songWtEnd)
                        return
                    meeting.songWtEnd = selectedValue
                }
                meeting.save()
            }
        }
    }
}

