/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.2
import QtLocation 5.9
import QtPositioning 5.8
import net.theocbase 1.0

Item {
    property string address
    property string mapProvider
    property string hereAppId
    property string hereToken
    onAddressChanged: {
        console.log("Address changed: " + address)
        _territories.geocodeAddress(address)
    }
    onMapProviderChanged: {
        if (mapProvider === "here") {
            console.log("use here provider")
            map.plugin = herePlugin
        } else {
            console.log("use osm provider")
            map.plugin = osmPlugin
        }
    }

    MessageDialog {
        id: messageDialog
        standardButtons: StandardButton.Ok
        icon: StandardIcon.Warning
        modality: Qt.ApplicationModal
    }

    Territories {
        id: _territories
        onGeocodeFinished: {
            console.log( "geocode finished " + geocodeResults.length )
            if (geocodeResults.length > 0) {
                map.zoomLevel = 14
                map.center = QtPositioning.coordinate(geocodeResults[0].latitude, geocodeResults[0].longitude)                

                marker.coordinate = QtPositioning.coordinate(geocodeResults[0].latitude, geocodeResults[0].longitude)
            } else {
                // default
                map.center = QtPositioning.coordinate(0, 0)
                map.zoomLevel = 1
            }
        }
        onGeocodeError: {
            messageDialog.title = qsTr("Display congregation address", "Display marker at the location of the congregation on the map");
            messageDialog.text = message;
            messageDialog.visible = true;
        }
    }

    Plugin {
        id: osmPlugin
        name: "osm"
    }
    Plugin {
        id: herePlugin
        name: "here"
        PluginParameter { name: "here.app_id"; value: hereAppId }
        PluginParameter { name: "here.token"; value: hereToken }

    }

    Map {
        id: map
        anchors.fill: parent
        //plugin: mapPlugin
        center: QtPositioning.coordinate(0, 0)
        zoomLevel: 1

        Slider {
            id: slider
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            orientation: Qt.Vertical
            from: map.minimumZoomLevel
            to: map.maximumZoomLevel
            value: map.zoomLevel
            onMoved: map.zoomLevel = value
        }

        onErrorChanged: {
            console.log("Error: " + errorString)
        }

        MapSettings {
            id: mapSettings
        }

        SimpleMarker{
            id: marker
            color: '#000000'
            markerType: 1
            markerScale: mapSettings.markerScale
        }
    }
}
