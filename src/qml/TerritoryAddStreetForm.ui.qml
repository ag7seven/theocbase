import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import QtQml.Models 2.2
import QtQuick.Dialogs 1.2

Item {
    id: item1
    property alias okButton: okButton
    property alias cancelButton: cancelButton
    property alias streetsModel: streetsModel
    property alias streetsTableView: streetsTableView
    property alias isBusy: busyIndicator.running

    anchors.fill: parent

    ListModel {
        id: streetsModel
    }

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        spacing: 10

        Text {
            id: name
            text: qsTr("Select the streets to be added to the territory:",
                       "Add street names to territoy")
        }

        TableView {
            id: streetsTableView
            Layout.fillWidth: true
            Layout.fillHeight: true

            Layout.columnSpan: 4

            model: streetsModel

            TableViewColumn {
                width: 60
                visible: true
                role: "isChecked"
                delegate: streetSelectionDelegate
            }

            TableViewColumn {
                title: qsTr("Street", "Street name")
                width: 230
                visible: true
                role: "streetName"
                elideMode: Text.ElideRight
            }

            rowDelegate: Rectangle {
                id: tableViewRowDelegate
                height: 25
                color: styleData.selected ? myPalette.highlight : (styleData.alternate ? myPalette.alternateBase : myPalette.base)
            }
        }

        RowLayout {
            id: rowLayout
            height: 33
            Layout.alignment: Qt.AlignRight | Qt.AlignBottom

            Button {
                id: okButton
                text: qsTr("OK")
            }

            Button {
                id: cancelButton
                text: qsTr("Cancel")
            }
        }
    }

    BusyIndicator {
        id: busyIndicator
        anchors.centerIn: parent
        running: true
    }
}
