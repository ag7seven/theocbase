#include <UIKit/UIKit.h>
#include <QGuiApplication>
#include <QtQuick>
#include "messagebox.h"
#include <QDebug>

MessageBox::MessageBox()

{
}

void MessageBox::show(QString title, QString message){
    //myViewController *mvc = [[myViewController alloc ] initWithMessageBox:this];
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title.toNSString()
            message:message.toNSString()
            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action __unused) {
                qDebug() << "OK Clicked";
                emit buttonClicked(true,-1);
            }];
    [alert addAction:okButton];
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];
    [qtController presentViewController:alert animated:YES completion:nil];
}

void MessageBox::showYesNo(QString title, QString message, int id){
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title.toNSString()
        message:message.toNSString()
        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action __unused) {
                qDebug() << "YES Clicked";
                emit buttonClicked(true,id);
            }];
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No"
            style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action __unused) {
                qDebug() << "NO Clicked";
                emit buttonClicked(false,id);
            }];
    [alert addAction:yesButton];
    [alert addAction:noButton];
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];
    [qtController presentViewController:alert animated:YES completion:nil];
}
