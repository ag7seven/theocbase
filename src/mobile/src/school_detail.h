#ifndef SCHOOL_DETAIL_H
#define SCHOOL_DETAIL_H

#include <QObject>
#include "../../school.h"
#include <QDate>

class school_detail : public school_item
{
    Q_OBJECT
    Q_PROPERTY(QString source READ getSource CONSTANT)
    Q_PROPERTY(QString theme READ getTheme CONSTANT)
    Q_PROPERTY(int number READ getAssignmentNumber CONSTANT)
    Q_PROPERTY(person *student READ getStudent CONSTANT)
    Q_PROPERTY(person *assistant READ getAssistant CONSTANT)
    Q_PROPERTY(person *volunteer READ getVolunteer CONSTANT)
    Q_PROPERTY(QVariant studies READ studies CONSTANT)
    Q_PROPERTY(QVariant settings READ school_settings CONSTANT)
    Q_PROPERTY(QString notes READ getNote WRITE setNote NOTIFY notesChanged)
    Q_PROPERTY(QString timing READ getTiming WRITE setTiming NOTIFY timingChanged)
    Q_PROPERTY(int setting READ getSettingNumber WRITE setSetting NOTIFY settingChanged)
    Q_PROPERTY(int study READ getActiveStudy CONSTANT)
    Q_PROPERTY(int nextstudy READ getNextStudy CONSTANT)
    Q_PROPERTY(bool exercise READ getStudyExercise CONSTANT)
    Q_PROPERTY(bool done READ getDone WRITE setDone NOTIFY doneChanged)
    Q_PROPERTY(AssignmentType type READ getType CONSTANT)
    Q_PROPERTY(QVariant volunteers READ volunteers CONSTANT)

public:
    school_detail();
    school_detail(school_item &item);

    QVariant studies();

    /**
     * @brief school_settings - Get all settings
     * @return - Setting names in variant list
     */
    QVariant school_settings();

    QVariant volunteers();

    int getActiveStudy();
    int getNextStudy();
    bool getStudyExercise();
    int getSettingNumber();

    Q_INVOKABLE void saveStudy(bool exerciseCompleted, bool assignmentDone, int nextStudy);

    /**
     * @brief saveChanges - Save changes in the database
     * @param notes - Notes text
     * @param timing - Timing
     * @param done - Assignment has been done
     * @param setting - Used setting. Note: This is selected index value in settings list (not number of setting)
     * @param volunteerid - Volunteer id. Parameter is optional.
     */
    Q_INVOKABLE void saveChanges(QString notes, QString timing, bool done, int setting, int volunteerid = -1);

signals:
    void notesChanged();
    void settingChanged();
    void timingChanged();
    void doneChanged();
private:
    schoolStudentStudy *mCurrentStudy;
    schoolStudentStudy *mNextStudy;
    QStringList settingList;
    QStringList studiesList;
    QStringList volunteersList;
    QList<school_setting *> allSettings;
    bool studyread;
};

#endif // SCHOOL_DETAIL_H
