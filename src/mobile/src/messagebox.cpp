#include "messagebox.h"
#include <QDebug>
#include <QMessageBox>

MessageBox::MessageBox()
{
}

void MessageBox::show(QString title, QString message){
    QMessageBox::information(0,title,message);
    emit buttonClicked(false, 0);
}

void MessageBox::showYesNo(QString title, QString message, int id){
    if (QMessageBox::question(0,title,message,QMessageBox::Yes,QMessageBox::No) == QMessageBox::Yes) {
        emit buttonClicked(true, id);
    }else{
        emit buttonClicked(false, id);
    }
}

