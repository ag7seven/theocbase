#ifndef QMLTRANSLATOR_H
#define QMLTRANSLATOR_H

#include <QObject>
#include <QTranslator>
#include <QApplication>
#include "../sql_class.h"

class QmlTranslator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int currentIndex READ getCurrentIndex WRITE setCurrentIndex NOTIFY languageChanged)
    Q_PROPERTY(QString changed READ emptyString NOTIFY languageChanged)
public:
    explicit QmlTranslator(QObject *parent = 0);
    ~QmlTranslator();

    void initTranslator();
    Q_INVOKABLE void setTranslation(QString langCode);
    int getCurrentIndex();
    void setCurrentIndex(int index);

    Q_INVOKABLE QStringList getLanguages();
    Q_INVOKABLE QString getLanguageName();

    QString emptyString() const;

signals:
    void languageChanged();
public slots:
private:
    QList<QPair<int,QString>> mLanguages;
    int mLanguageId;
    QTranslator mTranslatorMobile;
    QTranslator mTranslatorDesktop;
};

#endif // QMLTRANSLATOR_H
