#ifndef IOSUTIL_H
#define IOSUTIL_H

#include <QObject>

class iosutil : public QObject
{
    Q_OBJECT

public:
    static void initiOS();
    static QString getDeviceName();
private:
    static void setStatusBarColorLight();
};

#endif // IOSUTIL_H
