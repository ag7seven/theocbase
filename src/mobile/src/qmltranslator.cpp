#include "qmltranslator.h"

QmlTranslator::QmlTranslator(QObject *parent) : QObject(parent)
{    
}

QmlTranslator::~QmlTranslator()
{
    mLanguages.clear();
}

void QmlTranslator::initTranslator()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    mLanguageId = sql->getLanguageDefaultId();
    mLanguages  = sql->getLanguages();

    QStringList uilanguages;

    QString selectedLang = "";
    if (sql->getSetting("theocbase_language","") == ""){
        uilanguages = QLocale::system().uiLanguages();
    }else{
        uilanguages.append(sql->getSetting("theocbase_language",""));
    }
    for (QString l : uilanguages) {
        if (!l.startsWith(selectedLang)) break;
        if (mTranslatorMobile.load("theocbase_mobile_" + l.replace("-","_"), ":/translations/","_",".qm")) {
            qApp->installTranslator(&mTranslatorMobile);
            // install translations of desktop version
            if (mTranslatorDesktop.load("theocbase_" + l.replace("-","_"), ":/translations/","_",".qm"))
                qApp->installTranslator(&mTranslatorDesktop);

            selectedLang = l.left(2);
            if (l.length() == 5) break;
        }
    }

    // save language to database
    if (sql->getSetting("theocbase_language","") == "")
        sql->saveSetting("theocbase_language", selectedLang);
}

void QmlTranslator::setTranslation(QString langCode)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    if (mTranslatorMobile.load("theocbase_mobile_" + langCode.replace("-","_"), ":/translations/","_",".qm")) {
        qApp->removeTranslator(&mTranslatorMobile);
        qApp->installTranslator(&mTranslatorMobile);
        if (mTranslatorDesktop.load("theocbase_" + langCode.replace("-","_"),":/translations/","_",".qm")) {
            qApp->removeTranslator(&mTranslatorDesktop);
            qApp->installTranslator(&mTranslatorDesktop);
        }
        mLanguageId = sql->getLanguageId(langCode);
        sql->saveSetting("theocbase_language",langCode);
        emit languageChanged();
    }
}

int QmlTranslator::getCurrentIndex()
{
    int index = 0;
    for(int i=0;i<mLanguages.size();++i){
        if (mLanguages[i].first == mLanguageId)
            index = i;
    }
    return index;
}

void QmlTranslator::setCurrentIndex(int index)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    setTranslation(sql->getLanguageCode(mLanguages[index].first));
}

QStringList QmlTranslator::getLanguages()
{
    QStringList list;
    for(QPair<int,QString> p : mLanguages)
        list.append(p.second);
    return list;
}

QString QmlTranslator::getLanguageName()
{
    for(QPair<int,QString> p : mLanguages)
        if (p.first == mLanguageId)
            return p.second;
    return "";
}

QString QmlTranslator::emptyString() const
{
    return "";
}
