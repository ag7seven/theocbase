#ifndef SHAREUTILS_H
#define SHAREUTILS_H

#include <QObject>
#include <QStandardPaths>
#include <QUrl>
#include <QUrlQuery>
#include <QPoint>

class ShareUtils : public QObject
{
    Q_PROPERTY(QString receivedUrl READ receivedUrl WRITE setReceivedUrl NOTIFY receivedUrlChanged)
    Q_OBJECT

public:
    ShareUtils(QObject *parent = nullptr);
    Q_INVOKABLE void sendMail();
    Q_INVOKABLE void saveBackup();
    Q_INVOKABLE void shareText(const QString text);
    Q_INVOKABLE void loginDropbox(const QUrl &url);

    Q_INVOKABLE QString openFile(QString format);

    Q_INVOKABLE void setPoint(QPoint p) { mPoint = p; }

    QString receivedUrl();
    void setReceivedUrl(QString path);

    static ShareUtils* getInstance();

public slots:
    void handleUrlReceived(const QUrl &url);

signals:
    void receivedUrlChanged(QString filename);
    void error(QString message);
    void dbcallback(QString token);    
    
private:
    static ShareUtils* mInstance;
    QString mViewFilePath = "";
    QPoint mPoint;
};
#endif // SHAREUTILS_H
