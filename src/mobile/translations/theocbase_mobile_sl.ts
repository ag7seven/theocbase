<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sl">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>P</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ne določi nove naloge</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Pusti trenutno nalogo</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Napačni podatki</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Dodaj čas?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Naslov</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Vir</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Učenec</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Sogovornik</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Dokončano</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Nadomeščanje</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Določi kdo nadomešča</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Trenutna naloga</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Vaja zaključena</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Naslednja naloga</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Izberi naslednjo nalogo</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Čas</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Opombe</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Podrobnosti</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Zaženi štoparico</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Ustavi štoparico</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Okvir</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Izberite okvir</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Naslov</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Vir</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vodja</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Govornik</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Bralec</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Opombe</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Opombe</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>ZAKLADI IZ BOŽJE BESEDE</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>IZURIMO SE V OZNANJEVANJU</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>KRŠČANSKO ŽIVLJENJE</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsedujoči</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Svetovalec</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pesem %1 in molitev</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pesem</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vodja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Bralec</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Dvorana</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Pomožni</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Pomožni 2</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Uvodne besede</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Obnova, nato pregled programa za naslednji teden</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Molitev</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>GD</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>P1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>P2</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Pusti trenutno nalogo</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Zaženi štoparico</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Ustavi štoparico</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Dodaj čas?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Vir</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Učenec</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Okvir dogajanja</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Dokončano</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Čas</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Trenutna naloga</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Vaja opravljena</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Naslednja naloga</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Izberi naslednjo nalogo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Opombe</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Sogovornik</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Nadomeščanje</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Štoparica</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Sogovornik naj ne bi bil nasprotnega spola.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Podrobnosti</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Potegnite navzdol za osvežitev...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Izpustite za osvežitev...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Nalaganje...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Neuspešna prijava</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>Vpis v TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Uporabniško ime</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Geslo</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Vpis</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Nov uporabnik / pozabljeno geslo</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Uporabniško ime ali e-poštni naslov</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Ustvarite račun</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Obnovi geslo</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>E-poštni naslov ni najden!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Ste pozabili geslo?</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Vstopna stran</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Glavni</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Pomožni</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Pomožni 2</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Teden od %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Občinsko preučevanje Biblije</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Teokratična strežbena šola</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Službeni shod</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Javni govor</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Preučevanje Stražnega stolpa</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Shod med tednom</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Shod ob koncu tedna</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>med tednom</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>ob koncu tedna</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Pesem in molitev</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pesem %1 in molitev</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>JAVNI GOVOR</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>PREUČEVANJE STRAŽNEGA STOLPA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Pesem %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vodja</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Bralec</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Priimek</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brat</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sestra</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Postavljen brat</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Družina</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Član družine povezan z</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktni podatki</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Uporabi za šolo</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Vsi razredi</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Samo glavni razred</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Samo pomožni razredi</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Poudarki</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>št.1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>št.2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>št.3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Premor</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Družinski poglavar</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Sogovornik</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Pokliči %1</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Dejaven</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsedujoči</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Zakladi iz Božje besede</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Kopljimo za duhovnimi dragulji</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Branje Bibije</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Prvi pogovor</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Ponovni obisk</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Svetopisemski tečaj</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Pripravi predstavitve za ta mesec</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Naloge za Krščansko življenje</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Občinsko preučevanje Biblije</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Bralec za Obč. preuč. Biblije</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nov oznanjevalec</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Molitev</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Oznanjevalci</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Glavni</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Drugi</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Tretji</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Nastavitve</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Izpis</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Podatki</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Različica</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Domača stran TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Pošlji povratno informacijo</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Zadnjič posodobljeno: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Razpored</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Prikaži čas</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Prikaži trajanje</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Uporabniški vmesnik</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Jezik</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Vpis</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sinhroniziranje...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Pesem za Stražni stolp</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Ni določeno</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Časovnica</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>število tednov pred izbranim datumom</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>število tednov po izbranem datumu</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>tedni</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Koliko tednov po nalogi naj bo sive barve?</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Na voljo je posodobitev. Želiš prenesti?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Spremembe so zaznane lokalno in tudi v oblaku (%1 vrstic). Ali želiš ohraniti lokalne spremembe?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Podatki v oblaku so bili obnovljeni. Tvoji lokalni podatki bodo nadomeščeni. Nadaljujem?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ne določi naslednje naloge</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Ni določeno</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>P</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>B</translation>
    </message>
</context></TS>