<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>研</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>不要安排下个课题</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>留在目前的课题</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>数据无效</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>要记录时间吗？</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>资料来源</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>学生</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>助手</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>完成</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>代替者</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>请选代替者</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>目前课题</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>作业完成</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>下个课题</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>选择下个课题</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>训练班细节</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>开秒表</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>停秒表</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>模拟场合</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>选择模拟场合</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>资料来源</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>主持人</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>讲者</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>朗读者</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>细节</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>附注</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>上帝话语的宝藏</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>用心准备传道工作</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>基督徒的生活</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>主席</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>导师</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>唱诗第%1首及祷告</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>诗歌</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>主持人</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>朗读者</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>第一班</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>第二班</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>第三班</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>开场白</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>复习并概述下周的节目</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>祷告</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>导入时间表</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>留在目前的课题</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>开秒表</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>停秒表</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>要记录时间吗？</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>资料来源</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>学生</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>模拟场合</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>完成</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>目前课题</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>作业完成</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>下个课题</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>选择下个课题</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>助手</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>代替者</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>秒表</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>助手不该是异性。</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>课题</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>细节</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>下拉刷新</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>释放刷新</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>载入中...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>登录失败</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>登录 TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>新用户 ／ 忘记密码</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>用户名或邮箱</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>邮箱</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>创建帐号</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>重设密码</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>邮箱地址未找到!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>忘记密码</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>登录页面</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>第一</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>第二</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>第三</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>始于%1的一周</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>会众研经班</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>传道训练班</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>传道工作聚会</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>公众演讲</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>《守望台》研究班</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>平日聚会</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>周末聚会</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>平日</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>周末</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>外出讲者</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>这个周末有%1外出讲者</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>这个周末没有外出讲者</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>唱诗和祷告</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>唱诗第%1首和祷告</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>公众演讲</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>《守望台》研究班</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>唱诗第%1首</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>指挥</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>朗读者</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>导入《守望台》</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>会众</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>讲者</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>手机</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>电话</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>电邮</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>资讯</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>好客</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>公众演讲</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>名</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>姓</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>弟兄</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>姐妹</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>助理仆人</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>家庭</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>关联的家人</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>联络资料</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>电话</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>邮箱</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>可用在训练班</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>所有班</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>只用于第一班</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>不用于第一班</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>选讲</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>1号</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>2号</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>3号</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>暂停</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>家主</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>助手</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>呼叫%1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>活跃</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>主席</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>上帝话语的宝藏</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>挖掘属灵宝石</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>经文朗读</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>初次交谈</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>续访</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>圣经讨论</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>准备本月介绍词</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>基督徒的生活</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>会众研经班</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>会众研经班朗读者</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>新的传道员</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>祷告</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>招待公众演讲者的人</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>手机</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>平日聚会</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>演讲</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>周末聚会</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>守望台研究班主持</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>朗读者</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>传道员</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>第一</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>第二</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>第三</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>选择列表</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>设置</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>注销</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase 首页</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>反馈</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>上一次同步：%1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>程序</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>显示时间</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>显示时长</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>用户界面</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>电邮</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>正在同步…</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>主席</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>诗歌</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>主席</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>诗歌</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>《守望台》诗歌</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>《守望台》编号</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>文章编号</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>主持</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>朗读者</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>未设定</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>时间线</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>被选中日期之前有多少个星期</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>被选中日期只后有多少个星期</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>周</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>被选中日期之后有多少个星期为灰色</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>找到更新。需要同步吗？</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>本地和云端发现同样的变更(共%1行)。确认保存本地的变更吗?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>云数据已被重置。本地数据会被覆盖。继续？</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>不要安排下个课题</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>未设置</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>选</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>读</translation>
    </message>
</context></TS>