<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ne">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>अर्को बुँदामा काम गर्न नदिनुहोस्</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>हालै बुँदामा छोड्नुहोस्</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>अमान्य डाटा</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>समय रेकर्ड गर्नुहुन्छ?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>मूल विषय</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>विवरण</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>विद्यार्थी</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>सहयोगी</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>नतिजा</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>पूरा भयो</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>स्वयंसेवक</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>एक स्वयंसेवक चयन गर्नुहोस्</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>हालै बुँदा</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>अभ्यास ग-यो</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>अर्को बुँदा</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>अर्को बुँदा चयन गर्नुहोस्</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>समय</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>नोट</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>स्कुलको विवरण</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>स्टपवाच स्टार्ट गर्नुहोस्</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>स्टपवाच रोख्नुहोस्</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>भूमिका</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>भूमिका चयन गर्नुहोस्</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>मूल विषय</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>विवरण</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>सञ्चालक</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>वक्ता</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>पढाइ</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>नोटहरू</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>विवरण</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>नोट</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>बाइबलमा पाइने अनमोल धन</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>प्रचार गर्ने सीप तिखारौं</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ख्रिष्टियन जीवन</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>चेयरम्यान</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>सल्लाहकार</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>गीत %1 र प्रार्थना</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>गीत</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>सञ्चालक</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>पढाइ</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>मुख्य</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>दोस्रो</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>तेस्रो</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>परिचय</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>पुनरावृत्ति र अर्को हप्ताको कार्यक्रमको परिचय</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>प्रार्थना</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>तालिका आयात गर्नुहोस्...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>हालै बुँदामा छोड्नुहोस्</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>स्टपवाच स्टार्ट गर्नुहोस्</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>स्टपवाच रोख्नुहोस्</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>समय रेकर्ड गर्नुहुन्छ?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>मूल विषय</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>विवरण</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>विद्यार्थी</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>भूमिका</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>नतिजा</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>पूरा भयो</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>समय</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>हालै बुँदा</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>अभ्यास ग-यो</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>अर्को बुँदा</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>अर्को बुँदा चयन गर्नुहोस्</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>नोट</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>सहयोगी</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>स्वयंसेवक</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>स्टपवाच</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>सहयोगी विपरीत लिङ्‌गको व्यक्‍ति हुनु हुँदैन।</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>बुँदा</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>विवरण</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>ताजा गर्न मुनि ठान्नुहोस्...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>ताजा गर्न छोड्नुहोस्...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>लोड हुँदै...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>प्रवेश गर्न असफल भयो</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>TheocBase प्रवेश</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>प्रयोगकर्ता नाम</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>पासवर्ड</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>प्रवेश</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>नयाँ प्रयोगकर्ता / पासवर्ड बिर्सनुभयो?</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>प्रयोगकर्ता नाम वा इमेल</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>इमेल</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>नयाँ खाता बनाउने</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>पासवर्ड रिसेट गर्नुहोस्</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>इमेल ठेगाना फेला परेन!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>पासवर्ड बिर्सियो</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>प्रवेश पेज</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>मुख्य</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>दोस्रो</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>तेस्रो</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>%1 देखि सुरु हुने हप्ता</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>मण्डली बाइबल अध्ययन</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>ईश्‍वरतान्त्रिक सेवा स्कुल</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>सेवा सभा</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>जनभाषण</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>प्रहरीधरहरा अध्ययन</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>मध्यहप्ताको सभा</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>सप्ताहन्तको सभा</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>मध्यहप्ता</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>सप्ताहन्त</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>अरू मण्डलीमा जाने जनवक्ताहरू</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>यस सप्ताहन्त अरू मण्डलीमा एक जनवक्ता जानुहुनेछ</numerusform>
            <numerusform>यस सप्ताहन्त अरू मण्डलीमा जनवक्ताहरू जानुहुनेछ</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>यस सप्ताहन्त अरू मण्डलीमा गएर जनभाषण दिने कोही हुनेछैन</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>गीत र प्रार्थना</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>गीत %1 र प्रार्थना</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>जनभाषण</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>प्रहरीधरहरा अध्ययन</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>गीत %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>सञ्चालक</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>पढाइ</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>प्र.ध. आयात गर्नुहोस्...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>मूल विषय</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>मण्डली</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>वक्ता</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>मोबाइल</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>फोन</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>इमेल</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>अतिथि सेवक</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>जनभाषण</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>नाम</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>थर</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>ब्रदर</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>सिस्टर</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>सेवक</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>परिवार</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>परिवारको सदस्यसँग जोड्ने</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>सम्पर्क जानकारी</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>फोन</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>इमेल</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>स्कुलको लागि प्रयोग गर्नुहोस्</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>सबै कक्ष</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>मुख्य कक्ष मात्र</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>सहायक कक्ष मात्र</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>विशेषताहरू</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>नं.१</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>नं.२</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>नं.३</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>विश्राम</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>परिवारको शिर</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>सहयोगी</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>%1 लाई कल गर्नुहुन्छ?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>सक्रिय</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>चेयरम्यान</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>बाइबलमा पाइने अनमोल धन</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>बहुमुल्य रत्न खोजौं</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>बाइबल पढाई</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>पहिलो भेट</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>पुनःभेट</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>बाइबल अध्ययन</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>यस महिनाको प्रस्तुति तयार पार्नुहोस्‌</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>ख्रिष्टियन जीवनको भाषण</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>मण्डली बाइबल अध्ययन</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>मण्डली बाइबल अध्ययन पढाइ</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>नयाँ प्रकाशक</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>प्रार्थना</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>जनवक्ताको लागि अतिथिसेवक</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>मोबाइल</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>मध्यहप्ताको सभा</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>भाषण</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>सप्ताहन्तको सभा</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>प्रहरीधरहरा अध्ययन सञ्चालक</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>प्रहरीधरहरा पढाइ</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>प्रकाशकहरू</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>मुख्य</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>दोस्रो</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>तेस्रो</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>चयन सूची</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>सेटिङ्ग</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>बाहिरिने</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>जानकारी</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>भर्सन</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase होमपेज</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>प्रतिक्रिया</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>अन्तिम समीकरण भएको: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>तालिका</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>समय देखाउने</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>अवधि देखाउने</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>प्रयोगकर्ता इन्टरफेस</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>भाषा</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>प्रवेश</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>इमेल</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>नाम</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>सिङ्क्रोनाइज गर्दैछ...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>चेयरम्यान</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>गीत</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>सप्ताहन्तको सभाको चेयरम्यान</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>गीत</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>प्रहरीधरहरा गीत</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>प्रहरीधरहरा-को अङ्‌क</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>अध्ययन लेख</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>मूल विषय</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>सञ्चालक</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>पढाइ</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>सेट भएको छैन</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>समय रेखा</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>चयन गरिएको मिति भन्दा अगिल्लो हप्ताको सङ्ख्या</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>चयन गरिएको मिति भन्दा पछिल्लो हप्ताको सङ्ख्या</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>हप्ता</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>एक असाइनमेन्ट पछिल्लो हप्ताको सङ्ख्या, जसमा खैरो रङ्ग लगाइनेछन्</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>अपडेट उपलब्ध छ। तपाईँ समीकरण गर्न चाहनुहुन्छ?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>स्थानीय र क्लाउड दुवै (पंक्ति %1) एउटै परिवर्तन देखा पारे। तपाईं स्थानीयमा भएको परिवर्तन राख्न चाहनुहुन्छ?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>क्लाउड डाटा रिसेट गरिएको छ। तपाईंको स्थानीय डाटा प्रतिस्थापन गरिने छ। जारी राख्नुहुन्छ?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>अर्को बुँदामा काम गर्न नदिनुहोस्</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>सेट भएको छैन</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
</context></TS>