<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>S</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nächsten Schulungspunkt nicht zuteilen</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Bei derzeitigem Schulungspunkt bleiben</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Ungültige Daten</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Zeit hinzufügen?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Teilnehmer</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Abgeschlossen</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Einspringer</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Einspringer auswählen</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Derzeitiger Schulungspunkt</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Übungen abgeschlossen</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Nächster Schulungspunkt</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Nächsten Schulungspunkt auswählen</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Stoppuhr</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Aufgabendetails</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Stoppuhr starten</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stoppuhr anhalten</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Rahmen</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Rahmen auswählen</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Leiter</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Redner</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leser</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>SCHÄTZE AUS GOTTES WORT</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>UNS IM DIENST VERBESSERN</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>UNSER LEBEN ALS CHRIST</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Vorsitzender</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Ratgeber</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Lied %1 und Gebet</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Leiter</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leser</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Hauptsaal</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Nebenraum 1</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Nebenraum 2</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Einleitende Worte</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Wiederholung und Vorschau auf nächste Woche</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Gebet</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>LDZ-Plan importieren ...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>HS</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Bei derzeitigem Schulungspunkt bleiben</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Stoppuhr starten</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stoppuhr anhalten</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Zeit hinzufügen?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Teilnehmer</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Rahmen</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Abgeschlossen</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Stoppuhr</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Derzeitiger Schulungspunkt</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Übungen abgeschlossen</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Nächster Schulungspunkt</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Nächsten Schulungspunkt auswählen</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Einspringer</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stoppuhr</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Der Partner soll niemand vom anderen Geschlecht sein.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lektion</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Zum Aktualisieren ziehen ...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Zum Aktualisieren loslassen ...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Laden ...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Anmeldung fehlgeschlagen</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>TheocBase-Anmeldung</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Anmeldung</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Neuer Benutzer / Passwort vergessen</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Benutzername oder E-Mail-Adresse</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Benutzerkonto erstellen</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Passwort zurücksetzen</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>E-Mail-Adresse nicht gefunden!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Passwort vergessen</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Anmeldeseite</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Saal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Nebenraum 1</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Nebenraum 2</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Woche vom %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Versammlungsbibelstudium</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Theokratische Predigtdienstschule</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Dienstzusammenkunft</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Öffentlicher Vortrag</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Wachtturm-Studium</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Zusammenkunft unter der Woche</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Zusammenkunft am Wochenende</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Unter der Woche</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Am Wochenende</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>REDNER AUSWÄRTS</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 Redner ist dieses Wochenende auswärts.</numerusform>
            <numerusform>%1 Redner sind dieses Wochenende auswärts.</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Dieses Wochenende sind keine Redner auswärts.</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Lied und Gebet</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Lied %1 und Gebet</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>ÖFFENTLICHER VORTRAG</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>WACHTTURM-STUDIUM</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Lied %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Leiter</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leser</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>WT-Programm importieren ...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Versammlung</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Redner</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Gastgeber</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Öffentlicher Vortrag</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Bruder</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Schwester</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Diener</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Familie</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Familienmitglied von</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontaktinformationen</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Zuteilungen in der TPS</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Alle Räume</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Nur Hauptsaal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Nur Nebenräume</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Höhepunkte</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>Nr. 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>Nr. 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>Nr. 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Auszeit</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Familienhaupt</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Partner</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>%1 anrufen?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktiv</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Vorsitzender</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Schätze aus Gottes Wort</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Nach geistigen Schätzen graben</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Bibellesung</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Erstes Gespräch</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Rückbesuch</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Bibelstudium</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Sich mit den Gesprächsvorschlägen für den Monat vertraut machen</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Unser Leben als Christ (Programmpunkte)</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Versammlungsbibelstudium</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Versammlungsbibelstudium (Leser)</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Neuer Verkündiger</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Gebet</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Gastgeber für Vortragsredner</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobil</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Zusammenkunft unter der Woche</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Vortrag</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Zusammenkunft am Wochenende</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Wachtturm-Studium (Leiter)</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Wachtturm-Studium (Leser)</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Verkündiger</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Saal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Zweiter Raum</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Dritter Raum</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Auswahlliste</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase-Homepage</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Feedback</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Letzte Synchronisierung: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Terminplan</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Startzeit</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Benutzeroberfläche</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchronisierung ...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Vorsitz</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Vorsitzender der Zusammenkunft am Wochenende</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Wachtturm-Lied</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Wachtturmausgabe</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artikel</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Leiter</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Leser</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Nicht festgelegt</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Zeitleiste</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Anzahl Wochen vor dem ausgewählten Datum</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Anzahl Wochen nach dem ausgewählten Datum</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>Wochen</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Anzahl Wochen, die nach einer Zuteilung ausgegraut werden</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Neuere Daten verfügbar. Synchronisieren?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Die gleichen Änderungen wurden lokal und im Online-Speicher gefunden (%1 Zeilen). Die lokalen Änderungen behalten?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Der Cloud-Speicher wurde zurückgesetzt. Die lokalen Daten werden ersetzt. Fortfahren?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nächsten Schulungspunkt nicht zuteilen</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nicht festgelegt</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>Ls</translation>
    </message>
</context></TS>