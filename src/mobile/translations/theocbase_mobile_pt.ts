<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>EBC</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Não designar nova característica</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Permanecer na característica atual</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Dados inválidos.</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Adicionar tempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Matéria</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudante</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Concluído</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Substituto</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Escolha um substituto</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Característica atual</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercício realizado</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Próxima característica</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Escolha a próxima característica</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Anotações</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Detalhes da Escola</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Iniciar cronômetro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Parar cronômetro</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Cena</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Selecionar cenário</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalhes</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TESOUROS DA PALAVRA DE DEUS</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>EMPENHE-SE NO MINISTÉRIO</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VIVER COMO CRISTÃOS</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Conselheiro</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cântico %1 e oração</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Principal</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Sala B</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Sala C</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Comentários iniciais</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Recapitulação e Antevisão da Reunião da Próxima Semana</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Importar Programa</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>S.1</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>2 S.</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>3 S.</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Permanecer no estudo atual</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Iniciar cronômetro</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Parar cronômetro</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Adicionar controle de tempo?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Estudante</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Cena</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Concluido</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Estudo atual</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Exercício completo</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Próximo estudo</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Escolha o próximo estudo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Ajudante</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntário</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Cronômetro</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>O ajudante não deve ser alguém do sexo oposto.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Lição oratória</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalhes</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Reiniciar</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Carregando</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Falha ao iniciar sessão</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>Acessar TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Usuário</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Novo usuário / Esqueceu a senha</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Nome de usuário ou email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Criar conta</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Redefinir senha</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Endereço de e-mail não encontrado</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Esqueceu a senha</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>página de entrada</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Salão principal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Segunda sala</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Terceira sala</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Semana de %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>EBC</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Escola do Ministério Teocrático</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Reunião de Serviço</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Discurso Público</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Estudo Sentinela</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião de Semana</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Meio da semana</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Fim de semana</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Oradores fora</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 orador fora nesta semana</numerusform>
            <numerusform>%1 oradores fora nesta semana</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Nenhum orador fora nesta semana</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Cântico e Oração</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Cântico %1 e Oração</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISCURSO PÚBLICO</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>Estudo Sentinela</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Cântico %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Importar WT...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Congregação</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Orador</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Telemóvel</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>EMail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Anfitrião</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Discurso Público</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Sobrenome</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Irmão</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Irmã</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Servo</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Família</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Membro da família de</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Dados de contato</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Privilégios designados</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Todas as salas</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Apenas salão principal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Apenas 2ª Sala</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Destaques</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>Nº 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>Nº 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>Nº 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Suspenso</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Chefe de família</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Ajudante</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informação</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Telefonar para %1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Ativo</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>TESOUROS DA PALAVRA DE DEUS</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Em Busca de Pérolas Espirituais</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Leitura da Bíblia</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Primeira visita</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Revisita</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Estudo Bíblico</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Prepare as apresentações do mês</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Discursos de &quot;VIVER COMO CRISTÃOS&quot;</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Estudo Bíblico de Congregação</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Leitor do Estudo Bíblico</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Novo publicador</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Oração</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Anfitrião para Oradores Públicos</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Telemóvel</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reunião de Semana</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Discurso</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reunião do Fim de Semana</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Dirigente Estudo Sentinela</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Leitor do estudo da Sentinela</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Publicadores</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Salão principal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Segunda sala</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Terceira sala</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Lista de seleção</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Configuração</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Fechar sessão</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informações</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Página da TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Comentários/Sugestões</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Última sincronização: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Programa</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Mostrar Hora</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Mostrar duração</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Opções de interface do usuário</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sincronizando</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Presidente</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Presidente da Reunião Pública</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Cântico</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Cântico</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Edição de A Sentinela</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artigo</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Dirigente</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Leitor</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Não definido</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Cronograma</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Número de semanas antes da data seleccionada</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Número de semanas depois da data seleccionada</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semanas</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Número de semanas em cinza depois de uma designação</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Atualização disponível. Gostaria de sincronizar?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>As mesmas atualizações podem ser vistas em seu dispositivo e na nuvem ( %1 linhas). Quer descartar as atualizações da nuvem e salvar as as suas?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>O programa da reunião foi apagado da partilha na nuvem. A informação que está no seu computador será substituída. Deseja efectuar essa operação?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Não designar nova característica</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Não definido</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>D</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>Leit.</translation>
    </message>
</context></TS>