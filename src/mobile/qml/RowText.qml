import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1

Rectangle {
    id: rowtext
    width: parent.width
    implicitWidth: 200
    height: (textvalue.contentHeight > 40*dpi) ? textvalue.contentHeight + 10 : 40*dpi
    Layout.preferredHeight: height
    Layout.fillWidth: true

    property string title: ""
    property string text: ""
    property bool arrow: false
    property bool editable: false
    property bool firstrow: false
    property alias inputMethodHints: textField.inputMethodHints
    property bool strikeout: false

    signal clicked
    signal pressAndHold(var mouse)
    signal editingFinished()
    color: rowtextMouseArea.pressed ? (arrow ? "#D9D9D9" : "white") : "white"

    RowLayout {
        anchors.fill: parent
        anchors.leftMargin: 10*dpi
        anchors.rightMargin: 10*dpi
        Text {
            id: textTitle
            text: title
            color: enabled ? "black" : "grey"
            font.pointSize: app_info.fontsize
            verticalAlignment: Text.AlignVCenter
        }
        Text {
            id: textvalue
            Layout.fillWidth: true
            text: rowtext.text
            color: "grey"
            visible: !editable
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            font.pointSize: app_info.fontsize
            font.strikeout: rowtext.strikeout
        }

        TextField {
            id: textField
            Layout.fillWidth: true
            text: rowtext.text
            visible: editable
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            bottomPadding: topPadding
            onTextChanged: {
                rowtext.text = text
            }
            background: Rectangle {
                implicitHeight: 40
                implicitWidth: 100
                color: "transparent"
            }
            onEditingFinished: rowtext.editingFinished()
        }
        Rectangle {
            id: arrowrec
            width: 20*dpi
            height: 20*dpi
            //anchors.verticalCenter: parent.verticalCenter
            //border.width: 1
            visible: arrow
            color: rowtext.color
            Image {
                source: "qrc:///chevron_right.svg"
                //anchors.rightMargin: 5*dpi
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }
    MouseArea {
        id: rowtextMouseArea
        anchors.fill: parent
        //propagateComposedEvents: true
        //visible: !textField.activeFocus
        anchors.rightMargin: textField.visible && textField.activeFocus ? rowtext.width - textField.x : 0
        //preventStealing: true
        onClicked: {
            console.log("row click")
            if (editable && mouse.x >= textField.x){
                mouse.accepted = false
                textField.forceActiveFocus()
            }else{
                rowtext.focus = true
            }
            rowtext.clicked();
        }
        onPressAndHold: {
            textTitle.forceActiveFocus()
            rowtext.pressAndHold(mouse)
        }
    }

    Rectangle {
        width: parent.width
        anchors.top: parent.top
        height: app_info.linewidth
        color: "lightgrey"
        visible: firstrow
    }
    Rectangle {
        width: parent.width
        anchors.bottom: parent.bottom
        height: app_info.linewidth
        color: "lightgrey"
    }
}


