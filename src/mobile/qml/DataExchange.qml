import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Page {
    id: rectangle
    property string filePath: ""
    property url tmpurl

    Component.onCompleted: {
        readCurrentFile(filePath)
    }

    function readCurrentFile(f) {
        listView.model.clear()
        if (f !== "") {
            if (f.startsWith("file://") > -1) {
                f = f.replace("file://","")
            }
            _csync.readXmlFile(f)
        }
    }

    CSync {
        id: _csync
        onNewReportRow: {
            console.log("new report row: " + text)
            var c = "white"
            var img = ""
            switch(typ) {
            case CSync.Publisher: c = "#464442"; img = "qrc:///synced_publisher.svg"; break
            case CSync.MidweekMeeting: c = "#F8F2E9"; img = "qrc:///synced_mwm.svg"; break
            case CSync.WeekendMeeting: c = "#EBEDF1"; img = "qrc:///synced_wem.svg"; break
            case CSync.PublicTalk: c = "#EBEDF1"; img = "qrc:///synced_talks.svg"; break;
            case CSync.Obsolete:
            default: c = "#2F4870"; break
            }

            listView.model.append({"name": text, colorCode: c, imageSource: img})
        }
    }

    header: BaseToolbar {
        title: "Data Exchange - Import"
        componentLeft: ToolButton {
            text: "Done"
            icon.source: "qrc:///done.svg"
            icon.color: "white"
            onClicked: stackView.pop()
        }
    }

    ListView {
        id: listView
        anchors.fill: parent
        delegate: ItemDelegate {
            width: parent.width
            height: 40*dpi
            leftPadding: colorBox.width + 10
            text: name
            font.pointSize: app_info.fontsize

            indicator: Rectangle {
                id: colorBox
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 5
                width: height
                color: "transparent"
                border.width: 1
                border.color: imageBox.source == "" ? colorCode : "transparent"
                Image {
                    id: imageBox
                    anchors.fill: parent
                    source: imageSource
                    sourceSize: Qt.size(60,60)
                }
            }
        }
        model: ListModel {
        }
    }
}
