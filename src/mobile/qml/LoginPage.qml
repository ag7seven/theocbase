import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.3
import net.theocbase.mobile 1.0

Rectangle {
    width: 400 //parent.width
    height: 500 // parent.height
    color: "#30496F"
    id: loginPage
    property int method: 0    

    function loginCloud(){
        console.log("login")
        loginPage.forceActiveFocus()
        ccloud.authentication.setCredentials(textUsername.text,textPassword.text)
        ccloud.authentication.grant()
    }

    function createAccount(){
        loginPage.forceActiveFocus()               
        if (ccloud.createAccount(textUsername.text,textEmail.text,textPassword.text))
            loginCloud()
    }

    function resetPassword(){
        loginPage.forceActiveFocus()
        if (ccloud.resetPassword(textEmail.text)){
            msg.show("TheocBase","The instruction for resetting your password has been sent to " + textEmail.text)
        }else{
            msg.show("TheocBase", qsTr("Email address not found!"))
        }
        method = 0
    }

    Connections {
        target: ccloud.authentication
        onStatusChanged: {
            labelError.visible = true;
            switch(status)
            {
            case 0:
                labelError.text = "Not authenticated";
                break;
            case 1: case 3 :
                labelError.text = "Please wait...";
                break;
            case 2:
                labelError.text = "Authenticated";
                if (Qt.inputMethod.visible) Qt.inputMethod.hide()
                ccloud.autoSync = true
                ccloud.checkCloudUpdates(true);
                stackView.pop(StackView.Immediate);
                break;
            default:
                labelError.text = "";
                labelError.visible = false;
            }
        }
    }


    Component.onCompleted: {
        console.log("login completed")
        console.log(ccloud.logged())
    }

    FontLoader {
        id: openSansFont
        source: "qrc:/fonts/OpenSans-Regular.ttf"
    }

    ColumnLayout {
        id: column1
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.bottom: parent.bottom

        Item { Layout.fillHeight: true }
        Label {
            text: "theocbase cloud"
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            font.family: openSansFont.name
            font.bold: true
            font.pointSize: Math.round(app_info.fontsize * 1.8)
            color: "white"
            renderType: Text.QtRendering
        }

        TextField {
            id: textUsername
            Layout.fillWidth: true
            placeholderText: method == 0 ? qsTr("Username or Email") : qsTr("Username")
            font.pointSize: app_info.fontsize
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhMultiLine
            textColor: Qt.platform.os == "winrt" ||
                       Qt.platform.os == "winphone" ? "black" :
                                                      Qt.platform.os == "android" ? "white" : textUsername.textColor
            visible: method < 2
            Keys.onReturnPressed: {
                console.log("username return pressed")
                textPassword.focus = true
            }
        }
        TextField {
            id: textEmail
            Layout.fillWidth: true
            placeholderText: qsTr("Email")
            font.pointSize: app_info.fontsize
            inputMethodHints: Qt.ImhEmailCharactersOnly | Qt.ImhMultiLine | Qt.ImhNoAutoUppercase
            textColor: Qt.platform.os == "winrt" ||
                       Qt.platform.os == "winphone" ? "black" :
                                                      Qt.platform.os == "android" ? "white" : textEmail.textColor
            visible: method > 0
        }

        TextField {            
            Layout.fillWidth: true
            id: textPassword
            visible: method < 2
            anchors.horizontalCenterOffset: 0
            echoMode: TextInput.Password
            placeholderText: qsTr("Password")
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhMultiLine
            font.pointSize: app_info.fontsize
            textColor: Qt.platform.os == "winrt" ||
                       Qt.platform.os == "winphone" ? "black" :
                                                      Qt.platform.os == "android" ? "white" : textPassword.textColor
            Keys.onReturnPressed: {
                loginPage.forceActiveFocus()
                loginCloud()
            }
		}

        Button {
            Layout.maximumWidth: 300*dpi
            Layout.preferredWidth: 200*dpi
            Layout.fillWidth: true
            id: buttonLogin
            enabled: method === 0 ?
                         textUsername.text.length > 2 && textPassword.text.length > 2 :
                         method === 1 ? textUsername.text.length > 2 && textEmail.text.length > 2 && textPassword.text.length > 2 && textEmail.text.indexOf("@") > -1 :
                                        textEmail.text.length > 2 && textEmail.text.indexOf("@") > -1

			style: ButtonStyle{
                label: Text{
                    renderType: Text.QtRendering
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: control.enabled ? "black" : "grey"
                    text: control.text
                }
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 30
                    border.width: control.activeFocus ? 2 : 1
                    border.color: "#888"
                    radius: 4
                    color: control.pressed ? "grey" : "#eaf1f1"
                }
			}
            text: method == 0 ? qsTr("Login") : method == 1 ? qsTr("Create Account") : qsTr("Reset Password")
            onClicked: {
                switch(method){
                case 0: loginCloud(); break;
                case 1: createAccount(); break;
                case 2: resetPassword(); break;
                }
            }
        }

        Label {
            id: labelError
            anchors.horizontalCenter: parent.horizontalCenter
            text: "error"
            anchors.horizontalCenterOffset: 0
            visible: false
            color: "red"
            renderType: Text.QtRendering
        }

        Item  { Layout.minimumHeight: 10*dpi }
        Text {
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Forgot Password")
            color: mouseAreaForgot.pressed ? "lightgrey" : "darkgrey"
            visible: method == 0
            MouseArea {
                id: mouseAreaForgot
                anchors.fill: parent
                onClicked: {
                    labelError.visible = false
                    method = 2
                }
            }
        }
        Item { Layout.minimumHeight: 20*dpi }

        Text {
            text: (method == 0 ? qsTr("Create Account") : qsTr("Login Page")) + "  >"
            font.bold: true
            color: mouseAreaCreate.pressed ? "grey" : "white"
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            MouseArea {
                id: mouseAreaCreate
                anchors.fill: parent
                onClicked: {
                    labelError.visible = false
                    if(method == 0){
                        method = 1
                    }else{
                        method = 0
                    }
                }
            }
        }
        Item { Layout.fillHeight: true }
    }
}
