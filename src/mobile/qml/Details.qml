import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import net.theocbase.mobile 1.0

Rectangle {
    id: pageSchoolDetail
    implicitHeight: 600
    implicitWidth: 300
    width: parent.width
    height: parent.height
    color: "#EDEDF2"

    property int currentStudy: detobj.study
    property int nextStudy: detobj.nextstudy
    property int usedSetting: detobj.setting
    property bool assignments: detobj.number > 0 && detobj.number < 10
    property int volunteerid: detobj.volunteer ? detobj.volunteer.id : -1

    property string _no_next_study: qsTr("Do not assign the next study")
    property string _leave_study: qsTr("Leave on current study")
    property string _current_title: _title_text
    property string _title_text: qsTr("School Details")
    property color _title_color: "white"
    property string _start_timer_text: qsTr("Start stopwatch")
    property string _stop_timer_text: qsTr("Stop stopwatch")

    property SchoolDetail detobj
    property int currentindex: -1
    property date startTime

    property alias pageToolbar: topToolbar

    function saveChanges(){
        // hide keyboard
        if (Qt.inputMethod.visible) Qt.inputMethod.hide()

        if (schooltimer.running){
            _msg.showYesNo(qsTr(_stop_timer_text),"",1)
            return false
        }

        if (detobj.student){
            if (chkCompleted.checked){
                // setting
                if (detobj.type === SchoolDetail.Demonstration && usedSetting < 1 && volunteerid < 1){
                    _msg.show(qsTr("Invalid data"), qsTr("Select setting", "for demonstration"))
                    return false
                }
            }

            // save study
            if (detobj.number > 0 && detobj.number < 10){
                detobj.saveStudy(chkExercisesCompleted.checked,
                                 chkCompleted.checked,
                                 nextStudy)
            }

            detobj.saveChanges(textAreaNotes.text,
                               textTiming.text,
                               chkCompleted.checked,
                               usedSetting,
                               volunteerid)
        }
        return true
    }

    function timerTriggered(){
        if (schooltimer.running){
            schooltimer.stop()
            _title_color = "black"
            timerbutton.text = _start_timer_text
            _msg.showYesNo(qsTr("Add the timing?"), title.text,2)
        }else{
            startTime = new Date()
            schooltimer.running = true
            _title_color = "red"
            timerbutton.text = _stop_timer_text
        }
    }
    MsgBox { id: _msg }
    Connections {
        target: _msg
        onButtonClicked: {
            switch (id){
            case 1:
                if (ok) timerTriggered()
                break
            case 2:
                if (ok) textTiming.text = _current_title
                _current_title = _title_text
                break
            default: break
            }
        }
    }

    Timer {
        id: schooltimer
        interval: 500
        running: false
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            var elapsedTime = Math.round((new Date().getTime() - startTime.getTime()) / 1000)
            var minutes = Math.floor(elapsedTime / 60)
            var seconds = elapsedTime-minutes * 60
            _current_title = (minutes < 10 ? "0" : "") + minutes.toString() +
                    ":" + (seconds < 10 ? "0" : "") + seconds.toString()
        }
    }

    Component {
        id: topToolbar
        BaseToolbar {
            title: _current_title
            titleColor: _title_color
            componentLeft: ToolButton {
                iconSource: "qrc:///back.svg"
                style: Custom_ToolButtonStyle{}
                onClicked: {
                    // save changes
                    if (!saveChanges()) return;

                    // return to previous page
                    console.log("currentdate " + mainpage.currentdate.toString())
                    mainpage.refreshList(0)
                    stackView.pop()
                }
            }
            componentRightMiddle: ToolButton {
                iconSource: "qrc:///assignment_previous.svg"
                // disable when first in the shcool list
                enabled: currentindex > 0
                onClicked: {
                    if (!saveChanges()) return;
                    stackView.push({item: detailPage,
                                       properties: { detobj: schoolList.getAssignment(currentindex-1),
                                           currentindex: currentindex-1},
                                       replace: true,
                                   immediate: true})
                }
                style: Custom_ToolButtonStyle{}
            }
            componentRight: ToolButton {
                iconSource: "qrc:///assignment_next.svg"
                // disable when last in the school list
                enabled: currentindex < (schoolList.length -1)
                onClicked: {
                    if (!saveChanges()) return;
                    stackView.push({item: detailPage,
                                       properties: {detobj: schoolList.getAssignment(currentindex+1),
                                           currentindex: currentindex+1 },
                                   replace: true,
                                   immediate: true})
                }
                style: Custom_ToolButtonStyle{}
            }
        }
    }

    ScrollView {
        anchors.fill: parent
        contentItem: flickable1
    }

    Flickable {
        id: flickable1
        anchors.topMargin: 10*dpi
        anchors.bottomMargin: 10*dpi
        anchors.fill: parent
        contentHeight: layout.height
        flickableDirection: Flickable.VerticalFlick

        ColumnLayout {
            id: layout
            spacing: 0
            width: parent.width

            ToolButton {
                id: timerbutton
                Layout.fillWidth: true
                visible: !detobj.done && detobj.student
                text: _start_timer_text
                onClicked: {
                    timerTriggered()
                }
                style: Custom_ToolButtonStyle{}
            }

            Item {
                height: 10*dpi
                width: parent.width
            }

            RowText {
                title: qsTr("Theme")
                text: detobj.theme
                firstrow: true
            }
            RowText {
                title: qsTr("Source")
                text: detobj.source
            }
            RowText {
                title: qsTr("Student")
                text: detobj.student ? detobj.student.fullname : "-"
                strikeout: volunteerid > 0
            }

            RowText {
                title: qsTr("Assistant")
                text: detobj.assistant ? detobj.assistant.fullname : "-"
                visible: detobj.assistant
            }

            RowTitle { text: qsTr("Result") }

            RowSwitch {
                id: chkCompleted
                title: qsTr("Completed")
                checked: detobj.done
                enabled: (detobj.student)
                firstrow: true
                onClicked: {
                    if (!checked){ // reset volunteer data when unchecked
                        volunteerid = -1
                        textVolunteer.text = ""
                    }
                }
            }

            RowText {
                id: textVolunteer
                title: qsTr("Volunteer")
                text: detobj.volunteer ? detobj.volunteer.fullname : ""
                visible: chkCompleted.checked
                arrow: true

                onClicked: {
                    var volunteerindex = 0;
                    var listtest = detobj.volunteers; // every row contains <fullname>;<id>
                    var newlist = []
                    for(var i = 0; i < listtest.length; ++i){
                        newlist[i] = listtest[i].split(";")[0]
                        if (volunteerid === parseInt(listtest[i].split(";")[1])) volunteerindex = i
                    }

                    var volunteerPage = stackView.push({item: Qt.resolvedUrl("SelectionListPage.qml"),
                                                           properties: {model: newlist, pageTitle: qsTr("Select a volunteer"),
                                                               selectedRow: volunteerindex}})
                    volunteerPage.onActiveRowChanged.connect(function(index){
                        volunteerid = index > 0 ? listtest[index].split(";")[1] : -1
                        textVolunteer.text = index > 0 ? listtest[index].split(";")[0] : ""
                    })
                }
            }

            RowText {
                id: textCurrentStudy
                title: qsTr("Current Study")
                text: currentStudy === 0 ? "Not set" : detobj.studies[currentStudy]
                visible: (detobj.number > 0 && detobj.number < 10 && volunteerid < 1)
            }


            RowSwitch {
                id: chkExercisesCompleted
                title: qsTr("Exercises Completed")
                visible: currentStudy > 0 && (detobj.number > 0 && detobj.number < 10) && volunteerid < 1
                checked: detobj.exercise
                enabled: chkCompleted.checked // !detobj.done &&
            }
            RowText {
                id: textNextStudy
                title: qsTr("Next Study")
                text: (detobj.done && nextStudy === 0 || !detobj.done && currentStudy === 0) ?
                          _no_next_study : nextStudy > 0 ? detobj.studies[nextStudy] : _leave_study
                arrow: true
                visible: chkCompleted.checked && (detobj.number > 0 && detobj.number < 10) && volunteerid < 1 // && !detobj.done
                enabled: chkExercisesCompleted.checked
                onClicked: {
                    // hide keyboard
                    if (Qt.inputMethod.visible) Qt.inputMethod.hide()
                    var stydyPage = stackView.push({item: Qt.resolvedUrl("SelectionListPage.qml"),
                                       properties: {model: detobj.studies, pageTitle: qsTr("Select next study"),
                                                     selectedRow: nextStudy}})
                    stydyPage.onActiveRowChanged.connect(function(index){
                        nextStudy = index;
                        if (index === 0){
                            textNextStudy.text = _no_next_study
                        }else if(index === currentStudy){
                            textNextStudy.text = _leave_study
                        }else{
                            textNextStudy.text = detobj.studies[index]
                        }
                    })
                }
            }
            RowText {
                id: textTiming
                title: qsTr("Timing")
                text: detobj.timing
                editable: true
                // inputMethodHints: Qt.ImhPreferNumbers
                visible: chkCompleted.checked || text != ""
            }
            RowText {
                id: textSetting
                title: qsTr("Setting", "for demonstration")
                text: detobj.settings[usedSetting > -1 ? usedSetting : 0]
                arrow: true
                visible: detobj.type === SchoolDetail.Demonstration
                onClicked: {
                    // hide keyboard
                    if (Qt.inputMethod.visible) Qt.inputMethod.hide()
                    var settingPage = stackView.push({item: Qt.resolvedUrl("SelectionListPage.qml"),
                                       properties: {model: detobj.settings, pageTitle: qsTr("Select setting", "for demonstration"),
                                                     selectedRow: usedSetting}})
                    settingPage.onActiveRowChanged.connect(function(index){
                        usedSetting = index;
                        textSetting.text = detobj.settings[usedSetting > -1 ? usedSetting : 0]
                    })
                }
            }

            RowTitle { text: qsTr("Notes") }

            Rectangle {
                Layout.fillWidth: true
                height: 200*dpi
                TextArea {
                    Layout.fillWidth: true
                    id: textAreaNotes
                    anchors.fill: parent
                    text: detobj.notes
                    font.pointSize: app_info.fontsize
                    Component.onCompleted: {
                        if (Qt.platform.os == "winrt" || Qt.platform.os == "winphone"){
                            textAreaNotes.style = winstyle
                        }
                    }
                    Component{
                        id: winstyle
                        Custom_TextArea_WINPHONE{}
                    }
                }
            }
        } // layout
        MouseArea {
            anchors.fill: parent
            z: -1
            onClicked: {
                pageSchoolDetail.forceActiveFocus()
                if (Qt.inputMethod.visible) Qt.inputMethod.hide()
            }
        }
    } // flickable
}
