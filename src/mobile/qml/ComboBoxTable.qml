/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import net.theocbase.mobile 1.0

Item {
    id: comboBoxTable
    property string currentText
    property int currentId
    property var model
    property string col1Name: qsTr("Name", "Column name")
    property string col2Name: qsTr("Date", "Column name")
    property string col3Name: ""

    signal beforeMenuShown
    signal rowSelected(var id)

    Layout.preferredHeight: row.height
    Layout.preferredWidth: width
    width: 100

    RowText {
        id: row
        title: currentText
        arrow: true
        onClicked: {
            beforeMenuShown()
            console.log(currentText)
            console.log(model.rowCount())
            console.log(width / dpi)
            var index = -1
            if (currentText.length > 0) {
                for (var i = 0; i < model.rowCount(); i++) {
                    if (currentId > 0 && model.get(i).id === currentId)
                        index = i
                    if (model.get(i).name === currentText)
                        index = i
                }
            }
            stackView.push(listPage, {selectedRow: index})
        }
    }

    Component {
        id: listPage
        Page {
            property int selectedRow
            onSelectedRowChanged: {
                listView.currentIndex = selectedRow
            }

            header: BaseToolbar {
                title: title
                componentLeft: ToolButton {
                    icon.source: "qrc:/cancel.svg"
                    icon.color: "white"
                    onClicked: { stackView.pop() }
                }
                componentRightMiddle : ToolButton {
                    // Sort Menu
                    icon.source: "qrc:/sort.svg"
                    icon.color: "white"
                    onClicked: menu.open()
                    Menu {
                        id: menu
                        y: parent.height
                        MenuItem {
                            text: col1Name + (listView.model.sortColumn === 1 ?
                                                        listView.model.sortOrder === Qt.AscendingOrder ? " ↑" : " ↓" : "")
                            onTriggered: {
                                var order = Qt.AscendingOrder
                                if(listView.model.sortColumn === 1)
                                    order = listView.model.sortOrder === Qt.AscendingOrder ? Qt.DescendingOrder : Qt.AscendingOrder
                                comboBoxTable.model.sort(1, order)
                            }
                        }
                        MenuItem {
                            text: col2Name + (listView.model.sortColumn === 2 ?
                                                        listView.model.sortOrder === Qt.AscendingOrder ? " ↑" : " ↓" : "")
                            onTriggered: {
                                var order = Qt.AscendingOrder
                                if(listView.model.sortColumn === 2)
                                    order = listView.model.sortOrder === Qt.AscendingOrder ? Qt.DescendingOrder : Qt.AscendingOrder
                                comboBoxTable.model.sort(2, order)
                            }
                        }
                        MenuItem {
                            text: col3Name + (listView.model.sortColumn === 3 ?
                                                        listView.model.sortOrder === Qt.AscendingOrder ? " ↑" : " ↓" : "")
                            visible: col3Name.length > 0
                            height: visible ? implicitHeight : 0
                            onTriggered: {
                                var order = Qt.AscendingOrder
                                if(listView.model.sortColumn === 3)
                                    order = listView.model.sortOrder === Qt.AscendingOrder ? Qt.DescendingOrder : Qt.AscendingOrder
                                comboBoxTable.model.sort(3, order)
                            }
                        }
                    }
                }

                componentRight: ToolButton {
                    icon.source: "qrc:/done.svg"
                    icon.color: "white"
                    onClicked: {
                        var index = listView.currentIndex
                        if (typeof listView.model.get(index).id === "undefined") {
                            currentText = ""
                            currentId = -1
                            rowSelected(-1)
                        } else {
                            currentText = listView.model.get(index).name
                            currentId = listView.model.get(index).id
                            rowSelected(listView.model.get(index).id)
                        }
                        stackView.pop()
                    }
                }
            }

            ListView {
                id: listView
                anchors.fill: parent
                //currentIndex: listView.highlightedIndex
                model: comboBoxTable.model                

                ScrollIndicator.vertical: ScrollIndicator { }
                delegate:  ItemDelegate {
                    width: parent.width
                    GridLayout {
                        anchors.fill: parent
                        anchors.leftMargin: 10*dpi
                        anchors.rightMargin: checkBox.width
                        columns: width >= 500 ? 3 : 2
                        Label {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            elide: Text.ElideRight
                            text: typeof name === "undefined" ? "-" : name
                            verticalAlignment: Text.AlignVCenter
                            Layout.columnSpan: parent.columns === 2 ? 2 : 1
                            function resizeToContents() {}
                        }

                        Label {                            
                            Layout.preferredWidth: 150*dpi
                            Layout.fillHeight: true
                            text: typeof date == "undefined" ? "" : "◆ " + date
                            color: "grey"
                        }
                        Label {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            text: typeof date2 == "undefined" ? "" : "◇ " + date2
                            color: "grey"
                            font.italic: true
                            horizontalAlignment: Text.AlignRight
                        }
                    }
                    CheckBox {
                        id: checkBox
                        anchors.right: parent.right
                        checked: listView.currentIndex == index
                        onClicked: if (checked) listView.currentIndex = index
                    }
                    Rectangle {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom
                        height: app_info.linewidth
                        color: "lightgrey"
                    }
                    onClicked: {
                        listView.currentIndex = index
                        console.log("Row " + index + " clicked")
                    }
                }
            }
        }
    }
}
