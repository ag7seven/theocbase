/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.1
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Page {
    id: assignmentDialog
    property bool canSendMidweekMeetingReminders: accessControl.user && accessControl.user.hasPermission(PermissionRule.CanSendMidweekMeetingReminders)
    property string returnValue: ""
    property LMM_Assignment currentAssignment
    property int oldSpeakerId: -1
    property int oldAssistantId: -1
    property int currentindex: -1
    property int modelLength: 0
    signal gotoNext()
    signal gotoPrevious()    

    AssignmentController { id: myController }
    onCurrentAssignmentChanged: {
        if (!currentAssignment) return
        oldSpeakerId = currentAssignment.speaker ? currentAssignment.speaker.id : -1
        oldAssistantId = currentAssignment.assistant ? currentAssignment.assistant.id : -1
    }
    function saveChanges(){
        if (Qt.inputMethod.visible)
            Qt.inputMethod.hide()

        if (!currentAssignment)
            return

        var s_id = currentAssignment.speaker ? currentAssignment.speaker.id : -1
        var a_id = currentAssignment.assistant ? currentAssignment.assistant.id : -1

        if (currentAssignment.note !== textAreaNotes.text ||
                s_id !== oldSpeakerId || a_id !== oldAssistantId){
            currentAssignment.note = textAreaNotes.text
            currentAssignment.save()
            console.log("assignment saved")
            oldSpeakerId = s_id
            oldAssistantId = a_id
        }
        return true
    }

    header: ToolBar {
        height: 45 * dpi + toolbarTopMargin        
        RowLayout {
            anchors.fill: parent
            anchors.topMargin: toolbarTopMargin
            ToolButton {
                icon.source: "qrc:/back.svg"
                icon.color: "white"
                onClicked: {
                    if (!saveChanges())
                        return
                    stackView.pop()
                }
            }
            Label {
                Layout.fillWidth: true
                text: qsTr("Details", "Page title")
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pointSize: app_info.fontsize
                color: "white"
            }
            ToolButton {
                icon.source: "qrc:/share.svg"
                icon.color: "white"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    shareUtils.setPoint(mapToGlobal(width/2, height))
                    shareUtils.shareText(currentAssignment.getReminderText())
                }
            }
            ToolButton {
                icon.source: "qrc:/assignment_previous.svg"
                icon.color: enabled ? "white" : "grey"
                enabled: currentindex > 0
                onClicked: {
                    if (!saveChanges()) return;
                    gotoPrevious()
                }
            }
            ToolButton {
                icon.source: "qrc:/assignment_next.svg"
                icon.color: enabled ? "white" : "grey"
                enabled: currentindex < (modelLength -1)
                onClicked: {
                    if (!saveChanges()) return;
                    gotoNext()
                }
            }
        }
    }

    ScrollView {
        anchors.fill: parent
        contentItem: flickable
    }

    Flickable {
        id: flickable
        anchors.fill: parent
        contentHeight: layout.height
        flickableDirection: Flickable.VerticalFlick
        ColumnLayout {
            id: layout
            width: assignmentDialog.width
            spacing: 0
            //anchors.margins: 10
            RowTitle {
                text: qsTr("Theme")
            }
            Label {
                Layout.fillWidth: true
                text: currentAssignment ? currentAssignment.theme : ""
                wrapMode: Text.WordWrap
                padding: 10*dpi
                font.pointSize: app_info.fontsize
            }

            RowTitle {
                text: qsTr("Source")
            }
            Label {
                Layout.fillWidth: true
                text: currentAssignment ? currentAssignment.source : ""               
                wrapMode: Text.WordWrap
                padding: 10*dpi
                font.pointSize: app_info.fontsize
            }

            RowText {
                title: currentAssignment &&
                       currentAssignment.talkId === LMM_Schedule.TalkType_CBS ? qsTr("Conductor")
                                                                              : qsTr("Speaker")
                text: currentAssignment ? currentAssignment.speakerFullName : ""
                enabled: currentAssignment &&
                         currentAssignment.talkId != LMM_Schedule.TalkType_COTalk &&
                         currentAssignment.talkId != LMM_Schedule.TalkType_SampleConversationVideo &&
                         currentAssignment.talkId != LMM_Schedule.TalkType_ApplyYourself
                arrow: true
                onClicked: {
                    var mylist = currentAssignment.getSpeakerList()
                    var rowindex = currentAssignment.speaker ?
                                mylist.find(currentAssignment.speaker.id,1) : -1
                    var speakersPage = stackView.push(Qt.resolvedUrl("SelectionListPage.qml"),
                                                      {showName: true, model: mylist, selectedRow: rowindex, pageTitle: title})
                    speakersPage.onActiveRowChanged.connect(function(index){
                        var id = mylist.get(index).id
                        if (typeof id === "undefined"){
                            text = ""
                            currentAssignment.speaker = null
                        }else{
                            text = mylist.get(index).name
                            currentAssignment.speaker = myController.getPublisherById(id)
                        }
                        saveChanges()
                    })
                }
            }
            RowText {
                title: qsTr("Reader")
                text: currentAssignment && currentAssignment.assistant ? currentAssignment.assistant.fullname : ""
                visible: currentAssignment && currentAssignment.talkId === LMM_Schedule.TalkType_CBS
                arrow: true
                onClicked: {
                    var mylist = currentAssignment.getAssistantList()
                    var rowindex = currentAssignment.assistant ?
                                mylist.find(currentAssignment.assistant.id,1) : -1
                    console.log("reader row " + rowindex)
                    var readersPage = stackView.push(Qt.resolvedUrl("SelectionListPage.qml"),
                                                     {showName: true, model: mylist, selectedRow: rowindex, pageTitle: title})
                    readersPage.onActiveRowChanged.connect(function(index){
                        var id = mylist.get(index).id
                        if (typeof id === "undefined"){
                            text = ""
                            currentAssignment.assistant = null
                        }else{
                            currentAssignment.assistant = myController.getPublisherById(id)
                        }
                        saveChanges()
                    })
                }
            }

            RowTitle { text: qsTr("Notes") }

            Rectangle {
                Layout.fillWidth: true
                height: 200*dpi
                TextArea {
                    Layout.fillWidth: true
                    id: textAreaNotes
                    anchors.fill: parent
                    text: currentAssignment ? currentAssignment.note : ""
                    font.pointSize: app_info.fontsize
                    leftPadding: 10*dpi
                    rightPadding: 10*dpi
                    background: Rectangle {
                        implicitWidth: 200
                        implicitHeight: 40
                        color: "transparent"
                    }
                    onEditingFinished: saveChanges()
                }
            }
        }
    }
}
