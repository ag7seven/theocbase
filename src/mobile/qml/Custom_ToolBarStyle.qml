import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

ToolBarStyle {
    property bool topPosition: true
    property string backgroundColor: "#30496F"
    property bool showBorder: true
    padding {
        left: 0
        right: 0
        top: 0
        bottom: 0
    }
    background: Rectangle {
        implicitWidth: 100
        height: 45 * dpi
        //border.width: 2
        color: backgroundColor // "#F7F7F8"
        Rectangle {
            anchors.bottom: topPosition ? parent.bottom :  parent.top
            height: app_info.linewidth
            anchors.left: parent.left
            anchors.right: parent.right
            color: "grey"
            visible: showBorder
        }
    }
}
