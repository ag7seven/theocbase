/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import net.theocbase.mobile 1.0
import "js/moment.js" as Moment

Page {
    id: mainpage

    property date currentdate
    property int selectedTab: 0
    property int tabHeight: 45 * dpi
    property int tabMargin: 0
    property string exceptionText: ""
    property string weekStartingText: ""

    function activateTabByDay() {
        var d = new Date()
        selectedTab = (d.getDay() === 0 || d.getDay() === 6 ? 1 : 0)
    }

    function refreshList(addDays){
        stackView.pop(null)
        if (typeof addDays === 'undefined' || !moment(currentdate).isValid()){
            currentdate = moment().startOf('isoweek').toDate()
        }else{
            currentdate = moment(currentdate).add(addDays,'days').toDate()
        }

        console.log("refresh list " + currentdate.toString())
        lmMeeting.loadSchedule(currentdate)
        publicMeeting.loadSchedule(currentdate)
        outgoingSpeakers.currentDate = currentdate
        congCtrl.clearExceptionCache()
        exceptionText = congCtrl.getExceptionText(currentdate)
        weekStartingText = qsTr("Week starting %1").arg(Qt.formatDate(currentdate, Qt.DefaultLocaleShortDate))

        // meeting days
        var mDay = congCtrl.getMeetingDay(currentdate, 1)
        var wDay = congCtrl.getMeetingDay(currentdate, 3)
        lmMeeting.editpossible = mDay > 0
        publicMeeting.editpossible = wDay > 0
    }

    function changeTab(index) { selectedTab = index }

    Component.onCompleted: {
        console.log("on completed")
        refreshList(0)
    }

    CongregationCtrl { id: congCtrl }

    header: ToolBar {
        id: toolbarbase
        height: 45 * dpi + toolbarTopMargin + exceptionRect.height
        RowLayout {
            id: topRow
            height: 45 * dpi
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: toolbarTopMargin
            anchors.leftMargin: 10*dpi
            anchors.rightMargin: 10*dpi

            ColumnLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.margins: 2*dpi
                Text {
                    text: weekStartingText
                    color: "white"
                    Layout.fillWidth: true
                    font.pointSize: app_info.fontsizeSmall
                    fontSizeMode: Text.Fit
                }
                Text {
                    text: lmMeeting.bibleReadering
                    color: "#b6b6b6"
                    Layout.fillWidth: true
                    font.pointSize: app_info.fontsizeSmall
                    fontSizeMode: Text.Fit
                }
            }
            ToolButton {
                icon.source: "qrc:///chevron_left.svg"
                icon.color: "white"
                onClicked: { refreshList(-7) }
            }
            ToolButton {
                icon.source: "qrc:///today.svg"
                icon.color: "white"
                onClicked: { refreshList() }
            }
            ToolButton {
                icon.source: "qrc:///chevron_right.svg"
                icon.color: "white"
                onClicked: { refreshList(7) }
            }
        }
        Rectangle {
            id: exceptionRect
            anchors.top: topRow.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            visible: exceptionText != ""
            height: exceptionText == "" ? 0 : 30*dpi
            color: "red"
            Text {
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: exceptionText
                color: "white"
                visible: text != ""
            }
        }
        Item {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: exceptionRect.bottom
            height: tabHeight + tabMargin + 10*dpi
            z: -1
            clip: true
            TabBar {
                id: tabBar
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10*dpi // extra margin to show shadow
                currentIndex: selectedTab
                Material.background: "#254f8e"
                Material.foreground: "#b6b6b6"
                Material.accent: "white"
                Material.elevation: 4
                TabButton {
                    text: "●●○ " + qsTr("Midweek", "Midweek Meeting")
                    height: parent.height
                    onClicked: changeTab(0)
                    opacity: tabMargin > -tabHeight / 2 ? 1 : 0
                }
                TabButton {
                    text: "○○● " + qsTr("Weekend", "Weekend Meeting")
                    height: parent.height
                    onClicked: changeTab(1)
                    opacity: tabMargin > -tabHeight / 2 ? 1 : 0
                }
            }
        }
    }

    Rectangle {
        id: blueRect
        anchors.fill: parent
        anchors.topMargin: tabHeight
        implicitHeight: 600
        implicitWidth: 400

        color: currentdate.getFullYear() >= 2016 ? "white" : "#EFEFEF"

        Flickable {
            id: flickable1
            anchors.fill: parent
            contentHeight: layout.height
            flickableDirection: Flickable.VerticalFlick

            property double mstart: 0

            onMovementStarted: mstart = contentY
            onMovementEnded: {
                var v = mstart - contentY
                if (v < 0){
                    if (-v+10*dpi >= tabHeight) {
                        tabMargin = -tabHeight
                    } else if(tabMargin < 0) {
                        tabMargin = 0
                    }
                }else{
                    // show
                    tabMargin = 0
                }
                mstart = 0
            }
            onContentYChanged: {
                var v = mstart - contentY
                if (v < 0){
                    // hide
                    tabMargin = (v >= -tabHeight ? v : -tabHeight)
                }else{
                    // show
                    var currentValue = tabMargin
                    var newValue = -tabHeight+v // -tabBar.height+v
                    tabMargin = (v < tabHeight ? newValue > currentValue ? newValue : currentValue : 0)
                }
            }

            // show pull to refresh
            ListHeader {
                id: header
                flickable: flickable1
                y: -height
            }
            onDragEnded: if (header.refresh) { console.log("Refresh"); syncpage.runSync() }

            ColumnLayout {
                id: layout
                spacing: 0
                width: parent.width

                LMMSchedule_Mobile {
                    id: lmMeeting
                    visible: currentdate.getFullYear() >= 2016 && selectedTab == 0 && canViewMidweekMeetingSchedule
                }

                PublicMeetingSchedule_Mobile {
                    id: publicMeeting
                    Layout.fillWidth: true
                    visible: selectedTab == 1 && canViewWeekendMeetingSchedule
                }

                OutgoingSpeakers {
                    id: outgoingSpeakers
                    Layout.fillWidth: true
                    Layout.topMargin: 10*dpi
                    visible: selectedTab == 1 && canViewWeekendMeetingSchedule
                }
            }
        }
    }
}
