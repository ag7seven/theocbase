/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
    property alias navigation: stackViewRoot
    property alias detail: stackViewRoot
    property bool useSplitView: width > dpi*600
    property Item currentDetailitem

    function pop(args) {
        var item = arguments[0]
        var operation = arguments[1]
        if (stackViewDetail.depth == 1)
            stackViewDetail.clear(StackView.PopTransition)
        else
            return stackViewDetail.pop(item, operation)
    }
    function clearAndPush(args) {
        var item = arguments[0]
        var properties = arguments[1]
        var operation = arguments[2]
        stackViewDetail.clear(StackView.Immediate)
        return push(item, properties, operation)
    }

    function push(args) {
        var item = arguments[0]
        var properties = arguments[1]
        var operation = arguments[2]
        if (stackViewDetail.depth == 0 && operation === undefined)
            operation = StackView.PushTransition        
        return stackViewDetail.push(item, properties, operation)
    }

    function replace(args) {
        var target = arguments[0]
        var item = arguments[1]
        var properties = arguments[2]
        var operation = arguments[3]
        return stackViewDetail.replace(target, item, properties, operation)
    }
    onWidthChanged: stackViewDetail.calculateSize()

    StackView {
        id: stackViewRoot
        anchors.fill: parent
        anchors.rightMargin: useSplitView ? stackViewDetail.width : 0
        anchors.bottomMargin: 50*dpi
        clip: true
        onCurrentItemChanged: {
            stackViewDetail.clear(StackView.Immediate)
            stackViewDetail.sidebarWidth = 0
        }
    }

    StackView {
        id: stackViewDetail
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.bottomMargin: stackView.useSplitView || stackView.currentDetailitem == undefined ?  50*dpi : 0
        anchors.right: parent.right
        width: useSplitView ? sidebarWidth : parent.width
        property int sidebarWidth: 0
        onCurrentItemChanged: {            
            calculateSize()
            currentDetailitem = currentItem
        }
        clip: true
        pushEnter: Transition {
            XAnimator {
                from: stackViewDetail.width
                to: 0
                duration: 500
                easing.type: Easing.OutCubic
            }
        }
        pushExit: Transition {
            XAnimator {
                duration: 500
                easing.type: Easing.OutCubic
            }
        }

        popEnter: Transition {
            XAnimator {
                duration: 500
                easing.type: Easing.OutCubic
            }
        }
        popExit: Transition {
            XAnimator {
                from: 0
                to: stackViewDetail.width
                duration: 500
                easing.type: Easing.OutCubic
            }
        }
        function calculateSize() {
            sidebarWidth = depth == 0 ? 0 : parent.width / 2
        }
    }
    Rectangle {
        color: "lightgrey"
        anchors.left: stackViewDetail.left
        anchors.top: stackViewDetail.top
        anchors.bottom: stackViewDetail.bottom
        width: app_info.linewidth
        visible: useSplitView && detail.depth > 0
    }
}
