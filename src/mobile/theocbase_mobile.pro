TEMPLATE = app
QT += qml quick widgets sql network svg networkauth
TARGET = TheocBase
CONFIG += c++11
CONFIG -= bitcode

VERSION = 2019.09.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

# C++ files
include(src/src.pri)

# Default rules for deployment.
include(deployment.pri)

SOURCES +=
HEADERS +=

RESOURCES += \
    qml/qml.qrc \
    ../database.qrc \
    icons/icons.qrc \
    translations/translations_mobile.qrc \
    ../translations/translations.qrc \
    ../fonts/fonts.qrc

# var, prepend, append
defineReplace(prependAll) {
    for(a,$$1):result += $$2$${a}$$3
    return($$result)
}

lupdate_only {
    TR_EXCLUDE += $$PWD/../accesscontrol.*
    SOURCES = qml/*.qml \
        src/school_detail.cpp
}

# Supported languages
LANGUAGES = af bg cs da de en es et el fi fr gcf gn hu hr ht hy it ka lt my ne nl no pl pt pt_BR ro ru sk sl sr sv th uk zh

# Available translations
TRANSLATIONS = $$prependAll(LANGUAGES, $$PWD/translations/theocbase_mobile_, .ts)

# Additional import path used to resolve QML modules in Qt Creator's code model
# QML_IMPORT_PATH =

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/android/openssl/arm-1.0.2f/libcrypto.so \
        $$PWD/android/openssl/arm-1.0.2f/libssl.so
}

contains(ANDROID_TARGET_ARCH,armeabi) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/android/openssl/arm-1.0.2f/libcrypto.so \
        $$PWD/android/openssl/arm-1.0.2f/libssl.so
}

contains(ANDROID_TARGET_ARCH, arm64-v8a) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/android/openssl/arm64-1.0.2r/libcrypto.so \
        $$PWD/android/openssl/arm64-1.0.2r/libssl.so
}

contains(ANDROID_TARGET_ARCH,x86) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/android/openssl/x86/libcrypto.so \
        $$PWD/android/openssl/x86/libssl.so
}
