#include "helpviewer.h"
#include <QByteArray>
#include <QDir>
#include <QLibraryInfo>
#include <QMessageBox>
#include <QProcess>

HelpViewer::HelpViewer()
    : langCode("en"), proc(0)
{
    sql = &Singleton<sql_class>::Instance();
}

HelpViewer::~HelpViewer()
{
    if (proc && proc->state() == QProcess::Running) {
        proc->terminate();
        proc->waitForFinished(3000);
    }
    delete proc;
}

void HelpViewer::showHelp(const QString &page)
{
    if (!startHelpViewer())
        return;

    QByteArray ba("SetSource ");
    ba.append("qthelp://net.theocbase/doc/" + langCode + "/");

    proc->write(ba + page.toLocal8Bit() + '\n');
}

bool HelpViewer::startHelpViewer()
{
    if (!proc)
        proc = new QProcess();

    if (proc->state() != QProcess::Running) {
#if defined(QT_DEBUG)
        QString app = QLibraryInfo::location(QLibraryInfo::BinariesPath) + QDir::separator();
#else
        QString app = theocbaseDirPath + QDir::separator();
#endif
#if defined(Q_OS_LINUX)
        app = QLibraryInfo::location(QLibraryInfo::BinariesPath) + QDir::separator() + QLatin1String("assistant");
#elif defined(Q_OS_WIN)
        app += QLatin1String("assistant.exe");
#elif defined(Q_OS_MAC)
        app += QLatin1String("Assistant.app/Contents/MacOS/Assistant");
        if (!QFile::exists(app))
            app = theocbaseDirPath + "/../Resources/Assistant.app/Contents/MacOS/Assistant";
#endif
        qDebug() << "Qt Assistant path: " << app;

        QStringList args;

        // check for native language
        langCode = sql->getSetting("theocbase_language");
        QString dir = theocbaseDirPath + QDir::separator() + QString("docs") + QDir::separator();
#if defined(Q_OS_MAC)
        dir = theocbaseDirPath + "/../Resources/docs/";
#endif
        QString filename = dir + "theocbase_" + langCode + ".qhc";

        if (!QFile::exists(filename))
        {
            langCode = "en";
            filename = dir + "theocbase_en.qhc";
        }
        qDebug() << "Help-file path: " << filename;

        args << QLatin1String("-collectionFile")
             << filename
             << QLatin1String("-enableRemoteControl");

        proc->start(app, args);

        if (!proc->waitForStarted()) {
            QMessageBox::critical(nullptr,
                                  tr("TheocBase Help"),
                                  tr("Unable to launch the help viewer (%1)").arg(app));
            return false;
        }
    }
    return true;
}
