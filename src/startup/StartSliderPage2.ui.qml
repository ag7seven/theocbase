import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 480
    height: 640

    header: Label {
        rightPadding: 56
        leftPadding: 56
        text: "Page 2"
        topPadding: 66
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Label {
        text: "theocbase is a nice tool"
        rightPadding: 56
        leftPadding: 56        
    }
}
