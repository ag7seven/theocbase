/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHOOLUI_H
#define SCHOOLUI_H

#include <QtWidgets>
#include "school.h"
#include "ccongregation.h"
#include <QTimer>

class schoolComboBox;

/**
 * @brief The schoolui class - School User Interface Class
 *                             This class is used to show school summary and edit pages.
 */
class schoolui : public QObject
{
    Q_OBJECT
public:
    schoolui(QDate date, QObject *parent = 0);
    ~schoolui();

    /**
     * @brief setDate - Set current date
     * @param date - The first day of week
     */
    void setDate(QDate date);

    /**
     * @brief getDate - Get current date
     * @return - The first day of current week
     */
    QDate getDate();

    /**
     * @brief getSchoolEditTable - To show school edit page
     * @return - Edit page in QTableWidget control
     */
    QTableWidget *getSchoolEditTable();

    /**
     * @brief getSchoolTable - To show summary page of one week school program
     * @return - Summary page in QTableWidget control
     */
    QTableWidget *getSchoolTable();

public slots:

private slots:
    void beforeStudentListShown(QString name, schoolComboBox *object);
    void schoolComboBoxSelected(int index);
    void schoolResultButtonClicked();
    void showContextMenu(const QPoint& pos);
    void noSchoolClicked();

private:
    school s_class;
    QTableWidget *edittable;
    QTableWidget *table;
    std::vector<std::unique_ptr<school_item> > schoolprogram;
    QDate m_firstdayofweek;
    sql_class *sql;
    ccongregation c;

    QWidget *comboboxLayout(schoolComboBox *combo, QToolButton *button, schoolComboBox *combo2);
    static QTableWidgetItem *newTableWidget(const QString &text);

signals:
    void contextMenuShowStudent(QString name);
};

class schoolComboBox : public QComboBox
{
    Q_OBJECT
public:

    schoolComboBox(QWidget* parent = 0) : QComboBox(parent)
    {
    }

    virtual void showPopup()
    {
        QString name = this->objectName();
        emit beforeShowPopup(name,this);
        close_allowed = false;
        QTimer* t = new QTimer();
        t->setSingleShot(true);
        QObject::connect(t,&QTimer::timeout, [=](){
            close_allowed = true;
            t->deleteLater();
        });
        t->start(500);
        QComboBox::showPopup();
    }
    virtual void hidePopup()
    {
        if (!close_allowed) return;
        QComboBox::hidePopup();
    }
    virtual void setCurrentText(QString value)
    {
        if(this->findText(value,Qt::MatchExactly) < 0){
            if(value == ""){
                this->setCurrentIndex(-1);
            }else{
                this->addItem(value);
            }
        }else{
            this->setCurrentIndex(this->findText(value,Qt::MatchExactly));
        }
    }

private:
    bool close_allowed;

signals:
    void beforeShowPopup(QString objectname, schoolComboBox *object);
};

#endif // SCHOOLUI_H
