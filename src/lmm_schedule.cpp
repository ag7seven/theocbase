#include "lmm_schedule.h"

LMM_Schedule::LMM_Schedule(QObject *parent) : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    m_schedule_db_id = -1;
}

LMM_Schedule::LMM_Schedule(int talkId, int sequence, QDate date, QString theme, QString source, int study_number, int time, int dbId, QObject *parent) : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    m_schedule_db_id = dbId;
    m_talkid = talkId;
    m_sequence = sequence;
    m_date = date;
    m_theme = theme;
    m_source = source;
    m_study_number = study_number;
    if (time > 0) {
        m_time = time;
    } else {
        switch (m_talkid) {
        case TalkType_Treasures: m_time = 10; break;
        case TalkType_Digging: m_time = 8; break;
        case TalkType_BibleReading: m_time = 4; break;
        case TalkType_SampleConversationVideo: m_time = m_date.day() < 8 ? 4 : 5; break;
        case TalkType_InitialCall: m_time = 2; break;
        case TalkType_ReturnVisit1:
        case TalkType_ReturnVisit2:
        case TalkType_ReturnVisit3: m_time = 3; break;
        case TalkType_BibleStudy:
        case TalkType_StudentTalk: m_time = 6; break;
        case TalkType_CBS: m_time = 30; break;
        case TalkType_COTalk: m_time = 30; break;
        default: m_time = 10; break;
        }
    }
}

int LMM_Schedule::talkId() const
{
    return m_talkid;
}

int LMM_Schedule::dbTalkId() const
{
    return m_talkid * 10 + m_sequence;
}

int LMM_Schedule::sequence() const
{
    return m_sequence;
}

int LMM_Schedule::scheduleId() const
{
    return m_schedule_db_id;
}

int LMM_Schedule::dbTalkId(int talkId, int sequence)
{
    return talkId * 10 + sequence;
}

void LMM_Schedule::splitDbTalkId(int dbTalkId, int &talkId, int &sequence)
{
    talkId = dbTalkId / 10;
    sequence = dbTalkId % 10;
}

QString LMM_Schedule::getStringTalkType(int src) {
    switch(src) {
    case TalkType_Chairman: return "CMN";
    case TalkType_Treasures: return "TRS";
    case TalkType_Digging: return "DIG";
    case TalkType_BibleReading: return "BiR";
    case TalkType_SampleConversationVideo: return "SCV";
    case TalkType_ApplyYourself: return "AYR";
    case TalkType_InitialCall: return "ICL";
    case TalkType_ReturnVisit1: return "RV1";
    case TalkType_ReturnVisit2: return "RV2";
    case TalkType_ReturnVisit3: return "RV3";
    case TalkType_BibleStudy: return "BiS";
    case TalkType_StudentTalk: return "ST";
    case TalkType_LivingTalk1: return "LT1";
    case TalkType_LivingTalk2: return "LT2";
    case TalkType_LivingTalk3: return "LT3";
    case TalkType_CBS: return "CBS";
    case TalkType_COTalk: return "COT";
    }
    return "";
}

QString LMM_Schedule::getFullStringTalkType(int src)
{
    switch(src) {
    case TalkType_Chairman: return tr("Chairman", "talk title");
    case TalkType_Treasures: return tr("Treasures From God’s Word", "talk title");
    case TalkType_Digging: return tr("Digging for Spiritual Gems", "talk title");
    case TalkType_BibleReading: return tr("Bible Reading", "talk title");
    case TalkType_SampleConversationVideo: return tr("Sample Conversation Video", "talk title");
    case TalkType_ApplyYourself: return tr("Apply Yourself to Reading and Teaching");
    case TalkType_InitialCall: return tr("Initial Call", "talk title");
    case TalkType_ReturnVisit1: return tr("First Return Visit", "talk title");
    case TalkType_ReturnVisit2: return tr("Second Return Visit", "talk title");
    case TalkType_ReturnVisit3: return tr("Third Return Visit", "talk title");
    case TalkType_BibleStudy: return tr("Bible Study", "talk title");
    case TalkType_StudentTalk: return tr("Talk", "talk title");
    case TalkType_LivingTalk1: return tr("Living as Christians Talk 1", "talk title");
    case TalkType_LivingTalk2: return tr("Living as Christians Talk 2", "talk title");
    case TalkType_LivingTalk3: return tr("Living as Christians Talk 3", "talk title");
    case TalkType_CBS: return tr("Congregation Bible Study", "talk title");
    case TalkType_COTalk: return tr("Circuit Overseer's Talk", "talk title");
    }
    return "";
}

int LMM_Schedule::getTalkTypeFromFullString(QString name)
{
    static QHash<QString, int> talkNameToNumber;
    if (talkNameToNumber.count() == 0) {
        talkNameToNumber.insert(tr("Chairman", "talk title"), TalkType_Chairman);
        talkNameToNumber.insert(tr("Treasures From God’s Word", "talk title"), TalkType_Treasures);
        talkNameToNumber.insert(tr("Digging for Spiritual Gems", "talk title"), TalkType_Digging);
        talkNameToNumber.insert(tr("Bible Reading", "talk title"), TalkType_BibleReading);
        talkNameToNumber.insert(tr("Sample Conversation Video", "talk title"), TalkType_SampleConversationVideo);
        talkNameToNumber.insert(tr("Apply Yourself to Reading and Teaching", "talk title"), TalkType_ApplyYourself);
        talkNameToNumber.insert(tr("Initial Call", "talk title"), TalkType_InitialCall);
        talkNameToNumber.insert(tr("First Return Visit", "talk title"), TalkType_ReturnVisit1);
        talkNameToNumber.insert(tr("Second Return Visit", "talk title"), TalkType_ReturnVisit2);
        talkNameToNumber.insert(tr("Third Return Visit", "talk title"), TalkType_ReturnVisit3);
        talkNameToNumber.insert(tr("Bible Study", "talk title"), TalkType_BibleStudy);
        talkNameToNumber.insert(tr("Talk", "talk title"), TalkType_StudentTalk);
        talkNameToNumber.insert(tr("Living as Christians Talk 1", "talk title"), TalkType_LivingTalk1);
        talkNameToNumber.insert(tr("Living as Christians Talk 2", "talk title"), TalkType_LivingTalk2);
        talkNameToNumber.insert(tr("Living as Christians Talk 3", "talk title"), TalkType_LivingTalk3);
        talkNameToNumber.insert(tr("Congregation Bible Study", "talk title"), TalkType_CBS);
        talkNameToNumber.insert(tr("Circuit Overseer's Talk", "talk title"), TalkType_COTalk);
    }
    if (talkNameToNumber.contains(name))
        return talkNameToNumber[name];
    else
        return -1;
}

QList<int> LMM_Schedule::getExpectedTalks(QDate d) {
    QList<int> expected;
    expected.append(TalkType_Treasures * 10);
    expected.append(TalkType_Digging * 10);
    expected.append(TalkType_BibleReading * 10);
    if (d.year() < 2019)
    {
        if (d.day() < 8) {
            expected.append(TalkType_SampleConversationVideo * 10);
            expected.append(TalkType_ReturnVisit1 * 10);
            expected.append(TalkType_BibleStudy * 10);
        } else if (d.day() < 15) {
            expected.append(TalkType_InitialCall * 10);
            expected.append(TalkType_SampleConversationVideo * 10);
            expected.append(TalkType_StudentTalk * 10);
        } else if (d.day() < 22) {
            expected.append(TalkType_InitialCall * 10);
            expected.append(TalkType_ReturnVisit1 * 10);
            expected.append(TalkType_SampleConversationVideo * 10);
        } else {
            expected.append(TalkType_ReturnVisit2 * 10);
            expected.append(TalkType_ReturnVisit3 * 10);
            expected.append(TalkType_BibleStudy * 10);
        }
    } else {
        if (d.day() < 8) {
            expected.append(TalkType_ApplyYourself * 10);
            expected.append(TalkType_StudentTalk * 10);
        } else if (d.day() < 15) {
            expected.append(TalkType_SampleConversationVideo * 10);
            expected.append(TalkType_InitialCall * 10 + 0);
            expected.append(TalkType_InitialCall * 10 + 1);
            expected.append(TalkType_InitialCall * 10 + 2);
        } else if (d.day() < 22) {
            expected.append(TalkType_SampleConversationVideo * 10);
            expected.append(TalkType_ReturnVisit1 * 10 + 0);
            expected.append(TalkType_ReturnVisit1 * 10 + 1);
        } else {
            expected.append(TalkType_SampleConversationVideo * 10);
            expected.append(TalkType_ReturnVisit2 * 10);
            expected.append(TalkType_BibleStudy * 10);
        }

    }
    expected.append(TalkType_LivingTalk1 * 10);
    expected.append(TalkType_LivingTalk2 * 10);
    expected.append(TalkType_LivingTalk3 * 10);
    expected.append(TalkType_CBS * 10);

    return expected;
}

bool LMM_Schedule::loadSchedule(const QDate date, const int dbTalkId)
{
    m_date = date;
    LMM_Schedule::splitDbTalkId(dbTalkId, m_talkid, m_sequence);
    if (!date.isValid()) return false;

    sql_item queryitem;
    queryitem.insert(":date",date);
    queryitem.insert(":talk_id",dbTalkId);
    sql_items items = sql->selectSql("SELECT * FROM lmm_schedule WHERE date = :date AND talk_id = :talk_id AND active",
                                     &queryitem);
    if (items.size() > 0){
        m_schedule_db_id = items[0].value("id").toInt();
        setDate(date);
        setSource(items[0].value("source").toString());
        setTheme(items[0].value("theme").toString());
        setTime(items[0].value("time").toInt());
        return true;
    }else{
        return false;
    }
}

QDate LMM_Schedule::date() const
{
    return m_date;
}

void LMM_Schedule::setDate(QDate date)
{
    m_date = date;
    emit dateChanged(date);
}

int LMM_Schedule::time() const
{
    return m_time;
}

void LMM_Schedule::setTime(int time)
{
    m_time = time;
    emit timeChanged(time);
}

QString LMM_Schedule::theme() const
{
    return m_theme;
}

void LMM_Schedule::setTheme(QString theme)
{
    m_theme = theme;
    emit themeChanged(theme);
}

QString LMM_Schedule::source() const
{
    return m_source;
}

void LMM_Schedule::setSource(QString source)
{
    m_source = source;
    emit sourceChanged(source);
}

int LMM_Schedule::study_number() const
{
    return m_study_number;
}

void LMM_Schedule::setStudy_number(int study_number)
{
    m_study_number = study_number;
}

QString LMM_Schedule::study_name() const
{
    if (study_number() < 1)
        return "";
   return sql->selectScalar("select study_name from lmm_studies where study_number = ? and active", study_number(), "").toString();
}

bool LMM_Schedule::canMultiSchool() const
{
    return LMM_Schedule::canMultiSchool(m_talkid);
}

bool LMM_Schedule::canMultiSchool(int talkId)
{
    return
            (talkId > TalkType_Digging &&
            talkId < TalkType_LivingTalk1) ||
            talkId == TalkType_ApplyYourself;
}

bool LMM_Schedule::canCounsel() const
{
    return
            m_talkid > TalkType_Digging &&
            m_talkid < TalkType_LivingTalk1 &&
            m_talkid != TalkType_SampleConversationVideo;
}

bool LMM_Schedule::canHaveAssistant() const
{    
    return
            m_talkid > TalkType_SampleConversationVideo &&
            m_talkid < TalkType_StudentTalk;
}

QString LMM_Schedule::talkName() const
{
    return getFullStringTalkType(m_talkid);
}

bool LMM_Schedule::save()
{
    if (!date().isValid()) return false;
    if (talkId() < 1) return false;

    sql_item queryitems;
    queryitems.insert(":date",date());
    queryitems.insert(":talk_id",dbTalkId());
    m_schedule_db_id = sql->selectScalar("SELECT id FROM lmm_schedule WHERE date = :date AND talk_id = :talk_id and active", &queryitems, -1).toInt();

    sql_item insertitems;
    insertitems.insert("date",date());
    insertitems.insert("talk_id",dbTalkId());
    insertitems.insert("source",source());
    insertitems.insert("theme",theme());
    insertitems.insert("study_number",study_number());
    insertitems.insert("time",time());
    insertitems.insert("active",1);

    bool returnVal(false);
    if (m_schedule_db_id > 0){
        // update
        returnVal = sql->updateSql("lmm_schedule","id",QString::number(m_schedule_db_id),&insertitems);
    }else{
        // insert new row
        m_schedule_db_id = sql->insertSql("lmm_schedule",&insertitems,"id");
        returnVal = m_schedule_db_id > 0;
    }
    return returnVal;
}

void LMM_Schedule::RemoveDuplicates()
{
    sql_class *s(&Singleton<sql_class>::Instance());
    s->execSql("UPDATE lmm_schedule SET active=1,time_stamp=strftime('%s','now') WHERE id IN (SELECT id FROM lmm_schedule s inner join (SELECT date, theme, source FROM lmm_schedule GROUP BY date, theme, source HAVING count(theme) > 1) dups on s.date = dups.date and s.theme = dups.theme)");
    s->execSql("UPDATE lmm_schedule SET active=0,time_stamp=strftime('%s','now') WHERE id IN (SELECT id FROM lmm_schedule s inner join (SELECT date, theme, source, max(talk_id) keeptalkid FROM lmm_schedule GROUP BY date, theme, source HAVING count(theme) > 1) dups on s.date = dups.date and s.theme = dups.theme and s.talk_id != keeptalkid)");
    s->execSql("update lmm_assignment set lmm_schedule_id = ifnull( (select newid.id from lmm_schedule inactive inner join lmm_schedule newid on inactive.date = newid.date and inactive.theme = newid.theme and newid.active where lmm_assignment.lmm_schedule_id = inactive.id and inactive.active = 0 and lmm_assignment.id not in (select a.id from lmm_assignment a inner join lmm_schedule activesch on a.lmm_schedule_id = activesch.id and activesch.active) ), lmm_schedule_id), time_stamp=strftime('%s','now') ");
    s->execSql("delete from lmm_assignment where lmm_schedule_id in (select s.id from lmm_meeting m inner join lmm_schedule s on m.date = s.date inner join lmm_assignment a on s.id = a.lmm_schedule_id where s.date >= '2018-01-01' and s.talk_id = 5 and a.assignee_id <> m.chairman and a.assignee_id <> m.counselor2 and a.assignee_id <> m.counselor3)");
    s->execSql("delete from lmm_assignment where id in (select a.id from (select date, lmm_schedule_id, classnumber, max(id) keepid from lmm_assignment group by date, lmm_schedule_id, classnumber having count(*) >1) dups inner join lmm_assignment a on dups.date = a.date and dups.lmm_schedule_id = a.lmm_schedule_id and dups.classnumber = a.classnumber and dups.keepid <> a.id)");
}
