/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "importwintm.h"

importwintm::importwintm(QString filename)
{
    QFileInfo info(filename);
    m_directory = info.dir();
    thisweek = QDate::currentDate();
    sql = &Singleton<sql_class>::Instance();
}

void importwintm::Go()
{
    QFileInfoList files = m_directory.entryInfoList(QStringList("current.sd6"));
    // looping over one file is overkill.  But, we may need to import more of this type later.
    for(int i = 0; i < files.size(); i++)
        importpeople(files.at(i).absoluteFilePath());

    files = m_directory.entryInfoList(QStringList("hist6.dat"));
    // looping over one file is overkill.  But, we may need to import more of this type later.
    for(int i = 0; i < files.size(); i++)
        importhistory(files.at(i).absoluteFilePath());

    QMessageBox::information(0, QObject::tr("Import") + " WINTM", QObject::tr("Import Complete"));
}

void importwintm::importpeople(QString filename)
{
    oldIdToNewID.clear();
    sql_item firstandlast;
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    file.seek(0x80);
    data = file.readAll();
    for(pos = 0; pos < data.length();) {
        int startpos = pos;
        char temp;
        person p;
        int oldID = 0;
        int usefor = 0;

        p.setFirstname(readstring(17));
        p.setLastname(readstring(17));

        oldID = readint4(true);
        //p.setInfo("WINTMID: " + QString::number(oldID)); << not really needed
        pos+=4;

        pos++; // don't need anything from the first bit field
        pos++; // unused

        temp = readchar();
        p.setServant((temp & 2) > 0);
        if (p.servant()) usefor |= person::Highlights;
        if ((temp & 4) > 0) usefor |= person::No1;
        if ((temp & 8) > 0) usefor |= person::No2;
        if ((temp & 16) > 0) usefor |= person::No3;
        if ((temp & 32) > 0) usefor |= person::Assistant;

        temp = readchar();
        if ((temp & 1) > 0) usefor |= person::WtReader | person::CBSReader;
        if ((temp & 4) > 0) p.setGender(person::Female);
        if ((temp & 8) > 0) p.setGender(person::Male);

        pos+=26; // unused

        bool frontonly = false, backonly = false;
        temp = readchar(); if (temp == 0x0A) frontonly = true; else if (temp == 0x14) backonly = true;
        pos++;
        temp = readchar(); if (temp == 0x0A) frontonly = true; else if (temp == 0x14) backonly = true;
        pos++;
        temp = readchar(); if (temp == 0x0A) frontonly = true; else if (temp == 0x14) backonly = true;
        pos++;
        temp = readchar(); if (temp == 0x0A) frontonly = true; else if (temp == 0x14) backonly = true;
        pos++;

        if (backonly)
            usefor |= person::SchoolAux;
        else if (frontonly)
            usefor |= person::SchoolMain;

        p.setUsefor(usefor);
        p.setCongregationid(sql->getSetting("congregation_id", "1").toInt());

        firstandlast.insert(":firstname", p.firstname());
        firstandlast.insert(":lastname", p.lastname());

        QVariant newID = sql->selectScalar("select id from persons where firstname = :firstname and lastname = :lastname", &firstandlast);
        if (newID.isNull()) {
            p.update();
            oldIdToNewID.insert(oldID, p.id());
        } else {
            oldIdToNewID.insert(oldID, newID.toInt());
        }
        pos = startpos + 0x80;
    }

}

void importwintm::importhistory(QString filename)
{
    int lastschoolid = sql->selectScalar("select max(id) from school", NULL).toInt();
    QDate refDate(2002,12,30);
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    data = file.readAll();
    for(pos = 0; pos < data.length();) {
        int startpos = pos;
        int weeksPastRefDate = readint4(true);
        QDate schoolWeek = refDate.addDays(7 * weeksPastRefDate);
        qDebug() << "import school week: " << schoolWeek;
        pos += 22;
        if (getScheduleID(schoolWeek, 0) > 0) {
            sql_item param;
            param.insert(":lastschoolid", lastschoolid);
            param.insert(":date", schoolWeek);
            sql->removeSql("school", "id <= :lastschoolid and date = :date", &param);

            int bhID = readint4(true);
            qDebug() << "  bhID: " << bhID;
            pos += 20;
            saveschool_item(schoolWeek, 0, 1, bhID, 0);
            importhistory_oneclass(schoolWeek, 1);
            importhistory_oneclass(schoolWeek, 2);
            importhistory_oneclass(schoolWeek, 3);
        } else {
            qDebug() << "  not found in db";
        }
        pos = startpos + 0xB0;
    }
}

void importwintm::importhistory_oneclass(QDate date, int classnumber) 
{
    int n1 = readint4(true);
    pos += 20;
    int n2 = readint4(true);
    pos += 20;
    int n3 = readint4(true);
    pos += 20;
    int n2a = readint4(true);
    pos += 20;
    int n3a = readint4(true);
    pos -= 92;

    saveschool_item(date, 1, classnumber, n1, 0);
    saveschool_item(date, 2, classnumber, n2, n2a);
    saveschool_item(date, 3, classnumber, n3, n3a);
}

void importwintm::saveschool_item(QDate date, int number, int classnumber, int studentid, int assistantid)
{
    if (oldIdToNewID.contains(studentid)) {
        person *student = new person(oldIdToNewID.value(studentid));
        person *assistant = 0;
        if (assistantid > 0)
            assistant = new person(oldIdToNewID.value(assistantid));
        school_item *s = new school_item();
        int scheduleID = getScheduleID(date, number);
        if (scheduleID > 0) {
            s->setScheduleId(scheduleID);
            s->setDate(date);
            s->setClassNumber(classnumber);
            s->setStudent(student);
            s->setAssistant(assistant);
            //s->setDone(date < thisweek);  << if we were importing counsel history also, we could do something like this
            s->setDone(false);
            s->save();
        }
        delete student;
        if (assistant)
            delete assistant;
    }
}


int importwintm::getScheduleID(QDate date, int number)
{
    QHash<int, int> numbers;
    if (!cachedSchedule.contains(date)) {
        sql_items schedule = sql->selectSql("school_schedule","date", date.toString(Qt::ISODate),"number");
        numbers.clear();
        for (unsigned int i = 0; i<schedule.size(); i++) {
            qDebug() << "    s to int: " << schedule[i].value("id").toInt();
            numbers.insert(schedule[i].value("number").toInt(), schedule[i].value("id").toInt());
        }
        cachedSchedule.insert(date, numbers);
    } else
        numbers = cachedSchedule.value(date);
    if (numbers.contains(number)) {
        qDebug() << " Numbers count:";
        qDebug() << numbers.count();
        return numbers.value(number);
    }
    return -1;
}

QString importwintm::readstring(int maxlen)
{
    char *str = new char[maxlen + 1];
    char c;
    for(int pos2 = 0; pos2 <= maxlen; pos2++) {
        if (pos2 == maxlen)
            c = 0;
        else
            c = data.at(pos + pos2);
        str[pos2] = c;
        if (c == 0)
            break;
    }
    QString qstring = QString::fromLatin1(str);
    delete[] str;
    pos+=maxlen;
    return qstring;
}

uint importwintm::readint4(bool LittleEndian)
{
    uint value = 0;
    char c;
    uint shift = LittleEndian ? 0 : 24;
    uint eachshift = LittleEndian ? 8 : -8;
    for(int pos2 = 0; pos2 < 4; pos2++) {
        c = data.at(pos + pos2);
        value += c << shift;
        shift += eachshift;
    }
    pos += 4;
    return value;
}

char importwintm::readchar()
{
    return data.at(pos++);
}
