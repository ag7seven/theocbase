<!DOCTYPE html SYSTEM>
<html>
<head>
<meta charset="utf-8" />
<title>Territories - View and Edit</title>
<link rel="stylesheet" type="text/css" href="../documentation.css">
</link></head>
<body>
<h1>Territories</h1>
<h2><a id="getting_started"></a>Getting Started</h2>
<p>Territories are displayed by city, publisher, type or worked date [2] in the territory list box [1]. You can assign territories, display their boundaries and <a href="#find_territory_by_address">find territories by address</a>.</p>
<p><img src="../images/territory_manager.png" alt="Territory Manager" width="365" height="252" style="margin: 10px; float: right;" /></p>
<p>Take the following steps to start managing your territories:</p>
<ul>
<li>In the <a href="settings_territories.html">settings</a> add the cities that belong to your congregation's territory and the types of territories and addresses you want to use.</li>
<li><a href="#add_territory">Add territories</a> by clicking the&nbsp;<img src="../images/territory_add_24x24.png" alt="" width="24" height="24" /> button [3]. You can <a href="#draw_territory_boundary">draw the boundaries</a> on the map and <a href="#split_territory">split territories</a>. It is also possible to <a href="#import_territory_boundaries">import territories</a> through a KML-file and <a href="territories_addresses.html#import_territory_addresses">import addresses</a> through a CSV-file [5].</li>
<li>Edit the <a href="#edit_territory_attributes">territory attributes</a> of the selected territory [4].</li>
<li>Edit the territory <a href="territories_assignments.html">assignments</a>, <a href="territories_streets.html">streets</a> and <a href="territories_addresses.html">addresses</a> [6].</li>
</ul>
<h2><a id="territory_map">Territory Map</a></h2>
<h3><a id="zoom_to_territory">Zoom to a Territory</a></h3>
<ol>
<li>Double-click a territory in the territory list box.</li>
<li>The map is refreshed and displays the full extent of the selected territory.</li>
</ol>
<h3><a id="zoom_to_full">Zoom to the Full Extent of the Congregation's Territory</a></h3>
<ol>
<li>Press the <img src="../images/fullscreen_24x24.png" alt="" width="24" height="24" /> button</li>
<li>The map is refreshed and displays the full extent of the congregation's territory.</li>
</ol>
<h3><a id="show_hide_territories">Show/Hide Territories</a></h3>
<ol>
<li><span>Press the <strong>Show/hide territories</strong> button. The display changes between the following modes:<br /></span>
<div><img src="../images/borders-all_24x24.png" alt="" width="24" height="24" /> <span>Show all territories</span><br /><img src="../images/borders-off_24x24.png" alt="" width="24" height="24" /> <span>Hide all territories</span><br /><img src="../images/borders-selected_24x24.png" alt="" width="24" height="24" /> <span>Show selected territory only</span></div>
</li>
<li>The map is refreshed and displays the territories according to the current setting.</li>
</ol>
<h3><a id="show_hide_markers">Show/Hide Markers</a></h3>
<ol>
<li><span>Press the <strong>Show/hide markers</strong> button. The display changes between the following modes:<br /></span>
<div><img src="../images/map-marker-multiple_24x24.png" alt="" width="24" height="24" /> <span>Show all markers</span><br /><img src="../images/map-marker-off_24x24.png" alt="" width="24" height="24" /> <span>Hide all markers</span><br /><img src="../images/contact_address_24x24.png" alt="" width="24" height="24" /> <span>Show markers of the selected territory only</span></div>
</li>
<li>The map is refreshed and displays the markers according to the current setting.</li>
</ol>
<h3><a id="map_edit_mode">Switch Map Edit Mode</a></h3>
<p>It is possible to switch between viewing and editing the map contents.</p>
<ol>
<li><span>Press the <strong>Switch edit mode</strong> button. The following modes are available:<br /></span>
<div><img src="../images/territory_view_24x24.png" alt="" width="24" height="24" /> <span>View</span><br /><img src="../images/map-marker-edit_24x24.png" alt="" width="24" height="24" /> <span>Edit address markers</span><br /><img src="../images/territory_edit_24x24.png" alt="" width="24" height="24" /> <span>Edit territory boundaries</span></div>
</li>
<li>Depending on the selected mode different functions are available.</li>
</ol>
<h3><a id="find_territory_by_address">Find Territory by Address</a></h3>
<ol>
<li>Click on the <strong>search</strong> field.</li>
<li>Type in the address.</li>
<li>Hit the enter key. Markers show the search result in the map.</li>
<li>Click on the map and select the territory.</li>
</ol>
<h2><a id="edit_territories">Edit Territories</a></h2>
<h3><a id="add_territory">Add a Territory</a></h3>
<ol>
<li>Use the <img src="../images/territory_add_24x24.png" alt="" width="24" height="24" /> button to add a territory.</li>
<li>Edit the territory attributes.</li>
</ol>
<h3><a id="edit_territory_attributes">Edit Territory Attributes</a></h3>
<ol>
<li>Select the territory in the territory list box or in the map.</li>
<li>Enter the territory number in the <strong>No.</strong> field.</li>
<li>In <strong>Locality</strong> type in the locality of the territory.</li>
<li>Click in the <strong>City</strong> box and choose a city.</li>
<li>Click in the <strong>Type</strong> box and choose a type.</li>
<li>Place the cursor in the <strong>Remark</strong> field and start typing, if you want to add a remark.</li>
</ol>
<h3><a id="remove_territory">Remove a Territory</a></h3>
<ol>
<li>Select a territory in the territory list box.</li>
<li>Click the&nbsp;<img src="../images/territory_remove_24x24.png" alt="" width="24" height="24" /> button to remove the selected territory.</li>
</ol>
<h3><a id="import_territory_boundaries">Import Territory Boundaries</a></h3>
<ol>
<li>Click the <img src="../images/import_24x24.png" alt="" width="24" height="24" /> button.</li>
<li>In the <strong>Import territory data</strong> dialog select the option <strong>Territory boundaries</strong>.</li>
<li>Click on the <img src="../images/browse_folder_24x24.png" alt="" width="24" height="24" /> button.</li>
<li>Select a KML-file in the <strong>Open file</strong> dialog.</li>
<li><span>In the <strong>Match KML Fields</strong> box choose the fields that correspond to the <strong>Name</strong> and <strong>Description</strong> of a territory in the KML-file.</span>
<div class="note_text">Note:</div>
<div class="note">Territories are searched by the assigned <strong>Name</strong> field. If no territory is found, a new territory will be created. If the <strong>Description</strong> field is assigned, the corresponding value will be saved.</div>
<div class="caution_text">Caution:</div>
<div class="caution"> Assign the <strong>Name</strong> field only if it is distinctive.</div>
</li>
<li><span>Use the option <strong>Search by "Description" if territory is not found by "Name"</strong> to include the <strong>Description</strong> field also in the search.</span>
<div class="caution_text">Caution:</div>
<div class="caution"> Use this option only if <strong>Description</strong> is distinctive.</div>
</li>
<li>Click on the <strong>Import</strong> button.</li>
</ol>
<div class="tip_text">Tip:</div>
<div class="tip">You can use <strong>Google Maps</strong> to create a KML-file. Sign in and create a map. When you finish drawing the shape of a territory, enter the territory number in the <strong>Name</strong> field and the locality in the <strong>Description</strong> field. Finally you can save your map by clicking on the <strong>Export to KML</strong> menu item.</div>
<h3><a id="draw_territory_boundary">Draw a Territory's Boundary</a></h3>
<ol>
<li>Switch edit mode to edit territory boundaries <img src="../images/territory_edit_24x24.png" alt="" width="24" height="24" />.</li>
<li>Select a territory in the territory list box or in the map.</li>
<li>Click the <img src="../images/boundary_add_24x24.png" alt="" width="24" height="24" /> button.</li>
<li><span>Click along the corners of the territory area in the map. The new territory area is displayed in red.</span>
<div class="tip_text">Tip:</div>
<div class="tip">Zoom in and snap to the corners of a neighboring territory to avoid holes between them. As you move the mouse close to a point of the boundary, the cursor signals when snapping is possible.</div>
<div class="note_text">Note:</div>
<div class="note">You can cancel drawing by pressing the <strong>Escape</strong> key and undo the last point by pressing the <strong>Backspace</strong> key.</div>
</li>
<li>Click with the right mouse button on the map to finish drawing.</li>
</ol>
<h3><a id="remove_territory_boundary">Remove a Territory's Boundary</a></h3>
<ol>
<li>Switch edit mode to edit territory boundaries <img src="../images/territory_edit_24x24.png" alt="" width="24" height="24" />.</li>
<li>Select a territory in the map.</li>
<li>Click the <img src="../images/boundary_remove_24x24.png" alt="" width="24" height="24" /> button.</li>
</ol>
<h3><a id="split_territory">Split a Territory</a></h3>
<p><img src="../images/SplitTerritory.png" alt="Split territory" width="245" height="228" style="margin: 10px; float: right;" /></p>
<p>It is possible to split a territory and divide it into two parts. Each part will inherit the territory attributes (besides the territory number) and the record of assignments, while the addresses and streets will be divided according to the new boundaries.</p>
<ol>
<li>Switch edit mode to edit territory boundaries <img src="../images/territory_edit_24x24.png" alt="" width="24" height="24" />.</li>
<li>Select the territory in the territory list box or in the map.</li>
<li>Click the <img src="../images/boundary_split_24x24.png" alt="" width="24" height="24" /> button.</li>
<li><span>Click along the path [3] where you want to divide the territory.</span>
<div class="tip_text">Tip:</div>
<div class="tip">Start [1] and finish outside the territory and (if possible) snap to an existing point [2], before you continue drawing over the territory. Do it likewise before you leave the territory [4].</div>
<div class="note_text">Note:</div>
<div class="note">You can cancel drawing by pressing the <strong>Escape</strong> key and undo the last point by pressing the <strong>Backspace</strong> key.</div>
</li>
<li>Click around the area [5] you want to cut off. This part will be used to create a new territory.</li>
<li>Click with the right mouse button on the map to finish drawing and to split the selected territory.</li>
</ol>
</body>
</html>